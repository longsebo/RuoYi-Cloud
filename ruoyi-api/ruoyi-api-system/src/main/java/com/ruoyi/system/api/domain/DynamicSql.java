package com.ruoyi.system.api.domain;

import lombok.Data;

/**
 * 动态sql
 */
@Data
public class DynamicSql {
    private String sql;
}
