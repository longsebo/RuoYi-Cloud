package com.ruoyi.system.api.domain;

import lombok.Data;

/**
 * 注册录入用户信息
 */
@Data
public class UserInfo {
    //用户名
    private String username;
    //密码
    private String password;
    //手机号
    private String phone;
}
