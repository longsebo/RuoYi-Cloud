package com.ruoyi.system.api.domain;

import lombok.Data;

/**
 * 修改密码
 */
@Data
public class ChangePassword {
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 邮箱
     */
    private String mail;
    /**
     * 密码
     */
    private String password;
    /**
     * 验证码
     */
    private String verifyCode;
}
