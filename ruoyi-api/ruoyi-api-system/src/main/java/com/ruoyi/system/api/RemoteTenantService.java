package com.ruoyi.system.api;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.domain.RegisterInfo;
import com.ruoyi.system.api.factory.RemoteUserFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * 租户服务
 *
 * @author ruoyi
 */
@FeignClient(contextId = "remoteTenantService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteUserFallbackFactory.class)
public interface RemoteTenantService
{
    /**
     * 注册租户信息
     *
     * @param registerInfo 租户信息
     * @param source 请求来源
     * @return 结果
     */
    @PostMapping("/tenant")
    public R<String> registerTenantInfo(@RequestBody RegisterInfo registerInfo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
