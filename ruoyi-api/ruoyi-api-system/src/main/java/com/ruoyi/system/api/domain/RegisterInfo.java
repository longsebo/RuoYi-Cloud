package com.ruoyi.system.api.domain;

import lombok.Data;

/**
 *  注册信息
 */
@Data
public class RegisterInfo {
    /**
     * 租户信息
     */
    private TenantInfo tenantInfo;
    /**
     * 用户信息
     */
    private UserInfo userInfo;
}
