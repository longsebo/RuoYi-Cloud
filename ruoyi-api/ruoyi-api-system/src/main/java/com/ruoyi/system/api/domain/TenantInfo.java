package com.ruoyi.system.api.domain;

import lombok.Data;

/**
 * 注册时录入租户信息
 */
@Data
public class TenantInfo {
    /**
     * 租户名称
     */
    private String name;
    /**
     * 租户行业
     */
    private String industry;
    /**
     * 租户规模
     */
    private String employeeCount;
}
