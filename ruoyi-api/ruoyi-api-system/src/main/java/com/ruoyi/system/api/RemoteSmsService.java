package com.ruoyi.system.api;

import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.factory.RemoteUserFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 短信服务
 *
 * @author ruoyi
 */
@FeignClient(contextId = "remoteSmsService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteUserFallbackFactory.class)
public interface RemoteSmsService
{
    /**
     * 发送短信验证码
     *
     * @param  mobile 手机号
     * @return 结果
     */
    @PostMapping("/sms/sendSmsVerifyCode/{mobile}")
    public R<Boolean> sendSmsVerifyCode(@PathVariable("mobile") String mobile);
    /**
     * 验证码短信验证码
     *
     * @param  mobile 手机号
     * @param  code   验证码
     * @return 结果
     */
    @PostMapping("/sms/verifyCode/{mobile}/{code}")
    public R<Boolean> verifyCode(@PathVariable("mobile") String mobile,@PathVariable("code") String code);

}
