package com.ruoyi.system.api.domain;

import lombok.Data;

/**
 * 验证码邮件
 */
@Data
public class SysVerifyCodeEmail {
    /**
     * 邮箱
     */
    private String email;
    /**
     * 验证码
     */
    private String verifyCode;
}
