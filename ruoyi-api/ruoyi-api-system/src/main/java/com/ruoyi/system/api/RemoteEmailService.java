package com.ruoyi.system.api;

import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.factory.RemoteUserFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 邮件服务
 *
 * @author ruoyi
 */
@FeignClient(contextId = "remoteEmailService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteUserFallbackFactory.class)
public interface RemoteEmailService
{
    /**
     * 发送验证码
     *
     * @param mail 邮箱
     * @return 结果
     */
    @PostMapping("/mail/sendSmsVerifyCode/{mail}")
    public R<Boolean> sendVerifyCode(@PathVariable("mail") String mail);

    /**
     * 验证码邮件验证码
     *
     * @param mail 邮箱
     * @param code 验证码
     * @return 结果
     */
    @PostMapping("/mail/verifyCode/{mail}/{code}")
    public R<Boolean> verifyCode(@PathVariable("mail") String mail,@PathVariable("code") String code);
}
