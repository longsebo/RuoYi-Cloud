package com.ruoyi.system.api.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 租户对象 sys_tenant
 *
 * @author ruoyi
 * @date 2024-09-27
 */
public class SysTenant extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    public final static int TENANT_CODE_LEN = 8;
    /** $column.columnComment */
    private Long id;

    /** 企业名称 */
    @Excel(name = "企业名称")
    private String enterpriseName;

    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 行业 */
    @Excel(name = "行业")
    private String industry;

    /** 企业人数 */
    @Excel(name = "企业人数")
    private String enterpriseEmployeesNum;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setEnterpriseName(String enterpriseName)
    {
        this.enterpriseName = enterpriseName;
    }

    public String getEnterpriseName()
    {
        return enterpriseName;
    }
    public void setEnterpriseCode(String enterpriseCode)
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode()
    {
        return enterpriseCode;
    }
    public void setIndustry(String industry)
    {
        this.industry = industry;
    }

    public String getIndustry()
    {
        return industry;
    }
    public void setEnterpriseEmployeesNum(String enterpriseEmployeesNum)
    {
        this.enterpriseEmployeesNum = enterpriseEmployeesNum;
    }

    public String getEnterpriseEmployeesNum()
    {
        return enterpriseEmployeesNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("enterpriseName", getEnterpriseName())
            .append("enterpriseCode", getEnterpriseCode())
            .append("industry", getIndustry())
            .append("enterpriseEmployeesNum", getEnterpriseEmployeesNum())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
