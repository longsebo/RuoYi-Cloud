package com.ruoyi.system.api;

import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.domain.DynamicSql;
import com.ruoyi.system.api.factory.RemoteUserFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 动态sql服务
 *
 * @author ruoyi
 */
@FeignClient(contextId = "remoteDynamicSqlService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteUserFallbackFactory.class)
public interface RemoteDynamicSqlService
{
    /**
     * 发送短信验证码
     *
     * @param  dynamicSql 动态sql
     * @return 结果
     */
    @PostMapping("/dynamicsql/doupdate")
    public R<Boolean> doUpdate(@RequestBody DynamicSql dynamicSql);
}
