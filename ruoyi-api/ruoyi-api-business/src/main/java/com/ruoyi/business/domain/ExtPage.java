package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 页面定义对象 ext_page
 *
 * @author ruoyi
 * @date 2024-01-28
 */
public class ExtPage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 模块 */
    @Excel(name = "模块")
    private String module;

    /** 页面编码 */
    @Excel(name = "页面编码")
    private String pageCode;

    /** 页面名称 */
    @Excel(name = "页面名称")
    private String pageName;

    /** 页面内容 */
    @Excel(name = "页面内容")
    private String pageScheme;

    /** 页面类型编码 */
    @Excel(name = "页面类型编码")
    private String pageTypeCode;

    /** 页面参数 */
    @Excel(name = "页面参数")
    private String pageParameter;
    /**
     * 业务编码
     */
    private String businessCode;
    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    public String getEnterpriseCode() {
        return enterpriseCode;
    }

    public void setEnterpriseCode(String enterpriseCode) {
        this.enterpriseCode = enterpriseCode;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;
    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode =businessCode;
    }
    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setModule(String module)
    {
        this.module = module;
    }

    public String getModule()
    {
        return module;
    }
    public void setPageCode(String pageCode)
    {
        this.pageCode = pageCode;
    }

    public String getPageCode()
    {
        return pageCode;
    }
    public void setPageName(String pageName)
    {
        this.pageName = pageName;
    }

    public String getPageName()
    {
        return pageName;
    }
    public void setPageScheme(String pageScheme)
    {
        this.pageScheme = pageScheme;
    }

    public String getPageScheme()
    {
        return pageScheme;
    }
    public void setPageTypeCode(String pageTypeCode)
    {
        this.pageTypeCode = pageTypeCode;
    }

    public String getPageTypeCode()
    {
        return pageTypeCode;
    }
    public void setPageParameter(String pageParameter)
    {
        this.pageParameter = pageParameter;
    }

    public String getPageParameter()
    {
        return pageParameter;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("module", getModule())
            .append("pageCode", getPageCode())
            .append("pageName", getPageName())
            .append("pageScheme", getPageScheme())
            .append("pageTypeCode", getPageTypeCode())
            .append("pageParameter", getPageParameter())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
