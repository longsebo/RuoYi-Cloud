package com.ruoyi.business.api.factory;

import com.ruoyi.business.api.RemoteExtPageService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 日志服务降级处理
 *
 * @author ruoyi
 */
@Component
public class RemoteExtPageFactory implements FallbackFactory<RemoteExtPageService>
{
    private static final Logger log = LoggerFactory.getLogger(RemoteExtPageFactory.class);

    @Override
    public RemoteExtPageService create(Throwable throwable)
    {
        log.error("页面服务调用失败:{}", throwable.getMessage());
        return new RemoteExtPageService()
        {
            @Override
            public AjaxResult getPageByCode(@PathVariable("pageCode") String pageCode,@PathVariable("enterpriseCode") String enterpriseCode,@PathVariable("applicationCode") String applicationCode)
            {
                return AjaxResult.error("获取页面失败:" + throwable.getMessage());
            }

            @Override
            public AjaxResult getPageById(@PathVariable("pageId") long pageId, @PathVariable("enterpriseCode") String enterpriseCode,@PathVariable("applicationCode") String applicationCode) {
                return AjaxResult.error("获取页面失败:" + throwable.getMessage());
            }


        };

    }
}
