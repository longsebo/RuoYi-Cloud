package com.ruoyi.business.domain;

import lombok.Data;

@Data
public class ExtButton {
    private String id;
    private String name;
}
