package com.ruoyi.business.api;

import com.ruoyi.business.api.factory.RemoteExtPageFactory;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.web.domain.AjaxResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 页面服务
 *
 * @author ruoyi
 */
@FeignClient(contextId = "remoteExtPageService", value = ServiceNameConstants.BUSINESS_SERVICE, fallbackFactory = RemoteExtPageFactory.class)
public interface RemoteExtPageService {
    @GetMapping(value = "page/getPageByCode/{pageCode}/{enterpriseCode}/{applicationCode}")
    public AjaxResult getPageByCode(@PathVariable("pageCode") String pageCode,@PathVariable("enterpriseCode") String enterpriseCode,@PathVariable("applicationCode") String applicationCode);
    @GetMapping(value = "page/getPageById/{pageId}/{enterpriseCode}/{applicationCode}")
    public AjaxResult getPageById(@PathVariable("pageId") long pageId, @PathVariable("enterpriseCode") String enterpriseCode, @PathVariable("applicationCode") String applicationCode);
}
