package com.ruoyi.auth.form;

import lombok.Data;

/**
 * 用户登录对象
 *
 * @author ruoyi
 */
@Data
public class LoginBody
{
    /**
     * 密码验证模式
     */
    public final static String MODE_PASSWORD = "passwordMode";
    /**
     * 验证码模式
     */
    public final static String MODE_VERIFYCODE = "verifyCodeMode";
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 邮箱
     */
    private String mail;
    /**
     * 验证模式
     */
    private String verifyMode;
    /**
     * 验证码或密码
     */
    private String codeOrPassword;

    /**
     * 用户名
     */
    private String username;

    /**
     * 用户密码
     */
    private String password;


}
