package com.ruoyi.groovy;


import com.ruoyi.common.core.utils.StringUtils;

class StringHelper {
    /**
     * * 判断一个Collection是否为空， 包含List，Set，Queue
     *
     * @param coll 要判断的Collection
     * @return true：为空 false：非空
     */
     static boolean isEmpty(Collection<?> coll)
    {
        return StringUtils.isEmpty(coll);
    }

    /**
     * * 判断一个Collection是否非空，包含List，Set，Queue
     *
     * @param coll 要判断的Collection
     * @return true：非空 false：空
     */
     static boolean isNotEmpty(Collection<?> coll)
    {
        return StringUtils.isNotEmpty(coll);
    }

    /**
     * * 判断一个对象数组是否为空
     *
     * @param objects 要判断的对象数组
     ** @return true：为空 false：非空
     */
     static boolean isEmpty(Object[] objects)
    {
        return StringUtils.isEmpty(objects);
    }

    /**
     * * 判断一个对象数组是否非空
     *
     * @param objects 要判断的对象数组
     * @return true：非空 false：空
     */
     static boolean isNotEmpty(Object[] objects)
    {
        return StringUtils.isNotEmpty(objects);
    }

    /**
     * * 判断一个Map是否为空
     *
     * @param map 要判断的Map
     * @return true：为空 false：非空
     */
     static boolean isEmpty(Map<?, ?> map)
    {
        return StringUtils.isEmpty(map);
    }

    /**
     * * 判断一个Map是否为空
     *
     * @param map 要判断的Map
     * @return true：非空 false：空
     */
     static boolean isNotEmpty(Map<?, ?> map)
    {
        return StringUtils.isNotEmpty(map);
    }

    /**
     * * 判断一个字符串是否为空串
     *
     * @param str String
     * @return true：为空 false：非空
     */
     static boolean isEmpty(String str)
    {
        return StringUtils.isEmpty(str);
    }

    /**
     * * 判断一个字符串是否为非空串
     *
     * @param str String
     * @return true：非空串 false：空串
     */
     static boolean isNotEmpty(String str)
    {
        return StringUtils.isNotEmpty(str);
    }

    /**
     * * 判断一个对象是否为空
     *
     * @param object Object
     * @return true：为空 false：非空
     */
     static boolean isNull(Object object)
    {
        return StringUtils.isNull(object);
    }

    /**
     * * 判断一个对象是否非空
     *
     * @param object Object
     * @return true：非空 false：空
     */
     static boolean isNotNull(Object object)
    {
        return StringUtils.isNotNull(object);
    }

    /**
     * * 判断一个对象是否是数组类型（Java基本型别的数组）
     *
     * @param object 对象
     * @return true：是数组 false：不是数组
     */
     static boolean isArray(Object object)
    {
        return StringUtils.isArray(object);
    }

    /**
     * 去空格
     */
     static String trim(String str)
    {
        return StringUtils.trim(str);
    }

    /**
     * 截取字符串
     *
     * @param str 字符串
     * @param start 开始
     * @return 结果
     */
     static String substring(final String str, int start)
    {
        return StringUtils.substring(str, start);
    }

    /**
     * 截取字符串
     *
     * @param str 字符串
     * @param start 开始
     * @param end 结束
     * @return 结果
     */
     static String substring(final String str, int start, int end)
    {
        return StringUtils.substring(str, start, end);
    }

    /**
     * 判断是否为空，并且不是空白字符
     *
     * @param str 要判断的value
     * @return 结果
     */
     static boolean hasText(String str)
    {
        return StringUtils.hasText(str);
    }

    private static boolean containsText(CharSequence str)
    {
        return StringUtils.containsText(str);
    }

    /**
     * 格式化文本, {} 表示占位符<br>
     * 此方法只是简单将占位符 {} 按照顺序替换为参数<br>
     * 如果想输出 {} 使用 \\转义 { 即可，如果想输出 {} 之前的 \ 使用双转义符 \\\\ 即可<br>
     * 例：<br>
     * 通常使用：format("this is {} for {}", "a", "b") -> this is a for b<br>
     * 转义{}： format("this is \\{} for {}", "a", "b") -> this is \{} for a<br>
     * 转义\： format("this is \\\\{} for {}", "a", "b") -> this is \a for b<br>
     *
     * @param template 文本模板，被替换的部分用 {} 表示
     * @param params 参数值
     * @return 格式化后的文本
     */
     static String format(String template, Object... params)
    {
        return StringUtils.format(template, params);
    }

    /**
     * 是否为http(s)://开头
     *
     * @param link 链接
     * @return 结果
     */
     static boolean ishttp(String link)
    {
        return StringUtils.ishttp(link);
    }

    /**
     * 判断给定的collection列表中是否包含数组array 判断给定的数组array中是否包含给定的元素value
     *
     * @param collection 给定的集合
     * @param array 给定的数组
     * @return boolean 结果
     */
     static boolean containsAny(Collection<String> collection, String... array)
    {
        return StringUtils.containsAny(collection, array);
    }

    /**
     * 驼峰转下划线命名
     */
     static String toUnderScoreCase(String str)
    {
        return StringUtils.toUnderScoreCase(str);
    }

    /**
     * 是否包含字符串
     *
     * @param str 验证字符串
     * @param strs 字符串组
     * @return 包含返回true
     */
     static boolean inStringIgnoreCase(String str, String... strs)
    {
        return StringUtils.inStringIgnoreCase(str, strs);
    }

    /**
     * 将下划线大写方式命名的字符串转换为驼峰式。如果转换前的下划线大写方式命名的字符串为空，则返回空字符串。 例如：HELLO_WORLD->HelloWorld
     *
     * @param name 转换前的下划线大写方式命名的字符串
     * @return 转换后的驼峰式命名的字符串
     */
     static String convertToCamelCase(String name)
    {
        return StringUtils.convertToCamelCase(name);
    }

    /**
     * 驼峰式命名法
     * 例如：user_name->userName
     */
     static String toCamelCase(String s)
    {
        return StringUtils.toCamelCase(s);
    }

    /**
     * 查找指定字符串是否匹配指定字符串列表中的任意一个字符串
     *
     * @param str 指定字符串
     * @param strs 需要检查的字符串数组
     * @return 是否匹配
     */
     static boolean matches(String str, List<String> strs)
    {
        return StringUtils.matches(str, strs);
    }

    /**
     * 判断url是否与规则配置:
     * ? 表示单个字符;
     * * 表示一层路径内的任意字符串，不可跨层级;
     * ** 表示任意层路径;
     *
     * @param pattern 匹配规则
     * @param url 需要匹配的url
     * @return
     */
     static boolean isMatch(String pattern, String url)
    {
        return StringUtils.isMatch(pattern, url);
    }

    @SuppressWarnings("unchecked")
     static <T> T cast(Object obj)
    {
        return (T) StringUtils.cast(obj);
    }

    /**
     * 数字左边补齐0，使之达到指定长度。注意，如果数字转换为字符串后，长度大于size，则只保留 最后size个字符。
     *
     * @param num 数字对象
     * @param size 字符串指定长度
     * @return 返回数字的字符串格式，该字符串为指定长度。
     */
     static final String padl(final Number num, final int size)
    {
        return StringUtils.padl(num.toString(), size, '0');
    }

    /**
     * 字符串左补齐。如果原始字符串s长度大于size，则只保留最后size个字符。
     *
     * @param s 原始字符串
     * @param size 字符串指定长度
     * @param c 用于补齐的字符
     * @return 返回指定长度的字符串，由原字符串左补齐或截取得到。
     */
     static final String padl(final String s, final int size, final char c)
    {
        return StringUtils.padl(s, size, c);
    }
}
