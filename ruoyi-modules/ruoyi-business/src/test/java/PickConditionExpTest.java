import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PickConditionExpTest {
    @Test
    public void test1(){
        String regEx ="(>)(.*?)(</#if>)";
        Pattern r = Pattern.compile(regEx,Pattern.CASE_INSENSITIVE|Pattern.DOTALL);
        // Now create matcher object.
        Matcher m = r.matcher("<#if (productCode?? &&  productCode!='')>\n" +
                " business_product.product_code = ${productCode}\n" +
                " \t\t </#if>");
        StringBuffer sb = new StringBuffer();
        int i=0;
        while (m.find()){
            i++;
            //m.appendReplacement(sb,"?");
            if(m.groupCount()>0) {
                System.out.println(m.group(2));
            }
        }
    }
}
