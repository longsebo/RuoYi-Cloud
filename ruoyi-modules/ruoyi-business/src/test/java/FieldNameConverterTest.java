import com.ruoyi.business.utils.FieldNameConverter;
import org.junit.Test;
import static org.junit.Assert.*;

public class FieldNameConverterTest {

    @Test
    public void testCamelCaseConversion() {
        String result = FieldNameConverter.underline2Camel("hello_world",true);
        assertEquals("helloWorld", result);
    }

    @Test
    public void testUpperCaseConversion() {
        String result = FieldNameConverter.underline2Camel("example_domain",true);
        assertEquals("exampleDomain", result);
    }

    @Test
    public void testMixedCaseConversion() {
        String result = FieldNameConverter.underline2Camel("Apache_HTTP",true);
        assertEquals("apacheHttp", result);
    }

    @Test
    public void testSingleWordConversion() {
        String result = FieldNameConverter.underline2Camel("java",true);
        assertEquals("java", result);
    }

    @Test
    public void testUnderscoreConversion() {
        String result = FieldNameConverter.underline2Camel("some_field_name",true);
        assertEquals("someFieldName", result);
    }
}
