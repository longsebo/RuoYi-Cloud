import com.ruoyi.groovy.StringHelper;
import groovy.lang.Binding;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.stmt.BlockStatement;
import org.codehaus.groovy.ast.stmt.ExpressionStatement;
import org.codehaus.groovy.control.CompilePhase;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.codehaus.groovy.control.Phases;
import org.codehaus.groovy.control.SourceUnit;
import org.codehaus.groovy.control.customizers.ASTTransformationCustomizer;
import org.codehaus.groovy.transform.ASTTransformation;
import org.codehaus.groovy.transform.GroovyASTTransformation;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestGroovy {
    @Test
    public void test1() {
        GroovyShell shell = new GroovyShell();
        Script script =shell.parse("package groovy\n" +
                "\n" +
                "def HelloWorld(){\n" +
                "    println \"hello world\"\n" +
                "}");
        script.invokeMethod("HelloWorld",null);
    }
    @Test
    public void test2() {
        GroovyShell shell = new GroovyShell();
        shell.evaluate("package groovy\n" +
                "HelloWorld()\n" +
                "def HelloWorld(){\n" +
                "    println \"hello world\"\n" +
                "}");
    }
    @Test
    public void test3() {
        //创建GroovyShell
        GroovyShell groovyShell = new GroovyShell();
        //装载解析脚本代码
        Script script = groovyShell.parse("package groovy\n" +
                "\n" +
                "/**\n" +
                " * 简易加法\n" +
                " * @param a 数字a\n" +
                " * @param b 数字b\n" +
                " * @return 和\n" +
                " */\n" +
                "def add(int a, int b) {\n" +
                "    return a + b\n" +
                "}\n" +
                "\n" +
                "/**\n" +
                " * map转化为String\n" +
                " * @param paramMap 参数map\n" +
                " * @return 字符串\n" +
                " */\n" +
                "def mapToString(Map<String, String> paramMap) {\n" +
                "    StringBuilder stringBuilder = new StringBuilder();\n" +
                "    paramMap.forEach({ key, value ->\n" +
                "        stringBuilder.append(\"key:\" + key + \";value:\" + value)\n" +
                "    })\n" +
                "    return stringBuilder.toString()\n" +
                "}");
        //执行加法脚本
        Object[] params1 = new Object[]{1, 2};
        int sum = (int) script.invokeMethod("add", params1);
        System.out.println("a加b的和为:" + sum);
        //执行解析脚本
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("科目1", "语文");
        paramMap.put("科目2", "数学");
        Object[] params2 = new Object[]{paramMap};
        String result = (String) script.invokeMethod("mapToString", params2);
        System.out.println("mapToString:" + result);
    }
    @Test
    public void test4() {
        boolean retval = StringHelper.isEmpty("abc");
        System.out.println("retval:"+retval);
    }
    @Test
    public void test5() {
//        boolean retval = StringHelper.isEmpty("abc");
        //创建GroovyShell
        GroovyShell groovyShell = new GroovyShell();
        groovyShell.evaluate(
                "import com.ruoyi.groovy.StringHelper;" +
                "boolean retval = StringHelper.isEmpty(\"abc\");" +
                "println retval;" +
                "");
    }

    @Test
    public void testWithBinding() {
        // 创建 Binding 对象
        Binding binding = new Binding();
        binding.setVariable("inputVariable", "Hello from Java");

        // 创建 GroovyShell 并传入 Binding 对象
        GroovyShell shell = new GroovyShell(binding);

        // 定义 Groovy 脚本内容
        String scriptText = """
        println inputVariable
        """;

        // 执行 Groovy 脚本
        shell.evaluate(scriptText);
    }
    @Test
    public void testWithCustomMethod() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        // 创建 GroovyShell
        GroovyShell groovyShell = new GroovyShell();

        // 定义 Groovy 脚本内容
        String script1= "import java.lang.String;\n" +
                "class MyScript1 {\n" +
                "    def HelloWorld() {\n" +
                "        println \"Hello, World from MyScript1!\"\n" +
                "    }\n" +
                "}\n";
        String script2= "import java.lang.String;\n" +
                "class MyScript2 {\n" +
                "    def HelloWorld() {\n" +
                "        println \"Hello, World from MyScript2!\"\n" +
                "    }\n" +
                "}\n";
        // 合并两个脚本内容
        String combinedScriptContent = script1 +  script2;
        // 创建 GroovyShell
        System.out.println("Combined Script Content:\n" + combinedScriptContent);

        // 解析合并后的脚本内容
        Script script = groovyShell.parse(combinedScriptContent);

        // 使用 GroovyClassLoader 动态加载类
        ClassLoader classLoader = groovyShell.getClassLoader();

        // 加载 MyScript1 类
        Class<?> myScript1Class = classLoader.loadClass("MyScript1");
        Object myScript1Instance = myScript1Class.getDeclaredConstructor().newInstance();
        myScript1Class.getMethod("HelloWorld").invoke(myScript1Instance);

        // 加载 MyScript2 类
        Class<?> myScript2Class = classLoader.loadClass("MyScript2");
        Object myScript2Instance = myScript2Class.getDeclaredConstructor().newInstance();
        myScript2Class.getMethod("HelloWorld").invoke(myScript2Instance);

    }
    @Test
    public void testParseCallMethod() {
        // 定义 Groovy 脚本内容
        String scriptContent = """
        import java.lang.String;

        public class MyScript1 {
            public void a() {
                MyScript2 myScript2 = new MyScript2();
                myScript2.b();
                a(); // 递归调用
            }
        }

        public class MyScript2 {
            public void b() {
                MyScript3 myScript3 = new MyScript3();
                myScript3.c();
            }
        }

        public class MyScript3 {
            public void c() {
                System.out.println("Hello from MyScript3!");
            }
        }
        """;

        // 创建 CompilerConfiguration 并添加 ASTTransformationCustomizer
        CompilerConfiguration config = new CompilerConfiguration();
        ASTTransformationCustomizer customizer = new ASTTransformationCustomizer(new AnalyzeScriptTransformation());

        config.addCompilationCustomizers(customizer);

        // 创建 GroovyShell 并应用配置
        GroovyShell groovyShell = new GroovyShell(config);

        // 解析脚本内容
        groovyShell.parse(scriptContent);
    }

    @GroovyASTTransformation(phase = CompilePhase.CANONICALIZATION)
    private static class AnalyzeScriptTransformation implements ASTTransformation {
        @Override
        public void visit(ASTNode[] nodes, SourceUnit source) {
            Map<String, List<String>> methodCalls = new HashMap<>();

            for (ClassNode classNode : source.getAST().getClasses()) {
                for (MethodNode methodNode : classNode.getMethods()) {
                    List<String> calls = new ArrayList<>();
                    traverseMethodCalls(methodNode.getCode(), calls, methodNode.getName(), classNode.getName());
                    methodCalls.put(classNode.getName() + "." + methodNode.getName(), calls);
                }
            }

            // 打印方法调用关系
            for (Map.Entry<String, List<String>> entry : methodCalls.entrySet()) {
                System.out.println("Method " + entry.getKey() + " calls:");
                for (String call : entry.getValue()) {
                    System.out.println("  " + call);
                }
            }
        }

        private void traverseMethodCalls(ASTNode node, List<String> methodCalls, String currentMethodName, String currentClassName) {
            if (node instanceof BlockStatement) {
                BlockStatement block = (BlockStatement) node;
                for (ASTNode statement : block.getStatements()) {
                    traverseMethodCalls(statement, methodCalls, currentMethodName, currentClassName);
                }
            } else if (node instanceof ExpressionStatement) {
                ExpressionStatement exprStmt = (ExpressionStatement) node;
                traverseMethodCalls(exprStmt.getExpression(), methodCalls, currentMethodName, currentClassName);
            } else if (node instanceof MethodCallExpression) {
                MethodCallExpression methodCall = (MethodCallExpression) node;
                String methodName = methodCall.getMethodAsString();
                String targetClass = getTargetClass(methodCall);

                // 如果是递归调用，记录递归信息
                if (targetClass.equals(currentClassName) && methodName.equals(currentMethodName)) {
                    methodCalls.add("Recursive call to " + targetClass + "." + methodName);
                } else {
                    methodCalls.add(targetClass + "." + methodName);
                }
            }
        }

        private String getTargetClass(MethodCallExpression methodCall) {
            // 获取方法调用的目标对象表达式
            Expression objectExpression = methodCall.getObjectExpression();

            if (objectExpression == null || "this".equals(objectExpression.getText())) {
                return "this"; // 默认为当前类
            }

            // 尝试获取目标类名
            // 尝试获取目标类名
            ClassNode type = methodCall.getType();
            if (type != null) {
                return type.getName();
            }


            // 如果无法确定目标类，则返回未知
            return "unknown";
        }
    }
}
