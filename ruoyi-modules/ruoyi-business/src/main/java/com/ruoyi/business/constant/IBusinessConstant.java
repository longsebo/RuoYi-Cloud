package com.ruoyi.business.constant;

/**
 * 业务模块常量
 */
public interface IBusinessConstant {
    //mq类型
    /**
     * rocketmq
     */
    public final String MQ_TYPE_ROCKETMQ="rocketmq";
    /**
     * kafka
     */
    public final String MQ_TYPE_KAFKA="kafka";

    //mybatis 配置xml 变量
    /**
     * 用户名
     */
    public final String USER_NAME="username";
    /**
     * 密码
     */
    public final String PASSWORD="password";
    /**
     * url
     */
    public final String URL="url";
    /**
     * 驱动类
     */
    public final String DRIVER="driver";
    //表类型
    /**
     * 纯业务表
     */
    public final String TABLE_TYPE_01="01";
    /**
     * 流程业务表
     */
    public final String TABLE_TYPE_02="02";
    //信息范围
    /**
     * 私有字段
     */
    public final String SCOPE_PRIVATE="PRIVATE";
    /**
     * 业务默认字段
     */
    public final String SCOPE_ENTITY_DEFAULT="ENTITY_DEFAULT";
    /**
     * 流程默认字段
     */
    public final String SCOPE_WORKFLOW_DEFAULT="WORKFLOW_DEFAULT";
    /**
     * 全局
     */
    public final String SCOPE_GLOBAL="GLOBAL";
    //激活状态
    /**
     * 启用
     */
    public final String ACTIVE_STATE_01="01";
    /**
     * 禁用
     */
    public final String ACTIVE_STATE_02="02";
    //系统是否标志
    /**
     * 是
     */
    public final String SYSTEM_YES="Y";
    /**
     * 否
     */
    public final String SYSTEM_NO="N";
    //接口类型
    /**
     * 查询接口
     */
    public final String INTERFACE_TYPE_SEARCH="search";
    public final String ENCODE_UTF_8="UTF-8";
    public static final String SELECT_TPL_PATH = "select.tpl";
    //数据库类型
    /**
     * mysql
     */
    public final String DATABASE_TYPE_MYSQL="01";
    //预定字段
    /**
     * 条数字段
     */
    public final String COUNT_FIELD="cnt";
    /**
     * 1=1条件
     */
    public final String ONEEQONE="1=1";
}
