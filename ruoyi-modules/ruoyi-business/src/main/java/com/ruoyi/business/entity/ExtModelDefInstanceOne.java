package com.ruoyi.business.entity;


import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 模型表实例查询参数
 */
@Data
public class ExtModelDefInstanceOne extends BaseAPI {

    /**
     * 表英文名
     */
    @NotBlank
    private String enName;
    /**
     * 数据源名称
     */
    @NotBlank
    private String dataSourceName;

}
