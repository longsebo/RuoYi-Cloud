package com.ruoyi.business.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.ExtBusinessFunctionListening;

/**
 * 功能侦听Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface ExtBusinessFunctionListeningMapper
{
    /**
     * 查询功能侦听
     *
     * @param id 功能侦听主键
     * @return 功能侦听
     */
    public ExtBusinessFunctionListening selectExtBusinessFunctionListeningById(Long id);

    /**
     * 查询功能侦听列表
     *
     * @param extBusinessFunctionListening 功能侦听
     * @return 功能侦听集合
     */
    public List<ExtBusinessFunctionListening> selectExtBusinessFunctionListeningList(ExtBusinessFunctionListening extBusinessFunctionListening);

    /**
     * 新增功能侦听
     *
     * @param extBusinessFunctionListening 功能侦听
     * @return 结果
     */
    public int insertExtBusinessFunctionListening(ExtBusinessFunctionListening extBusinessFunctionListening);

    /**
     * 修改功能侦听
     *
     * @param extBusinessFunctionListening 功能侦听
     * @return 结果
     */
    public int updateExtBusinessFunctionListening(ExtBusinessFunctionListening extBusinessFunctionListening);

    /**
     * 删除功能侦听
     *
     * @param id 功能侦听主键
     * @return 结果
     */
    public int deleteExtBusinessFunctionListeningById(Long id);

    /**
     * 批量删除功能侦听
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtBusinessFunctionListeningByIds(Long[] ids);

    /**
     * 查询满足条件条数
     * @param map 查询条件
     * @return
     */
    int selectCount(Map<String, Object> map);
}
