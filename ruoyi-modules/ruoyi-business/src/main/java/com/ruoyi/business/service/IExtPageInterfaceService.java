package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.domain.ExtPageInterface;

/**
 * 页面接口关系Service接口
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
public interface IExtPageInterfaceService 
{
    /**
     * 查询页面接口关系
     * 
     * @param id 页面接口关系主键
     * @return 页面接口关系
     */
    public ExtPageInterface selectExtPageInterfaceById(Long id);

    /**
     * 查询页面接口关系列表
     * 
     * @param extPageInterface 页面接口关系
     * @return 页面接口关系集合
     */
    public List<ExtPageInterface> selectExtPageInterfaceList(ExtPageInterface extPageInterface);

    /**
     * 新增页面接口关系
     * 
     * @param extPageInterface 页面接口关系
     * @return 结果
     */
    public int insertExtPageInterface(ExtPageInterface extPageInterface);

    /**
     * 修改页面接口关系
     * 
     * @param extPageInterface 页面接口关系
     * @return 结果
     */
    public int updateExtPageInterface(ExtPageInterface extPageInterface);

    /**
     * 批量删除页面接口关系
     * 
     * @param ids 需要删除的页面接口关系主键集合
     * @return 结果
     */
    public int deleteExtPageInterfaceByIds(Long[] ids);

    /**
     * 删除页面接口关系信息
     * 
     * @param id 页面接口关系主键
     * @return 结果
     */
    public int deleteExtPageInterfaceById(Long id);
}
