package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.util.List;

/**
 * 接口参数对象 ext_interface_parameter
 *
 * @author ruoyi
 * @date 2024-01-22
 */
public class ExtInterfaceParameter extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 参数名称 */
    @Excel(name = "参数名称")
    private String parameterName;

    /** 参数描述 */
    @Excel(name = "参数描述")
    private String parameterDesc;

    /** 前端是否可见 */
    @Excel(name = "前端是否可见")
    private String isFrontpageVisible;

    /** 参数类型 */
    @Excel(name = "参数类型")
    private String parameterType;

    /** 参数格式 */
    @Excel(name = "参数格式")
    private String parameterFormat;

    /** 父参数id */
    @Excel(name = "父参数id")
    private Long parentId;
    /**
     * 接口编码
     */
    private String interfaceCode;
    /** 子节点 */
    private List<ExtInterfaceParameter> children;
    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;    
    

    public String getInterfaceCode() {
        return interfaceCode;
    }

    public void setInterfaceCode(String interfaceCode) {
        this.interfaceCode = interfaceCode;
    }

    public List<ExtInterfaceParameter> getChildren() {
        return children;
    }

    public void setChildren(List<ExtInterfaceParameter> children) {
        this.children = children;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setParameterName(String parameterName)
    {
        this.parameterName = parameterName;
    }

    public String getParameterName()
    {
        return parameterName;
    }
    public void setParameterDesc(String parameterDesc)
    {
        this.parameterDesc = parameterDesc;
    }

    public String getParameterDesc()
    {
        return parameterDesc;
    }
    public void setIsFrontpageVisible(String isFrontpageVisible)
    {
        this.isFrontpageVisible = isFrontpageVisible;
    }

    public String getIsFrontpageVisible()
    {
        return isFrontpageVisible;
    }
    public void setParameterType(String parameterType)
    {
        this.parameterType = parameterType;
    }

    public String getParameterType()
    {
        return parameterType;
    }
    public void setParameterFormat(String parameterFormat)
    {
        this.parameterFormat = parameterFormat;
    }

    public String getParameterFormat()
    {
        return parameterFormat;
    }
    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    public Long getParentId()
    {
        return parentId;
    }
    public void setEnterpriseCode(String enterpriseCode) 
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode() 
    {
        return enterpriseCode;
    }
    public void setApplicationCode(String applicationCode) 
    {
        this.applicationCode = applicationCode;
    }

    public String getApplicationCode() 
    {
        return applicationCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("parameterName", getParameterName())
            .append("parameterDesc", getParameterDesc())
            .append("isFrontpageVisible", getIsFrontpageVisible())
            .append("parameterType", getParameterType())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("parameterFormat", getParameterFormat())
            .append("parentId", getParentId())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
