package com.ruoyi.business.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.service.IExtBusinessFunctionListeningService;
import com.ruoyi.common.core.exception.ServiceException;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.common.security.utils.SecurityUtils;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.ExtBusinessMqConfMapper;
import com.ruoyi.business.domain.ExtBusinessMqConf;
import com.ruoyi.business.service.IExtBusinessMqConfService;

/**
 * MQ配置定义Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@Service
public class ExtBusinessMqConfServiceImpl implements IExtBusinessMqConfService
{
    @Autowired
    private ExtBusinessMqConfMapper extBusinessMqConfMapper;
    @Lazy
    @Autowired
    private IExtBusinessFunctionListeningService extBusinessFunctionListeningService;
    /**
     * 查询MQ配置定义
     *
     * @param id MQ配置定义主键
     * @return MQ配置定义
     */
    @Override
    public ExtBusinessMqConf selectExtBusinessMqConfById(Long id)
    {
        return extBusinessMqConfMapper.selectExtBusinessMqConfById(id);
    }

    /**
     * 查询MQ配置定义列表
     *
     * @param extBusinessMqConf MQ配置定义
     * @return MQ配置定义
     */
    @Override
    public List<ExtBusinessMqConf> selectExtBusinessMqConfList(ExtBusinessMqConf extBusinessMqConf)
    {
        return extBusinessMqConfMapper.selectExtBusinessMqConfList(extBusinessMqConf);
    }

    /**
     * 新增MQ配置定义
     *
     * @param extBusinessMqConf MQ配置定义
     * @return 结果
     */
    @Override
    public int insertExtBusinessMqConf(ExtBusinessMqConf extBusinessMqConf)
    {
        //新增检查
        if(extBusinessMqConf==null)
            throw new ServiceException("配置对象为空!");
        //名称不能为空
        if(StringUtils.isEmpty(extBusinessMqConf.getMqName()))
            throw new ServiceException("名称不能为空");
        //名称不能重复
        Map<String,Object> searchMap = new HashMap<>();
        searchMap.put("mqName",extBusinessMqConf.getMqName());
        if(extBusinessMqConfMapper.selectCount(searchMap)>0)
            throw new ServiceException("mq名称:"+extBusinessMqConf.getMqName()+"已经存在!");
        //类型不能为空
        if(StringUtils.isEmpty(extBusinessMqConf.getMqType()))
            throw new ServiceException("类型不能为空");
        //配置不能为空
        if(StringUtils.isEmpty(extBusinessMqConf.getMqConfig()))
            throw new ServiceException("MQ配置不能为空");
        extBusinessMqConf.setCreateTime(DateUtils.getNowDate());
        extBusinessMqConf.setCreateBy(SecurityUtils.getUsername());
        return extBusinessMqConfMapper.insertExtBusinessMqConf(extBusinessMqConf);
    }

    /**
     * 修改MQ配置定义
     *
     * @param extBusinessMqConf MQ配置定义
     * @return 结果
     */
    @Override
    public int updateExtBusinessMqConf(ExtBusinessMqConf extBusinessMqConf)
    {
        //新增检查
        if(extBusinessMqConf==null)
            throw new ServiceException("配置对象为空!");
        //id不能为空
        if(extBusinessMqConf.getId()==null||extBusinessMqConf.getId()==0L)
            throw new ServiceException("id不能为空！");
        //名称不能为空
        if(StringUtils.isEmpty(extBusinessMqConf.getMqName()))
            throw new ServiceException("名称不能为空");
        //名称不能重复
        Map<String,Object> searchMap = new HashMap<>();
        searchMap.put("mqName",extBusinessMqConf.getMqName());
        searchMap.put("notId",extBusinessMqConf.getId());
        if(extBusinessMqConfMapper.selectCount(searchMap)>0)
            throw new ServiceException("mq名称:"+extBusinessMqConf.getMqName()+"已经存在!");
        //类型不能为空
        if(StringUtils.isEmpty(extBusinessMqConf.getMqType()))
            throw new ServiceException("类型不能为空");
        //配置不能为空
        if(StringUtils.isEmpty(extBusinessMqConf.getMqConfig()))
            throw new ServiceException("MQ配置不能为空");
        extBusinessMqConf.setUpdateTime(DateUtils.getNowDate());
        extBusinessMqConf.setUpdateBy(SecurityUtils.getUsername());
        return extBusinessMqConfMapper.updateExtBusinessMqConf(extBusinessMqConf);
    }

    /**
     * 批量删除MQ配置定义
     *
     * @param ids 需要删除的MQ配置定义主键
     * @return 结果
     */
    @Override
    public int deleteExtBusinessMqConfByIds(Long[] ids)
    {
        for(Long id:ids) {
            //检查是否引用
            if (checkIsUse(id))
                throw new ServiceException("功能侦听在引用，请撤销引用再删除!");
        }
        return extBusinessMqConfMapper.deleteExtBusinessMqConfByIds(ids);
    }

    /**
     * 删除MQ配置定义信息
     *
     * @param id MQ配置定义主键
     * @return 结果
     */
    @Override
    public int deleteExtBusinessMqConfById(Long id)
    {
        //检查是否引用
        if(checkIsUse(id))
            throw new ServiceException("功能侦听在引用，请撤销引用再删除!");
        return extBusinessMqConfMapper.deleteExtBusinessMqConfById(id);
    }

    /**
     * 查询满足条件数量
     *
     * @param searchMap 查询条件
     * @return 返回满足条件数量
     */
    @Override
    public int selectCount(Map<String, Object> searchMap) {
        return extBusinessMqConfMapper.selectCount(searchMap);
    }

    /**
     * 判断mq是否在引用
     * @param id
     * @return
     */
    private boolean checkIsUse(Long id) {
        //先查mq名称
        ExtBusinessMqConf mqConfig = selectExtBusinessMqConfById(id);
        if(mqConfig==null)
            throw new ServiceException("Mq 配置id:"+id+"不存在！");
        Map<String,Object> map = new HashMap<>();
        map.put("mqName",mqConfig.getMqName());
        return (extBusinessFunctionListeningService.selectCount(map)>0);
    }
}
