package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.util.List;

/**
 * 页面参数对象 ext_page_parameter
 *
 * @author ruoyi
 * @date 2024-01-28
 */
public class ExtPageParameter extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 参数名称 */
    @Excel(name = "参数名称")
    private String parameterName;

    /** 参数描述 */
    @Excel(name = "参数描述")
    private String parameterDesc;

    /** parameter_type=参数类型{
                object=对象,
                array=数组,
                string=字符串,
                date=日期,
                datetime=日期时间,
                time=时间,
                integer=整数,
               decimal=小数
             } */
    @Excel(name = """
                parameter_type=参数类型{
                object=对象,
                array=数组,
                string=字符串,
                date=日期,
                datetime=日期时间,
                time=时间,
                integer=整数,
               decimal=小数
             }
             """)
    private String parameterType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String parameterFormat;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long parentId;
    /**
     * 页面编码
     */
    private String pageCode;
    /** 子节点 */
    private List<ExtPageParameter> children;
    
        /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;

    public List<ExtPageParameter> getChildren() {
        return children;
    }

    public void setChildren(List<ExtPageParameter> children) {
        this.children = children;
    }

    public void setPageCode(String pageCode)
    {
        this.pageCode = pageCode;
    }

    public String getPageCode()
    {
        return pageCode;
    }
    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setParameterName(String parameterName)
    {
        this.parameterName = parameterName;
    }

    public String getParameterName()
    {
        return parameterName;
    }
    public void setParameterDesc(String parameterDesc)
    {
        this.parameterDesc = parameterDesc;
    }

    public String getParameterDesc()
    {
        return parameterDesc;
    }
    public void setParameterType(String parameterType)
    {
        this.parameterType = parameterType;
    }

    public String getParameterType()
    {
        return parameterType;
    }
    public void setParameterFormat(String parameterFormat)
    {
        this.parameterFormat = parameterFormat;
    }

    public String getParameterFormat()
    {
        return parameterFormat;
    }
    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    public Long getParentId()
    {
        return parentId;
    }
    public void setEnterpriseCode(String enterpriseCode) 
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode() 
    {
        return enterpriseCode;
    }
    public void setApplicationCode(String applicationCode) 
    {
        this.applicationCode = applicationCode;
    }

    public String getApplicationCode() 
    {
        return applicationCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("parameterName", getParameterName())
            .append("parameterDesc", getParameterDesc())
            .append("parameterType", getParameterType())
            .append("parameterFormat", getParameterFormat())
            .append("parentId", getParentId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
