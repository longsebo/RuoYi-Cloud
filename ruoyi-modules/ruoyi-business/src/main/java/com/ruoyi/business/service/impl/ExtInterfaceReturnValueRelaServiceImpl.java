package com.ruoyi.business.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.ExtInterfaceReturnValueRelaMapper;
import com.ruoyi.business.domain.ExtInterfaceReturnValueRela;
import com.ruoyi.business.service.IExtInterfaceReturnValueRelaService;

/**
 * 接口返回值关系Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
@Service
public class ExtInterfaceReturnValueRelaServiceImpl implements IExtInterfaceReturnValueRelaService 
{
    @Autowired
    private ExtInterfaceReturnValueRelaMapper extInterfaceReturnValueRelaMapper;

    /**
     * 查询接口返回值关系
     * 
     * @param id 接口返回值关系主键
     * @return 接口返回值关系
     */
    @Override
    public ExtInterfaceReturnValueRela selectExtInterfaceReturnValueRelaById(Long id)
    {
        return extInterfaceReturnValueRelaMapper.selectExtInterfaceReturnValueRelaById(id);
    }

    /**
     * 查询接口返回值关系列表
     * 
     * @param extInterfaceReturnValueRela 接口返回值关系
     * @return 接口返回值关系
     */
    @Override
    public List<ExtInterfaceReturnValueRela> selectExtInterfaceReturnValueRelaList(ExtInterfaceReturnValueRela extInterfaceReturnValueRela)
    {
        return extInterfaceReturnValueRelaMapper.selectExtInterfaceReturnValueRelaList(extInterfaceReturnValueRela);
    }

    /**
     * 新增接口返回值关系
     * 
     * @param extInterfaceReturnValueRela 接口返回值关系
     * @return 结果
     */
    @Override
    public int insertExtInterfaceReturnValueRela(ExtInterfaceReturnValueRela extInterfaceReturnValueRela)
    {
        extInterfaceReturnValueRela.setCreateTime(DateUtils.getNowDate());
        return extInterfaceReturnValueRelaMapper.insertExtInterfaceReturnValueRela(extInterfaceReturnValueRela);
    }

    /**
     * 修改接口返回值关系
     * 
     * @param extInterfaceReturnValueRela 接口返回值关系
     * @return 结果
     */
    @Override
    public int updateExtInterfaceReturnValueRela(ExtInterfaceReturnValueRela extInterfaceReturnValueRela)
    {
        extInterfaceReturnValueRela.setUpdateTime(DateUtils.getNowDate());
        return extInterfaceReturnValueRelaMapper.updateExtInterfaceReturnValueRela(extInterfaceReturnValueRela);
    }

    /**
     * 批量删除接口返回值关系
     * 
     * @param ids 需要删除的接口返回值关系主键
     * @return 结果
     */
    @Override
    public int deleteExtInterfaceReturnValueRelaByIds(Long[] ids)
    {
        return extInterfaceReturnValueRelaMapper.deleteExtInterfaceReturnValueRelaByIds(ids);
    }

    /**
     * 删除接口返回值关系信息
     * 
     * @param id 接口返回值关系主键
     * @return 结果
     */
    @Override
    public int deleteExtInterfaceReturnValueRelaById(Long id)
    {
        return extInterfaceReturnValueRelaMapper.deleteExtInterfaceReturnValueRelaById(id);
    }
}
