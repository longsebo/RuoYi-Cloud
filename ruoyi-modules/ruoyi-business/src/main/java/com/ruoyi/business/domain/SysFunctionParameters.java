package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 sys_function_parameters
 * 
 * @author ruoyi
 * @date 2025-02-16
 */
public class SysFunctionParameters extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long parameterId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String functionCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String parameterCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String parameterName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String parameterType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sortNumber;

    public void setParameterId(Long parameterId) 
    {
        this.parameterId = parameterId;
    }

    public Long getParameterId() 
    {
        return parameterId;
    }
    public void setFunctionCode(String functionCode) 
    {
        this.functionCode = functionCode;
    }

    public String getFunctionCode() 
    {
        return functionCode;
    }
    public void setParameterCode(String parameterCode) 
    {
        this.parameterCode = parameterCode;
    }

    public String getParameterCode() 
    {
        return parameterCode;
    }
    public void setParameterName(String parameterName) 
    {
        this.parameterName = parameterName;
    }

    public String getParameterName() 
    {
        return parameterName;
    }
    public void setParameterType(String parameterType) 
    {
        this.parameterType = parameterType;
    }

    public String getParameterType() 
    {
        return parameterType;
    }
    public void setSortNumber(Long sortNumber) 
    {
        this.sortNumber = sortNumber;
    }

    public Long getSortNumber() 
    {
        return sortNumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("parameterId", getParameterId())
            .append("functionCode", getFunctionCode())
            .append("parameterCode", getParameterCode())
            .append("parameterName", getParameterName())
            .append("parameterType", getParameterType())
            .append("sortNumber", getSortNumber())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
