package com.ruoyi.business.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.ExtModelOptionValueMapper;
import com.ruoyi.business.domain.ExtModelOptionValue;
import com.ruoyi.business.service.IExtModelOptionValueService;

/**
 * 选项值Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
@Service
public class ExtModelOptionValueServiceImpl implements IExtModelOptionValueService 
{
    @Autowired
    private ExtModelOptionValueMapper extModelOptionValueMapper;

    /**
     * 查询选项值
     * 
     * @param id 选项值主键
     * @return 选项值
     */
    @Override
    public ExtModelOptionValue selectExtModelOptionValueById(Long id)
    {
        return extModelOptionValueMapper.selectExtModelOptionValueById(id);
    }

    /**
     * 查询选项值列表
     * 
     * @param extModelOptionValue 选项值
     * @return 选项值
     */
    @Override
    public List<ExtModelOptionValue> selectExtModelOptionValueList(ExtModelOptionValue extModelOptionValue)
    {
        return extModelOptionValueMapper.selectExtModelOptionValueList(extModelOptionValue);
    }

    /**
     * 新增选项值
     * 
     * @param extModelOptionValue 选项值
     * @return 结果
     */
    @Override
    public int insertExtModelOptionValue(ExtModelOptionValue extModelOptionValue)
    {
        extModelOptionValue.setCreateTime(DateUtils.getNowDate());
        return extModelOptionValueMapper.insertExtModelOptionValue(extModelOptionValue);
    }

    /**
     * 修改选项值
     * 
     * @param extModelOptionValue 选项值
     * @return 结果
     */
    @Override
    public int updateExtModelOptionValue(ExtModelOptionValue extModelOptionValue)
    {
        extModelOptionValue.setUpdateTime(DateUtils.getNowDate());
        return extModelOptionValueMapper.updateExtModelOptionValue(extModelOptionValue);
    }

    /**
     * 批量删除选项值
     * 
     * @param ids 需要删除的选项值主键
     * @return 结果
     */
    @Override
    public int deleteExtModelOptionValueByIds(Long[] ids)
    {
        return extModelOptionValueMapper.deleteExtModelOptionValueByIds(ids);
    }

    /**
     * 删除选项值信息
     * 
     * @param id 选项值主键
     * @return 结果
     */
    @Override
    public int deleteExtModelOptionValueById(Long id)
    {
        return extModelOptionValueMapper.deleteExtModelOptionValueById(id);
    }
}
