package com.ruoyi.business.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.core.exception.ServiceException;
import com.ruoyi.common.security.annotation.DefaultUpdateProperty;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.ExtIntefaceUrlMapper;
import com.ruoyi.business.domain.ExtIntefaceUrl;
import com.ruoyi.business.service.IExtIntefaceUrlService;

/**
 * 通用URLService业务层处理
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@Service
public class ExtIntefaceUrlServiceImpl implements IExtIntefaceUrlService
{
    @Autowired
    private ExtIntefaceUrlMapper extIntefaceUrlMapper;

    /**
     * 查询通用URL
     *
     * @param id 通用URL主键
     * @return 通用URL
     */
    @Override
    public ExtIntefaceUrl selectExtIntefaceUrlById(Long id)
    {
        return extIntefaceUrlMapper.selectExtIntefaceUrlById(id);
    }

    /**
     * 查询通用URL列表
     *
     * @param extIntefaceUrl 通用URL
     * @return 通用URL
     */
    @Override
    public List<ExtIntefaceUrl> selectExtIntefaceUrlList(ExtIntefaceUrl extIntefaceUrl)
    {
        return extIntefaceUrlMapper.selectExtIntefaceUrlList(extIntefaceUrl);
    }

    /**
     * 新增通用URL
     *
     * @param extIntefaceUrl 通用URL
     * @return 结果
     */
    @Override
    public int insertExtIntefaceUrl( ExtIntefaceUrl extIntefaceUrl)
    {
        //extIntefaceUrl.setCreateTime(DateUtils.getNowDate());
        Map<String,Object> searchMap = new HashMap<>();
        //名称不能重复
        searchMap.put("name",extIntefaceUrl.getName());
        if(extIntefaceUrlMapper.selectCount(searchMap)>0)
            throw new ServiceException("名称:"+extIntefaceUrl.getName()+"已经存在!");
        searchMap.clear();
        //URL不能重复
        searchMap.put("url",extIntefaceUrl.getUrl());
        if(extIntefaceUrlMapper.selectCount(searchMap)>0)
            throw new ServiceException("url:"+extIntefaceUrl.getUrl()+"已经存在!");
        return extIntefaceUrlMapper.insertExtIntefaceUrl(extIntefaceUrl);
    }

    /**
     * 修改通用URL
     *
     * @param extIntefaceUrl 通用URL
     * @return 结果
     */
    @Override
    public int updateExtIntefaceUrl(@DefaultUpdateProperty ExtIntefaceUrl extIntefaceUrl)
    {
//        extIntefaceUrl.setUpdateTime(DateUtils.getNowDate());
        if(extIntefaceUrl == null)
            throw new ServiceException("插入对象为空！");
        if(extIntefaceUrl.getId()==null||extIntefaceUrl.getId()==0L)
            throw new ServiceException("对象id为空!");
        Map<String,Object> searchMap = new HashMap<>();
        //名称不能重复
        searchMap.put("name",extIntefaceUrl.getName());
        searchMap.put("notId",extIntefaceUrl.getId());
        if(extIntefaceUrlMapper.selectCount(searchMap)>0)
            throw new ServiceException("名称:"+extIntefaceUrl.getName()+"已经存在!");
        //URL不能重复
        searchMap.clear();
        searchMap.put("url",extIntefaceUrl.getUrl());
        searchMap.put("notId",extIntefaceUrl.getId());
        if(extIntefaceUrlMapper.selectCount(searchMap)>0)
            throw new ServiceException("url:"+extIntefaceUrl.getUrl()+"已经存在!");
        return extIntefaceUrlMapper.updateExtIntefaceUrl(extIntefaceUrl);
    }

    /**
     * 批量删除通用URL
     *
     * @param ids 需要删除的通用URL主键
     * @return 结果
     */
    @Override
    public int deleteExtIntefaceUrlByIds(Long[] ids)
    {
        return extIntefaceUrlMapper.deleteExtIntefaceUrlByIds(ids);
    }

    /**
     * 删除通用URL信息
     *
     * @param id 通用URL主键
     * @return 结果
     */
    @Override
    public int deleteExtIntefaceUrlById(Long id)
    {
        return extIntefaceUrlMapper.deleteExtIntefaceUrlById(id);
    }
}
