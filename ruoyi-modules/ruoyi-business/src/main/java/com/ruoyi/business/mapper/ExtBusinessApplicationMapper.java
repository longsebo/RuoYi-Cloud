package com.ruoyi.business.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.ExtBusinessApplication;

/**
 * 应用定义Mapper接口
 *
 * @author ruoyi
 * @date 2024-09-24
 */
public interface ExtBusinessApplicationMapper
{
    /**
     * 查询应用定义
     *
     * @param id 应用定义主键
     * @return 应用定义
     */
    public ExtBusinessApplication selectExtBusinessApplicationById(Long id);

    /**
     * 查询应用定义列表
     *
     * @param extBusinessApplication 应用定义
     * @return 应用定义集合
     */
    public List<ExtBusinessApplication> selectExtBusinessApplicationList(ExtBusinessApplication extBusinessApplication);

    /**
     * 新增应用定义
     *
     * @param extBusinessApplication 应用定义
     * @return 结果
     */
    public int insertExtBusinessApplication(ExtBusinessApplication extBusinessApplication);

    /**
     * 修改应用定义
     *
     * @param extBusinessApplication 应用定义
     * @return 结果
     */
    public int updateExtBusinessApplication(ExtBusinessApplication extBusinessApplication);

    /**
     * 删除应用定义
     *
     * @param id 应用定义主键
     * @return 结果
     */
    public int deleteExtBusinessApplicationById(Long id);

    /**
     * 批量删除应用定义
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtBusinessApplicationByIds(Long[] ids);

    /**
     * 查询满足条件条数
     * @param searchMap 查询条件
     * @return 返回满足条件数
     */
    public int selectCount(Map<String, Object> searchMap);
}
