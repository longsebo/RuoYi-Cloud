package com.ruoyi.business.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.SysFunctionParametersMapper;
import com.ruoyi.business.domain.SysFunctionParameters;
import com.ruoyi.business.service.ISysFunctionParametersService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2025-02-16
 */
@Service
public class SysFunctionParametersServiceImpl implements ISysFunctionParametersService 
{
    @Autowired
    private SysFunctionParametersMapper sysFunctionParametersMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param parameterId 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public SysFunctionParameters selectSysFunctionParametersByParameterId(Long parameterId)
    {
        return sysFunctionParametersMapper.selectSysFunctionParametersByParameterId(parameterId);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param sysFunctionParameters 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<SysFunctionParameters> selectSysFunctionParametersList(SysFunctionParameters sysFunctionParameters)
    {
        return sysFunctionParametersMapper.selectSysFunctionParametersList(sysFunctionParameters);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param sysFunctionParameters 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertSysFunctionParameters(SysFunctionParameters sysFunctionParameters)
    {
        sysFunctionParameters.setCreateTime(DateUtils.getNowDate());
        return sysFunctionParametersMapper.insertSysFunctionParameters(sysFunctionParameters);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param sysFunctionParameters 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateSysFunctionParameters(SysFunctionParameters sysFunctionParameters)
    {
        sysFunctionParameters.setUpdateTime(DateUtils.getNowDate());
        return sysFunctionParametersMapper.updateSysFunctionParameters(sysFunctionParameters);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param parameterIds 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteSysFunctionParametersByParameterIds(Long[] parameterIds)
    {
        return sysFunctionParametersMapper.deleteSysFunctionParametersByParameterIds(parameterIds);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param parameterId 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteSysFunctionParametersByParameterId(Long parameterId)
    {
        return sysFunctionParametersMapper.deleteSysFunctionParametersByParameterId(parameterId);
    }
}
