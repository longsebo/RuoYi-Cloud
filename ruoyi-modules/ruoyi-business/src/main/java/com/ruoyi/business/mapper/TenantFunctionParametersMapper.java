package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.domain.TenantFunctionParameters;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2025-02-16
 */
public interface TenantFunctionParametersMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param parameterId 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public TenantFunctionParameters selectTenantFunctionParametersByParameterId(Long parameterId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param tenantFunctionParameters 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<TenantFunctionParameters> selectTenantFunctionParametersList(TenantFunctionParameters tenantFunctionParameters);

    /**
     * 新增【请填写功能名称】
     * 
     * @param tenantFunctionParameters 【请填写功能名称】
     * @return 结果
     */
    public int insertTenantFunctionParameters(TenantFunctionParameters tenantFunctionParameters);

    /**
     * 修改【请填写功能名称】
     * 
     * @param tenantFunctionParameters 【请填写功能名称】
     * @return 结果
     */
    public int updateTenantFunctionParameters(TenantFunctionParameters tenantFunctionParameters);

    /**
     * 删除【请填写功能名称】
     * 
     * @param parameterId 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteTenantFunctionParametersByParameterId(Long parameterId);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param parameterIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTenantFunctionParametersByParameterIds(Long[] parameterIds);
}
