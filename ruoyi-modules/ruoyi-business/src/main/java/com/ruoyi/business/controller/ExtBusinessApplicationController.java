package com.ruoyi.business.controller;

import com.ruoyi.business.domain.ExtBusinessApplication;
import com.ruoyi.business.service.IExtBusinessApplicationService;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.DefaultCreateProperty;
import com.ruoyi.common.security.annotation.DefaultUpdateProperty;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 应用定义Controller
 *
 * @author ruoyi
 * @date 2024-09-24
 */
@RestController
@RequestMapping("/application")
public class ExtBusinessApplicationController extends BaseController
{
    @Autowired
    private IExtBusinessApplicationService extBusinessApplicationService;

    /**
     * 查询应用定义列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ExtBusinessApplication extBusinessApplication)
    {
        startPage();
        List<ExtBusinessApplication> list = extBusinessApplicationService.selectExtBusinessApplicationList(extBusinessApplication);
        return getDataTable(list);
    }

    /**
     * 导出应用定义列表
     */
    @RequiresPermissions("business:application:export")
    @Log(title = "应用定义", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtBusinessApplication extBusinessApplication)
    {
        List<ExtBusinessApplication> list = extBusinessApplicationService.selectExtBusinessApplicationList(extBusinessApplication);
        ExcelUtil<ExtBusinessApplication> util = new ExcelUtil<ExtBusinessApplication>(ExtBusinessApplication.class);
        util.exportExcel(response, list, "应用定义数据");
    }

    /**
     * 获取应用定义详细信息
     */
    @RequiresPermissions("business:application:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extBusinessApplicationService.selectExtBusinessApplicationById(id));
    }

    /**
     * 新增应用定义
     */
    @RequiresPermissions("business:application:add")
    @Log(title = "应用定义", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@DefaultCreateProperty ExtBusinessApplication extBusinessApplication)
    {
        //不能使用@EnterpriseAndApplicationCodeProperty注解 ，所以增加手动设置
        extBusinessApplication.setEnterpriseCode(SecurityUtils.getEnterpriseCode());
        return toAjax(extBusinessApplicationService.insertExtBusinessApplication(extBusinessApplication));
    }

    /**
     * 修改应用定义
     */
    @RequiresPermissions("business:application:edit")
    @Log(title = "应用定义", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@DefaultUpdateProperty ExtBusinessApplication extBusinessApplication)
    {
        //不能使用@EnterpriseAndApplicationCodeProperty注解 ，所以增加手动设置
        extBusinessApplication.setEnterpriseCode(SecurityUtils.getEnterpriseCode());
        return toAjax(extBusinessApplicationService.updateExtBusinessApplication(extBusinessApplication));
    }

    /**
     * 删除应用定义
     */
    @RequiresPermissions("business:application:remove")
    @Log(title = "应用定义", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extBusinessApplicationService.deleteExtBusinessApplicationByIds(ids));
    }
}
