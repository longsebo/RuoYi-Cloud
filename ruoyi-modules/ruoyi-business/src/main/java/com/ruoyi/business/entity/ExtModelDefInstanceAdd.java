package com.ruoyi.business.entity;

import lombok.Data;

import java.util.Map;

/**
 * 新增模型实例
 */
@Data
public class ExtModelDefInstanceAdd extends BaseAPI {
    /**
     * 数据源名称
     */
    private String dataSourceName;
    /**
     * 表英文名
     */
    private String enName;
    /**
     * 字段数据
     */
    private Map<String, Object> data;
}
