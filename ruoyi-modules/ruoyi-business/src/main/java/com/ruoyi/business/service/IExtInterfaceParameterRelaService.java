package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.domain.ExtInterfaceParameterRela;

/**
 * 接口参数关系Service接口
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
public interface IExtInterfaceParameterRelaService 
{
    /**
     * 查询接口参数关系
     * 
     * @param id 接口参数关系主键
     * @return 接口参数关系
     */
    public ExtInterfaceParameterRela selectExtInterfaceParameterRelaById(Long id);

    /**
     * 查询接口参数关系列表
     * 
     * @param extInterfaceParameterRela 接口参数关系
     * @return 接口参数关系集合
     */
    public List<ExtInterfaceParameterRela> selectExtInterfaceParameterRelaList(ExtInterfaceParameterRela extInterfaceParameterRela);

    /**
     * 新增接口参数关系
     * 
     * @param extInterfaceParameterRela 接口参数关系
     * @return 结果
     */
    public int insertExtInterfaceParameterRela(ExtInterfaceParameterRela extInterfaceParameterRela);

    /**
     * 修改接口参数关系
     * 
     * @param extInterfaceParameterRela 接口参数关系
     * @return 结果
     */
    public int updateExtInterfaceParameterRela(ExtInterfaceParameterRela extInterfaceParameterRela);

    /**
     * 批量删除接口参数关系
     * 
     * @param ids 需要删除的接口参数关系主键集合
     * @return 结果
     */
    public int deleteExtInterfaceParameterRelaByIds(Long[] ids);

    /**
     * 删除接口参数关系信息
     * 
     * @param id 接口参数关系主键
     * @return 结果
     */
    public int deleteExtInterfaceParameterRelaById(Long id);
}
