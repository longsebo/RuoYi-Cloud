/**
 *
 */
package com.ruoyi.business.domain.sqldesign;


import com.ruoyi.common.core.utils.StringUtils;

/**
 * 	@author Administrator
 *	抽象条件树模型s
 */
public class AbstractConditionTreeModel implements IConditionTreeModel {
	protected int type;
	protected String parentLevel="";
	protected int currentLevel=0;
	@Override
	public int getType() {
		return type;
	}

	@Override
	public String getLevel() {
		if(StringUtils.isEmpty(parentLevel)) {
			return String.valueOf(currentLevel);
		}else {
			return parentLevel+"."+currentLevel;
		}
	}

	@Override
	public void setParentLevel(String value) {
		this.parentLevel = value;
	}

	@Override
	public void setCurrentLevel(int value) {
		this.currentLevel = value;
	}

	@Override
	public int getCurrentLevel() {
		return currentLevel;
	}

	@Override
	public String getParentLevel() {
		return parentLevel;
	}

}
