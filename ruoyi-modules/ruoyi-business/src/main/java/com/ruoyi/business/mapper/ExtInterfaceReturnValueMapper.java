package com.ruoyi.business.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.ExtInterfaceReturnValue;
import org.apache.ibatis.annotations.Param;

/**
 * 接口返回值Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-22
 */
public interface ExtInterfaceReturnValueMapper
{
    /**
     * 查询接口返回值
     *
     * @param id 接口返回值主键
     * @return 接口返回值
     */
    public ExtInterfaceReturnValue selectExtInterfaceReturnValueById(Long id);

    /**
     * 查询接口返回值列表
     *
     * @param extInterfaceReturnValue 接口返回值
     * @return 接口返回值集合
     */
    public List<ExtInterfaceReturnValue> selectExtInterfaceReturnValueList(ExtInterfaceReturnValue extInterfaceReturnValue);

    /**
     * 新增接口返回值
     *
     * @param extInterfaceReturnValue 接口返回值
     * @return 结果
     */
    public int insertExtInterfaceReturnValue(ExtInterfaceReturnValue extInterfaceReturnValue);

    /**
     * 修改接口返回值
     *
     * @param extInterfaceReturnValue 接口返回值
     * @return 结果
     */
    public int updateExtInterfaceReturnValue(ExtInterfaceReturnValue extInterfaceReturnValue);

    /**
     * 删除接口返回值
     *
     * @param id 接口返回值主键
     * @return 结果
     */
    public int deleteExtInterfaceReturnValueById(Long id);

    /**
     * 批量删除接口返回值
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtInterfaceReturnValueByIds(Long[] ids);

    /**
     * 根据接口编码删除接口返回
     * @param interfaceCode
     * @return
     */
    public int deleteByInterfaceCode(@Param("interfaceCode") String interfaceCode);

    /**
     * 根据查询条件返回满足记录数
     * @param searchMap
     * @return
     */
    public int selectCount(Map<String, Object> searchMap);
}
