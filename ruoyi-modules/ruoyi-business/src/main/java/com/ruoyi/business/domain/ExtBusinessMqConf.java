package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * MQ配置定义对象 ext_business_mq_conf
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
public class ExtBusinessMqConf extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String mqName;

    /** MQ类型 */
    @Excel(name = "MQ类型")
    private String mqType;

    /** MQ配置 */
    @Excel(name = "MQ配置")
    private String mqConfig;

    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMqName(String mqName) 
    {
        this.mqName = mqName;
    }

    public String getMqName() 
    {
        return mqName;
    }
    public void setMqType(String mqType) 
    {
        this.mqType = mqType;
    }

    public String getMqType() 
    {
        return mqType;
    }
    public void setMqConfig(String mqConfig) 
    {
        this.mqConfig = mqConfig;
    }

    public String getMqConfig() 
    {
        return mqConfig;
    }
    public void setEnterpriseCode(String enterpriseCode) 
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode() 
    {
        return enterpriseCode;
    }
    public void setApplicationCode(String applicationCode) 
    {
        this.applicationCode = applicationCode;
    }

    public String getApplicationCode() 
    {
        return applicationCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mqName", getMqName())
            .append("mqType", getMqType())
            .append("mqConfig", getMqConfig())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
