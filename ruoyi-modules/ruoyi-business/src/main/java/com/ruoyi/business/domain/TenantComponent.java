package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.util.List;

/**
 * 租户级别组件，是各个租户自定义组件对象 tenant_component
 *
 * @author ruoyi
 * @date 2025-02-16
 */
public class TenantComponent extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String enterpriseCode;

    /** $column.columnComment */
    private Long componentId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String groupCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String className;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String componentName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String componentIconUrl;

    /** 启用禁用状态：00--禁用 01--启用 */
    @Excel(name = "启用禁用状态：00--禁用 01--启用")
    private String status;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sortNumber;
    private List<String> groupCodes;

    public void setEnterpriseCode(String enterpriseCode)
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode()
    {
        return enterpriseCode;
    }
    public void setComponentId(Long componentId)
    {
        this.componentId = componentId;
    }

    public Long getComponentId()
    {
        return componentId;
    }
    public void setGroupCode(String groupCode)
    {
        this.groupCode = groupCode;
    }

    public String getGroupCode()
    {
        return groupCode;
    }
    public void setClassName(String className)
    {
        this.className = className;
    }

    public String getClassName()
    {
        return className;
    }
    public void setComponentName(String componentName)
    {
        this.componentName = componentName;
    }

    public String getComponentName()
    {
        return componentName;
    }
    public void setComponentIconUrl(String componentIconUrl)
    {
        this.componentIconUrl = componentIconUrl;
    }

    public String getComponentIconUrl()
    {
        return componentIconUrl;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setSortNumber(Long sortNumber)
    {
        this.sortNumber = sortNumber;
    }

    public Long getSortNumber()
    {
        return sortNumber;
    }

    public List<String> getGroupCodes() {
        return groupCodes;
    }

    public void setGroupCodes(List<String> groupCodes) {
        this.groupCodes = groupCodes;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("enterpriseCode", getEnterpriseCode())
            .append("componentId", getComponentId())
            .append("groupCode", getGroupCode())
            .append("className", getClassName())
            .append("componentName", getComponentName())
            .append("componentIconUrl", getComponentIconUrl())
            .append("remark", getRemark())
            .append("status", getStatus())
            .append("sortNumber", getSortNumber())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
