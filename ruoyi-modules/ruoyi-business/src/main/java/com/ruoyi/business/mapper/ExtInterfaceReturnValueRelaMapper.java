package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.domain.ExtInterfaceReturnValueRela;
import org.apache.ibatis.annotations.Param;

/**
 * 接口返回值关系Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface ExtInterfaceReturnValueRelaMapper
{
    /**
     * 查询接口返回值关系
     *
     * @param id 接口返回值关系主键
     * @return 接口返回值关系
     */
    public ExtInterfaceReturnValueRela selectExtInterfaceReturnValueRelaById(Long id);

    /**
     * 查询接口返回值关系列表
     *
     * @param extInterfaceReturnValueRela 接口返回值关系
     * @return 接口返回值关系集合
     */
    public List<ExtInterfaceReturnValueRela> selectExtInterfaceReturnValueRelaList(ExtInterfaceReturnValueRela extInterfaceReturnValueRela);

    /**
     * 新增接口返回值关系
     *
     * @param extInterfaceReturnValueRela 接口返回值关系
     * @return 结果
     */
    public int insertExtInterfaceReturnValueRela(ExtInterfaceReturnValueRela extInterfaceReturnValueRela);

    /**
     * 修改接口返回值关系
     *
     * @param extInterfaceReturnValueRela 接口返回值关系
     * @return 结果
     */
    public int updateExtInterfaceReturnValueRela(ExtInterfaceReturnValueRela extInterfaceReturnValueRela);

    /**
     * 删除接口返回值关系
     *
     * @param id 接口返回值关系主键
     * @return 结果
     */
    public int deleteExtInterfaceReturnValueRelaById(Long id);

    /**
     * 批量删除接口返回值关系
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtInterfaceReturnValueRelaByIds(Long[] ids);

    /**
     * 根据接口编码删除接口返回值关系表
     * @param interfaceCode
     * @return
     */
    public int deleteByInterfaceCode(@Param("interfaceCode") String interfaceCode);

    /**
     * 根据接口返回值id删除关系表
     * @param id
     * @return
     */
    public int deleteByReturnValueId(@Param("id") Long id);
}
