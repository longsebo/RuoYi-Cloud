package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 选项值对象 ext_model_option_value
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
public class ExtModelOptionValue extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 选项类型ID */
    @Excel(name = "选项类型ID")
    private Long extModelOptionTypeId;

    /** 选项值名称 */
    @Excel(name = "选项值名称")
    private String optionValueName;

    /** 选项值 */
    @Excel(name = "选项值")
    private String optionValue;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 排序号 */
    @Excel(name = "排序号")
    private Long orderNo;

    /** 选项父级ID */
    @Excel(name = "选项父级ID")
    private Long parentId;

    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setExtModelOptionTypeId(Long extModelOptionTypeId) 
    {
        this.extModelOptionTypeId = extModelOptionTypeId;
    }

    public Long getExtModelOptionTypeId() 
    {
        return extModelOptionTypeId;
    }
    public void setOptionValueName(String optionValueName) 
    {
        this.optionValueName = optionValueName;
    }

    public String getOptionValueName() 
    {
        return optionValueName;
    }
    public void setOptionValue(String optionValue) 
    {
        this.optionValue = optionValue;
    }

    public String getOptionValue() 
    {
        return optionValue;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setOrderNo(Long orderNo) 
    {
        this.orderNo = orderNo;
    }

    public Long getOrderNo() 
    {
        return orderNo;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setEnterpriseCode(String enterpriseCode) 
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode() 
    {
        return enterpriseCode;
    }
    public void setApplicationCode(String applicationCode) 
    {
        this.applicationCode = applicationCode;
    }

    public String getApplicationCode() 
    {
        return applicationCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("extModelOptionTypeId", getExtModelOptionTypeId())
            .append("optionValueName", getOptionValueName())
            .append("optionValue", getOptionValue())
            .append("status", getStatus())
            .append("orderNo", getOrderNo())
            .append("parentId", getParentId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
