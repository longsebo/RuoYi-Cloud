package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.domain.TenantComponent;

/**
 * 租户级别组件，是各个租户自定义组件Service接口
 * 
 * @author ruoyi
 * @date 2025-02-16
 */
public interface ITenantComponentService 
{
    /**
     * 查询租户级别组件，是各个租户自定义组件
     * 
     * @param componentId 租户级别组件，是各个租户自定义组件主键
     * @return 租户级别组件，是各个租户自定义组件
     */
    public TenantComponent selectTenantComponentByComponentId(Long componentId);

    /**
     * 查询租户级别组件，是各个租户自定义组件列表
     * 
     * @param tenantComponent 租户级别组件，是各个租户自定义组件
     * @return 租户级别组件，是各个租户自定义组件集合
     */
    public List<TenantComponent> selectTenantComponentList(TenantComponent tenantComponent);

    /**
     * 新增租户级别组件，是各个租户自定义组件
     * 
     * @param tenantComponent 租户级别组件，是各个租户自定义组件
     * @return 结果
     */
    public int insertTenantComponent(TenantComponent tenantComponent);

    /**
     * 修改租户级别组件，是各个租户自定义组件
     * 
     * @param tenantComponent 租户级别组件，是各个租户自定义组件
     * @return 结果
     */
    public int updateTenantComponent(TenantComponent tenantComponent);

    /**
     * 批量删除租户级别组件，是各个租户自定义组件
     * 
     * @param componentIds 需要删除的租户级别组件，是各个租户自定义组件主键集合
     * @return 结果
     */
    public int deleteTenantComponentByComponentIds(Long[] componentIds);

    /**
     * 删除租户级别组件，是各个租户自定义组件信息
     * 
     * @param componentId 租户级别组件，是各个租户自定义组件主键
     * @return 结果
     */
    public int deleteTenantComponentByComponentId(Long componentId);
}
