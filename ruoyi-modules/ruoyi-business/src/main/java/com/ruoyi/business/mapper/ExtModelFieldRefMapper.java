package com.ruoyi.business.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.ExtModelDef;
import com.ruoyi.business.domain.ExtModelField;
import com.ruoyi.business.domain.ExtModelFieldRef;
import org.apache.ibatis.annotations.Param;

/**
 * 模型字段引用Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface ExtModelFieldRefMapper
{
    /**
     * 查询模型字段引用
     *
     * @param id 模型字段引用主键
     * @return 模型字段引用
     */
    public ExtModelFieldRef selectExtModelFieldRefById(Long id);

    /**
     * 查询模型字段引用列表
     *
     * @param extModelFieldRef 模型字段引用
     * @return 模型字段引用集合
     */
    public List<ExtModelFieldRef> selectExtModelFieldRefList(ExtModelFieldRef extModelFieldRef);

    /**
     * 新增模型字段引用
     *
     * @param extModelFieldRef 模型字段引用
     * @return 结果
     */
    public int insertExtModelFieldRef(ExtModelFieldRef extModelFieldRef);

    /**
     * 修改模型字段引用
     *
     * @param extModelFieldRef 模型字段引用
     * @return 结果
     */
    public int updateExtModelFieldRef(ExtModelFieldRef extModelFieldRef);

    /**
     * 删除模型字段引用
     *
     * @param id 模型字段引用主键
     * @return 结果
     */
    public int deleteExtModelFieldRefById(Long id);

    /**
     * 批量删除模型字段引用
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtModelFieldRefByIds(Long[] ids);

    /**
     * 根据表英文名删除字段引用
     * @param extModelDef
     * @return
     */
    public int deleteByTableName(@Param("extModelDef") ExtModelDef extModelDef);

    /**
     * 查询满足条件记录数
     * @param extModelFieldRef
     * @return
     */
    public int selectCount(ExtModelFieldRef extModelFieldRef);

    /**
     * 根据条件删除引用字段
     * @param extModelFieldRef
     * @return
     */
    public int deleteExtModelFieldRefByModel(ExtModelFieldRef extModelFieldRef);

    /**
     * 更新数据源名称
     * @param oldExtModelDef  旧的模型
     * @param datasourceName  新的模型
     * @return
     */
    public int updateReferenFieldsDataSourceName(@Param("oldExtModelDef") ExtModelDef oldExtModelDef,@Param("newDatasourceName") String datasourceName);

    /**
     * 关联字段定义表，按数据源+表英文名+字段名或字段中文名查询
     * @param searchMap
     * @return
     */
    public int  selectCountAssociation(Map<String, Object> searchMap);
    /**
     * 更新字段引用表英文表名
     * @param datasourceName 数据源名称
     * @param oldEnName 旧表名
     * @param newEnName 新表名
     */
    public int updateReferenFieldsEnName(@Param("datasourceName") String datasourceName,@Param("oldEnName") String oldEnName,@Param("newEnName") String newEnName);
}
