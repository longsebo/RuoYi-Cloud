package com.ruoyi.business.entity;
import lombok.Data;

import java.util.Map;

/**
 * 模型查询
 */
@Data
public class ExtModelDefInstanceList extends BaseAPI {
    /**
     * 数据源名称
     */
    private String dataSourceName;
    /**
     * 查询条件
     */
    private Map<String, Object> searchCondition;
}
