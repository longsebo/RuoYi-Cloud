package com.ruoyi.business.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.ExtModelDatasource;

/**
 * 数据源定义Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface ExtModelDatasourceMapper
{
    /**
     * 查询数据源定义
     *
     * @param id 数据源定义主键
     * @return 数据源定义
     */
    public ExtModelDatasource selectExtModelDatasourceById(Long id);

    /**
     * 查询数据源定义列表
     *
     * @param extModelDatasource 数据源定义
     * @return 数据源定义集合
     */
    public List<ExtModelDatasource> selectExtModelDatasourceList(ExtModelDatasource extModelDatasource);

    /**
     * 新增数据源定义
     *
     * @param extModelDatasource 数据源定义
     * @return 结果
     */
    public int insertExtModelDatasource(ExtModelDatasource extModelDatasource);

    /**
     * 修改数据源定义
     *
     * @param extModelDatasource 数据源定义
     * @return 结果
     */
    public int updateExtModelDatasource(ExtModelDatasource extModelDatasource);

    /**
     * 删除数据源定义
     *
     * @param id 数据源定义主键
     * @return 结果
     */
    public int deleteExtModelDatasourceById(Long id);

    /**
     * 批量删除数据源定义
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtModelDatasourceByIds(Long[] ids);

    /**
     * 查询满足条件的条数
     * @param searchMap 查询条件
     * @return 返回满足条件条数
     */
    public int  selectCount(Map<String, Object> searchMap);
}
