package com.ruoyi.business.controller;

import com.ruoyi.business.domain.SysFunctionParameters;
import com.ruoyi.business.service.ISysFunctionParametersService;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 【请填写功能名称】Controller
 *
 * @author ruoyi
 * @date 2025-02-16
 */
@RestController
@RequestMapping("/sysFunctionParameters")
public class SysFunctionParametersController extends BaseController
{
    @Autowired
    private ISysFunctionParametersService sysFunctionParametersService;

    /**
     * 查询【请填写功能名称】列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SysFunctionParameters sysFunctionParameters)
    {
        startPage();
        List<SysFunctionParameters> list = sysFunctionParametersService.selectSysFunctionParametersList(sysFunctionParameters);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysFunctionParameters sysFunctionParameters)
    {
        List<SysFunctionParameters> list = sysFunctionParametersService.selectSysFunctionParametersList(sysFunctionParameters);
        ExcelUtil<SysFunctionParameters> util = new ExcelUtil<SysFunctionParameters>(SysFunctionParameters.class);
        util.exportExcel(response, list, "【请填写功能名称】数据");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @GetMapping(value = "/{parameterId}")
    public AjaxResult getInfo(@PathVariable("parameterId") Long parameterId)
    {
        return success(sysFunctionParametersService.selectSysFunctionParametersByParameterId(parameterId));
    }

    /**
     * 新增【请填写功能名称】
     */
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty SysFunctionParameters sysFunctionParameters)
    {
        return toAjax(sysFunctionParametersService.insertSysFunctionParameters(sysFunctionParameters));
    }

    /**
     * 修改【请填写功能名称】
     */
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty SysFunctionParameters sysFunctionParameters)
    {
        return toAjax(sysFunctionParametersService.updateSysFunctionParameters(sysFunctionParameters));
    }

    /**
     * 删除【请填写功能名称】
     */
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
	@DeleteMapping("/{parameterIds}")
    public AjaxResult remove(@PathVariable Long[] parameterIds)
    {
        return toAjax(sysFunctionParametersService.deleteSysFunctionParametersByParameterIds(parameterIds));
    }
}
