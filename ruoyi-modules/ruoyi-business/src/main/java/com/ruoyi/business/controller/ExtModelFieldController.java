package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.business.domain.ExtModelFieldRef;
import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtModelField;
import com.ruoyi.business.service.IExtModelFieldService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 模型字段Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@RestController
@RequestMapping("/field")
public class ExtModelFieldController extends BaseController
{
    @Autowired
    private IExtModelFieldService extModelFieldService;

    /**
     * 查询模型字段列表
     */
    @RequiresPermissions("business:field:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtModelField extModelField)
    {
        startPage();
        List<ExtModelField> list = extModelFieldService.selectExtModelFieldList(extModelField);
        return getDataTable(list);
    }

    /**
     * 导出模型字段列表
     */
    @RequiresPermissions("business:field:export")
    @Log(title = "模型字段", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtModelField extModelField)
    {
        List<ExtModelField> list = extModelFieldService.selectExtModelFieldList(extModelField);
        ExcelUtil<ExtModelField> util = new ExcelUtil<ExtModelField>(ExtModelField.class);
        util.exportExcel(response, list, "模型字段数据");
    }

    /**
     * 获取模型字段详细信息
     */
    @RequiresPermissions("business:field:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extModelFieldService.selectExtModelFieldById(id));
    }

    /**
     * 新增模型字段
     */
    @RequiresPermissions("business:field:add")
    @Log(title = "模型字段", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty ExtModelField extModelField)
    {
        return toAjax(extModelFieldService.insertExtModelField(extModelField));
    }

    /**
     * 修改模型字段
     */
    @RequiresPermissions("business:field:edit")
    @Log(title = "模型字段", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty ExtModelField extModelField)
    {
        return toAjax(extModelFieldService.updateExtModelField(extModelField));
    }

    /**
     * 删除模型字段
     */
    @RequiresPermissions("business:field:remove")
    @Log(title = "模型字段", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extModelFieldService.deleteExtModelFieldByIds(ids));
    }
    /**
     * 通过数据源名称和表英文名非翻页查询模型字段列表(模型字段和字段引用表关联)
     */
    @RequiresPermissions("business:field:listnopage")
    @GetMapping("/listnopage")
    public AjaxResult listNoPage(ExtModelFieldRef extModelFieldRef)
    {
        List<ExtModelField> list = extModelFieldService.selectExtModelFieldListRef(extModelFieldRef);
        return success(list);
    }
    /**
     * 非翻页查询模型字段列表
     */
    @RequiresPermissions("business:field:listAll")
    @GetMapping("/listAll")
    public AjaxResult listAll(ExtModelField extModelField)
    {
        List<ExtModelField> list = extModelFieldService.selectExtModelFieldList(extModelField);
        return success(list);
    }
    /**
     * 查询某模型表字段列表
     */
    @RequiresPermissions("business:field:listModelField")
    @GetMapping("/listModelField")
    public AjaxResult listModelField(ExtModelField extModelField)
    {
        List<ExtModelField> list = extModelFieldService.selectModelField(extModelField);
        return success(list);
    }
    /**
     * 根据引用字段id，数据源名称，表英文名 删除模型字段
     */
    @RequiresPermissions("business:field:removeByExtModelField")
    @Log(title = "模型字段", businessType = BusinessType.DELETE)
    @PostMapping("/removeByExtModelField")
    public AjaxResult removeByExtModelField(@RequestBody ExtModelField extModelField)
    {
        return toAjax(extModelFieldService.removeByExtModelField(extModelField));
    }
}
