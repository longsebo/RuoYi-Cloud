package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 对外宣传为功能点，即开发人员的函数对象 sys_function
 * 
 * @author ruoyi
 * @date 2025-02-16
 */
public class SysFunction extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long functionId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long componentId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String className;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String functionCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String functionName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String functionDesc;

    /** 启用禁用状态：00--禁用 01--启用 */
    @Excel(name = "启用禁用状态：00--禁用 01--启用")
    private String status;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sortNumber;

    /** 是否有返回值：N---否，Y---是 */
    @Excel(name = "是否有返回值：N---否，Y---是")
    private String isReturnvalue;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String returnType;

    public void setFunctionId(Long functionId) 
    {
        this.functionId = functionId;
    }

    public Long getFunctionId() 
    {
        return functionId;
    }
    public void setComponentId(Long componentId) 
    {
        this.componentId = componentId;
    }

    public Long getComponentId() 
    {
        return componentId;
    }
    public void setClassName(String className) 
    {
        this.className = className;
    }

    public String getClassName() 
    {
        return className;
    }
    public void setFunctionCode(String functionCode) 
    {
        this.functionCode = functionCode;
    }

    public String getFunctionCode() 
    {
        return functionCode;
    }
    public void setFunctionName(String functionName) 
    {
        this.functionName = functionName;
    }

    public String getFunctionName() 
    {
        return functionName;
    }
    public void setFunctionDesc(String functionDesc) 
    {
        this.functionDesc = functionDesc;
    }

    public String getFunctionDesc() 
    {
        return functionDesc;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setSortNumber(Long sortNumber) 
    {
        this.sortNumber = sortNumber;
    }

    public Long getSortNumber() 
    {
        return sortNumber;
    }
    public void setIsReturnvalue(String isReturnvalue) 
    {
        this.isReturnvalue = isReturnvalue;
    }

    public String getIsReturnvalue() 
    {
        return isReturnvalue;
    }
    public void setReturnType(String returnType) 
    {
        this.returnType = returnType;
    }

    public String getReturnType() 
    {
        return returnType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("functionId", getFunctionId())
            .append("componentId", getComponentId())
            .append("className", getClassName())
            .append("functionCode", getFunctionCode())
            .append("functionName", getFunctionName())
            .append("functionDesc", getFunctionDesc())
            .append("status", getStatus())
            .append("sortNumber", getSortNumber())
            .append("isReturnvalue", getIsReturnvalue())
            .append("returnType", getReturnType())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
