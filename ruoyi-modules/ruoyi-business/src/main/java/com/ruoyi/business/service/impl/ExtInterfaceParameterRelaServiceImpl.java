package com.ruoyi.business.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.ExtInterfaceParameterRelaMapper;
import com.ruoyi.business.domain.ExtInterfaceParameterRela;
import com.ruoyi.business.service.IExtInterfaceParameterRelaService;

/**
 * 接口参数关系Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
@Service
public class ExtInterfaceParameterRelaServiceImpl implements IExtInterfaceParameterRelaService 
{
    @Autowired
    private ExtInterfaceParameterRelaMapper extInterfaceParameterRelaMapper;

    /**
     * 查询接口参数关系
     * 
     * @param id 接口参数关系主键
     * @return 接口参数关系
     */
    @Override
    public ExtInterfaceParameterRela selectExtInterfaceParameterRelaById(Long id)
    {
        return extInterfaceParameterRelaMapper.selectExtInterfaceParameterRelaById(id);
    }

    /**
     * 查询接口参数关系列表
     * 
     * @param extInterfaceParameterRela 接口参数关系
     * @return 接口参数关系
     */
    @Override
    public List<ExtInterfaceParameterRela> selectExtInterfaceParameterRelaList(ExtInterfaceParameterRela extInterfaceParameterRela)
    {
        return extInterfaceParameterRelaMapper.selectExtInterfaceParameterRelaList(extInterfaceParameterRela);
    }

    /**
     * 新增接口参数关系
     * 
     * @param extInterfaceParameterRela 接口参数关系
     * @return 结果
     */
    @Override
    public int insertExtInterfaceParameterRela(ExtInterfaceParameterRela extInterfaceParameterRela)
    {
        extInterfaceParameterRela.setCreateTime(DateUtils.getNowDate());
        return extInterfaceParameterRelaMapper.insertExtInterfaceParameterRela(extInterfaceParameterRela);
    }

    /**
     * 修改接口参数关系
     * 
     * @param extInterfaceParameterRela 接口参数关系
     * @return 结果
     */
    @Override
    public int updateExtInterfaceParameterRela(ExtInterfaceParameterRela extInterfaceParameterRela)
    {
        extInterfaceParameterRela.setUpdateTime(DateUtils.getNowDate());
        return extInterfaceParameterRelaMapper.updateExtInterfaceParameterRela(extInterfaceParameterRela);
    }

    /**
     * 批量删除接口参数关系
     * 
     * @param ids 需要删除的接口参数关系主键
     * @return 结果
     */
    @Override
    public int deleteExtInterfaceParameterRelaByIds(Long[] ids)
    {
        return extInterfaceParameterRelaMapper.deleteExtInterfaceParameterRelaByIds(ids);
    }

    /**
     * 删除接口参数关系信息
     * 
     * @param id 接口参数关系主键
     * @return 结果
     */
    @Override
    public int deleteExtInterfaceParameterRelaById(Long id)
    {
        return extInterfaceParameterRelaMapper.deleteExtInterfaceParameterRelaById(id);
    }
}
