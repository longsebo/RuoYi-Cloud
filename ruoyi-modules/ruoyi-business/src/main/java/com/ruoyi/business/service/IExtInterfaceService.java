package com.ruoyi.business.service;

import java.util.List;

import com.ruoyi.business.domain.BusinessFunctionTree;
import com.ruoyi.business.domain.ExtInterface;
import com.ruoyi.business.domain.sqldesign.SqlDesignModel;

/**
 * 接口Service接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface IExtInterfaceService
{
    /**
     * 查询接口
     *
     * @param id 接口主键
     * @return 接口
     */
    public ExtInterface selectExtInterfaceById(Long id);

    /**
     * 查询接口列表
     *
     * @param extInterface 接口
     * @return 接口集合
     */
    public List<ExtInterface> selectExtInterfaceList(ExtInterface extInterface);

    /**
     * 新增接口
     *
     * @param extInterface 接口
     * @return 结果
     */
    public int insertExtInterface(ExtInterface extInterface);

    /**
     * 修改接口
     *
     * @param extInterface 接口
     * @return 结果
     */
    public int updateExtInterface(ExtInterface extInterface);

    /**
     * 批量删除接口
     *
     * @param ids 需要删除的接口主键集合
     * @return 结果
     */
    public int deleteExtInterfaceByIds(Long[] ids);

    /**
     * 删除接口信息
     *
     * @param id 接口主键
     * @return 结果
     */
    public int deleteExtInterfaceById(Long id);

    /**
     *  查询接口树列表
     * @param extInterface
     * @return
     */
    public  List<BusinessFunctionTree> treeList(ExtInterface extInterface);

    /**
     * 产生sql
     * @param sqlDesignModel
     * @return
     */
    String generateSql(SqlDesignModel sqlDesignModel) throws Exception;
}
