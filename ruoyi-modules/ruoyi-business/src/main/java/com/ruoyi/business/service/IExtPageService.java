package com.ruoyi.business.service;

import com.ruoyi.business.domain.BusinessFunctionTree;
import com.ruoyi.business.domain.ExtPage;

import java.util.List;

/**
 * 页面定义Service接口
 *
 * @author ruoyi
 * @date 2024-01-28
 */
public interface IExtPageService
{
    /**
     * 查询页面定义
     *
     * @param id 页面定义主键
     * @return 页面定义
     */
    public ExtPage selectExtPageById(Long id);

    /**
     * 查询页面定义列表
     *
     * @param extPage 页面定义
     * @return 页面定义集合
     */
    public List<ExtPage> selectExtPageList(ExtPage extPage);

    /**
     * 新增页面定义
     *
     * @param extPage 页面定义
     * @return 结果
     */
    public int insertExtPage(ExtPage extPage);

    /**
     * 修改页面定义
     *
     * @param extPage 页面定义
     * @return 结果
     */
    public int updateExtPage(ExtPage extPage);

    /**
     * 批量删除页面定义
     *
     * @param ids 需要删除的页面定义主键集合
     * @param businessCode 业务编码
     * @return 结果
     */
    public int deleteExtPageByIds(Long[] ids, String businessCode);

    /**
     * 删除页面定义信息
     *
     * @param id 页面定义主键
     * @return 结果
     */
    public int deleteExtPageById(Long id,String businessCode);

    /**
     * 更新页面设计
     * @param extPage
     * @return
     */
    public int updateDesign(ExtPage extPage);

    /**
     * 查询所有页面树列表
     *
     * @param extPage
     * @return
     */
    List<BusinessFunctionTree> treeList(ExtPage extPage);

    /**
     * 转换extPage列表为业务功能树节点
     * @param list
     * @return
     */
    List<BusinessFunctionTree> toBusinessFunctionTree(List<ExtPage> list);

    ExtPage getPageByCode(String pageCode, String enterpriseCode, String applicationCode);

    ExtPage getPageById(Long pageId, String enterpriseCode, String applicationCode);
}
