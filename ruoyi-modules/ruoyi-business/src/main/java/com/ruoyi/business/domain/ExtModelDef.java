package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

import javax.validation.constraints.NotBlank;

/**
 * 模型定义对象 ext_model_def
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public class ExtModelDef extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 中文名 */
    @Excel(name = "中文名")
    @NotBlank
    private String cnName;

    /** 英文名 */
    @Excel(name = "英文名")
    @NotBlank
    private String enName;

    /** 数据源名称 */
    @Excel(name = "数据源名称")
    @NotBlank
    private String datasourceName;

    /** 状态 */
    @Excel(name = "状态")
    private String status;
/** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;
    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

    /** 业务编码*/
    private String businessCode;
 /** 表类型{
	01=纯业务表,
     02=流程业务表
   } */
    @Excel(name = "表类型")
    private String tableType;
    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCnName(String cnName)
    {
        this.cnName = cnName;
    }

    public String getCnName()
    {
        return cnName;
    }
    public void setEnName(String enName)
    {
        this.enName = enName;
    }

    public String getEnName()
    {
        return enName;
    }
    public void setDatasourceName(String datasourceName)
    {
        this.datasourceName = datasourceName;
    }

    public String getDatasourceName()
    {
        return datasourceName;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setTableType(String tableType) 
    {
        this.tableType = tableType;
    }

    public String getTableType() 
    {
        return tableType;
    }
    public void setEnterpriseCode(String enterpriseCode) 
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode() 
    {
        return enterpriseCode;
    }
    public void setApplicationCode(String applicationCode) 
    {
        this.applicationCode = applicationCode;
    }

    public String getApplicationCode() 
    {
        return applicationCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("cnName", getCnName())
            .append("enName", getEnName())
            .append("datasourceName", getDatasourceName())
            .append("status", getStatus())
            .append("remark", getRemark())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("tableType", getTableType())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
