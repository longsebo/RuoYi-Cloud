package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.SysComponentGroup;
import com.ruoyi.business.service.ISysComponentGroupService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 系统级别分组，最基本的组。一般不会变化。除非系统版本升级Controller
 *
 * @author ruoyi
 * @date 2025-02-16
 */
@RestController
@RequestMapping("/sysComponentGroup")
public class SysComponentGroupController extends BaseController
{
    @Autowired
    private ISysComponentGroupService sysComponentGroupService;

    /**
     * 查询系统级别分组，最基本的组。一般不会变化。除非系统版本升级列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SysComponentGroup sysComponentGroup)
    {
        startPage();
        List<SysComponentGroup> list = sysComponentGroupService.selectSysComponentGroupList(sysComponentGroup);
        return getDataTable(list);
    }

    /**
     * 导出系统级别分组，最基本的组。一般不会变化。除非系统版本升级列表
     */
    @Log(title = "系统级别分组，最基本的组。一般不会变化。除非系统版本升级", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysComponentGroup sysComponentGroup)
    {
        List<SysComponentGroup> list = sysComponentGroupService.selectSysComponentGroupList(sysComponentGroup);
        ExcelUtil<SysComponentGroup> util = new ExcelUtil<SysComponentGroup>(SysComponentGroup.class);
        util.exportExcel(response, list, "系统级别分组，最基本的组。一般不会变化。除非系统版本升级数据");
    }

    /**
     * 获取系统级别分组，最基本的组。一般不会变化。除非系统版本升级详细信息
     */
    @GetMapping(value = "/{groupId}")
    public AjaxResult getInfo(@PathVariable("groupId") Long groupId)
    {
        return success(sysComponentGroupService.selectSysComponentGroupByGroupId(groupId));
    }

    /**
     * 新增系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     */
    @Log(title = "系统级别分组，最基本的组。一般不会变化。除非系统版本升级", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysComponentGroup sysComponentGroup)
    {
        return toAjax(sysComponentGroupService.insertSysComponentGroup(sysComponentGroup));
    }

    /**
     * 修改系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     */
    @Log(title = "系统级别分组，最基本的组。一般不会变化。除非系统版本升级", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysComponentGroup sysComponentGroup)
    {
        return toAjax(sysComponentGroupService.updateSysComponentGroup(sysComponentGroup));
    }

    /**
     * 删除系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     */
    @Log(title = "系统级别分组，最基本的组。一般不会变化。除非系统版本升级", businessType = BusinessType.DELETE)
	@DeleteMapping("/{groupIds}")
    public AjaxResult remove(@PathVariable Long[] groupIds)
    {
        return toAjax(sysComponentGroupService.deleteSysComponentGroupByGroupIds(groupIds));
    }
}
