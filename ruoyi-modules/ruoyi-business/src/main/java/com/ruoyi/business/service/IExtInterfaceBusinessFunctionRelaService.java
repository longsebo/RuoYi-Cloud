package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.domain.ExtInterfaceBusinessFunctionRela;

/**
 * 业务功能接口关系Service接口
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
public interface IExtInterfaceBusinessFunctionRelaService 
{
    /**
     * 查询业务功能接口关系
     * 
     * @param id 业务功能接口关系主键
     * @return 业务功能接口关系
     */
    public ExtInterfaceBusinessFunctionRela selectExtInterfaceBusinessFunctionRelaById(Long id);

    /**
     * 查询业务功能接口关系列表
     * 
     * @param extInterfaceBusinessFunctionRela 业务功能接口关系
     * @return 业务功能接口关系集合
     */
    public List<ExtInterfaceBusinessFunctionRela> selectExtInterfaceBusinessFunctionRelaList(ExtInterfaceBusinessFunctionRela extInterfaceBusinessFunctionRela);

    /**
     * 新增业务功能接口关系
     * 
     * @param extInterfaceBusinessFunctionRela 业务功能接口关系
     * @return 结果
     */
    public int insertExtInterfaceBusinessFunctionRela(ExtInterfaceBusinessFunctionRela extInterfaceBusinessFunctionRela);

    /**
     * 修改业务功能接口关系
     * 
     * @param extInterfaceBusinessFunctionRela 业务功能接口关系
     * @return 结果
     */
    public int updateExtInterfaceBusinessFunctionRela(ExtInterfaceBusinessFunctionRela extInterfaceBusinessFunctionRela);

    /**
     * 批量删除业务功能接口关系
     * 
     * @param ids 需要删除的业务功能接口关系主键集合
     * @return 结果
     */
    public int deleteExtInterfaceBusinessFunctionRelaByIds(Long[] ids);

    /**
     * 删除业务功能接口关系信息
     * 
     * @param id 业务功能接口关系主键
     * @return 结果
     */
    public int deleteExtInterfaceBusinessFunctionRelaById(Long id);
}
