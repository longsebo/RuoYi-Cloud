package com.ruoyi.business.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.ExtBusinessFunctionListening;

/**
 * 功能侦听Service接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface IExtBusinessFunctionListeningService
{
    /**
     * 查询功能侦听
     *
     * @param id 功能侦听主键
     * @return 功能侦听
     */
    public ExtBusinessFunctionListening selectExtBusinessFunctionListeningById(Long id);

    /**
     * 查询功能侦听列表
     *
     * @param extBusinessFunctionListening 功能侦听
     * @return 功能侦听集合
     */
    public List<ExtBusinessFunctionListening> selectExtBusinessFunctionListeningList(ExtBusinessFunctionListening extBusinessFunctionListening);

    /**
     * 新增功能侦听
     *
     * @param extBusinessFunctionListening 功能侦听
     * @return 结果
     */
    public int insertExtBusinessFunctionListening(ExtBusinessFunctionListening extBusinessFunctionListening);

    /**
     * 修改功能侦听
     *
     * @param extBusinessFunctionListening 功能侦听
     * @return 结果
     */
    public int updateExtBusinessFunctionListening(ExtBusinessFunctionListening extBusinessFunctionListening);

    /**
     * 批量删除功能侦听
     *
     * @param ids 需要删除的功能侦听主键集合
     * @return 结果
     */
    public int deleteExtBusinessFunctionListeningByIds(Long[] ids);

    /**
     * 删除功能侦听信息
     *
     * @param id 功能侦听主键
     * @return 结果
     */
    public int deleteExtBusinessFunctionListeningById(Long id);

    /**
     * 查询满足条件个数
     * @param map 条件map
     * @return 返回条数
     */
    public int selectCount(Map<String, Object> map);
}
