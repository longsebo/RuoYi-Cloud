package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.domain.ExtInterfaceBusinessFunctionRela;
import org.apache.ibatis.annotations.Param;

/**
 * 业务功能接口关系Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface ExtInterfaceBusinessFunctionRelaMapper
{
    /**
     * 查询业务功能接口关系
     *
     * @param id 业务功能接口关系主键
     * @return 业务功能接口关系
     */
    public ExtInterfaceBusinessFunctionRela selectExtInterfaceBusinessFunctionRelaById(Long id);

    /**
     * 查询业务功能接口关系列表
     *
     * @param extInterfaceBusinessFunctionRela 业务功能接口关系
     * @return 业务功能接口关系集合
     */
    public List<ExtInterfaceBusinessFunctionRela> selectExtInterfaceBusinessFunctionRelaList(ExtInterfaceBusinessFunctionRela extInterfaceBusinessFunctionRela);

    /**
     * 新增业务功能接口关系
     *
     * @param extInterfaceBusinessFunctionRela 业务功能接口关系
     * @return 结果
     */
    public int insertExtInterfaceBusinessFunctionRela(ExtInterfaceBusinessFunctionRela extInterfaceBusinessFunctionRela);

    /**
     * 修改业务功能接口关系
     *
     * @param extInterfaceBusinessFunctionRela 业务功能接口关系
     * @return 结果
     */
    public int updateExtInterfaceBusinessFunctionRela(ExtInterfaceBusinessFunctionRela extInterfaceBusinessFunctionRela);

    /**
     * 删除业务功能接口关系
     *
     * @param id 业务功能接口关系主键
     * @return 结果
     */
    public int deleteExtInterfaceBusinessFunctionRelaById(Long id);

    /**
     * 批量删除业务功能接口关系
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtInterfaceBusinessFunctionRelaByIds(Long[] ids);

    /**
     * 根据接口编码+业务编码删除记录
     * @param interfaceCode 接口编码
     * @param businessCode 业务编码
     * @return
     */
    int deleteByInterfaceCodeAndBusinessCode(@Param("interfaceCode") String interfaceCode, @Param("businessCode") String businessCode);
}
