package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.domain.ExtInterfaceReturnValue;

/**
 * 接口返回值Service接口
 *
 * @author ruoyi
 * @date 2024-01-22
 */
public interface IExtInterfaceReturnValueService
{
    /**
     * 查询接口返回值
     *
     * @param id 接口返回值主键
     * @return 接口返回值
     */
    public ExtInterfaceReturnValue selectExtInterfaceReturnValueById(Long id);

    /**
     * 查询接口返回值列表
     *
     * @param extInterfaceReturnValue 接口返回值
     * @return 接口返回值集合
     */
    public List<ExtInterfaceReturnValue> selectExtInterfaceReturnValueList(ExtInterfaceReturnValue extInterfaceReturnValue);

    /**
     * 新增接口返回值
     *
     * @param extInterfaceReturnValue 接口返回值
     * @return 结果
     */
    public int insertExtInterfaceReturnValue(ExtInterfaceReturnValue extInterfaceReturnValue);

    /**
     * 修改接口返回值
     *
     * @param extInterfaceReturnValue 接口返回值
     * @return 结果
     */
    public int updateExtInterfaceReturnValue(ExtInterfaceReturnValue extInterfaceReturnValue);

    /**
     * 批量删除接口返回值
     *
     * @param ids 需要删除的接口返回值主键集合
     * @return 结果
     */
    public int deleteExtInterfaceReturnValueByIds(Long[] ids);

    /**
     * 删除接口返回值信息
     *
     * @param id 接口返回值主键
     * @return 结果
     */
    public int deleteExtInterfaceReturnValueById(Long id);

    /**
     * 根据查询条件，构造树列表
     * @param extInterfaceReturnValue
     * @return
     */
    List<ExtInterfaceReturnValue> selectTree(ExtInterfaceReturnValue extInterfaceReturnValue);
}
