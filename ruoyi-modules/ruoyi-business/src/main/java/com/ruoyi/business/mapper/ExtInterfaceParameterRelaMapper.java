package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.domain.ExtInterfaceParameterRela;
import org.apache.ibatis.annotations.Param;

/**
 * 接口参数关系Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface ExtInterfaceParameterRelaMapper
{
    /**
     * 查询接口参数关系
     *
     * @param id 接口参数关系主键
     * @return 接口参数关系
     */
    public ExtInterfaceParameterRela selectExtInterfaceParameterRelaById(Long id);

    /**
     * 查询接口参数关系列表
     *
     * @param extInterfaceParameterRela 接口参数关系
     * @return 接口参数关系集合
     */
    public List<ExtInterfaceParameterRela> selectExtInterfaceParameterRelaList(ExtInterfaceParameterRela extInterfaceParameterRela);

    /**
     * 新增接口参数关系
     *
     * @param extInterfaceParameterRela 接口参数关系
     * @return 结果
     */
    public int insertExtInterfaceParameterRela(ExtInterfaceParameterRela extInterfaceParameterRela);

    /**
     * 修改接口参数关系
     *
     * @param extInterfaceParameterRela 接口参数关系
     * @return 结果
     */
    public int updateExtInterfaceParameterRela(ExtInterfaceParameterRela extInterfaceParameterRela);

    /**
     * 删除接口参数关系
     *
     * @param id 接口参数关系主键
     * @return 结果
     */
    public int deleteExtInterfaceParameterRelaById(Long id);

    /**
     * 批量删除接口参数关系
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtInterfaceParameterRelaByIds(Long[] ids);

    /**
     * 根据接口编码删除接口参数关系表
     * @param interfaceCode 接口编码
     * @return
     */
    public int  deleteByInterfaceCode(@Param("interfaceCode") String interfaceCode);

    /**
     * 根据参数id删除关系表
     * @param id
     * @return
     */
    public int deleteByParameterId(@Param("id") Long id);
}
