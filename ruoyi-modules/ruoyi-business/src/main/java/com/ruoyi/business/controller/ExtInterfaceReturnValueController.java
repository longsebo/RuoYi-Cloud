package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.business.domain.ExtInterfaceReturnValue;
import com.ruoyi.common.security.annotation.DefaultCreateProperty;
import com.ruoyi.common.security.annotation.DefaultUpdateProperty;
import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtInterfaceReturnValue;
import com.ruoyi.business.service.IExtInterfaceReturnValueService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 接口返回值Controller
 *
 * @author ruoyi
 * @date 2024-01-22
 */
@RestController
@RequestMapping("/value")
public class ExtInterfaceReturnValueController extends BaseController
{
    @Autowired
    private IExtInterfaceReturnValueService extInterfaceReturnValueService;

    /**
     * 查询接口返回值列表
     */
    @RequiresPermissions("business:value:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtInterfaceReturnValue extInterfaceReturnValue)
    {
        startPage();
        List<ExtInterfaceReturnValue> list = extInterfaceReturnValueService.selectExtInterfaceReturnValueList(extInterfaceReturnValue);
        return getDataTable(list);
    }

    /**
     * 导出接口返回值列表
     */
    @RequiresPermissions("business:value:export")
    @Log(title = "接口返回值", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtInterfaceReturnValue extInterfaceReturnValue)
    {
        List<ExtInterfaceReturnValue> list = extInterfaceReturnValueService.selectExtInterfaceReturnValueList(extInterfaceReturnValue);
        ExcelUtil<ExtInterfaceReturnValue> util = new ExcelUtil<ExtInterfaceReturnValue>(ExtInterfaceReturnValue.class);
        util.exportExcel(response, list, "接口返回值数据");
    }

    /**
     * 获取接口返回值详细信息
     */
    @RequiresPermissions("business:value:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extInterfaceReturnValueService.selectExtInterfaceReturnValueById(id));
    }

    /**
     * 新增接口返回值
     */
    @RequiresPermissions("business:value:add")
    @Log(title = "接口返回值", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@DefaultCreateProperty @EnterpriseAndApplicationCodeProperty ExtInterfaceReturnValue extInterfaceReturnValue)
    {
        return toAjax(extInterfaceReturnValueService.insertExtInterfaceReturnValue(extInterfaceReturnValue));
    }

    /**
     * 修改接口返回值
     */
    @RequiresPermissions("business:value:edit")
    @Log(title = "接口返回值", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@DefaultUpdateProperty @EnterpriseAndApplicationCodeProperty ExtInterfaceReturnValue extInterfaceReturnValue)
    {
        return toAjax(extInterfaceReturnValueService.updateExtInterfaceReturnValue(extInterfaceReturnValue));
    }

    /**
     * 删除接口返回值
     */
    @RequiresPermissions("business:value:remove")
    @Log(title = "接口返回值", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extInterfaceReturnValueService.deleteExtInterfaceReturnValueByIds(ids));
    }
    /**
     * 查询参数树
     * @return
     */
    @RequiresPermissions("business:value:tree")
    @PostMapping("/tree")
    public AjaxResult tree(@RequestBody ExtInterfaceReturnValue extInterfaceReturnValue)
    {
//        startPage();
        List<ExtInterfaceReturnValue> list = extInterfaceReturnValueService.selectTree(extInterfaceReturnValue);
        return AjaxResult.success(list);
    }
}
