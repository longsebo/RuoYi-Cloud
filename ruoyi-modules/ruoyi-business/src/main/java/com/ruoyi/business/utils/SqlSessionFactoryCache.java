package com.ruoyi.business.utils;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * sqlSessionFactory缓存
 */
@Component
public class SqlSessionFactoryCache {
    private Map<String,SqlSessionFactory> cacheMap = new HashMap<>();

    /**
     * 放入缓存
     * @param dataSourceName
     * @param sqlSessionFactory
     */
    public  void put(String dataSourceName, SqlSessionFactory sqlSessionFactory){
        synchronized(cacheMap) {
            cacheMap.put(dataSourceName, sqlSessionFactory);
        }
    }

    /**
     * 从缓存读取
     * @param dataSourceName
     * @return
     */
    public  SqlSessionFactory get(String dataSourceName){
        synchronized(cacheMap) {
            return cacheMap.get(dataSourceName);
        }
    }

    /**
     * 删除数据源
     * @param dataSourceName
     * @return
     */
    public  SqlSessionFactory remove(String dataSourceName){
        synchronized(cacheMap) {
            return cacheMap.remove(dataSourceName);
        }
    }
}
