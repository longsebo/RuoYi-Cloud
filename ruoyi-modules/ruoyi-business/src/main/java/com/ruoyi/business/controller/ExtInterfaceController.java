package com.ruoyi.business.controller;

import com.ruoyi.business.domain.BusinessFunctionTree;
import com.ruoyi.business.domain.ExtInterface;
import com.ruoyi.business.domain.sqldesign.SqlDesignModel;
import com.ruoyi.business.service.IExtInterfaceService;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.DefaultCreateProperty;
import com.ruoyi.common.security.annotation.DefaultUpdateProperty;
import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 接口Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@RestController
@RequestMapping("/interface")
public class ExtInterfaceController extends BaseController
{
    @Autowired
    private IExtInterfaceService extInterfaceService;

    /**
     * 查询接口列表
     */
    @RequiresPermissions("business:interface:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtInterface extInterface)
    {
        startPage();
        List<ExtInterface> list = extInterfaceService.selectExtInterfaceList(extInterface);
        return getDataTable(list);
    }

    /**
     * 导出接口列表
     */
    @RequiresPermissions("business:interface:export")
    @Log(title = "接口", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtInterface extInterface)
    {
        List<ExtInterface> list = extInterfaceService.selectExtInterfaceList(extInterface);
        ExcelUtil<ExtInterface> util = new ExcelUtil<ExtInterface>(ExtInterface.class);
        util.exportExcel(response, list, "接口数据");
    }

    /**
     * 获取接口详细信息
     */
    @RequiresPermissions("business:interface:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extInterfaceService.selectExtInterfaceById(id));
    }

    /**
     * 新增接口
     */
    @RequiresPermissions("business:interface:add")
    @Log(title = "接口", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@DefaultCreateProperty @EnterpriseAndApplicationCodeProperty ExtInterface extInterface)
    {
        return toAjax(extInterfaceService.insertExtInterface(extInterface));
    }

    /**
     * 修改接口
     */
    @RequiresPermissions("business:interface:edit")
    @Log(title = "接口", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@DefaultUpdateProperty @EnterpriseAndApplicationCodeProperty ExtInterface extInterface)
    {
        return toAjax(extInterfaceService.updateExtInterface(extInterface));
    }

    /**
     * 删除接口
     */
    @RequiresPermissions("business:interface:remove")
    @Log(title = "接口", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extInterfaceService.deleteExtInterfaceByIds(ids));
    }
    /**
     * 非翻页查询接口列表
     */
    @RequiresPermissions("business:interface:listAll")
    @GetMapping("/listAll")
    public AjaxResult listAll(ExtInterface extInterface)
    {
        List<ExtInterface> list = extInterfaceService.selectExtInterfaceList(extInterface);
        return success(list);
    }
    /**
     * 查询所有接口树列表
     */
    @RequiresPermissions("business:interface:tree")
    @GetMapping("/tree")
    public AjaxResult tree(ExtInterface extInterface)
    {
        List<BusinessFunctionTree> list = extInterfaceService.treeList(extInterface);
        return success(list);
    }
    /**
     * 产生sql
     */
    @RequiresPermissions("business:interface:generatesql")
    @PostMapping("/generatesql")
    public AjaxResult generatesql(@RequestBody SqlDesignModel sqlDesignModel)
    {
        try {
            return success((Object)extInterfaceService.generateSql(sqlDesignModel));
        }catch(Exception e){
            logger.error("产生sql失败!",e);
            return error(e.getMessage());
        }

    }
}
