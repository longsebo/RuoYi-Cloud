package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtModelOptionValue;
import com.ruoyi.business.service.IExtModelOptionValueService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 选项值Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@RestController
@RequestMapping("/optionValue")
public class ExtModelOptionValueController extends BaseController
{
    @Autowired
    private IExtModelOptionValueService extModelOptionValueService;

    /**
     * 查询选项值列表
     */
    @RequiresPermissions("business:value:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtModelOptionValue extModelOptionValue)
    {
        startPage();
        List<ExtModelOptionValue> list = extModelOptionValueService.selectExtModelOptionValueList(extModelOptionValue);
        return getDataTable(list);
    }

    /**
     * 导出选项值列表
     */
    @RequiresPermissions("business:value:export")
    @Log(title = "选项值", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtModelOptionValue extModelOptionValue)
    {
        List<ExtModelOptionValue> list = extModelOptionValueService.selectExtModelOptionValueList(extModelOptionValue);
        ExcelUtil<ExtModelOptionValue> util = new ExcelUtil<ExtModelOptionValue>(ExtModelOptionValue.class);
        util.exportExcel(response, list, "选项值数据");
    }

    /**
     * 获取选项值详细信息
     */
    @RequiresPermissions("business:value:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extModelOptionValueService.selectExtModelOptionValueById(id));
    }

    /**
     * 新增选项值
     */
    @RequiresPermissions("business:value:add")
    @Log(title = "选项值", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty ExtModelOptionValue extModelOptionValue)
    {
        return toAjax(extModelOptionValueService.insertExtModelOptionValue(extModelOptionValue));
    }

    /**
     * 修改选项值
     */
    @RequiresPermissions("business:value:edit")
    @Log(title = "选项值", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty ExtModelOptionValue extModelOptionValue)
    {
        return toAjax(extModelOptionValueService.updateExtModelOptionValue(extModelOptionValue));
    }

    /**
     * 删除选项值
     */
    @RequiresPermissions("business:value:remove")
    @Log(title = "选项值", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extModelOptionValueService.deleteExtModelOptionValueByIds(ids));
    }
}
