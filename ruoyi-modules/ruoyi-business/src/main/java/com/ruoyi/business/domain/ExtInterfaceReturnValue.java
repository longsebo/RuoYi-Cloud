package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.util.List;

/**
 * 接口返回值对象 ext_interface_return_value
 *
 * @author ruoyi
 * @date 2024-01-22
 */
public class ExtInterfaceReturnValue extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 返回值名称 */
    @Excel(name = "返回值名称")
    private String returnName;

    /** 返回值描述 */
    @Excel(name = "返回值描述")
    private String returnDesc;

    /** 前端是否可见 */
    @Excel(name = "前端是否可见")
    private String isFrontpageVisible;

    /** 返回值类型 */
    @Excel(name = "返回值类型")
    private String returnType;

    /** 返回值格式 */
    @Excel(name = "返回值格式")
    private String returnFormat;

    /** 父参数id */
    @Excel(name = "父参数id")
    private Long parentId;
    /**
     * 接口编码
     */
    private String interfaceCode;
    /**
     * 下级列表
     */
    private List<ExtInterfaceReturnValue> children;

    public String getInterfaceCode() {
        return interfaceCode;
    }

    public void setInterfaceCode(String interfaceCode) {
        this.interfaceCode = interfaceCode;
    }

    public List<ExtInterfaceReturnValue> getChildren() {
        return children;
    }

    public void setChildren(List<ExtInterfaceReturnValue> children) {
        this.children = children;
    }


    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setReturnName(String returnName)
    {
        this.returnName = returnName;
    }

    public String getReturnName()
    {
        return returnName;
    }
    public void setReturnDesc(String returnDesc)
    {
        this.returnDesc = returnDesc;
    }

    public String getReturnDesc()
    {
        return returnDesc;
    }
    public void setIsFrontpageVisible(String isFrontpageVisible)
    {
        this.isFrontpageVisible = isFrontpageVisible;
    }

    public String getIsFrontpageVisible()
    {
        return isFrontpageVisible;
    }
    public void setReturnType(String returnType)
    {
        this.returnType = returnType;
    }

    public String getReturnType()
    {
        return returnType;
    }
    public void setReturnFormat(String returnFormat)
    {
        this.returnFormat = returnFormat;
    }

    public String getReturnFormat()
    {
        return returnFormat;
    }
    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    public Long getParentId()
    {
        return parentId;
    }
    public void setEnterpriseCode(String enterpriseCode) 
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode() 
    {
        return enterpriseCode;
    }
    public void setApplicationCode(String applicationCode) 
    {
        this.applicationCode = applicationCode;
    }

    public String getApplicationCode() 
    {
        return applicationCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("returnName", getReturnName())
            .append("returnDesc", getReturnDesc())
            .append("isFrontpageVisible", getIsFrontpageVisible())
            .append("returnType", getReturnType())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("returnFormat", getReturnFormat())
            .append("parentId", getParentId())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
