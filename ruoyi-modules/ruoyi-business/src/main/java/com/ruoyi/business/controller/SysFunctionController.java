package com.ruoyi.business.controller;

import com.ruoyi.business.domain.SysFunction;
import com.ruoyi.business.service.ISysFunctionService;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 对外宣传为功能点，即开发人员的函数Controller
 *
 * @author ruoyi
 * @date 2025-02-16
 */
@RestController
@RequestMapping("/sysFunction")
public class SysFunctionController extends BaseController
{
    @Autowired
    private ISysFunctionService sysFunctionService;

    /**
     * 查询对外宣传为功能点，即开发人员的函数列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SysFunction sysFunction)
    {
        startPage();
        List<SysFunction> list = sysFunctionService.selectSysFunctionList(sysFunction);
        return getDataTable(list);
    }

    /**
     * 导出对外宣传为功能点，即开发人员的函数列表
     */
    @Log(title = "对外宣传为功能点，即开发人员的函数", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysFunction sysFunction)
    {
        List<SysFunction> list = sysFunctionService.selectSysFunctionList(sysFunction);
        ExcelUtil<SysFunction> util = new ExcelUtil<SysFunction>(SysFunction.class);
        util.exportExcel(response, list, "对外宣传为功能点，即开发人员的函数数据");
    }

    /**
     * 获取对外宣传为功能点，即开发人员的函数详细信息
     */
    @GetMapping(value = "/{functionId}")
    public AjaxResult getInfo(@PathVariable("functionId") Long functionId)
    {
        return success(sysFunctionService.selectSysFunctionByFunctionId(functionId));
    }

    /**
     * 新增对外宣传为功能点，即开发人员的函数
     */
    @Log(title = "对外宣传为功能点，即开发人员的函数", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty SysFunction sysFunction)
    {
        return toAjax(sysFunctionService.insertSysFunction(sysFunction));
    }

    /**
     * 修改对外宣传为功能点，即开发人员的函数
     */
    @Log(title = "对外宣传为功能点，即开发人员的函数", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty SysFunction sysFunction)
    {
        return toAjax(sysFunctionService.updateSysFunction(sysFunction));
    }

    /**
     * 删除对外宣传为功能点，即开发人员的函数
     */
    @Log(title = "对外宣传为功能点，即开发人员的函数", businessType = BusinessType.DELETE)
	@DeleteMapping("/{functionIds}")
    public AjaxResult remove(@PathVariable Long[] functionIds)
    {
        return toAjax(sysFunctionService.deleteSysFunctionByFunctionIds(functionIds));
    }
}
