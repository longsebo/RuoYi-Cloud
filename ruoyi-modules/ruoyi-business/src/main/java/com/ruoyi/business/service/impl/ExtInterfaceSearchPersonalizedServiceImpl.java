package com.ruoyi.business.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.ExtInterfaceSearchPersonalizedMapper;
import com.ruoyi.business.domain.ExtInterfaceSearchPersonalized;
import com.ruoyi.business.service.IExtInterfaceSearchPersonalizedService;

/**
 * 查询接口个性化Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
@Service
public class ExtInterfaceSearchPersonalizedServiceImpl implements IExtInterfaceSearchPersonalizedService 
{
    @Autowired
    private ExtInterfaceSearchPersonalizedMapper extInterfaceSearchPersonalizedMapper;

    /**
     * 查询查询接口个性化
     * 
     * @param id 查询接口个性化主键
     * @return 查询接口个性化
     */
    @Override
    public ExtInterfaceSearchPersonalized selectExtInterfaceSearchPersonalizedById(Long id)
    {
        return extInterfaceSearchPersonalizedMapper.selectExtInterfaceSearchPersonalizedById(id);
    }

    /**
     * 查询查询接口个性化列表
     * 
     * @param extInterfaceSearchPersonalized 查询接口个性化
     * @return 查询接口个性化
     */
    @Override
    public List<ExtInterfaceSearchPersonalized> selectExtInterfaceSearchPersonalizedList(ExtInterfaceSearchPersonalized extInterfaceSearchPersonalized)
    {
        return extInterfaceSearchPersonalizedMapper.selectExtInterfaceSearchPersonalizedList(extInterfaceSearchPersonalized);
    }

    /**
     * 新增查询接口个性化
     * 
     * @param extInterfaceSearchPersonalized 查询接口个性化
     * @return 结果
     */
    @Override
    public int insertExtInterfaceSearchPersonalized(ExtInterfaceSearchPersonalized extInterfaceSearchPersonalized)
    {
        extInterfaceSearchPersonalized.setCreateTime(DateUtils.getNowDate());
        return extInterfaceSearchPersonalizedMapper.insertExtInterfaceSearchPersonalized(extInterfaceSearchPersonalized);
    }

    /**
     * 修改查询接口个性化
     * 
     * @param extInterfaceSearchPersonalized 查询接口个性化
     * @return 结果
     */
    @Override
    public int updateExtInterfaceSearchPersonalized(ExtInterfaceSearchPersonalized extInterfaceSearchPersonalized)
    {
        extInterfaceSearchPersonalized.setUpdateTime(DateUtils.getNowDate());
        return extInterfaceSearchPersonalizedMapper.updateExtInterfaceSearchPersonalized(extInterfaceSearchPersonalized);
    }

    /**
     * 批量删除查询接口个性化
     * 
     * @param ids 需要删除的查询接口个性化主键
     * @return 结果
     */
    @Override
    public int deleteExtInterfaceSearchPersonalizedByIds(Long[] ids)
    {
        return extInterfaceSearchPersonalizedMapper.deleteExtInterfaceSearchPersonalizedByIds(ids);
    }

    /**
     * 删除查询接口个性化信息
     * 
     * @param id 查询接口个性化主键
     * @return 结果
     */
    @Override
    public int deleteExtInterfaceSearchPersonalizedById(Long id)
    {
        return extInterfaceSearchPersonalizedMapper.deleteExtInterfaceSearchPersonalizedById(id);
    }
}
