package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.domain.SysComponent;

/**
 * 系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件Service接口
 * 
 * @author ruoyi
 * @date 2025-02-16
 */
public interface ISysComponentService 
{
    /**
     * 查询系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     * 
     * @param componentId 系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件主键
     * @return 系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     */
    public SysComponent selectSysComponentByComponentId(Long componentId);

    /**
     * 查询系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件列表
     * 
     * @param sysComponent 系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     * @return 系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件集合
     */
    public List<SysComponent> selectSysComponentList(SysComponent sysComponent);

    /**
     * 新增系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     * 
     * @param sysComponent 系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     * @return 结果
     */
    public int insertSysComponent(SysComponent sysComponent);

    /**
     * 修改系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     * 
     * @param sysComponent 系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     * @return 结果
     */
    public int updateSysComponent(SysComponent sysComponent);

    /**
     * 批量删除系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     * 
     * @param componentIds 需要删除的系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件主键集合
     * @return 结果
     */
    public int deleteSysComponentByComponentIds(Long[] componentIds);

    /**
     * 删除系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件信息
     * 
     * @param componentId 系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件主键
     * @return 结果
     */
    public int deleteSysComponentByComponentId(Long componentId);
}
