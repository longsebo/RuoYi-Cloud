package com.ruoyi.business.domain.sqldesign;

import java.util.List;

/**
 * sql设计模型
 */
public class SqlDesignModel {
    /**
     * 选择列模型
     */
    private List<SelectColumnItemModel> selectColumnTabModel;
    /**
     * 是否排重
     */
    private boolean distinct;
    /**
     * 条件列树模型
     */
    private List<ConditionTreeModel> conditionTreeModel;
    /**
     * 分组列树模型
     */
    private AbstractConditionTreeModel groupConditionTreeModel;
    /**
     * 表模型列表
     */
    private List<TableModel> tablesModel;
    /**
     * 排序列模型 sortColumnModel
     */
    private List<SortColumnItemModel> sortColumnModel;
    /**
     * 表关系模型 tableJoinModels
     */
    private List<DbTableJoinModel> tableJoinModels;

    public List<SelectColumnItemModel> getSelectColumnTabModel() {
        return selectColumnTabModel;
    }

    public void setSelectColumnTabModel(List<SelectColumnItemModel> selectColumnTabModel) {
        this.selectColumnTabModel = selectColumnTabModel;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public List<ConditionTreeModel> getConditionTreeModel() {
        return conditionTreeModel;
    }

    public void setConditionTreeModel(List<ConditionTreeModel> conditionTreeModel) {
        this.conditionTreeModel = conditionTreeModel;
    }

    public AbstractConditionTreeModel getGroupConditionTreeModel() {
        return groupConditionTreeModel;
    }

    public void setGroupConditionTreeModel(AbstractConditionTreeModel groupConditionTreeModel) {
        this.groupConditionTreeModel = groupConditionTreeModel;
    }

    public List<TableModel> getTablesModel() {
        return tablesModel;
    }

    public void setTablesModel(List<TableModel> tablesModel) {
        this.tablesModel = tablesModel;
    }

    public List<SortColumnItemModel> getSortColumnModel() {
        return sortColumnModel;
    }

    public void setSortColumnModel(List<SortColumnItemModel> sortColumnModel) {
        this.sortColumnModel = sortColumnModel;
    }

    public List<DbTableJoinModel> getTableJoinModels() {
        return tableJoinModels;
    }

    public void setTableJoinModels(List<DbTableJoinModel> tableJoinModels) {
        this.tableJoinModels = tableJoinModels;
    }
}
