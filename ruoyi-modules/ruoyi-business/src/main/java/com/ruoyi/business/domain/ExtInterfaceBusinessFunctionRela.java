package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 业务功能接口关系对象 ext_interface_business_function_rela
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
public class ExtInterfaceBusinessFunctionRela extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 接口编码 */
    @Excel(name = "接口编码")
    private String interfaceCode;

    /** 业务编码 */
    @Excel(name = "业务编码")
    private String businessCode;

    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setInterfaceCode(String interfaceCode) 
    {
        this.interfaceCode = interfaceCode;
    }

    public String getInterfaceCode() 
    {
        return interfaceCode;
    }
    public void setBusinessCode(String businessCode) 
    {
        this.businessCode = businessCode;
    }

    public String getBusinessCode() 
    {
        return businessCode;
    }
    public void setEnterpriseCode(String enterpriseCode) 
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode() 
    {
        return enterpriseCode;
    }
    public void setApplicationCode(String applicationCode) 
    {
        this.applicationCode = applicationCode;
    }

    public String getApplicationCode() 
    {
        return applicationCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("interfaceCode", getInterfaceCode())
            .append("businessCode", getBusinessCode())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
