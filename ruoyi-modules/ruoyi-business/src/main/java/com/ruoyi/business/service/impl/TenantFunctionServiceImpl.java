package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.TenantFunction;
import com.ruoyi.business.mapper.TenantFunctionMapper;
import com.ruoyi.business.service.ITenantFunctionService;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.UniqueIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 对外宣传为功能点，即开发人员的函数Service业务层处理
 *
 * @author ruoyi
 * @date 2025-02-16
 */
@Service
public class TenantFunctionServiceImpl implements ITenantFunctionService
{
    @Autowired
    private TenantFunctionMapper tenantFunctionMapper;

    /**
     * 查询对外宣传为功能点，即开发人员的函数
     *
     * @param functionId 对外宣传为功能点，即开发人员的函数主键
     * @return 对外宣传为功能点，即开发人员的函数
     */
    @Override
    public TenantFunction selectTenantFunctionByFunctionId(Long functionId)
    {
        return tenantFunctionMapper.selectTenantFunctionByFunctionId(functionId);
    }

    /**
     * 查询对外宣传为功能点，即开发人员的函数列表
     *
     * @param tenantFunction 对外宣传为功能点，即开发人员的函数
     * @return 对外宣传为功能点，即开发人员的函数
     */
    @Override
    public List<TenantFunction> selectTenantFunctionList(TenantFunction tenantFunction)
    {
        return tenantFunctionMapper.selectTenantFunctionList(tenantFunction);
    }

    /**
     * 新增对外宣传为功能点，即开发人员的函数
     *
     * @param tenantFunction 对外宣传为功能点，即开发人员的函数
     * @return 结果
     */
    @Override
    public int insertTenantFunction(TenantFunction tenantFunction)
    {
        tenantFunction.setCreateTime(DateUtils.getNowDate());
        //自动产生组件功能编码，并保证唯一
        String functionCode;
        TenantFunction searchTenantFunction = new TenantFunction();
        while (true) {
            functionCode = UniqueIdGenerator.getUniqueClassName(10);
            searchTenantFunction.setFunctionCode(functionCode);
            List<TenantFunction> list = tenantFunctionMapper.selectTenantFunctionList(searchTenantFunction);
            if (list.size() == 0) {
                tenantFunction.setFunctionCode(functionCode);
                break;
            }
        }
        return tenantFunctionMapper.insertTenantFunction(tenantFunction);
    }

    /**
     * 修改对外宣传为功能点，即开发人员的函数
     *
     * @param tenantFunction 对外宣传为功能点，即开发人员的函数
     * @return 结果
     */
    @Override
    public int updateTenantFunction(TenantFunction tenantFunction)
    {
        tenantFunction.setUpdateTime(DateUtils.getNowDate());
        return tenantFunctionMapper.updateTenantFunction(tenantFunction);
    }

    /**
     * 批量删除对外宣传为功能点，即开发人员的函数
     *
     * @param functionIds 需要删除的对外宣传为功能点，即开发人员的函数主键
     * @return 结果
     */
    @Override
    public int deleteTenantFunctionByFunctionIds(Long[] functionIds)
    {
        return tenantFunctionMapper.deleteTenantFunctionByFunctionIds(functionIds);
    }

    /**
     * 删除对外宣传为功能点，即开发人员的函数信息
     *
     * @param functionId 对外宣传为功能点，即开发人员的函数主键
     * @return 结果
     */
    @Override
    public int deleteTenantFunctionByFunctionId(Long functionId)
    {
        return tenantFunctionMapper.deleteTenantFunctionByFunctionId(functionId);
    }
}
