package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.domain.SysFunction;

/**
 * 对外宣传为功能点，即开发人员的函数Mapper接口
 * 
 * @author ruoyi
 * @date 2025-02-16
 */
public interface SysFunctionMapper 
{
    /**
     * 查询对外宣传为功能点，即开发人员的函数
     * 
     * @param functionId 对外宣传为功能点，即开发人员的函数主键
     * @return 对外宣传为功能点，即开发人员的函数
     */
    public SysFunction selectSysFunctionByFunctionId(Long functionId);

    /**
     * 查询对外宣传为功能点，即开发人员的函数列表
     * 
     * @param sysFunction 对外宣传为功能点，即开发人员的函数
     * @return 对外宣传为功能点，即开发人员的函数集合
     */
    public List<SysFunction> selectSysFunctionList(SysFunction sysFunction);

    /**
     * 新增对外宣传为功能点，即开发人员的函数
     * 
     * @param sysFunction 对外宣传为功能点，即开发人员的函数
     * @return 结果
     */
    public int insertSysFunction(SysFunction sysFunction);

    /**
     * 修改对外宣传为功能点，即开发人员的函数
     * 
     * @param sysFunction 对外宣传为功能点，即开发人员的函数
     * @return 结果
     */
    public int updateSysFunction(SysFunction sysFunction);

    /**
     * 删除对外宣传为功能点，即开发人员的函数
     * 
     * @param functionId 对外宣传为功能点，即开发人员的函数主键
     * @return 结果
     */
    public int deleteSysFunctionByFunctionId(Long functionId);

    /**
     * 批量删除对外宣传为功能点，即开发人员的函数
     * 
     * @param functionIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysFunctionByFunctionIds(Long[] functionIds);
}
