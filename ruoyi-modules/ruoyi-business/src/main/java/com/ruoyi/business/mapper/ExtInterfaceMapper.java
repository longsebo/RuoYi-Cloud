package com.ruoyi.business.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.ExtInterface;

/**
 * 接口Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface ExtInterfaceMapper
{
    /**
     * 查询接口
     *
     * @param id 接口主键
     * @return 接口
     */
    public ExtInterface selectExtInterfaceById(Long id);

    /**
     * 查询接口列表
     *
     * @param extInterface 接口
     * @return 接口集合
     */
    public List<ExtInterface> selectExtInterfaceList(ExtInterface extInterface);

    /**
     * 新增接口
     *
     * @param extInterface 接口
     * @return 结果
     */
    public int insertExtInterface(ExtInterface extInterface);

    /**
     * 修改接口
     *
     * @param extInterface 接口
     * @return 结果
     */
    public int updateExtInterface(ExtInterface extInterface);

    /**
     * 删除接口
     *
     * @param id 接口主键
     * @return 结果
     */
    public int deleteExtInterfaceById(Long id);

    /**
     * 批量删除接口
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtInterfaceByIds(Long[] ids);

    /**
     * 查询满足条件数量
     * @param searchMap 查询条件
     * @return 返回满足条件数量
     */
    public int selectCount(Map<String, Object> searchMap);

    /**
     *  关联业务功能树查询满足条件的接口列表
     * @param extInterface
     * @return
     */
    public List<ExtInterface> selectInterfaceListAssignBusinessFunction(ExtInterface extInterface);
}
