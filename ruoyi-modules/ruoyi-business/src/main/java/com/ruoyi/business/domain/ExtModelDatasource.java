package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

import javax.validation.constraints.NotBlank;

/**
 * 数据源定义对象 ext_model_datasource
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public class ExtModelDatasource extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 数据源名称 */
    @Excel(name = "数据源名称")
    @NotBlank
    private String datasourceName;

    /** 驱动类 */
    @Excel(name = "驱动类")
    @NotBlank
    private String driverClass;

    /** URL */
    @Excel(name = "URL")
    @NotBlank
    private String url;

    /** 用户名 */
    @Excel(name = "用户名")
    private String userName;

    /** 密码 */
//    @Excel(name = "密码")
    private String password;
    /** 数据库类型 */
    @Excel(name = "数据库类型")
    private String dbType;
    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;
    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setDatasourceName(String datasourceName)
    {
        this.datasourceName = datasourceName;
    }

    public String getDatasourceName()
    {
        return datasourceName;
    }
    public void setDriverClass(String driverClass)
    {
        this.driverClass = driverClass;
    }

    public String getDriverClass()
    {
        return driverClass;
    }
    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getUrl()
    {
        return url;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassword()
    {
        return password;
    }
    public void setDbType(String dbType)
    {
        this.dbType = dbType;
    }

    public String getDbType()
    {
        return dbType;
    }
    public void setEnterpriseCode(String enterpriseCode) 
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode() 
    {
        return enterpriseCode;
    }
    public void setApplicationCode(String applicationCode) 
    {
        this.applicationCode = applicationCode;
    }

    public String getApplicationCode() 
    {
        return applicationCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("datasourceName", getDatasourceName())
            .append("driverClass", getDriverClass())
            .append("url", getUrl())
            .append("userName", getUserName())
            .append("password", getPassword())
            .append("remark", getRemark())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("dbType", getDbType())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
