package com.ruoyi.business.domain.sqldesign;

import java.awt.Point;
import java.awt.Rectangle;

/**
 *
 * @author Administrator
 * 数据库列关联模型
 */
public class DbColumnJoinModel {
	/**
	 * 关联操作符
	 */
	private String operatorSign;
	/**
	 * 开始列名
	 */
	private String startColumnName;
	/**
	 * 结束列名
	 */
	private String endColumnName;
	/**
	 * 开始点
	 */
	private Point startPt;
	/**
	 * 结束点
	 */
	private Point endPt;
	/**
	 * 开始索引
	 */
	private int startIndex;
	/**
	 * 结束索引
	 */
	private int endIndex;
	/**
	 * 	模型框：画表连接图标及操作符
	 */
	private Rectangle modelRect;
	public String getOperatorSign() {
		return operatorSign;
	}
	public void setOperatorSign(String operatorSign) {
		this.operatorSign = operatorSign;
	}
	public String getStartColumnName() {
		return startColumnName;
	}
	public void setStartColumnName(String startColumnName) {
		this.startColumnName = startColumnName;
	}
	public String getEndColumnName() {
		return endColumnName;
	}
	public void setEndColumnName(String endColumnName) {
		this.endColumnName = endColumnName;
	}
	public Point getStartPt() {
		return startPt;
	}
	public void setStartPt(Point startPt) {
		this.startPt = startPt;
	}
	public Point getEndPt() {
		return endPt;
	}
	public void setEndPt(Point endPt) {
		this.endPt = endPt;
	}
	public int getStartIndex() {
		return startIndex;
	}
	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}
	public int getEndIndex() {
		return endIndex;
	}
	public void setEndIndex(int endIndex) {
		this.endIndex = endIndex;
	}
	public Rectangle getModelRect() {
		return modelRect;
	}
	public void setModelRect(Rectangle modelRect) {
		this.modelRect = modelRect;
	}
}
