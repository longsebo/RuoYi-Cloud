package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 对外宣传为功能点，即开发人员的函数对象 tenant_function
 * 
 * @author ruoyi
 * @date 2025-02-16
 */
public class TenantFunction extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long functionId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String enterpriseCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String className;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String functionCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String functionName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String functionDesc;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String groovySource;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String designScript;

    /** 这个字段意思是所有依赖的源码都在这里。比如A->B 则B的类及方法在这里，加上A实现也在 */
    @Excel(name = "这个字段意思是所有依赖的源码都在这里。比如A->B 则B的类及方法在这里，加上A实现也在")
    private String fullGroovySource;

    /** 启用禁用状态：00--禁用 01--启用 */
    @Excel(name = "启用禁用状态：00--禁用 01--启用")
    private String status;

    /** 是否有返回值：N---否，Y---是 */
    @Excel(name = "是否有返回值：N---否，Y---是")
    private String isReturnvalue;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String returnType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sortNumber;

    public void setFunctionId(Long functionId) 
    {
        this.functionId = functionId;
    }

    public Long getFunctionId() 
    {
        return functionId;
    }
    public void setEnterpriseCode(String enterpriseCode) 
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode() 
    {
        return enterpriseCode;
    }
    public void setClassName(String className) 
    {
        this.className = className;
    }

    public String getClassName() 
    {
        return className;
    }
    public void setFunctionCode(String functionCode) 
    {
        this.functionCode = functionCode;
    }

    public String getFunctionCode() 
    {
        return functionCode;
    }
    public void setFunctionName(String functionName) 
    {
        this.functionName = functionName;
    }

    public String getFunctionName() 
    {
        return functionName;
    }
    public void setFunctionDesc(String functionDesc) 
    {
        this.functionDesc = functionDesc;
    }

    public String getFunctionDesc() 
    {
        return functionDesc;
    }
    public void setGroovySource(String groovySource) 
    {
        this.groovySource = groovySource;
    }

    public String getGroovySource() 
    {
        return groovySource;
    }
    public void setDesignScript(String designScript) 
    {
        this.designScript = designScript;
    }

    public String getDesignScript() 
    {
        return designScript;
    }
    public void setFullGroovySource(String fullGroovySource) 
    {
        this.fullGroovySource = fullGroovySource;
    }

    public String getFullGroovySource() 
    {
        return fullGroovySource;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setIsReturnvalue(String isReturnvalue) 
    {
        this.isReturnvalue = isReturnvalue;
    }

    public String getIsReturnvalue() 
    {
        return isReturnvalue;
    }
    public void setReturnType(String returnType) 
    {
        this.returnType = returnType;
    }

    public String getReturnType() 
    {
        return returnType;
    }
    public void setSortNumber(Long sortNumber) 
    {
        this.sortNumber = sortNumber;
    }

    public Long getSortNumber() 
    {
        return sortNumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("functionId", getFunctionId())
            .append("enterpriseCode", getEnterpriseCode())
            .append("className", getClassName())
            .append("functionCode", getFunctionCode())
            .append("functionName", getFunctionName())
            .append("functionDesc", getFunctionDesc())
            .append("groovySource", getGroovySource())
            .append("designScript", getDesignScript())
            .append("fullGroovySource", getFullGroovySource())
            .append("status", getStatus())
            .append("isReturnvalue", getIsReturnvalue())
            .append("returnType", getReturnType())
            .append("sortNumber", getSortNumber())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
