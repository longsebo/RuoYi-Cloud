package com.ruoyi.business.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.SysFunctionMapper;
import com.ruoyi.business.domain.SysFunction;
import com.ruoyi.business.service.ISysFunctionService;

/**
 * 对外宣传为功能点，即开发人员的函数Service业务层处理
 * 
 * @author ruoyi
 * @date 2025-02-16
 */
@Service
public class SysFunctionServiceImpl implements ISysFunctionService 
{
    @Autowired
    private SysFunctionMapper sysFunctionMapper;

    /**
     * 查询对外宣传为功能点，即开发人员的函数
     * 
     * @param functionId 对外宣传为功能点，即开发人员的函数主键
     * @return 对外宣传为功能点，即开发人员的函数
     */
    @Override
    public SysFunction selectSysFunctionByFunctionId(Long functionId)
    {
        return sysFunctionMapper.selectSysFunctionByFunctionId(functionId);
    }

    /**
     * 查询对外宣传为功能点，即开发人员的函数列表
     * 
     * @param sysFunction 对外宣传为功能点，即开发人员的函数
     * @return 对外宣传为功能点，即开发人员的函数
     */
    @Override
    public List<SysFunction> selectSysFunctionList(SysFunction sysFunction)
    {
        return sysFunctionMapper.selectSysFunctionList(sysFunction);
    }

    /**
     * 新增对外宣传为功能点，即开发人员的函数
     * 
     * @param sysFunction 对外宣传为功能点，即开发人员的函数
     * @return 结果
     */
    @Override
    public int insertSysFunction(SysFunction sysFunction)
    {
        sysFunction.setCreateTime(DateUtils.getNowDate());
        return sysFunctionMapper.insertSysFunction(sysFunction);
    }

    /**
     * 修改对外宣传为功能点，即开发人员的函数
     * 
     * @param sysFunction 对外宣传为功能点，即开发人员的函数
     * @return 结果
     */
    @Override
    public int updateSysFunction(SysFunction sysFunction)
    {
        sysFunction.setUpdateTime(DateUtils.getNowDate());
        return sysFunctionMapper.updateSysFunction(sysFunction);
    }

    /**
     * 批量删除对外宣传为功能点，即开发人员的函数
     * 
     * @param functionIds 需要删除的对外宣传为功能点，即开发人员的函数主键
     * @return 结果
     */
    @Override
    public int deleteSysFunctionByFunctionIds(Long[] functionIds)
    {
        return sysFunctionMapper.deleteSysFunctionByFunctionIds(functionIds);
    }

    /**
     * 删除对外宣传为功能点，即开发人员的函数信息
     * 
     * @param functionId 对外宣传为功能点，即开发人员的函数主键
     * @return 结果
     */
    @Override
    public int deleteSysFunctionByFunctionId(Long functionId)
    {
        return sysFunctionMapper.deleteSysFunctionByFunctionId(functionId);
    }
}
