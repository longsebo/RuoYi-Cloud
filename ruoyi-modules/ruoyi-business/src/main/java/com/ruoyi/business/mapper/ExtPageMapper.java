package com.ruoyi.business.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.ExtPage;

/**
 * 页面定义Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-28
 */
public interface ExtPageMapper
{
    /**
     * 查询页面定义
     *
     * @param id 页面定义主键
     * @return 页面定义
     */
    public ExtPage selectExtPageById(Long id);

    /**
     * 查询页面定义列表
     *
     * @param extPage 页面定义
     * @return 页面定义集合
     */
    public List<ExtPage> selectExtPageList(ExtPage extPage);

    /**
     * 新增页面定义
     *
     * @param extPage 页面定义
     * @return 结果
     */
    public int insertExtPage(ExtPage extPage);

    /**
     * 修改页面定义
     *
     * @param extPage 页面定义
     * @return 结果
     */
    public int updateExtPage(ExtPage extPage);

    /**
     * 删除页面定义
     *
     * @param id 页面定义主键
     * @param businessCode
     * @return 结果
     */
    public int deleteExtPageById(Long id);

    /**
     * 批量删除页面定义
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtPageByIds(Long[] ids);

    /**
     * 查询满足条件记录数
     * @param searchMap
     * @return
     */
    public int  selectCount(Map<String, Object> searchMap);

    /**
     * 关键功能节点查询满足条件的页面列表
     * @param extPage
     * @return
     */
    public List<ExtPage> selectListAssignBusinessFunction(ExtPage extPage);
}
