package com.ruoyi.business.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.BusinessFunctionTree;
import com.ruoyi.business.domain.ExtBusinessFunction;

/**
 * 业务功能Service接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface IExtBusinessFunctionService
{
    /**
     * 查询业务功能
     *
     * @param id 业务功能主键
     * @return 业务功能
     */
    public ExtBusinessFunction selectExtBusinessFunctionById(Long id);

    /**
     * 查询业务功能列表
     *
     * @param extBusinessFunction 业务功能
     * @return 业务功能集合
     */
    public List<ExtBusinessFunction> selectExtBusinessFunctionList(ExtBusinessFunction extBusinessFunction);

    /**
     * 新增业务功能
     *
     * @param extBusinessFunction 业务功能
     * @return 结果
     */
    public int insertExtBusinessFunction(ExtBusinessFunction extBusinessFunction);

    /**
     * 修改业务功能
     *
     * @param extBusinessFunction 业务功能
     * @return 结果
     */
    public int updateExtBusinessFunction(ExtBusinessFunction extBusinessFunction);

    /**
     * 批量删除业务功能
     *
     * @param ids 需要删除的业务功能主键集合
     * @return 结果
     */
    public int deleteExtBusinessFunctionByIds(Long[] ids);

    /**
     * 删除业务功能信息
     *
     * @param id 业务功能主键
     * @return 结果
     */
    public int deleteExtBusinessFunctionById(Long id);

    /**
     * 查询业务功能树
     * @param extBusinessFunction
     * @return
     */
    public List<ExtBusinessFunction> selectExtBusinessFunctionTree(ExtBusinessFunction extBusinessFunction);
    /**
     * 根据条件查询满足条数
     * @param searchMap 查询条件
     * @return 条数
     */
    public int searchCount(Map<String, Object> searchMap);

    /**
     * 判断业务编码是否为末级
     * @param extBusinessFunction
     * @return
     */
    public boolean isLastLevel(ExtBusinessFunction extBusinessFunction);
    /**
     * 查询业务功能树
     * @param extBusinessFunction
     * @return
     */
    public List<BusinessFunctionTree> selectCommonBusinessFunctionTree(ExtBusinessFunction extBusinessFunction);
}
