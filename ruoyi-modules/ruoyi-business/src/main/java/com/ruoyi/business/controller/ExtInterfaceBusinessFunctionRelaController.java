package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtInterfaceBusinessFunctionRela;
import com.ruoyi.business.service.IExtInterfaceBusinessFunctionRelaService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 业务功能接口关系Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@RestController
@RequestMapping("/interfaceBusinessFunctionRela")
public class ExtInterfaceBusinessFunctionRelaController extends BaseController
{
    @Autowired
    private IExtInterfaceBusinessFunctionRelaService extInterfaceBusinessFunctionRelaService;

    /**
     * 查询业务功能接口关系列表
     */
    @RequiresPermissions("business:rela:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtInterfaceBusinessFunctionRela extInterfaceBusinessFunctionRela)
    {
        startPage();
        List<ExtInterfaceBusinessFunctionRela> list = extInterfaceBusinessFunctionRelaService.selectExtInterfaceBusinessFunctionRelaList(extInterfaceBusinessFunctionRela);
        return getDataTable(list);
    }

    /**
     * 导出业务功能接口关系列表
     */
    @RequiresPermissions("business:rela:export")
    @Log(title = "业务功能接口关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtInterfaceBusinessFunctionRela extInterfaceBusinessFunctionRela)
    {
        List<ExtInterfaceBusinessFunctionRela> list = extInterfaceBusinessFunctionRelaService.selectExtInterfaceBusinessFunctionRelaList(extInterfaceBusinessFunctionRela);
        ExcelUtil<ExtInterfaceBusinessFunctionRela> util = new ExcelUtil<ExtInterfaceBusinessFunctionRela>(ExtInterfaceBusinessFunctionRela.class);
        util.exportExcel(response, list, "业务功能接口关系数据");
    }

    /**
     * 获取业务功能接口关系详细信息
     */
    @RequiresPermissions("business:rela:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extInterfaceBusinessFunctionRelaService.selectExtInterfaceBusinessFunctionRelaById(id));
    }

    /**
     * 新增业务功能接口关系
     */
    @RequiresPermissions("business:rela:add")
    @Log(title = "业务功能接口关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty ExtInterfaceBusinessFunctionRela extInterfaceBusinessFunctionRela)
    {
        return toAjax(extInterfaceBusinessFunctionRelaService.insertExtInterfaceBusinessFunctionRela(extInterfaceBusinessFunctionRela));
    }

    /**
     * 修改业务功能接口关系
     */
    @RequiresPermissions("business:rela:edit")
    @Log(title = "业务功能接口关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty ExtInterfaceBusinessFunctionRela extInterfaceBusinessFunctionRela)
    {
        return toAjax(extInterfaceBusinessFunctionRelaService.updateExtInterfaceBusinessFunctionRela(extInterfaceBusinessFunctionRela));
    }

    /**
     * 删除业务功能接口关系
     */
    @RequiresPermissions("business:rela:remove")
    @Log(title = "业务功能接口关系", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extInterfaceBusinessFunctionRelaService.deleteExtInterfaceBusinessFunctionRelaByIds(ids));
    }
}
