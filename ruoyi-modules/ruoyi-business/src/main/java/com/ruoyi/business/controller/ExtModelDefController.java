package com.ruoyi.business.controller;

import com.ruoyi.business.domain.BusinessFunctionTree;
import com.ruoyi.business.domain.ExtModelDef;
import com.ruoyi.business.entity.ExtModelDefInstanceAdd;
import com.ruoyi.business.entity.ExtModelDefInstanceList;
import com.ruoyi.business.entity.ExtModelDefInstanceOne;
import com.ruoyi.business.entity.ExtModelDefInstanceUpdate;
import com.ruoyi.business.service.IExtModelDefService;
import com.ruoyi.business.service.IExtModelFieldService;
import com.ruoyi.common.core.utils.bean.BeanUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.PageDomain;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.core.web.page.TableSupport;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * 模型定义Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@RestController
@RequestMapping("/def")
public class ExtModelDefController extends BaseController
{
    @Autowired
    private IExtModelDefService extModelDefService;
    @Autowired
    private IExtModelFieldService extModelFieldService;
    /**
     * 查询模型定义列表
     */
    @RequiresPermissions("business:def:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtModelDef extModelDef)
    {
        startPage();
        List<ExtModelDef> list = extModelDefService.selectExtModelDefList(extModelDef);
        return getDataTable(list);
    }

    /**
     * 导出模型定义列表
     */
    @RequiresPermissions("business:def:export")
    @Log(title = "模型定义", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtModelDef extModelDef)
    {
        List<ExtModelDef> list = extModelDefService.selectExtModelDefList(extModelDef);
        ExcelUtil<ExtModelDef> util = new ExcelUtil<ExtModelDef>(ExtModelDef.class);
        util.exportExcel(response, list, "模型定义数据");
    }

    /**
     * 获取模型定义详细信息
     */
    @RequiresPermissions("business:def:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extModelDefService.selectExtModelDefById(id));
    }

    /**
     * 新增模型定义
     */
    @RequiresPermissions("business:def:add")
    @Log(title = "模型定义", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty   ExtModelDef extModelDef)
    {
        return toAjax(extModelDefService.insertExtModelDef(extModelDef));
    }

    /**
     * 修改模型定义
     */
    @RequiresPermissions("business:def:edit")
    @Log(title = "模型定义", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty  ExtModelDef extModelDef)
    {
        return toAjax(extModelDefService.updateExtModelDef(extModelDef));
    }

    /**
     * 删除模型定义
     */
    @RequiresPermissions("business:def:remove")
    @Log(title = "模型定义", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extModelDefService.deleteExtModelDefByIds(ids));
    }
    /**
     * 查询模型定义列表
     */
    @RequiresPermissions("business:def:getInstance")
    @GetMapping("/getInstance")
    public AjaxResult getInstance(ExtModelDefInstanceOne instanceFindParam)
    {
        Map<String,Object> map = extModelDefService.getInstance(instanceFindParam);
        return success(map);
    }

    @PostMapping("/instance/create")
    public AjaxResult createInstance(@RequestBody ExtModelDefInstanceAdd param) {
        return success(extModelDefService.createInstance(param));
    }

    @PostMapping("/instance/update")
    public AjaxResult updateInstance(@RequestBody ExtModelDefInstanceUpdate param) {
        return success(extModelDefService.updateInstance(param));
    }

    @DeleteMapping("/instance")
    public AjaxResult deleteInstance(@RequestBody ExtModelDefInstanceOne param) {
        extModelDefService.deleteInstance(param);
        return success();
    }
    @PostMapping("/instance/createorupdate")
    public AjaxResult createOrUpdateInstance(@RequestBody ExtModelDefInstanceAdd param) {
        if(param.getId()==null||param.getId()==0L) {
            return success(extModelDefService.createInstance(param));
        }else{
            ExtModelDefInstanceUpdate extModelDefInstanceUpdate = new ExtModelDefInstanceUpdate();
            BeanUtils.copyProperties(param, extModelDefInstanceUpdate);
            return success(extModelDefService.updateInstance(extModelDefInstanceUpdate));
        }
    }
    /**
     * 查询所有模型树列表
     */
    @RequiresPermissions("business:def:tree")
    @GetMapping("/tree")
    public AjaxResult tree(ExtModelDef extModelDef)
    {
        List<BusinessFunctionTree> list = extModelDefService.treeList(extModelDef);
        return success(list);
    }
    /**
     * 查询模型所有字段
     */
    @RequiresPermissions("business:def:getallfield")
    @GetMapping("/getAllField")
    public AjaxResult getAllField(ExtModelDef extModelDef)
    {
        return success(extModelFieldService.selectAllFieldsByDsAndEnName(extModelDef));
    }
    /**
     * 翻页查询接口参数列表
     */
    @RequiresPermissions("business:def:listinstance")
    @PostMapping("/listInstance")
    public TableDataInfo listInstance(@RequestBody ExtModelDefInstanceList extModelDefInstanceList)
    {
//        startPage();
        PageDomain pageDomain = TableSupport.buildPageRequest();
//        Integer pageNum = pageDomain.getPageNum();
//        Integer pageSize = pageDomain.getPageSize();
        List<Map<String, Object>>  list = extModelDefService.listInstance(extModelDefInstanceList,pageDomain);
        int cnt = extModelDefService.countInstance(extModelDefInstanceList);
        TableDataInfo tableDataInfo= getDataTable(list);
        tableDataInfo.setTotal(cnt);
        return tableDataInfo;
    }
    /**
     * 查询接口参数列表(非翻页)
     */
    @RequiresPermissions("business:def:listallallinstance")
    @PostMapping("/listAllInstance")
    public AjaxResult listAllInstance(@RequestBody ExtModelDefInstanceList extModelDefInstanceList)
    {
        List<Map<String, Object>>  list = extModelDefService.listAllInstance(extModelDefInstanceList);
        return success(list);
    }
}
