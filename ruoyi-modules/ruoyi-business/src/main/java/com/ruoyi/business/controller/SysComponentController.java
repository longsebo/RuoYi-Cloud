package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.SysComponent;
import com.ruoyi.business.service.ISysComponentService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件Controller
 *
 * @author ruoyi
 * @date 2025-02-16
 */
@RestController
@RequestMapping("/sysComponent")
public class SysComponentController extends BaseController
{
    @Autowired
    private ISysComponentService sysComponentService;

    /**
     * 查询系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SysComponent sysComponent)
    {
        startPage();
        List<SysComponent> list = sysComponentService.selectSysComponentList(sysComponent);
        return getDataTable(list);
    }

    /**
     * 导出系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件列表
     */
    @Log(title = "系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysComponent sysComponent)
    {
        List<SysComponent> list = sysComponentService.selectSysComponentList(sysComponent);
        ExcelUtil<SysComponent> util = new ExcelUtil<SysComponent>(SysComponent.class);
        util.exportExcel(response, list, "系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级");
    }

    /**
     * 获取系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件详细信息
     */
    @GetMapping(value = "/{componentId}")
    public AjaxResult getInfo(@PathVariable("componentId") Long componentId)
    {
        return success(sysComponentService.selectSysComponentByComponentId(componentId));
    }

    /**
     * 新增系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     */
    @Log(title = "系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysComponent sysComponent)
    {
        return toAjax(sysComponentService.insertSysComponent(sysComponent));
    }

    /**
     * 修改系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     */
    @Log(title = "系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysComponent sysComponent)
    {
        return toAjax(sysComponentService.updateSysComponent(sysComponent));
    }

    /**
     * 删除系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     */
    @Log(title = "系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级", businessType = BusinessType.DELETE)
	@DeleteMapping("/{componentIds}")
    public AjaxResult remove(@PathVariable Long[] componentIds)
    {
        return toAjax(sysComponentService.deleteSysComponentByComponentIds(componentIds));
    }
}
