package com.ruoyi.business.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * 系统级别分组，最基本的组。一般不会变化。除非系统版本升级对象 sys_component_group
 *
 * @author ruoyi
 * @date 2025-02-16
 */
public class SysComponentGroup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long groupId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String groupCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String groupName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String groupIconUrl;

    /** 启用禁用状态：00--禁用 01--启用 */
    @Excel(name = "启用禁用状态：00--禁用 01--启用")
    private String status;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sortNumber;
    private List<SysComponent> sysComponents;

    public void setGroupId(Long groupId)
    {
        this.groupId = groupId;
    }

    public Long getGroupId()
    {
        return groupId;
    }
    public void setGroupCode(String groupCode)
    {
        this.groupCode = groupCode;
    }

    public String getGroupCode()
    {
        return groupCode;
    }
    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public String getGroupName()
    {
        return groupName;
    }
    public void setGroupIconUrl(String groupIconUrl)
    {
        this.groupIconUrl = groupIconUrl;
    }

    public String getGroupIconUrl()
    {
        return groupIconUrl;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setSortNumber(Long sortNumber)
    {
        this.sortNumber = sortNumber;
    }

    public Long getSortNumber()
    {
        return sortNumber;
    }

    public List<SysComponent> getSysComponents() {
        return sysComponents;
    }

    public void setSysComponents(List<SysComponent> sysComponents) {
        this.sysComponents = sysComponents;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("groupId", getGroupId())
            .append("groupCode", getGroupCode())
            .append("groupName", getGroupName())
            .append("groupIconUrl", getGroupIconUrl())
            .append("remark", getRemark())
            .append("status", getStatus())
            .append("sortNumber", getSortNumber())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
