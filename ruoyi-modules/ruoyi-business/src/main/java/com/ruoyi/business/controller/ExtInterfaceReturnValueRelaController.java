package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtInterfaceReturnValueRela;
import com.ruoyi.business.service.IExtInterfaceReturnValueRelaService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 接口返回值关系Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@RestController
@RequestMapping("/interfaceReturnValueRela")
public class ExtInterfaceReturnValueRelaController extends BaseController
{
    @Autowired
    private IExtInterfaceReturnValueRelaService extInterfaceReturnValueRelaService;

    /**
     * 查询接口返回值关系列表
     */
    @RequiresPermissions("business:rela:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtInterfaceReturnValueRela extInterfaceReturnValueRela)
    {
        startPage();
        List<ExtInterfaceReturnValueRela> list = extInterfaceReturnValueRelaService.selectExtInterfaceReturnValueRelaList(extInterfaceReturnValueRela);
        return getDataTable(list);
    }

    /**
     * 导出接口返回值关系列表
     */
    @RequiresPermissions("business:rela:export")
    @Log(title = "接口返回值关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtInterfaceReturnValueRela extInterfaceReturnValueRela)
    {
        List<ExtInterfaceReturnValueRela> list = extInterfaceReturnValueRelaService.selectExtInterfaceReturnValueRelaList(extInterfaceReturnValueRela);
        ExcelUtil<ExtInterfaceReturnValueRela> util = new ExcelUtil<ExtInterfaceReturnValueRela>(ExtInterfaceReturnValueRela.class);
        util.exportExcel(response, list, "接口返回值关系数据");
    }

    /**
     * 获取接口返回值关系详细信息
     */
    @RequiresPermissions("business:rela:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extInterfaceReturnValueRelaService.selectExtInterfaceReturnValueRelaById(id));
    }

    /**
     * 新增接口返回值关系
     */
    @RequiresPermissions("business:rela:add")
    @Log(title = "接口返回值关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty ExtInterfaceReturnValueRela extInterfaceReturnValueRela)
    {
        return toAjax(extInterfaceReturnValueRelaService.insertExtInterfaceReturnValueRela(extInterfaceReturnValueRela));
    }

    /**
     * 修改接口返回值关系
     */
    @RequiresPermissions("business:rela:edit")
    @Log(title = "接口返回值关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty ExtInterfaceReturnValueRela extInterfaceReturnValueRela)
    {
        return toAjax(extInterfaceReturnValueRelaService.updateExtInterfaceReturnValueRela(extInterfaceReturnValueRela));
    }

    /**
     * 删除接口返回值关系
     */
    @RequiresPermissions("business:rela:remove")
    @Log(title = "接口返回值关系", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extInterfaceReturnValueRelaService.deleteExtInterfaceReturnValueRelaByIds(ids));
    }
}
