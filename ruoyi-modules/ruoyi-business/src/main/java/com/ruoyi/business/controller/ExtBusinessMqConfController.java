package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtBusinessMqConf;
import com.ruoyi.business.service.IExtBusinessMqConfService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * MQ配置定义Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@RestController
@RequestMapping("/conf")
public class ExtBusinessMqConfController extends BaseController
{
    @Autowired
    private IExtBusinessMqConfService extBusinessMqConfService;

    /**
     * 查询MQ配置定义列表
     */
    @RequiresPermissions("business:conf:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtBusinessMqConf extBusinessMqConf)
    {
        startPage();
        List<ExtBusinessMqConf> list = extBusinessMqConfService.selectExtBusinessMqConfList(extBusinessMqConf);
        return getDataTable(list);
    }

    /**
     * 导出MQ配置定义列表
     */
    @RequiresPermissions("business:conf:export")
    @Log(title = "MQ配置定义", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtBusinessMqConf extBusinessMqConf)
    {
        List<ExtBusinessMqConf> list = extBusinessMqConfService.selectExtBusinessMqConfList(extBusinessMqConf);
        ExcelUtil<ExtBusinessMqConf> util = new ExcelUtil<ExtBusinessMqConf>(ExtBusinessMqConf.class);
        util.exportExcel(response, list, "MQ配置定义数据");
    }

    /**
     * 获取MQ配置定义详细信息
     */
    @RequiresPermissions("business:conf:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extBusinessMqConfService.selectExtBusinessMqConfById(id));
    }

    /**
     * 新增MQ配置定义
     */
    @RequiresPermissions("business:conf:add")
    @Log(title = "MQ配置定义", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty ExtBusinessMqConf extBusinessMqConf)
    {
        return toAjax(extBusinessMqConfService.insertExtBusinessMqConf(extBusinessMqConf));
    }

    /**
     * 修改MQ配置定义
     */
    @RequiresPermissions("business:conf:edit")
    @Log(title = "MQ配置定义", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty ExtBusinessMqConf extBusinessMqConf)
    {
        return toAjax(extBusinessMqConfService.updateExtBusinessMqConf(extBusinessMqConf));
    }

    /**
     * 删除MQ配置定义
     */
    @RequiresPermissions("business:conf:remove")
    @Log(title = "MQ配置定义", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extBusinessMqConfService.deleteExtBusinessMqConfByIds(ids));
    }
}
