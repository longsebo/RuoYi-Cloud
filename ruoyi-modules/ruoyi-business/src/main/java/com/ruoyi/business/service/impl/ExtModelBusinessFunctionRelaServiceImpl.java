package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.domain.ExtModelDef;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.ExtModelBusinessFunctionRelaMapper;
import com.ruoyi.business.domain.ExtModelBusinessFunctionRela;
import com.ruoyi.business.service.IExtModelBusinessFunctionRelaService;

/**
 * 业务功能模型关系Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@Service
public class ExtModelBusinessFunctionRelaServiceImpl implements IExtModelBusinessFunctionRelaService
{
    @Autowired
    private ExtModelBusinessFunctionRelaMapper extModelBusinessFunctionRelaMapper;

    /**
     * 查询业务功能模型关系
     *
     * @param id 业务功能模型关系主键
     * @return 业务功能模型关系
     */
    @Override
    public ExtModelBusinessFunctionRela selectExtModelBusinessFunctionRelaById(Long id)
    {
        return extModelBusinessFunctionRelaMapper.selectExtModelBusinessFunctionRelaById(id);
    }

    /**
     * 查询业务功能模型关系列表
     *
     * @param extModelBusinessFunctionRela 业务功能模型关系
     * @return 业务功能模型关系
     */
    @Override
    public List<ExtModelBusinessFunctionRela> selectExtModelBusinessFunctionRelaList(ExtModelBusinessFunctionRela extModelBusinessFunctionRela)
    {
        return extModelBusinessFunctionRelaMapper.selectExtModelBusinessFunctionRelaList(extModelBusinessFunctionRela);
    }

    /**
     * 新增业务功能模型关系
     *
     * @param extModelBusinessFunctionRela 业务功能模型关系
     * @return 结果
     */
    @Override
    public int insertExtModelBusinessFunctionRela(ExtModelBusinessFunctionRela extModelBusinessFunctionRela)
    {
        extModelBusinessFunctionRela.setCreateTime(DateUtils.getNowDate());
        return extModelBusinessFunctionRelaMapper.insertExtModelBusinessFunctionRela(extModelBusinessFunctionRela);
    }

    /**
     * 修改业务功能模型关系
     *
     * @param extModelBusinessFunctionRela 业务功能模型关系
     * @return 结果
     */
    @Override
    public int updateExtModelBusinessFunctionRela(ExtModelBusinessFunctionRela extModelBusinessFunctionRela)
    {
        extModelBusinessFunctionRela.setUpdateTime(DateUtils.getNowDate());
        return extModelBusinessFunctionRelaMapper.updateExtModelBusinessFunctionRela(extModelBusinessFunctionRela);
    }

    /**
     * 批量删除业务功能模型关系
     *
     * @param ids 需要删除的业务功能模型关系主键
     * @return 结果
     */
    @Override
    public int deleteExtModelBusinessFunctionRelaByIds(Long[] ids)
    {
        return extModelBusinessFunctionRelaMapper.deleteExtModelBusinessFunctionRelaByIds(ids);
    }

    /**
     * 删除业务功能模型关系信息
     *
     * @param id 业务功能模型关系主键
     * @return 结果
     */
    @Override
    public int deleteExtModelBusinessFunctionRelaById(Long id)
    {
        return extModelBusinessFunctionRelaMapper.deleteExtModelBusinessFunctionRelaById(id);
    }

    /**
     * 根据条件删除记录
     *
     * @param functionRela
     * @return
     */
    @Override
    public int deleteExtModelBusinessFunctionRela(ExtModelBusinessFunctionRela functionRela) {
        return extModelBusinessFunctionRelaMapper.deleteExtModelBusinessFunctionRela(functionRela);
    }

    /**
     * 更新功能模型关系表的数据源名称+表英文名
     *
     * @param oldExtModelDef
     * @param extModelDef
     * @return
     */
    @Override
    public int updateBusinessFunctionRelaDsEnName(ExtModelDef oldExtModelDef, ExtModelDef extModelDef) {
        return extModelBusinessFunctionRelaMapper.updateBusinessFunctionRelaDsEnName(oldExtModelDef,extModelDef);
    }
}
