package com.ruoyi.business.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.BusinessFunctionTree;
import com.ruoyi.business.domain.ExtBusinessFunction;

/**
 * 业务功能Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface ExtBusinessFunctionMapper
{
    /**
     * 查询业务功能
     *
     * @param id 业务功能主键
     * @return 业务功能
     */
    public ExtBusinessFunction selectExtBusinessFunctionById(Long id);

    /**
     * 查询业务功能列表
     *
     * @param extBusinessFunction 业务功能
     * @return 业务功能集合
     */
    public List<ExtBusinessFunction> selectExtBusinessFunctionList(ExtBusinessFunction extBusinessFunction);

    /**
     * 新增业务功能
     *
     * @param extBusinessFunction 业务功能
     * @return 结果
     */
    public int insertExtBusinessFunction(ExtBusinessFunction extBusinessFunction);

    /**
     * 修改业务功能
     *
     * @param extBusinessFunction 业务功能
     * @return 结果
     */
    public int updateExtBusinessFunction(ExtBusinessFunction extBusinessFunction);

    /**
     * 删除业务功能
     *
     * @param id 业务功能主键
     * @return 结果
     */
    public int deleteExtBusinessFunctionById(Long id);

    /**
     * 批量删除业务功能
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtBusinessFunctionByIds(Long[] ids);

    /**
     * 查询满足条件记录数
     * @param searchMap
     * @return
     */
    public int  selectCount(Map searchMap);

    /**
     * 查询业务功能列表
     * @param extBusinessFunction
     * @return 返回通用树列表
     */
    List<BusinessFunctionTree> selectCommonBusinessFunctionList(ExtBusinessFunction extBusinessFunction);
}
