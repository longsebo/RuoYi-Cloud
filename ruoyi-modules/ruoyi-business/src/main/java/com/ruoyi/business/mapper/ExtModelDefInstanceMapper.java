package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.*;
import com.ruoyi.business.entity.ExtModelDefInstanceAdd;
import com.ruoyi.business.entity.ExtModelDefInstanceField;
import com.ruoyi.business.entity.ExtModelDefInstanceOne;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 模型定义实例Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-12
 */
public interface ExtModelDefInstanceMapper
{
    /**
     * 根据id查询模式表定义实例记录
     * @param instanceFindParam
     * @return
     */
    Map<String, Object> getInstance(ExtModelDefInstanceOne instanceFindParam);

    /**
     * 创建表
     * @param t
     * @param defaultFields
     */
    void createTable(@Param("table") Table t, @Param("fields") List<TableField> defaultFields);

    /**
     * 删除表
     * @param enName
     */
    void dropTable(@Param("enName") String enName);

    /**
     * 修改表名
     * @param oldName
     * @param newName
     */
    void renameTableName(@Param("oldName")String oldName, @Param("newName")String newName);

    /**
     * 修改表注释
     * @param extModelDef
     */
    void changeTableComment(@Param("extModelDef") ExtModelDef extModelDef);

    /**
     * 新增字段
     * @param extModelField
     */
    void addField(@Param("extModelField") ExtModelField extModelField);

    /**
     * 修复字段
     * @param tableName 表名
     * @param tableField 模型字段引用
     */
    void modifyField(@Param("tableName") String tableName, @Param("tableField") TableField tableField);

    /**
     * 删除字段
     * @param enName
     * @param fieldEnName
     */
    void dropField(@Param("tableName") String enName, @Param("fieldName") String fieldEnName);
    /**
     * 增强版修复字段
     * @param tableName 表名
     * @param oldFieldName 旧字段名
     * @param tableField 模型字段引用
     */
    void modifyAllField(@Param("tableName") String tableName, @Param("oldFieldName")String oldFieldName, @Param("tableField") TableField tableField);

    /**
     *  插入实例记录
     * @param extModelDefInstanceAdd 实例模型
     * @param instanceFields 实例数据列表
     * @return
     */
    int createInstance(@Param("instance") ExtModelDefInstanceAdd extModelDefInstanceAdd, @Param("fields") List<ExtModelDefInstanceField> instanceFields);

    /**
     * 更新实例记录
     * @param id 主键id
     * @param enName 表英文名
     * @param instanceFields 实例数据字段
     * @return
     */
    int updateInstance(@Param("id") Long id,@Param("tableName") String enName,@Param("fields") List<ExtModelDefInstanceField> instanceFields);

    /**
     *  删除实例记录
     * @param id   主键id
     * @param enName 表英文名
     * @return 删除记录数
     */
    int deleteInstance(@Param("id") Long id,@Param("tableName") String enName);
}
