package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.domain.TenantFunction;

/**
 * 对外宣传为功能点，即开发人员的函数Mapper接口
 * 
 * @author ruoyi
 * @date 2025-02-16
 */
public interface TenantFunctionMapper 
{
    /**
     * 查询对外宣传为功能点，即开发人员的函数
     * 
     * @param functionId 对外宣传为功能点，即开发人员的函数主键
     * @return 对外宣传为功能点，即开发人员的函数
     */
    public TenantFunction selectTenantFunctionByFunctionId(Long functionId);

    /**
     * 查询对外宣传为功能点，即开发人员的函数列表
     * 
     * @param tenantFunction 对外宣传为功能点，即开发人员的函数
     * @return 对外宣传为功能点，即开发人员的函数集合
     */
    public List<TenantFunction> selectTenantFunctionList(TenantFunction tenantFunction);

    /**
     * 新增对外宣传为功能点，即开发人员的函数
     * 
     * @param tenantFunction 对外宣传为功能点，即开发人员的函数
     * @return 结果
     */
    public int insertTenantFunction(TenantFunction tenantFunction);

    /**
     * 修改对外宣传为功能点，即开发人员的函数
     * 
     * @param tenantFunction 对外宣传为功能点，即开发人员的函数
     * @return 结果
     */
    public int updateTenantFunction(TenantFunction tenantFunction);

    /**
     * 删除对外宣传为功能点，即开发人员的函数
     * 
     * @param functionId 对外宣传为功能点，即开发人员的函数主键
     * @return 结果
     */
    public int deleteTenantFunctionByFunctionId(Long functionId);

    /**
     * 批量删除对外宣传为功能点，即开发人员的函数
     * 
     * @param functionIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTenantFunctionByFunctionIds(Long[] functionIds);
}
