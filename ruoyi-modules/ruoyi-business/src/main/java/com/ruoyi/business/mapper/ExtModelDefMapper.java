package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.ExtModelDef;

import java.util.List;
import java.util.Map;

/**
 * 模型定义Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface ExtModelDefMapper
{
    /**
     * 查询模型定义
     *
     * @param id 模型定义主键
     * @return 模型定义
     */
    public ExtModelDef selectExtModelDefById(Long id);

    /**
     * 查询模型定义列表
     *
     * @param extModelDef 模型定义
     * @return 模型定义集合
     */
    public List<ExtModelDef> selectExtModelDefList(ExtModelDef extModelDef);

    /**
     * 新增模型定义
     *
     * @param extModelDef 模型定义
     * @return 结果
     */
    public int insertExtModelDef(ExtModelDef extModelDef);

    /**
     * 修改模型定义
     *
     * @param extModelDef 模型定义
     * @return 结果
     */
    public int updateExtModelDef(ExtModelDef extModelDef);

    /**
     * 删除模型定义
     *
     * @param id 模型定义主键
     * @return 结果
     */
    public int deleteExtModelDefById(Long id);

    /**
     * 批量删除模型定义
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtModelDefByIds(Long[] ids);

    /**
     * 查询满足条件个数
     * @param searchMap 查询条件
     * @return 返回满足条件条数
     */
    public int  selectCount(Map<String, Object> searchMap);

    /**
     * 查询模型定义树
     * @param extModelDef
     * @return
     */
    public List<ExtModelDef> selectModelDefListAssignBusinessFunction(ExtModelDef extModelDef);
}
