package com.ruoyi.business.domain;

import lombok.Data;

/**
 * 数据库表定义
 */
@Data
public class Table {
    /**
     * 表名
     */
    private String tableName;
    /**
     * 注释
     */
    private String comment;
}

