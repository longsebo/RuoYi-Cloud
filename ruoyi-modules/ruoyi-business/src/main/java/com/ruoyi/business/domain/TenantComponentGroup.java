package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.util.List;

/**
 * 租户级别分组，由各自租户维护对象 tenant_component_group
 *
 * @author ruoyi
 * @date 2025-02-16
 */
public class TenantComponentGroup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long groupId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String enterpriseCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String groupCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String groupName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String groupIconUrl;

    /** 启用禁用状态：00--禁用 01--启用 */
    @Excel(name = "启用禁用状态：00--禁用 01--启用")
    private String status;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sortNumber;

    public List<TenantComponent> getTenantComponents() {
        return tenantComponents;
    }

    public void setTenantComponents(List<TenantComponent> tenantComponents) {
        this.tenantComponents = tenantComponents;
    }

    List<TenantComponent> tenantComponents;


    public void setGroupId(Long groupId)
    {
        this.groupId = groupId;
    }

    public Long getGroupId()
    {
        return groupId;
    }
    public void setEnterpriseCode(String enterpriseCode)
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode()
    {
        return enterpriseCode;
    }
    public void setGroupCode(String groupCode)
    {
        this.groupCode = groupCode;
    }

    public String getGroupCode()
    {
        return groupCode;
    }
    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public String getGroupName()
    {
        return groupName;
    }
    public void setGroupIconUrl(String groupIconUrl)
    {
        this.groupIconUrl = groupIconUrl;
    }

    public String getGroupIconUrl()
    {
        return groupIconUrl;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setSortNumber(Long sortNumber)
    {
        this.sortNumber = sortNumber;
    }

    public Long getSortNumber()
    {
        return sortNumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("groupId", getGroupId())
            .append("enterpriseCode", getEnterpriseCode())
            .append("groupCode", getGroupCode())
            .append("groupName", getGroupName())
            .append("groupIconUrl", getGroupIconUrl())
            .append("remark", getRemark())
            .append("status", getStatus())
            .append("sortNumber", getSortNumber())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
