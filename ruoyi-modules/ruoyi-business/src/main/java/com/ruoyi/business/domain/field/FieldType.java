package com.ruoyi.business.domain.field;

public enum FieldType {
    number,
    text,
    option,
    user,
    dept,
    date,

    /**
     * TODO:
     */
    entity,
    /**
     * TODO:
     */
    workflow
    ;

}
