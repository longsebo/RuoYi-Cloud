package com.ruoyi.business.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.ExtPageParameterRelaMapper;
import com.ruoyi.business.domain.ExtPageParameterRela;
import com.ruoyi.business.service.IExtPageParameterRelaService;

/**
 * 页面参数关系Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-28
 */
@Service
public class ExtPageParameterRelaServiceImpl implements IExtPageParameterRelaService 
{
    @Autowired
    private ExtPageParameterRelaMapper extPageParameterRelaMapper;

    /**
     * 查询页面参数关系
     * 
     * @param id 页面参数关系主键
     * @return 页面参数关系
     */
    @Override
    public ExtPageParameterRela selectExtPageParameterRelaById(Long id)
    {
        return extPageParameterRelaMapper.selectExtPageParameterRelaById(id);
    }

    /**
     * 查询页面参数关系列表
     * 
     * @param extPageParameterRela 页面参数关系
     * @return 页面参数关系
     */
    @Override
    public List<ExtPageParameterRela> selectExtPageParameterRelaList(ExtPageParameterRela extPageParameterRela)
    {
        return extPageParameterRelaMapper.selectExtPageParameterRelaList(extPageParameterRela);
    }

    /**
     * 新增页面参数关系
     * 
     * @param extPageParameterRela 页面参数关系
     * @return 结果
     */
    @Override
    public int insertExtPageParameterRela(ExtPageParameterRela extPageParameterRela)
    {
        extPageParameterRela.setCreateTime(DateUtils.getNowDate());
        return extPageParameterRelaMapper.insertExtPageParameterRela(extPageParameterRela);
    }

    /**
     * 修改页面参数关系
     * 
     * @param extPageParameterRela 页面参数关系
     * @return 结果
     */
    @Override
    public int updateExtPageParameterRela(ExtPageParameterRela extPageParameterRela)
    {
        extPageParameterRela.setUpdateTime(DateUtils.getNowDate());
        return extPageParameterRelaMapper.updateExtPageParameterRela(extPageParameterRela);
    }

    /**
     * 批量删除页面参数关系
     * 
     * @param ids 需要删除的页面参数关系主键
     * @return 结果
     */
    @Override
    public int deleteExtPageParameterRelaByIds(Long[] ids)
    {
        return extPageParameterRelaMapper.deleteExtPageParameterRelaByIds(ids);
    }

    /**
     * 删除页面参数关系信息
     * 
     * @param id 页面参数关系主键
     * @return 结果
     */
    @Override
    public int deleteExtPageParameterRelaById(Long id)
    {
        return extPageParameterRelaMapper.deleteExtPageParameterRelaById(id);
    }
}
