package com.ruoyi.business.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.ExtIntefaceUrl;

/**
 * 通用URLMapper接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface ExtIntefaceUrlMapper
{
    /**
     * 查询通用URL
     *
     * @param id 通用URL主键
     * @return 通用URL
     */
    public ExtIntefaceUrl selectExtIntefaceUrlById(Long id);

    /**
     * 查询通用URL列表
     *
     * @param extIntefaceUrl 通用URL
     * @return 通用URL集合
     */
    public List<ExtIntefaceUrl> selectExtIntefaceUrlList(ExtIntefaceUrl extIntefaceUrl);

    /**
     * 新增通用URL
     *
     * @param extIntefaceUrl 通用URL
     * @return 结果
     */
    public int insertExtIntefaceUrl(ExtIntefaceUrl extIntefaceUrl);

    /**
     * 修改通用URL
     *
     * @param extIntefaceUrl 通用URL
     * @return 结果
     */
    public int updateExtIntefaceUrl(ExtIntefaceUrl extIntefaceUrl);

    /**
     * 删除通用URL
     *
     * @param id 通用URL主键
     * @return 结果
     */
    public int deleteExtIntefaceUrlById(Long id);

    /**
     * 批量删除通用URL
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtIntefaceUrlByIds(Long[] ids);

    /**
     * 查询满足条件记录数
     * @param searchMap
     * @return
     */
    public int selectCount(Map<String, Object> searchMap);
}
