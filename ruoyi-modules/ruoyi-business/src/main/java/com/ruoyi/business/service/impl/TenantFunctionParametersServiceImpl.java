package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.domain.TenantFunction;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.UniqueIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.TenantFunctionParametersMapper;
import com.ruoyi.business.domain.TenantFunctionParameters;
import com.ruoyi.business.service.ITenantFunctionParametersService;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2025-02-16
 */
@Service
public class TenantFunctionParametersServiceImpl implements ITenantFunctionParametersService
{
    @Autowired
    private TenantFunctionParametersMapper tenantFunctionParametersMapper;

    /**
     * 查询【请填写功能名称】
     *
     * @param parameterId 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public TenantFunctionParameters selectTenantFunctionParametersByParameterId(Long parameterId)
    {
        return tenantFunctionParametersMapper.selectTenantFunctionParametersByParameterId(parameterId);
    }

    /**
     * 查询【请填写功能名称】列表
     *
     * @param tenantFunctionParameters 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<TenantFunctionParameters> selectTenantFunctionParametersList(TenantFunctionParameters tenantFunctionParameters)
    {
        return tenantFunctionParametersMapper.selectTenantFunctionParametersList(tenantFunctionParameters);
    }

    /**
     * 新增【请填写功能名称】
     *
     * @param tenantFunctionParameters 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertTenantFunctionParameters(TenantFunctionParameters tenantFunctionParameters)
    {
        tenantFunctionParameters.setCreateTime(DateUtils.getNowDate());
        //自动产生功能参数编码，并保证唯一
        String parameterCode;
        TenantFunctionParameters searchTenantFunctionParameter = new TenantFunctionParameters();
        while (true) {
            parameterCode = UniqueIdGenerator.getUniqueClassName(10);
            searchTenantFunctionParameter.setParameterCode(parameterCode);
            List<TenantFunctionParameters> list = tenantFunctionParametersMapper.selectTenantFunctionParametersList(searchTenantFunctionParameter);
            if (list.size() == 0) {
                tenantFunctionParameters.setParameterCode(parameterCode);
                break;
            }
        }
        return tenantFunctionParametersMapper.insertTenantFunctionParameters(tenantFunctionParameters);
    }

    /**
     * 修改【请填写功能名称】
     *
     * @param tenantFunctionParameters 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateTenantFunctionParameters(TenantFunctionParameters tenantFunctionParameters)
    {
        tenantFunctionParameters.setUpdateTime(DateUtils.getNowDate());
        return tenantFunctionParametersMapper.updateTenantFunctionParameters(tenantFunctionParameters);
    }

    /**
     * 批量删除【请填写功能名称】
     *
     * @param parameterIds 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTenantFunctionParametersByParameterIds(Long[] parameterIds)
    {
        return tenantFunctionParametersMapper.deleteTenantFunctionParametersByParameterIds(parameterIds);
    }

    /**
     * 删除【请填写功能名称】信息
     *
     * @param parameterId 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTenantFunctionParametersByParameterId(Long parameterId)
    {
        return tenantFunctionParametersMapper.deleteTenantFunctionParametersByParameterId(parameterId);
    }
}
