/**
 *
 */
package com.ruoyi.business.domain.sqldesign;

import lombok.Data;

import java.util.List;

/**
 * 	@author Administrator
 * 	条件树节点模型
 */
@Data
public class ConditionTreeModel extends AbstractConditionTreeModel {
	/**
	 * 	左边操作列/表达式
	 */
	private String left="";
	/**
	 * 	操作符
	 */
	private String operator="=";
	/**
	 * 	右边操作列/表达式
	 */
	private String right="";
	private int currentLevel;
	private String parentLevel;
	private String id;
	/**
	 * 是否可选 1--可选
	 */
	private String isOptional;
	//直接下属的条件
	private List<ConditionTreeModel> childConditionTreeModels;
	/**
	 * 	条件关系类型
	 */
	private String conditionRelaType;


}
