package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.ExtPageParameter;
import com.ruoyi.business.domain.ExtPageParameterRela;
import com.ruoyi.business.mapper.ExtPageParameterMapper;
import com.ruoyi.business.mapper.ExtPageParameterRelaMapper;
import com.ruoyi.business.service.IExtPageParameterService;
import com.ruoyi.common.core.exception.ServiceException;
import com.ruoyi.common.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 页面参数Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-28
 */
@Service
public class ExtPageParameterServiceImpl implements IExtPageParameterService
{
    @Autowired
    private ExtPageParameterMapper extPageParameterMapper;
    @Autowired
    private ExtPageParameterRelaMapper extPageParameterRelaMapper;
    /**
     * 查询页面参数
     *
     * @param id 页面参数主键
     * @return 页面参数
     */
    @Override
    public ExtPageParameter selectExtPageParameterById(Long id)
    {
        return extPageParameterMapper.selectExtPageParameterById(id);
    }

    /**
     * 查询页面参数列表
     *
     * @param extPageParameter 页面参数
     * @return 页面参数
     */
    @Override
    public List<ExtPageParameter> selectExtPageParameterList(ExtPageParameter extPageParameter)
    {
        return extPageParameterMapper.selectExtPageParameterList(extPageParameter);
    }

    /**
     * 新增页面参数
     *
     * @param extPageParameter 页面参数
     * @return 结果
     */
    @Override
    public int insertExtPageParameter(ExtPageParameter extPageParameter)
    {
        //校验新增参数
        verifyInsert(extPageParameter);
        int rowNum =  extPageParameterMapper.insertExtPageParameter(extPageParameter);
        //插入页面参数关系表
        ExtPageParameterRela relaVo;
        relaVo = new ExtPageParameterRela();
        relaVo.setPageCode(extPageParameter.getPageCode());
        relaVo.setExtPageParameterId(extPageParameter.getId());
        relaVo.setCreateBy(extPageParameter.getCreateBy());
        relaVo.setCreateTime(extPageParameter.getCreateTime());
        relaVo.setApplicationCode(extPageParameter.getApplicationCode());
        relaVo.setEnterpriseCode(extPageParameter.getEnterpriseCode());
        extPageParameterRelaMapper.insertExtPageParameterRela(relaVo);
        return rowNum;
    }

    /**
     *  校验新增参数
     * @param extPageParameter
     */
    private void verifyInsert(ExtPageParameter extPageParameter) {
        //入口参数为空
        if (extPageParameter == null)
            throw new ServiceException("入口参数为空");
        //参数名称不能为空
        if (StringUtils.isEmpty(extPageParameter.getParameterName()))
            throw new ServiceException("参数名称不能为空");

        //参数类型不能为空
        if (StringUtils.isEmpty(extPageParameter.getParameterType()))
            throw new ServiceException("参数类型不能为空");
        //如果父id不为空，则校验父id是否存在
        if (extPageParameter.getParentId() != null) {
            ExtPageParameter parentExtPageParameter = extPageParameterMapper.selectExtPageParameterById(extPageParameter.getParentId());
            if (parentExtPageParameter == null)
                throw new ServiceException("父id不存在");
        }
        //页面编码不能为空
        if (StringUtils.isEmpty(extPageParameter.getPageCode()))
            throw new ServiceException("页面编码不能为空");
        //同一个页面编码下，参数名称不能重复
        Map<String, Object> searchParams = new HashMap<>();
        searchParams.put("parameterName", extPageParameter.getParameterName());
        searchParams.put("pageCode", extPageParameter.getPageCode());
        if(extPageParameterMapper.selectCount(searchParams)>0)
            throw new ServiceException("同一个页面编码下，参数名称不能重复");
    }
    /**
     * 修改页面参数
     *
     * @param extPageParameter 页面参数
     * @return 结果
     */
    @Override
    public int updateExtPageParameter(ExtPageParameter extPageParameter)
    {
        //校验修改参数
        verifyUpdate(extPageParameter);
        return extPageParameterMapper.updateExtPageParameter(extPageParameter);
    }

    /**
     *   校验修改参数
     * @param extPageParameter
     */
    private void verifyUpdate(ExtPageParameter extPageParameter) {
        //入口参数为空
        if (extPageParameter == null)
            throw new ServiceException("入口参数为空");
        //id不能为空
        if (extPageParameter.getId() == null)
            throw new ServiceException("id不能为空");
        // 参数名称不能为空
        if (StringUtils.isEmpty(extPageParameter.getParameterName()))
            throw new ServiceException("参数名称不能为空");
        //参数类型不能为空
        if (StringUtils.isEmpty(extPageParameter.getParameterType()))
            throw new ServiceException("参数类型不能为空");
        //如果父id不为空，则校验父id是否存在
        if (extPageParameter.getParentId() != null) {
            ExtPageParameter parentExtPageParameter = extPageParameterMapper.selectExtPageParameterById(extPageParameter.getParentId());
            if (parentExtPageParameter == null)
                throw new ServiceException("父id不存在");
        }
        //页面编码不能为空
        if (StringUtils.isEmpty(extPageParameter.getPageCode()))
            throw new ServiceException("页面编码不能为空");
        //同一个页面编码下，参数名称不能重复
        Map<String, Object> searchParams = new HashMap<>();
        searchParams.put("parameterName", extPageParameter.getParameterName());
        searchParams.put("pageCode", extPageParameter.getPageCode());
        searchParams.put("notId",extPageParameter.getId());
        if(extPageParameterMapper.selectCount(searchParams)>0)
            throw new ServiceException("同一个页面编码下，参数名称不能重复");
    }

    /**
     * 批量删除页面参数
     *
     * @param ids 需要删除的页面参数主键
     * @return 结果
     */
    @Override
    public int deleteExtPageParameterByIds(Long[] ids)
    {
        int sum = 0;
        for(Long id:ids){
            sum += deleteExtPageParameterById(id);
        }
        return sum;
    }

    /**
     * 删除页面参数信息
     *
     * @param id 页面参数主键
     * @return 结果
     */
    @Override
    public int deleteExtPageParameterById(Long id)
    {
        //检查是否有下级依赖
        Map<String,Object> searchMap = new HashMap<>();
        searchMap.put("parentId",id);
        if(extPageParameterMapper.selectCount(searchMap)>0)
            throw new ServiceException("存在下级依赖，不允许删除!");
        int rowNum = extPageParameterMapper.deleteExtPageParameterById(id);
        //删除成功后，删除页面参数关系表
        if(rowNum>0){
            //根据页面参数id删除页面参数关系表
            extPageParameterRelaMapper.deleteExtPageParameterRelaByParameterId(id);
        }
        return rowNum;
    }

    /**
     * 返回页面参数树形列表
     *
     * @param extPageParameter
     * @return
     */
    @Override
    public List<ExtPageParameter> selectTree(ExtPageParameter extPageParameter) {
        //根据条件查询返回列表
        List <ExtPageParameter> results = extPageParameterMapper.selectExtPageParameterList(extPageParameter);
        //构造树列表
        List<ExtPageParameter> treeList = buildTree(results);
        return treeList;
    }
    /**
     * 构造功能树
     * @param list
     * @return
     */
    private List<ExtPageParameter> buildTree(List<ExtPageParameter> list) {
        List<ExtPageParameter> retList = new ArrayList<>();

        //循环构造第一级
        for(ExtPageParameter interfaceParameter:list){
            if(interfaceParameter.getParentId()==null||interfaceParameter.getParentId()==0L){
                retList.add(recursionMakeTreeNode(interfaceParameter,list));
            }
        }
        return retList;
    }

    /**
     * 递归构造当前节点及下级节点
     * @param interfaceParameter
     * @param list
     * @return
     */
    private ExtPageParameter recursionMakeTreeNode(ExtPageParameter interfaceParameter, List<ExtPageParameter> list) {
        //获取直接下级
        List<ExtPageParameter> childList = list.stream().filter(item->interfaceParameter.getId().equals(item.getParentId())).collect(Collectors.toList());
        interfaceParameter.setChildren(childList);
        //递归下级
        for(ExtPageParameter child:childList){
            recursionMakeTreeNode(child,list);
        }
        return interfaceParameter;
    }
}
