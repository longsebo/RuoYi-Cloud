/**
 *
 */
package com.ruoyi.business.domain.sqldesign;

import lombok.Data;

/**
 * 	@author Administrator
 * 	列项模型
 */
@Data
public class SelectColumnItemModel {
	/**
	 * 	列/表达式
	 */
	private String columAndExp;
	/**
	 * 	列别名
	 */
	private String alias;
	/**
	 * 	聚合
	 */
	private String aggregation;
	/**
	 * 	是否分组
	 */
	private Boolean group;
	/**
	 * 	中文名
	 */
	private String chineseName;
	/**
	 * 	是否界面查询列
	 */
	private boolean uiSelectColumn;
	/**
	 * 	原始表名
	 */
	private String orgTableName;
	/**
	 * 	表别名
	 */
	private String tableAlias;
	/**
	 * 字段名
	 */
	private String fieldName;

}
