package com.ruoyi.business.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.ExtBusinessAppBusMapper;
import com.ruoyi.business.domain.ExtBusinessAppBus;
import com.ruoyi.business.service.IExtBusinessAppBusService;

/**
 * 应用业务关系Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-09-23
 */
@Service
public class ExtBusinessAppBusServiceImpl implements IExtBusinessAppBusService 
{
    @Autowired
    private ExtBusinessAppBusMapper extBusinessAppBusMapper;

    /**
     * 查询应用业务关系
     * 
     * @param id 应用业务关系主键
     * @return 应用业务关系
     */
    @Override
    public ExtBusinessAppBus selectExtBusinessAppBusById(Long id)
    {
        return extBusinessAppBusMapper.selectExtBusinessAppBusById(id);
    }

    /**
     * 查询应用业务关系列表
     * 
     * @param extBusinessAppBus 应用业务关系
     * @return 应用业务关系
     */
    @Override
    public List<ExtBusinessAppBus> selectExtBusinessAppBusList(ExtBusinessAppBus extBusinessAppBus)
    {
        return extBusinessAppBusMapper.selectExtBusinessAppBusList(extBusinessAppBus);
    }

    /**
     * 新增应用业务关系
     * 
     * @param extBusinessAppBus 应用业务关系
     * @return 结果
     */
    @Override
    public int insertExtBusinessAppBus(ExtBusinessAppBus extBusinessAppBus)
    {
        extBusinessAppBus.setCreateTime(DateUtils.getNowDate());
        return extBusinessAppBusMapper.insertExtBusinessAppBus(extBusinessAppBus);
    }

    /**
     * 修改应用业务关系
     * 
     * @param extBusinessAppBus 应用业务关系
     * @return 结果
     */
    @Override
    public int updateExtBusinessAppBus(ExtBusinessAppBus extBusinessAppBus)
    {
        extBusinessAppBus.setUpdateTime(DateUtils.getNowDate());
        return extBusinessAppBusMapper.updateExtBusinessAppBus(extBusinessAppBus);
    }

    /**
     * 批量删除应用业务关系
     * 
     * @param ids 需要删除的应用业务关系主键
     * @return 结果
     */
    @Override
    public int deleteExtBusinessAppBusByIds(Long[] ids)
    {
        return extBusinessAppBusMapper.deleteExtBusinessAppBusByIds(ids);
    }

    /**
     * 删除应用业务关系信息
     * 
     * @param id 应用业务关系主键
     * @return 结果
     */
    @Override
    public int deleteExtBusinessAppBusById(Long id)
    {
        return extBusinessAppBusMapper.deleteExtBusinessAppBusById(id);
    }
}
