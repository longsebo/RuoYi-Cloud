package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.ruoyi.common.security.annotation.DefaultCreateProperty;
import com.ruoyi.common.security.annotation.DefaultUpdateProperty;
import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import com.ruoyi.common.security.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtBusinessFunctionListening;
import com.ruoyi.business.service.IExtBusinessFunctionListeningService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 功能侦听Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@RestController
@RequestMapping("/listening")
public class ExtBusinessFunctionListeningController extends BaseController
{
    @Autowired
    private IExtBusinessFunctionListeningService extBusinessFunctionListeningService;

    /**
     * 查询功能侦听列表
     */
    @RequiresPermissions("business:listening:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtBusinessFunctionListening extBusinessFunctionListening)
    {
        startPage();
        List<ExtBusinessFunctionListening> list = extBusinessFunctionListeningService.selectExtBusinessFunctionListeningList(extBusinessFunctionListening);
        return getDataTable(list);
    }

    /**
     * 导出功能侦听列表
     */
    @RequiresPermissions("business:listening:export")
    @Log(title = "功能侦听", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtBusinessFunctionListening extBusinessFunctionListening)
    {
        List<ExtBusinessFunctionListening> list = extBusinessFunctionListeningService.selectExtBusinessFunctionListeningList(extBusinessFunctionListening);
        ExcelUtil<ExtBusinessFunctionListening> util = new ExcelUtil<ExtBusinessFunctionListening>(ExtBusinessFunctionListening.class);
        util.exportExcel(response, list, "功能侦听数据");
    }

    /**
     * 获取功能侦听详细信息
     */
    @RequiresPermissions("business:listening:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extBusinessFunctionListeningService.selectExtBusinessFunctionListeningById(id));
    }

    /**
     * 新增功能侦听
     */
    @RequiresPermissions("business:listening:add")
    @Log(title = "功能侦听", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@DefaultCreateProperty ExtBusinessFunctionListening extBusinessFunctionListening)
    {
        //不能使用@EnterpriseAndApplicationCodeProperty注解 ，所以增加手动设置
        extBusinessFunctionListening.setEnterpriseCode(SecurityUtils.getEnterpriseCode());
        return toAjax(extBusinessFunctionListeningService.insertExtBusinessFunctionListening(extBusinessFunctionListening));
    }

    /**
     * 修改功能侦听
     */
    @RequiresPermissions("business:listening:edit")
    @Log(title = "功能侦听", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit( @DefaultUpdateProperty ExtBusinessFunctionListening extBusinessFunctionListening)
    {
        //不能使用@EnterpriseAndApplicationCodeProperty注解 ，所以增加手动设置
        extBusinessFunctionListening.setEnterpriseCode(SecurityUtils.getEnterpriseCode());
        return toAjax(extBusinessFunctionListeningService.updateExtBusinessFunctionListening(extBusinessFunctionListening));
    }

    /**
     * 删除功能侦听
     */
    @RequiresPermissions("business:listening:remove")
    @Log(title = "功能侦听", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extBusinessFunctionListeningService.deleteExtBusinessFunctionListeningByIds(ids));
    }
}
