package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.domain.ExtInterfaceReturnValueRela;

/**
 * 接口返回值关系Service接口
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
public interface IExtInterfaceReturnValueRelaService 
{
    /**
     * 查询接口返回值关系
     * 
     * @param id 接口返回值关系主键
     * @return 接口返回值关系
     */
    public ExtInterfaceReturnValueRela selectExtInterfaceReturnValueRelaById(Long id);

    /**
     * 查询接口返回值关系列表
     * 
     * @param extInterfaceReturnValueRela 接口返回值关系
     * @return 接口返回值关系集合
     */
    public List<ExtInterfaceReturnValueRela> selectExtInterfaceReturnValueRelaList(ExtInterfaceReturnValueRela extInterfaceReturnValueRela);

    /**
     * 新增接口返回值关系
     * 
     * @param extInterfaceReturnValueRela 接口返回值关系
     * @return 结果
     */
    public int insertExtInterfaceReturnValueRela(ExtInterfaceReturnValueRela extInterfaceReturnValueRela);

    /**
     * 修改接口返回值关系
     * 
     * @param extInterfaceReturnValueRela 接口返回值关系
     * @return 结果
     */
    public int updateExtInterfaceReturnValueRela(ExtInterfaceReturnValueRela extInterfaceReturnValueRela);

    /**
     * 批量删除接口返回值关系
     * 
     * @param ids 需要删除的接口返回值关系主键集合
     * @return 结果
     */
    public int deleteExtInterfaceReturnValueRelaByIds(Long[] ids);

    /**
     * 删除接口返回值关系信息
     * 
     * @param id 接口返回值关系主键
     * @return 结果
     */
    public int deleteExtInterfaceReturnValueRelaById(Long id);
}
