package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtInterfaceParameterRela;
import com.ruoyi.business.service.IExtInterfaceParameterRelaService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 接口参数关系Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@RestController
@RequestMapping("/interfaceParameterRela")
public class ExtInterfaceParameterRelaController extends BaseController
{
    @Autowired
    private IExtInterfaceParameterRelaService extInterfaceParameterRelaService;

    /**
     * 查询接口参数关系列表
     */
    @RequiresPermissions("business:rela:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtInterfaceParameterRela extInterfaceParameterRela)
    {
        startPage();
        List<ExtInterfaceParameterRela> list = extInterfaceParameterRelaService.selectExtInterfaceParameterRelaList(extInterfaceParameterRela);
        return getDataTable(list);
    }

    /**
     * 导出接口参数关系列表
     */
    @RequiresPermissions("business:rela:export")
    @Log(title = "接口参数关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtInterfaceParameterRela extInterfaceParameterRela)
    {
        List<ExtInterfaceParameterRela> list = extInterfaceParameterRelaService.selectExtInterfaceParameterRelaList(extInterfaceParameterRela);
        ExcelUtil<ExtInterfaceParameterRela> util = new ExcelUtil<ExtInterfaceParameterRela>(ExtInterfaceParameterRela.class);
        util.exportExcel(response, list, "接口参数关系数据");
    }

    /**
     * 获取接口参数关系详细信息
     */
    @RequiresPermissions("business:rela:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extInterfaceParameterRelaService.selectExtInterfaceParameterRelaById(id));
    }

    /**
     * 新增接口参数关系
     */
    @RequiresPermissions("business:rela:add")
    @Log(title = "接口参数关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty ExtInterfaceParameterRela extInterfaceParameterRela)
    {
        return toAjax(extInterfaceParameterRelaService.insertExtInterfaceParameterRela(extInterfaceParameterRela));
    }

    /**
     * 修改接口参数关系
     */
    @RequiresPermissions("business:rela:edit")
    @Log(title = "接口参数关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty ExtInterfaceParameterRela extInterfaceParameterRela)
    {
        return toAjax(extInterfaceParameterRelaService.updateExtInterfaceParameterRela(extInterfaceParameterRela));
    }

    /**
     * 删除接口参数关系
     */
    @RequiresPermissions("business:rela:remove")
    @Log(title = "接口参数关系", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extInterfaceParameterRelaService.deleteExtInterfaceParameterRelaByIds(ids));
    }
}
