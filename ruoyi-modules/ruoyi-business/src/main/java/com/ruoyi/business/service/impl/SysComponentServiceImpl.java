package com.ruoyi.business.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.SysComponentMapper;
import com.ruoyi.business.domain.SysComponent;
import com.ruoyi.business.service.ISysComponentService;

/**
 * 系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件Service业务层处理
 * 
 * @author ruoyi
 * @date 2025-02-16
 */
@Service
public class SysComponentServiceImpl implements ISysComponentService 
{
    @Autowired
    private SysComponentMapper sysComponentMapper;

    /**
     * 查询系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     * 
     * @param componentId 系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件主键
     * @return 系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     */
    @Override
    public SysComponent selectSysComponentByComponentId(Long componentId)
    {
        return sysComponentMapper.selectSysComponentByComponentId(componentId);
    }

    /**
     * 查询系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件列表
     * 
     * @param sysComponent 系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     * @return 系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     */
    @Override
    public List<SysComponent> selectSysComponentList(SysComponent sysComponent)
    {
        return sysComponentMapper.selectSysComponentList(sysComponent);
    }

    /**
     * 新增系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     * 
     * @param sysComponent 系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     * @return 结果
     */
    @Override
    public int insertSysComponent(SysComponent sysComponent)
    {
        sysComponent.setCreateTime(DateUtils.getNowDate());
        return sysComponentMapper.insertSysComponent(sysComponent);
    }

    /**
     * 修改系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     * 
     * @param sysComponent 系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     * @return 结果
     */
    @Override
    public int updateSysComponent(SysComponent sysComponent)
    {
        sysComponent.setUpdateTime(DateUtils.getNowDate());
        return sysComponentMapper.updateSysComponent(sysComponent);
    }

    /**
     * 批量删除系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件
     * 
     * @param componentIds 需要删除的系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件主键
     * @return 结果
     */
    @Override
    public int deleteSysComponentByComponentIds(Long[] componentIds)
    {
        return sysComponentMapper.deleteSysComponentByComponentIds(componentIds);
    }

    /**
     * 删除系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件信息
     * 
     * @param componentId 系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件主键
     * @return 结果
     */
    @Override
    public int deleteSysComponentByComponentId(Long componentId)
    {
        return sysComponentMapper.deleteSysComponentByComponentId(componentId);
    }
}
