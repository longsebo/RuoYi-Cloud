package com.ruoyi.business.service;

import org.apache.ibatis.session.SqlSession;

/**
 * 数据源服务接口
 */
public interface IDataSourceService {
    /**
     * 切换数据源
     * @param dataSourceName
     * @return
     */
    public SqlSession switchDataSource(String dataSourceName);
}
