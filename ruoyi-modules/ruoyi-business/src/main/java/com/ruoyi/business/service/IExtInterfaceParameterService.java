package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.domain.ExtInterfaceParameter;

/**
 * 接口参数Service接口
 *
 * @author ruoyi
 * @date 2024-01-22
 */
public interface IExtInterfaceParameterService
{
    /**
     * 查询接口参数
     *
     * @param id 接口参数主键
     * @return 接口参数
     */
    public ExtInterfaceParameter selectExtInterfaceParameterById(Long id);

    /**
     * 查询接口参数列表
     *
     * @param extInterfaceParameter 接口参数
     * @return 接口参数集合
     */
    public List<ExtInterfaceParameter> selectExtInterfaceParameterList(ExtInterfaceParameter extInterfaceParameter);

    /**
     * 新增接口参数
     *
     * @param extInterfaceParameter 接口参数
     * @return 结果
     */
    public int insertExtInterfaceParameter(ExtInterfaceParameter extInterfaceParameter);

    /**
     * 修改接口参数
     *
     * @param extInterfaceParameter 接口参数
     * @return 结果
     */
    public int updateExtInterfaceParameter(ExtInterfaceParameter extInterfaceParameter);

    /**
     * 批量删除接口参数
     *
     * @param ids 需要删除的接口参数主键集合
     * @return 结果
     */
    public int deleteExtInterfaceParameterByIds(Long[] ids);

    /**
     * 删除接口参数信息
     *
     * @param id 接口参数主键
     * @return 结果
     */
    public int deleteExtInterfaceParameterById(Long id);

    /**
     * 根据查询条件，返回一颗树列表
     * @param extInterfaceParameter
     * @return
     */
    List<ExtInterfaceParameter> selectTree(ExtInterfaceParameter extInterfaceParameter);
}
