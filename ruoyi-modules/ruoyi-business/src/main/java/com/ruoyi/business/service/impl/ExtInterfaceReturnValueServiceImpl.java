package com.ruoyi.business.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.ruoyi.business.domain.ExtInterfaceReturnValue;
import com.ruoyi.business.domain.ExtInterfaceReturnValueRela;
import com.ruoyi.business.mapper.ExtInterfaceReturnValueRelaMapper;
import com.ruoyi.common.core.exception.ServiceException;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.security.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.ExtInterfaceReturnValueMapper;
import com.ruoyi.business.domain.ExtInterfaceReturnValue;
import com.ruoyi.business.service.IExtInterfaceReturnValueService;

/**
 * 接口返回值Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-22
 */
@Service
public class ExtInterfaceReturnValueServiceImpl implements IExtInterfaceReturnValueService
{
    @Autowired
    private ExtInterfaceReturnValueMapper extInterfaceReturnValueMapper;
    @Autowired
    private ExtInterfaceReturnValueRelaMapper extInterfaceReturnValueRelaMapper;
    /**
     * 查询接口返回值
     *
     * @param id 接口返回值主键
     * @return 接口返回值
     */
    @Override
    public ExtInterfaceReturnValue selectExtInterfaceReturnValueById(Long id)
    {
        return extInterfaceReturnValueMapper.selectExtInterfaceReturnValueById(id);
    }

    /**
     * 查询接口返回值列表
     *
     * @param extInterfaceReturnValue 接口返回值
     * @return 接口返回值
     */
    @Override
    public List<ExtInterfaceReturnValue> selectExtInterfaceReturnValueList(ExtInterfaceReturnValue extInterfaceReturnValue)
    {
        return extInterfaceReturnValueMapper.selectExtInterfaceReturnValueList(extInterfaceReturnValue);
    }

    /**
     * 新增接口返回值
     *
     * @param extInterfaceReturnValue 接口返回值
     * @return 结果
     */
    @Override
    public int insertExtInterfaceReturnValue(ExtInterfaceReturnValue extInterfaceReturnValue)
    {
        //插入前校验
        checkInsert(extInterfaceReturnValue);

        int num = extInterfaceReturnValueMapper.insertExtInterfaceReturnValue(extInterfaceReturnValue);
        //同时插入接口返回值关系表
        ExtInterfaceReturnValueRela extInterfaceReturnValueRela = new ExtInterfaceReturnValueRela();
        extInterfaceReturnValueRela.setExtInterfaceReturnValueId(extInterfaceReturnValue.getId());
        extInterfaceReturnValueRela.setInterfaceCode(extInterfaceReturnValue.getInterfaceCode());
        extInterfaceReturnValueRela.setCreateBy(SecurityUtils.getUsername());
        extInterfaceReturnValueRela.setCreateTime(DateUtils.getNowDate());
        extInterfaceReturnValueRela.setEnterpriseCode(extInterfaceReturnValue.getEnterpriseCode());
        extInterfaceReturnValueRela.setApplicationCode(extInterfaceReturnValue.getApplicationCode());
        extInterfaceReturnValueRelaMapper.insertExtInterfaceReturnValueRela(extInterfaceReturnValueRela);
        return num;
    }

    /**
     * 插入前校验
     * @param extInterfaceReturnValue
     */
    private void checkInsert(ExtInterfaceReturnValue extInterfaceReturnValue) {
        //对象不能为空
        if(extInterfaceReturnValue==null)
            throw new ServiceException("对象不能为空!");
        //接口编码不能为空
        if(StringUtils.isEmpty(extInterfaceReturnValue.getInterfaceCode()))
            throw new ServiceException("接口编码不能为空!");
        //名称不能为空
        if(StringUtils.isEmpty(extInterfaceReturnValue.getReturnName()))
            throw new ServiceException("名称不能为空");
        //名称不能重复，在同一个接口下
        Map<String,Object> searchMap = new HashMap<>();
        searchMap.put("interfaceCode",extInterfaceReturnValue.getInterfaceCode());
        searchMap.put("returnName",extInterfaceReturnValue.getReturnName());
        if(extInterfaceReturnValueMapper.selectCount(searchMap)>0)
            throw new ServiceException("在同一接口下，返回名称:"+extInterfaceReturnValue.getReturnName()+"已经存在!");
        //描述不能为空
        if(StringUtils.isEmpty(extInterfaceReturnValue.getReturnDesc()))
            throw new ServiceException("返回描述不能为空！");
        //同一接口下，描述不能为重复
        searchMap.clear();
        searchMap.put("interfaceCode",extInterfaceReturnValue.getInterfaceCode());
        searchMap.put("returnDesc",extInterfaceReturnValue.getReturnDesc());
        if(extInterfaceReturnValueMapper.selectCount(searchMap)>0)
            throw new ServiceException("在同一接口下，返回描述:"+extInterfaceReturnValue.getReturnDesc()+"已经存在!");
        //类型不能为空
        if(StringUtils.isEmpty(extInterfaceReturnValue.getReturnType()))
            throw new ServiceException("返回类型不能为空！");
        //父id不为空，则必须有效
        if(extInterfaceReturnValue.getParentId()!=null && extInterfaceReturnValue.getParentId()!=0L){
            searchMap.clear();
            searchMap.put("id",extInterfaceReturnValue.getId());
            if(extInterfaceReturnValueMapper.selectCount(searchMap)==0)
                throw new ServiceException("父级id无效!");
        }

    }

    /**
     * 修改接口返回值
     *
     * @param extInterfaceReturnValue 接口返回值
     * @return 结果
     */
    @Override
    public int updateExtInterfaceReturnValue(ExtInterfaceReturnValue extInterfaceReturnValue)
    {
        //更新前校验
        checkUpdate(extInterfaceReturnValue);
        return extInterfaceReturnValueMapper.updateExtInterfaceReturnValue(extInterfaceReturnValue);
    }

    /**
     * 更新前校验
     * @param extInterfaceReturnValue
     */
    private void checkUpdate(ExtInterfaceReturnValue extInterfaceReturnValue) {
        //对象不能为空
        if(extInterfaceReturnValue==null)
            throw new ServiceException("对象不能为空!");
        //对象id不能为空
        if(extInterfaceReturnValue.getId()==null||extInterfaceReturnValue.getId()==0L)
            throw new ServiceException("对象id不能为空!");
        //接口编码不能为空
        if(StringUtils.isEmpty(extInterfaceReturnValue.getInterfaceCode()))
            throw new ServiceException("接口编码不能为空!");
        //名称不能为空
        if(StringUtils.isEmpty(extInterfaceReturnValue.getReturnName()))
            throw new ServiceException("名称不能为空");
        //名称不能重复，在同一个接口下
        Map<String,Object> searchMap = new HashMap<>();
        searchMap.put("interfaceCode",extInterfaceReturnValue.getInterfaceCode());
        searchMap.put("returnName",extInterfaceReturnValue.getReturnName());
        searchMap.put("notId",extInterfaceReturnValue.getId());
        if(extInterfaceReturnValueMapper.selectCount(searchMap)>0)
            throw new ServiceException("在同一接口下，返回名称:"+extInterfaceReturnValue.getReturnName()+"已经存在!");
        //描述不能为空
        if(StringUtils.isEmpty(extInterfaceReturnValue.getReturnDesc()))
            throw new ServiceException("返回描述不能为空！");
        //同一接口下，描述不能为重复
        searchMap.clear();
        searchMap.put("interfaceCode",extInterfaceReturnValue.getInterfaceCode());
        searchMap.put("returnDesc",extInterfaceReturnValue.getReturnDesc());
        searchMap.put("notId",extInterfaceReturnValue.getId());
        if(extInterfaceReturnValueMapper.selectCount(searchMap)>0)
            throw new ServiceException("在同一接口下，返回描述:"+extInterfaceReturnValue.getReturnDesc()+"已经存在!");
        //类型不能为空
        if(StringUtils.isEmpty(extInterfaceReturnValue.getReturnType()))
            throw new ServiceException("返回类型不能为空！");
        //父id不为空，则必须有效
        if(extInterfaceReturnValue.getParentId()!=null && extInterfaceReturnValue.getParentId()!=0L){
            searchMap.clear();
            searchMap.put("id",extInterfaceReturnValue.getId());
            if(extInterfaceReturnValueMapper.selectCount(searchMap)==0)
                throw new ServiceException("父级id无效!");
        }
    }

    /**
     * 批量删除接口返回值
     *
     * @param ids 需要删除的接口返回值主键
     * @return 结果
     */
    @Override
    public int deleteExtInterfaceReturnValueByIds(Long[] ids)
    {
        int sum = 0;
        for(Long id:ids){
            sum +=deleteExtInterfaceReturnValueById(id);
        }
        return sum;
    }

    /**
     * 删除接口返回值信息
     *
     * @param id 接口返回值主键
     * @return 结果
     */
    @Override
    public int deleteExtInterfaceReturnValueById(Long id)
    {
        //检查是否有下级依赖，有则不能删除则不允许删除
        Map<String,Object> searchMap = new HashMap<>();
        searchMap.put("parentId",id);
        if(extInterfaceReturnValueMapper.selectCount(searchMap)>0)
            throw new ServiceException("存在下级依赖，不允许删除!");
        //删除接口返回值关系表
        extInterfaceReturnValueRelaMapper.deleteByReturnValueId(id);
        return extInterfaceReturnValueMapper.deleteExtInterfaceReturnValueById(id);
    }

    /**
     * 根据查询条件，构造树列表
     *
     * @param extInterfaceReturnValue
     * @return
     */
    @Override
    public List<ExtInterfaceReturnValue> selectTree(ExtInterfaceReturnValue extInterfaceReturnValue) {
        //根据条件查询返回列表
        List <ExtInterfaceReturnValue> results = extInterfaceReturnValueMapper.selectExtInterfaceReturnValueList(extInterfaceReturnValue);
        //构造树列表
        List<ExtInterfaceReturnValue> treeList = buildTree(results);
        return treeList;
    }
    /**
     * 构造功能树
     * @param list
     * @return
     */
    private List<ExtInterfaceReturnValue> buildTree(List<ExtInterfaceReturnValue> list) {
        List<ExtInterfaceReturnValue> retList = new ArrayList<>();

        //循环构造第一级
        for(ExtInterfaceReturnValue extInterfaceReturnValue:list){
            if(extInterfaceReturnValue.getParentId()==null||extInterfaceReturnValue.getParentId()==0L){
                retList.add(recursionMakeTreeNode(extInterfaceReturnValue,list));
            }
        }
        return retList;
    }

    /**
     * 递归构造当前节点及下级节点
     * @param interfaceParameter
     * @param list
     * @return
     */
    private ExtInterfaceReturnValue recursionMakeTreeNode(ExtInterfaceReturnValue interfaceParameter, List<ExtInterfaceReturnValue> list) {
        //获取直接下级
        List<ExtInterfaceReturnValue> childList = list.stream().filter(item->interfaceParameter.getId().equals(item.getParentId())).collect(Collectors.toList());
        interfaceParameter.setChildren(childList);
        //递归下级
        for(ExtInterfaceReturnValue child:childList){
            recursionMakeTreeNode(child,list);
        }
        return interfaceParameter;
    }
}
