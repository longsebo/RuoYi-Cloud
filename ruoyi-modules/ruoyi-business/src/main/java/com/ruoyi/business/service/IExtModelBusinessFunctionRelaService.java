package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.domain.ExtModelBusinessFunctionRela;
import com.ruoyi.business.domain.ExtModelDef;

/**
 * 业务功能模型关系Service接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface IExtModelBusinessFunctionRelaService
{
    /**
     * 查询业务功能模型关系
     *
     * @param id 业务功能模型关系主键
     * @return 业务功能模型关系
     */
    public ExtModelBusinessFunctionRela selectExtModelBusinessFunctionRelaById(Long id);

    /**
     * 查询业务功能模型关系列表
     *
     * @param extModelBusinessFunctionRela 业务功能模型关系
     * @return 业务功能模型关系集合
     */
    public List<ExtModelBusinessFunctionRela> selectExtModelBusinessFunctionRelaList(ExtModelBusinessFunctionRela extModelBusinessFunctionRela);

    /**
     * 新增业务功能模型关系
     *
     * @param extModelBusinessFunctionRela 业务功能模型关系
     * @return 结果
     */
    public int insertExtModelBusinessFunctionRela(ExtModelBusinessFunctionRela extModelBusinessFunctionRela);

    /**
     * 修改业务功能模型关系
     *
     * @param extModelBusinessFunctionRela 业务功能模型关系
     * @return 结果
     */
    public int updateExtModelBusinessFunctionRela(ExtModelBusinessFunctionRela extModelBusinessFunctionRela);

    /**
     * 批量删除业务功能模型关系
     *
     * @param ids 需要删除的业务功能模型关系主键集合
     * @return 结果
     */
    public int deleteExtModelBusinessFunctionRelaByIds(Long[] ids);

    /**
     * 删除业务功能模型关系信息
     *
     * @param id 业务功能模型关系主键
     * @return 结果
     */
    public int deleteExtModelBusinessFunctionRelaById(Long id);

    /**
     * 根据条件删除记录
     * @param functionRela
     * @return
     */
    public int  deleteExtModelBusinessFunctionRela(ExtModelBusinessFunctionRela functionRela);
    /**
     * 更新功能模型关系表的数据源名称+表英文名
     * @param oldExtModelDef
     * @param extModelDef
     * @return
     */
    public int updateBusinessFunctionRelaDsEnName(ExtModelDef oldExtModelDef, ExtModelDef extModelDef);
}
