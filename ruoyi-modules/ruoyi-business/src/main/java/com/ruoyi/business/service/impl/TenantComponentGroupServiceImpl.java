package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.SysComponent;
import com.ruoyi.business.domain.SysComponentGroup;
import com.ruoyi.business.domain.TenantComponent;
import com.ruoyi.business.domain.TenantComponentGroup;
import com.ruoyi.business.mapper.TenantComponentGroupMapper;
import com.ruoyi.business.mapper.TenantComponentMapper;
import com.ruoyi.business.service.ISysComponentGroupService;
import com.ruoyi.business.service.ITenantComponentGroupService;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.UniqueIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 租户级别分组，由各自租户维护Service业务层处理
 *
 * @author ruoyi
 * @date 2025-02-16
 */
@Service
public class TenantComponentGroupServiceImpl implements ITenantComponentGroupService
{
    @Autowired
    private TenantComponentGroupMapper tenantComponentGroupMapper;
    @Autowired
    private ISysComponentGroupService sysComponentGroupService;
    @Autowired
    private TenantComponentMapper tenantComponentMapper;

    /**
     * 查询租户级别分组，由各自租户维护
     *
     * @param groupId 租户级别分组，由各自租户维护主键
     * @return 租户级别分组，由各自租户维护
     */
    @Override
    public TenantComponentGroup selectTenantComponentGroupByGroupId(Long groupId)
    {
        return tenantComponentGroupMapper.selectTenantComponentGroupByGroupId(groupId);
    }

    /**
     * 查询租户级别分组，由各自租户维护列表
     *
     * @param tenantComponentGroup 租户级别分组，由各自租户维护
     * @return 租户级别分组，由各自租户维护
     */
    @Override
    public List<TenantComponentGroup> selectTenantComponentGroupList(TenantComponentGroup tenantComponentGroup)
    {
        return tenantComponentGroupMapper.selectTenantComponentGroupList(tenantComponentGroup);
    }

    /**
     * 新增租户级别分组，由各自租户维护
     *
     * @param tenantComponentGroup 租户级别分组，由各自租户维护
     * @return 结果
     */
    @Override
    public int insertTenantComponentGroup(TenantComponentGroup tenantComponentGroup)
    {
        tenantComponentGroup.setCreateTime(DateUtils.getNowDate());
        TenantComponentGroup searchTenantComponentGroup = new TenantComponentGroup();
        //自动产生组件编码，并保证唯一
        String groupCode;
        while (true) {
            groupCode = UniqueIdGenerator.getUniqueId(10);
            searchTenantComponentGroup.setGroupCode(groupCode);
            List<TenantComponentGroup> list = tenantComponentGroupMapper.selectTenantComponentGroupList(searchTenantComponentGroup);
            if (list.size() == 0) {
                tenantComponentGroup.setGroupCode(groupCode);
                break;
            }
        }

        return tenantComponentGroupMapper.insertTenantComponentGroup(tenantComponentGroup);
    }

    /**
     * 修改租户级别分组，由各自租户维护
     *
     * @param tenantComponentGroup 租户级别分组，由各自租户维护
     * @return 结果
     */
    @Override
    public int updateTenantComponentGroup(TenantComponentGroup tenantComponentGroup)
    {
        tenantComponentGroup.setUpdateTime(DateUtils.getNowDate());
        return tenantComponentGroupMapper.updateTenantComponentGroup(tenantComponentGroup);
    }

    /**
     * 批量删除租户级别分组，由各自租户维护
     *
     * @param groupIds 需要删除的租户级别分组，由各自租户维护主键
     * @return 结果
     */
    @Override
    public int deleteTenantComponentGroupByGroupIds(Long[] groupIds)
    {
        return tenantComponentGroupMapper.deleteTenantComponentGroupByGroupIds(groupIds);
    }

    /**
     * 删除租户级别分组，由各自租户维护信息
     *
     * @param groupId 租户级别分组，由各自租户维护主键
     * @return 结果
     */
    @Override
    public int deleteTenantComponentGroupByGroupId(Long groupId)
    {
        return tenantComponentGroupMapper.deleteTenantComponentGroupByGroupId(groupId);
    }

    @Override
    public List<TenantComponentGroup> selectAllGroup() {
        //查询系统级别的组及组件
        List<SysComponentGroup> sysComponentGroups =sysComponentGroupService.selectAllGroup();
        //查询租户级别的组及组件

        TenantComponentGroup searchTenantComponentGroup = new TenantComponentGroup();
        searchTenantComponentGroup.setStatus("01");
        List<TenantComponentGroup> tenantComponentGroups = tenantComponentGroupMapper.selectTenantComponentGroupList(searchTenantComponentGroup);
        //收集组编码，放到列表
        List<String> groupCodes = new ArrayList<>();
        for (TenantComponentGroup tenantComponentGroup : tenantComponentGroups) {
            groupCodes.add(tenantComponentGroup.getGroupCode());
        }
        TenantComponent searchTenantComponent;
        searchTenantComponent = new TenantComponent();
        searchTenantComponent.setGroupCodes(groupCodes);
        searchTenantComponent.setStatus("01");
        if(groupCodes.size()>0) {
            List<TenantComponent> tenantComponents = tenantComponentMapper.selectTenantComponentList(searchTenantComponent);
            //循环tenantComponentGroups,根据组编码过滤tenantComponents
            for (TenantComponentGroup tenantComponentGroup1 : tenantComponentGroups) {
                //使用jdk8 stream 流过滤
                tenantComponentGroup1.setTenantComponents(tenantComponents.stream().filter(tenantComponent -> tenantComponentGroup1.getGroupCode().equals(tenantComponent.getGroupCode())).toList());
            }
        }
        //将sysComponentGroups转化为TenantComponentGroups
        for (SysComponentGroup sysComponentGroup1 : sysComponentGroups) {
            TenantComponentGroup tenantComponentGroup1 = new TenantComponentGroup();
            tenantComponentGroup1.setGroupCode(sysComponentGroup1.getGroupCode());
            tenantComponentGroup1.setGroupName(sysComponentGroup1.getGroupName());
            tenantComponentGroup1.setGroupIconUrl(sysComponentGroup1.getGroupIconUrl());
            tenantComponentGroup1.setRemark(sysComponentGroup1.getRemark());
            tenantComponentGroup1.setStatus(sysComponentGroup1.getStatus());
            tenantComponentGroup1.setSortNumber(sysComponentGroup1.getSortNumber());
            tenantComponentGroup1.setTenantComponents(toTenantComponents(sysComponentGroup1.getSysComponents()));
            tenantComponentGroups.add(tenantComponentGroup1);
        }

        return tenantComponentGroups;
    }

    /**
     * 将系统组件转换为租户组件
     * @param sysComponents
     * @return
     */
    private List<TenantComponent> toTenantComponents(List<SysComponent> sysComponents) {
        List<TenantComponent> tenantComponents = new ArrayList<>();
        for (SysComponent sysComponent : sysComponents) {
            TenantComponent tenantComponent = new TenantComponent();
            tenantComponent.setComponentId(sysComponent.getComponentId());
            tenantComponent.setComponentName(sysComponent.getComponentName());
            tenantComponent.setComponentIconUrl(sysComponent.getComponentIconUrl());
            tenantComponent.setClassName(sysComponent.getClassName());
            tenantComponent.setStatus(sysComponent.getStatus());
            tenantComponent.setSortNumber(sysComponent.getSortNumber());
            tenantComponent.setRemark(sysComponent.getRemark());
            tenantComponent.setGroupCode(sysComponent.getGroupCode());
            tenantComponents.add(tenantComponent);
        }
        return tenantComponents;
    }
}
