package com.ruoyi.business.entity;

import lombok.Data;

/**
 * 模型定义实例字段
 */
@Data
public class ExtModelDefInstanceField extends BaseAPI {
    /**
     * 字段名
     */
    private String fieldName;
    /**
     * 字段类型
     */
    private String fieldType;
    /**
     * 字段值
     */
    private Object fieldValue;
}
