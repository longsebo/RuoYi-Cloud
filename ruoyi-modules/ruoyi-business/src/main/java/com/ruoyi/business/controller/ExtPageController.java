package com.ruoyi.business.controller;

import com.ruoyi.business.domain.BusinessFunctionTree;
import com.ruoyi.business.domain.ExtPage;
import com.ruoyi.business.service.IExtPageService;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.DefaultCreateProperty;
import com.ruoyi.common.security.annotation.DefaultUpdateProperty;
import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 页面定义Controller
 *
 * @author ruoyi
 * @date 2024-01-28
 */
@RestController
@RequestMapping("/page")
public class ExtPageController extends BaseController
{
    @Autowired
    private IExtPageService extPageService;

    /**
     * 查询页面定义列表
     */
    @RequiresPermissions("business:page:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtPage extPage)
    {
        startPage();
        List<ExtPage> list = extPageService.selectExtPageList(extPage);
        return getDataTable(list);
    }

    /**
     * 导出页面定义列表
     */
    @RequiresPermissions("business:page:export")
    @Log(title = "页面定义", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtPage extPage)
    {
        List<ExtPage> list = extPageService.selectExtPageList(extPage);
        ExcelUtil<ExtPage> util = new ExcelUtil<ExtPage>(ExtPage.class);
        util.exportExcel(response, list, "页面定义数据");
    }

    /**
     * 获取页面定义详细信息
     */
    @RequiresPermissions("business:page:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extPageService.selectExtPageById(id));
    }

    /**
     * 新增页面定义
     */
    @RequiresPermissions("business:page:add")
    @Log(title = "页面定义", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@DefaultCreateProperty @EnterpriseAndApplicationCodeProperty ExtPage extPage)
    {
        return toAjax(extPageService.insertExtPage(extPage));
    }

    /**
     * 修改页面定义
     */
    @RequiresPermissions("business:page:edit")
    @Log(title = "页面定义", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@DefaultUpdateProperty @EnterpriseAndApplicationCodeProperty ExtPage extPage)
    {
        return toAjax(extPageService.updateExtPage(extPage));
    }

    /**
     * 删除页面定义
     */
    @RequiresPermissions("business:page:remove")
    @Log(title = "页面定义", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}/{businessCode}")
    public AjaxResult remove(@PathVariable Long[] ids,@PathVariable String businessCode)
    {
        return toAjax(extPageService.deleteExtPageByIds(ids,businessCode));
    }
    /**
     * 修改页面設計
     */
    @RequiresPermissions("business:page:editdesign")
    @Log(title = "页面定义", businessType = BusinessType.UPDATE)
    @PostMapping("/updatedesign")
    public AjaxResult editdesign(@DefaultUpdateProperty ExtPage extPage)
    {
        return toAjax(extPageService.updateDesign(extPage));
    }
    /**
     * 查询所有页面树列表
     */
    @RequiresPermissions("business:page:tree")
    @GetMapping("/tree")
    public AjaxResult tree(ExtPage extPage)
    {
        List<BusinessFunctionTree> list = extPageService.treeList(extPage);
        return success(list);
    }
    /**
     * 查询页面定义所有满足条件列表
     */
    @RequiresPermissions("business:page:listAll")
    @GetMapping("/listAll")
    public AjaxResult listAll(ExtPage extPage)
    {
        List<ExtPage> list = extPageService.selectExtPageList(extPage);
        List<BusinessFunctionTree> list1 = extPageService.toBusinessFunctionTree(list);
        return success(list1);
    }
    @GetMapping(value = "getPageByCode/{pageCode}/{enterpriseCode}/{applicationCode}")
    public AjaxResult getPageByCode(@PathVariable("pageCode") String pageCode,@PathVariable("enterpriseCode") String enterpriseCode,@PathVariable("applicationCode") String applicationCode){
        return success(extPageService.getPageByCode(pageCode,enterpriseCode,applicationCode));

    }
    @GetMapping(value = "getPageById/{pageId}/{enterpriseCode}/{applicationCode}")
    public AjaxResult getPageById(@PathVariable("pageId") Long pageId,@PathVariable("enterpriseCode") String enterpriseCode,@PathVariable("applicationCode") String applicationCode){
        return success(extPageService.getPageById(pageId,enterpriseCode,applicationCode));

    }
}
