package com.ruoyi.business.controller;

import com.ruoyi.business.domain.BusinessFunctionTree;
import com.ruoyi.business.domain.ExtBusinessFunction;
import com.ruoyi.business.service.IExtBusinessFunctionService;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 业务功能Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@RestController
@RequestMapping("/function")
public class ExtBusinessFunctionController extends BaseController
{
    @Autowired
    private IExtBusinessFunctionService extBusinessFunctionService;

    /**
     * 查询业务功能列表
     */
    @RequiresPermissions("business:function:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtBusinessFunction extBusinessFunction)
    {
        startPage();
        List<ExtBusinessFunction> list = extBusinessFunctionService.selectExtBusinessFunctionList(extBusinessFunction);
        return getDataTable(list);
    }

    /**
     * 导出业务功能列表
     */
    @RequiresPermissions("business:function:export")
    @Log(title = "业务功能", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtBusinessFunction extBusinessFunction)
    {
        List<ExtBusinessFunction> list = extBusinessFunctionService.selectExtBusinessFunctionList(extBusinessFunction);
        ExcelUtil<ExtBusinessFunction> util = new ExcelUtil<ExtBusinessFunction>(ExtBusinessFunction.class);
        util.exportExcel(response, list, "业务功能数据");
    }

    /**
     * 获取业务功能详细信息
     */
    @RequiresPermissions("business:function:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extBusinessFunctionService.selectExtBusinessFunctionById(id));
    }

    /**
     * 新增业务功能
     */
    @RequiresPermissions("business:function:add")
    @Log(title = "业务功能", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty ExtBusinessFunction extBusinessFunction)
    {
        return toAjax(extBusinessFunctionService.insertExtBusinessFunction(extBusinessFunction));
    }

    /**
     * 修改业务功能
     */
    @RequiresPermissions("business:function:edit")
    @Log(title = "业务功能", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty ExtBusinessFunction extBusinessFunction)
    {
        return toAjax(extBusinessFunctionService.updateExtBusinessFunction(extBusinessFunction));
    }

    /**
     * 删除业务功能
     */
    @RequiresPermissions("business:function:remove")
    @Log(title = "业务功能", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extBusinessFunctionService.deleteExtBusinessFunctionByIds(ids));
    }
    /**
     * 查询业务功能树
     */
    @RequiresPermissions("business:function:tree")
    @PostMapping("/tree")
    public AjaxResult tree(@RequestBody ExtBusinessFunction extBusinessFunction)
    {
        List<ExtBusinessFunction> list = extBusinessFunctionService.selectExtBusinessFunctionTree(extBusinessFunction);
        return success(list);
    }
    /**
     * 判断某业务编码是否为末级
     */
    @RequiresPermissions("business:function:isLastLevel")
    @PostMapping("/isLastLevel")
    public AjaxResult isLastLevel(@RequestBody ExtBusinessFunction extBusinessFunction)
    {
        boolean isLastLevel =  extBusinessFunctionService.isLastLevel(extBusinessFunction);
        return success(isLastLevel);
    }
    /**
     * 查询第一层业务功能列表
     */
    @RequiresPermissions("business:function:listAll")
    @GetMapping("/listAll")
    public AjaxResult listAll(ExtBusinessFunction extBusinessFunction)
    {
       List<BusinessFunctionTree> listTree = extBusinessFunctionService.selectCommonBusinessFunctionTree(extBusinessFunction);
       return success(listTree);
    }
}
