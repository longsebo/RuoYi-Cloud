package com.ruoyi.business.controller;

import com.ruoyi.business.domain.TenantFunctionParameters;
import com.ruoyi.business.service.ITenantFunctionParametersService;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 【请填写功能名称】Controller
 *
 * @author ruoyi
 * @date 2025-02-16
 */
@RestController
@RequestMapping("/tenantFunctionP")
public class TenantFunctionParametersController extends BaseController
{
    @Autowired
    private ITenantFunctionParametersService tenantFunctionParametersService;

    /**
     * 查询【请填写功能名称】列表
     */
//    @RequiresPermissions("business:parameters:list")
    @GetMapping("/list")
    public TableDataInfo list(TenantFunctionParameters tenantFunctionParameters)
    {
        startPage();
        List<TenantFunctionParameters> list = tenantFunctionParametersService.selectTenantFunctionParametersList(tenantFunctionParameters);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("business:parameters:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TenantFunctionParameters tenantFunctionParameters)
    {
        List<TenantFunctionParameters> list = tenantFunctionParametersService.selectTenantFunctionParametersList(tenantFunctionParameters);
        ExcelUtil<TenantFunctionParameters> util = new ExcelUtil<TenantFunctionParameters>(TenantFunctionParameters.class);
        util.exportExcel(response, list, "【请填写功能名称】数据");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @RequiresPermissions("business:parameters:query")
    @GetMapping(value = "/{parameterId}")
    public AjaxResult getInfo(@PathVariable("parameterId") Long parameterId)
    {
        return success(tenantFunctionParametersService.selectTenantFunctionParametersByParameterId(parameterId));
    }

    /**
     * 新增【请填写功能名称】
     */
    @RequiresPermissions("business:parameters:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty TenantFunctionParameters tenantFunctionParameters)
    {
        return toAjax(tenantFunctionParametersService.insertTenantFunctionParameters(tenantFunctionParameters));
    }

    /**
     * 修改【请填写功能名称】
     */
    @RequiresPermissions("business:parameters:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty TenantFunctionParameters tenantFunctionParameters)
    {
        return toAjax(tenantFunctionParametersService.updateTenantFunctionParameters(tenantFunctionParameters));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("business:parameters:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
	@DeleteMapping("/{parameterIds}")
    public AjaxResult remove(@PathVariable Long[] parameterIds)
    {
        return toAjax(tenantFunctionParametersService.deleteTenantFunctionParametersByParameterIds(parameterIds));
    }
}
