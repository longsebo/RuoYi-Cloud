package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

import javax.validation.constraints.NotBlank;

/**
 * 功能侦听对象 ext_business_function_listening
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public class ExtBusinessFunctionListening extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 应用编码 */
    @Excel(name = "应用编码")
    @NotBlank
    private String applicationCode;

    /** MQ名称 */
    @Excel(name = "MQ名称")
    @NotBlank
    private String mqName;

    /** 主题 */
    @Excel(name = "主题")
    @NotBlank
    private String topic;

    /** 标签 */
    @Excel(name = "标签")
    private String tag;

    /** 监听服务名称 */
    @Excel(name = "监听服务名称")
    @NotBlank
    private String listenServiceName;

    /** 转发主题 */
    @Excel(name = "转发主题")
    private String forwardTopic;

    /** 转发标签 */
    @Excel(name = "转发标签")
    private String forwardTag;

    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setApplicationCode(String applicationCode)
    {
        this.applicationCode = applicationCode;
    }

    public String getApplicationCode()
    {
        return applicationCode;
    }
    public void setMqName(String mqName)
    {
        this.mqName = mqName;
    }

    public String getMqName()
    {
        return mqName;
    }
    public void setTopic(String topic)
    {
        this.topic = topic;
    }

    public String getTopic()
    {
        return topic;
    }
    public void setTag(String tag)
    {
        this.tag = tag;
    }

    public String getTag()
    {
        return tag;
    }
    public void setListenServiceName(String listenServiceName)
    {
        this.listenServiceName = listenServiceName;
    }

    public String getListenServiceName()
    {
        return listenServiceName;
    }
    public void setForwardTopic(String forwardTopic)
    {
        this.forwardTopic = forwardTopic;
    }

    public String getForwardTopic()
    {
        return forwardTopic;
    }
    public void setForwardTag(String forwardTag)
    {
        this.forwardTag = forwardTag;
    }

    public String getForwardTag()
    {
        return forwardTag;
    }
    public void setEnterpriseCode(String enterpriseCode) 
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode() 
    {
        return enterpriseCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("applicationCode", getApplicationCode())
            .append("mqName", getMqName())
            .append("topic", getTopic())
            .append("tag", getTag())
            .append("listenServiceName", getListenServiceName())
            .append("forwardTopic", getForwardTopic())
            .append("forwardTag", getForwardTag())
            .append("remark", getRemark())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("enterpriseCode", getEnterpriseCode())
            .toString();
    }
}
