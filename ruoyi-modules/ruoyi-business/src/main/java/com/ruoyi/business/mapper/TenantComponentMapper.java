package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.domain.TenantComponent;

/**
 * 租户级别组件，是各个租户自定义组件Mapper接口
 * 
 * @author ruoyi
 * @date 2025-02-16
 */
public interface TenantComponentMapper 
{
    /**
     * 查询租户级别组件，是各个租户自定义组件
     * 
     * @param componentId 租户级别组件，是各个租户自定义组件主键
     * @return 租户级别组件，是各个租户自定义组件
     */
    public TenantComponent selectTenantComponentByComponentId(Long componentId);

    /**
     * 查询租户级别组件，是各个租户自定义组件列表
     * 
     * @param tenantComponent 租户级别组件，是各个租户自定义组件
     * @return 租户级别组件，是各个租户自定义组件集合
     */
    public List<TenantComponent> selectTenantComponentList(TenantComponent tenantComponent);

    /**
     * 新增租户级别组件，是各个租户自定义组件
     * 
     * @param tenantComponent 租户级别组件，是各个租户自定义组件
     * @return 结果
     */
    public int insertTenantComponent(TenantComponent tenantComponent);

    /**
     * 修改租户级别组件，是各个租户自定义组件
     * 
     * @param tenantComponent 租户级别组件，是各个租户自定义组件
     * @return 结果
     */
    public int updateTenantComponent(TenantComponent tenantComponent);

    /**
     * 删除租户级别组件，是各个租户自定义组件
     * 
     * @param componentId 租户级别组件，是各个租户自定义组件主键
     * @return 结果
     */
    public int deleteTenantComponentByComponentId(Long componentId);

    /**
     * 批量删除租户级别组件，是各个租户自定义组件
     * 
     * @param componentIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTenantComponentByComponentIds(Long[] componentIds);
}
