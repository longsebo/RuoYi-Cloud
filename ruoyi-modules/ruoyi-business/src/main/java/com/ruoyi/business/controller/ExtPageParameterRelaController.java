package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtPageParameterRela;
import com.ruoyi.business.service.IExtPageParameterRelaService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 页面参数关系Controller
 *
 * @author ruoyi
 * @date 2024-01-28
 */
@RestController
@RequestMapping("/pageParameterRela")
public class ExtPageParameterRelaController extends BaseController
{
    @Autowired
    private IExtPageParameterRelaService extPageParameterRelaService;

    /**
     * 查询页面参数关系列表
     */
    @RequiresPermissions("business:rela:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtPageParameterRela extPageParameterRela)
    {
        startPage();
        List<ExtPageParameterRela> list = extPageParameterRelaService.selectExtPageParameterRelaList(extPageParameterRela);
        return getDataTable(list);
    }

    /**
     * 导出页面参数关系列表
     */
    @RequiresPermissions("business:rela:export")
    @Log(title = "页面参数关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtPageParameterRela extPageParameterRela)
    {
        List<ExtPageParameterRela> list = extPageParameterRelaService.selectExtPageParameterRelaList(extPageParameterRela);
        ExcelUtil<ExtPageParameterRela> util = new ExcelUtil<ExtPageParameterRela>(ExtPageParameterRela.class);
        util.exportExcel(response, list, "页面参数关系数据");
    }

    /**
     * 获取页面参数关系详细信息
     */
    @RequiresPermissions("business:rela:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extPageParameterRelaService.selectExtPageParameterRelaById(id));
    }

    /**
     * 新增页面参数关系
     */
    @RequiresPermissions("business:rela:add")
    @Log(title = "页面参数关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty ExtPageParameterRela extPageParameterRela)
    {
        return toAjax(extPageParameterRelaService.insertExtPageParameterRela(extPageParameterRela));
    }

    /**
     * 修改页面参数关系
     */
    @RequiresPermissions("business:rela:edit")
    @Log(title = "页面参数关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty ExtPageParameterRela extPageParameterRela)
    {
        return toAjax(extPageParameterRelaService.updateExtPageParameterRela(extPageParameterRela));
    }

    /**
     * 删除页面参数关系
     */
    @RequiresPermissions("business:rela:remove")
    @Log(title = "页面参数关系", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extPageParameterRelaService.deleteExtPageParameterRelaByIds(ids));
    }
}
