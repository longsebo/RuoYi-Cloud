package com.ruoyi.business.service;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

/**
 * 查询服务
 */
public interface ISqlService {
    /**
     * 非翻页查询
     *
     * @param connection
     * @param connection  数据库连接
     * @param parameterMap     sql关联的参数
     * @return
     */
    public List<Map<String, Object>> search(Connection connection, String sql, Map<String,Object> parameterMap) throws Exception ;
    /**
     * 翻页查询
     *
     * @param connection
     * @param connection  数据库连接
     * @param parameterMap     sql关联的参数
     * @return
     */
    public List<Map<String, Object>> searchPage(Connection connection, String sql, Map<String,Object> parameterMap,int pageNo,int pageSize) throws Exception ;
}
