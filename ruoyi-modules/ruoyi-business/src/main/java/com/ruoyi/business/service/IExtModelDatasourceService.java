package com.ruoyi.business.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.ExtModelDatasource;

/**
 * 数据源定义Service接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface IExtModelDatasourceService
{
    /**
     * 查询数据源定义
     *
     * @param id 数据源定义主键
     * @return 数据源定义
     */
    public ExtModelDatasource selectExtModelDatasourceById(Long id);

    /**
     * 查询数据源定义列表
     *
     * @param extModelDatasource 数据源定义
     * @return 数据源定义集合
     */
    public List<ExtModelDatasource> selectExtModelDatasourceList(ExtModelDatasource extModelDatasource);

    /**
     * 新增数据源定义
     *
     * @param extModelDatasource 数据源定义
     * @return 结果
     */
    public int insertExtModelDatasource(ExtModelDatasource extModelDatasource);

    /**
     * 修改数据源定义
     *
     * @param extModelDatasource 数据源定义
     * @return 结果
     */
    public int updateExtModelDatasource(ExtModelDatasource extModelDatasource);

    /**
     * 批量删除数据源定义
     *
     * @param ids 需要删除的数据源定义主键集合
     * @return 结果
     */
    public int deleteExtModelDatasourceByIds(Long[] ids);

    /**
     * 删除数据源定义信息
     *
     * @param id 数据源定义主键
     * @return 结果
     */
    public int deleteExtModelDatasourceById(Long id);

    /**
     * 校验数据库连接
     * @param id
     */
    public void checkConnect(Long id);

    /**
     * 根据条件查询满足条数
     * @param searchMap 查询条件
     * @return 条数
     */
    int selectCount(Map<String, Object> searchMap);
}
