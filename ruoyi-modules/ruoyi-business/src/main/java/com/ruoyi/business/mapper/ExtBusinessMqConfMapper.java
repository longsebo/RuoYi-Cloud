package com.ruoyi.business.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.ExtBusinessMqConf;

/**
 * MQ配置定义Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface ExtBusinessMqConfMapper
{
    /**
     * 查询MQ配置定义
     *
     * @param id MQ配置定义主键
     * @return MQ配置定义
     */
    public ExtBusinessMqConf selectExtBusinessMqConfById(Long id);

    /**
     * 查询MQ配置定义列表
     *
     * @param extBusinessMqConf MQ配置定义
     * @return MQ配置定义集合
     */
    public List<ExtBusinessMqConf> selectExtBusinessMqConfList(ExtBusinessMqConf extBusinessMqConf);

    /**
     * 新增MQ配置定义
     *
     * @param extBusinessMqConf MQ配置定义
     * @return 结果
     */
    public int insertExtBusinessMqConf(ExtBusinessMqConf extBusinessMqConf);

    /**
     * 修改MQ配置定义
     *
     * @param extBusinessMqConf MQ配置定义
     * @return 结果
     */
    public int updateExtBusinessMqConf(ExtBusinessMqConf extBusinessMqConf);

    /**
     * 删除MQ配置定义
     *
     * @param id MQ配置定义主键
     * @return 结果
     */
    public int deleteExtBusinessMqConfById(Long id);

    /**
     * 批量删除MQ配置定义
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtBusinessMqConfByIds(Long[] ids);

    /**
     * 根据条件查询条数
     * @param searchMap
     * @return
     */
    public int selectCount(Map<String, Object> searchMap);
}
