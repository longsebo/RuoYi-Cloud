package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.domain.ExtModelOptionValue;

/**
 * 选项值Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
public interface ExtModelOptionValueMapper 
{
    /**
     * 查询选项值
     * 
     * @param id 选项值主键
     * @return 选项值
     */
    public ExtModelOptionValue selectExtModelOptionValueById(Long id);

    /**
     * 查询选项值列表
     * 
     * @param extModelOptionValue 选项值
     * @return 选项值集合
     */
    public List<ExtModelOptionValue> selectExtModelOptionValueList(ExtModelOptionValue extModelOptionValue);

    /**
     * 新增选项值
     * 
     * @param extModelOptionValue 选项值
     * @return 结果
     */
    public int insertExtModelOptionValue(ExtModelOptionValue extModelOptionValue);

    /**
     * 修改选项值
     * 
     * @param extModelOptionValue 选项值
     * @return 结果
     */
    public int updateExtModelOptionValue(ExtModelOptionValue extModelOptionValue);

    /**
     * 删除选项值
     * 
     * @param id 选项值主键
     * @return 结果
     */
    public int deleteExtModelOptionValueById(Long id);

    /**
     * 批量删除选项值
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtModelOptionValueByIds(Long[] ids);
}
