package com.ruoyi.business.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.ExtBusinessMqConf;

/**
 * MQ配置定义Service接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface IExtBusinessMqConfService
{
    /**
     * 查询MQ配置定义
     *
     * @param id MQ配置定义主键
     * @return MQ配置定义
     */
    public ExtBusinessMqConf selectExtBusinessMqConfById(Long id);

    /**
     * 查询MQ配置定义列表
     *
     * @param extBusinessMqConf MQ配置定义
     * @return MQ配置定义集合
     */
    public List<ExtBusinessMqConf> selectExtBusinessMqConfList(ExtBusinessMqConf extBusinessMqConf);

    /**
     * 新增MQ配置定义
     *
     * @param extBusinessMqConf MQ配置定义
     * @return 结果
     */
    public int insertExtBusinessMqConf(ExtBusinessMqConf extBusinessMqConf);

    /**
     * 修改MQ配置定义
     *
     * @param extBusinessMqConf MQ配置定义
     * @return 结果
     */
    public int updateExtBusinessMqConf(ExtBusinessMqConf extBusinessMqConf);

    /**
     * 批量删除MQ配置定义
     *
     * @param ids 需要删除的MQ配置定义主键集合
     * @return 结果
     */
    public int deleteExtBusinessMqConfByIds(Long[] ids);

    /**
     * 删除MQ配置定义信息
     *
     * @param id MQ配置定义主键
     * @return 结果
     */
    public int deleteExtBusinessMqConfById(Long id);

    /**
     * 查询满足条件数量
     * @param searchMap 查询条件
     * @return 返回满足条件数量
     */
    public int selectCount(Map<String, Object> searchMap);
}
