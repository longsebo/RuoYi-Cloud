package com.ruoyi.business.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.common.security.annotation.DefaultCreateProperty;
import com.ruoyi.common.security.annotation.DefaultUpdateProperty;
import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtIntefaceUrl;
import com.ruoyi.business.service.IExtIntefaceUrlService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 通用URLController
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@Slf4j
@RestController
@RequestMapping("/url")
public class ExtIntefaceUrlController extends BaseController
{
    @Autowired
    private IExtIntefaceUrlService extIntefaceUrlService;

    /**
     * 查询通用URL列表
     */
    @RequiresPermissions("business:url:list")
    @GetMapping("/list")
    public TableDataInfo list( ExtIntefaceUrl extIntefaceUrl)
    {
        startPage();
        List<ExtIntefaceUrl> list = extIntefaceUrlService.selectExtIntefaceUrlList(extIntefaceUrl);
        return getDataTable(list);
    }

    /**
     * 导出通用URL列表
     */
    @RequiresPermissions("business:url:export")
    @Log(title = "通用URL", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtIntefaceUrl extIntefaceUrl)
    {
        List<ExtIntefaceUrl> list = extIntefaceUrlService.selectExtIntefaceUrlList(extIntefaceUrl);
        ExcelUtil<ExtIntefaceUrl> util = new ExcelUtil<ExtIntefaceUrl>(ExtIntefaceUrl.class);
        util.exportExcel(response, list, "通用URL数据");
    }

    /**
     * 获取通用URL详细信息
     */
    @RequiresPermissions("business:url:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extIntefaceUrlService.selectExtIntefaceUrlById(id));
    }

    /**
     * 新增通用URL
     */
    @RequiresPermissions("business:url:add")
    @Log(title = "通用URL", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@DefaultCreateProperty @EnterpriseAndApplicationCodeProperty ExtIntefaceUrl extIntefaceUrl)
    {
        System.out.println("extIntefaceUrl:"+JSON.toJSONString(extIntefaceUrl));
        return toAjax(extIntefaceUrlService.insertExtIntefaceUrl(extIntefaceUrl));
    }

    /**
     * 修改通用URL
     */
    @RequiresPermissions("business:url:edit")
    @Log(title = "通用URL", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@DefaultUpdateProperty @EnterpriseAndApplicationCodeProperty ExtIntefaceUrl extIntefaceUrl)
    {
        System.out.println("extIntefaceUrl:"+JSON.toJSONString(extIntefaceUrl));
        return toAjax(extIntefaceUrlService.updateExtIntefaceUrl(extIntefaceUrl));
    }

    /**
     * 删除通用URL
     */
    @RequiresPermissions("business:url:remove")
    @Log(title = "通用URL", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extIntefaceUrlService.deleteExtIntefaceUrlByIds(ids));
    }
    /**
     * 查询所有通用URL列表
     */
    @RequiresPermissions("business:url:listAll")
    @GetMapping("/listAll")
    public AjaxResult listAll( ExtIntefaceUrl extIntefaceUrl)
    {
        List<ExtIntefaceUrl> list = extIntefaceUrlService.selectExtIntefaceUrlList(extIntefaceUrl);
        return success(list);
    }
}
