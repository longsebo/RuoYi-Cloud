package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.domain.SysFunctionParameters;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2025-02-16
 */
public interface SysFunctionParametersMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param parameterId 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public SysFunctionParameters selectSysFunctionParametersByParameterId(Long parameterId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param sysFunctionParameters 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<SysFunctionParameters> selectSysFunctionParametersList(SysFunctionParameters sysFunctionParameters);

    /**
     * 新增【请填写功能名称】
     * 
     * @param sysFunctionParameters 【请填写功能名称】
     * @return 结果
     */
    public int insertSysFunctionParameters(SysFunctionParameters sysFunctionParameters);

    /**
     * 修改【请填写功能名称】
     * 
     * @param sysFunctionParameters 【请填写功能名称】
     * @return 结果
     */
    public int updateSysFunctionParameters(SysFunctionParameters sysFunctionParameters);

    /**
     * 删除【请填写功能名称】
     * 
     * @param parameterId 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteSysFunctionParametersByParameterId(Long parameterId);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param parameterIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysFunctionParametersByParameterIds(Long[] parameterIds);
}
