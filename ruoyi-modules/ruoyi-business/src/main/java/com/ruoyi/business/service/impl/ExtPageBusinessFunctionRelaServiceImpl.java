package com.ruoyi.business.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.ExtPageBusinessFunctionRelaMapper;
import com.ruoyi.business.domain.ExtPageBusinessFunctionRela;
import com.ruoyi.business.service.IExtPageBusinessFunctionRelaService;

/**
 * 业务功能页面关系Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-28
 */
@Service
public class ExtPageBusinessFunctionRelaServiceImpl implements IExtPageBusinessFunctionRelaService 
{
    @Autowired
    private ExtPageBusinessFunctionRelaMapper extPageBusinessFunctionRelaMapper;

    /**
     * 查询业务功能页面关系
     * 
     * @param id 业务功能页面关系主键
     * @return 业务功能页面关系
     */
    @Override
    public ExtPageBusinessFunctionRela selectExtPageBusinessFunctionRelaById(Long id)
    {
        return extPageBusinessFunctionRelaMapper.selectExtPageBusinessFunctionRelaById(id);
    }

    /**
     * 查询业务功能页面关系列表
     * 
     * @param extPageBusinessFunctionRela 业务功能页面关系
     * @return 业务功能页面关系
     */
    @Override
    public List<ExtPageBusinessFunctionRela> selectExtPageBusinessFunctionRelaList(ExtPageBusinessFunctionRela extPageBusinessFunctionRela)
    {
        return extPageBusinessFunctionRelaMapper.selectExtPageBusinessFunctionRelaList(extPageBusinessFunctionRela);
    }

    /**
     * 新增业务功能页面关系
     * 
     * @param extPageBusinessFunctionRela 业务功能页面关系
     * @return 结果
     */
    @Override
    public int insertExtPageBusinessFunctionRela(ExtPageBusinessFunctionRela extPageBusinessFunctionRela)
    {
        extPageBusinessFunctionRela.setCreateTime(DateUtils.getNowDate());
        return extPageBusinessFunctionRelaMapper.insertExtPageBusinessFunctionRela(extPageBusinessFunctionRela);
    }

    /**
     * 修改业务功能页面关系
     * 
     * @param extPageBusinessFunctionRela 业务功能页面关系
     * @return 结果
     */
    @Override
    public int updateExtPageBusinessFunctionRela(ExtPageBusinessFunctionRela extPageBusinessFunctionRela)
    {
        extPageBusinessFunctionRela.setUpdateTime(DateUtils.getNowDate());
        return extPageBusinessFunctionRelaMapper.updateExtPageBusinessFunctionRela(extPageBusinessFunctionRela);
    }

    /**
     * 批量删除业务功能页面关系
     * 
     * @param ids 需要删除的业务功能页面关系主键
     * @return 结果
     */
    @Override
    public int deleteExtPageBusinessFunctionRelaByIds(Long[] ids)
    {
        return extPageBusinessFunctionRelaMapper.deleteExtPageBusinessFunctionRelaByIds(ids);
    }

    /**
     * 删除业务功能页面关系信息
     * 
     * @param id 业务功能页面关系主键
     * @return 结果
     */
    @Override
    public int deleteExtPageBusinessFunctionRelaById(Long id)
    {
        return extPageBusinessFunctionRelaMapper.deleteExtPageBusinessFunctionRelaById(id);
    }
}
