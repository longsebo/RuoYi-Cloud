package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.TenantFunction;
import com.ruoyi.business.service.ITenantFunctionService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 对外宣传为功能点，即开发人员的函数Controller
 *
 * @author ruoyi
 * @date 2025-02-16
 */
@RestController
@RequestMapping("/tenantFunction")
public class TenantFunctionController extends BaseController
{
    @Autowired
    private ITenantFunctionService tenantFunctionService;

    /**
     * 查询对外宣传为功能点，即开发人员的函数列表
     */
    @RequiresPermissions("business:tenantfunction:list")
    @GetMapping("/list")
    public TableDataInfo list(TenantFunction tenantFunction)
    {
        startPage();
        List<TenantFunction> list = tenantFunctionService.selectTenantFunctionList(tenantFunction);
        return getDataTable(list);
    }

    /**
     * 导出对外宣传为功能点，即开发人员的函数列表
     */
    @RequiresPermissions("business:tenantfunction:export")
    @Log(title = "对外宣传为功能点，即开发人员的函数", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TenantFunction tenantFunction)
    {
        List<TenantFunction> list = tenantFunctionService.selectTenantFunctionList(tenantFunction);
        ExcelUtil<TenantFunction> util = new ExcelUtil<TenantFunction>(TenantFunction.class);
        util.exportExcel(response, list, "对外宣传为功能点，即开发人员的函数数据");
    }

    /**
     * 获取对外宣传为功能点，即开发人员的函数详细信息
     */
    @RequiresPermissions("business:tenantfunction:query")
    @GetMapping(value = "/{functionId}")
    public AjaxResult getInfo(@PathVariable("functionId") Long functionId)
    {
        return success(tenantFunctionService.selectTenantFunctionByFunctionId(functionId));
    }

    /**
     * 新增对外宣传为功能点，即开发人员的函数
     */
    @RequiresPermissions("business:tenantfunction:add")
    @Log(title = "对外宣传为功能点，即开发人员的函数", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty TenantFunction tenantFunction)
    {
        return toAjax(tenantFunctionService.insertTenantFunction(tenantFunction));
    }

    /**
     * 修改对外宣传为功能点，即开发人员的函数
     */
    @RequiresPermissions("business:tenantfunction:edit")
    @Log(title = "对外宣传为功能点，即开发人员的函数", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty TenantFunction tenantFunction)
    {
        return toAjax(tenantFunctionService.updateTenantFunction(tenantFunction));
    }

    /**
     * 删除对外宣传为功能点，即开发人员的函数
     */
    @RequiresPermissions("business:tenantfunction:remove")
    @Log(title = "对外宣传为功能点，即开发人员的函数", businessType = BusinessType.DELETE)
	@DeleteMapping("/{functionIds}")
    public AjaxResult remove(@PathVariable Long[] functionIds)
    {
        return toAjax(tenantFunctionService.deleteTenantFunctionByFunctionIds(functionIds));
    }
}
