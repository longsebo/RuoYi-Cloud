package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.domain.ExtPageParameterRela;
import org.apache.ibatis.annotations.Param;

/**
 * 页面参数关系Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-28
 */
public interface ExtPageParameterRelaMapper
{
    /**
     * 查询页面参数关系
     *
     * @param id 页面参数关系主键
     * @return 页面参数关系
     */
    public ExtPageParameterRela selectExtPageParameterRelaById(Long id);

    /**
     * 查询页面参数关系列表
     *
     * @param extPageParameterRela 页面参数关系
     * @return 页面参数关系集合
     */
    public List<ExtPageParameterRela> selectExtPageParameterRelaList(ExtPageParameterRela extPageParameterRela);

    /**
     * 新增页面参数关系
     *
     * @param extPageParameterRela 页面参数关系
     * @return 结果
     */
    public int insertExtPageParameterRela(ExtPageParameterRela extPageParameterRela);

    /**
     * 修改页面参数关系
     *
     * @param extPageParameterRela 页面参数关系
     * @return 结果
     */
    public int updateExtPageParameterRela(ExtPageParameterRela extPageParameterRela);

    /**
     * 删除页面参数关系
     *
     * @param id 页面参数关系主键
     * @return 结果
     */
    public int deleteExtPageParameterRelaById(Long id);

    /**
     * 批量删除页面参数关系
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtPageParameterRelaByIds(Long[] ids);

    /**
     * 根据参数id删除页面参数关系
     * @param id
     * @return
     */
    public int  deleteExtPageParameterRelaByParameterId(@Param("pageParameterId") Long id);
}
