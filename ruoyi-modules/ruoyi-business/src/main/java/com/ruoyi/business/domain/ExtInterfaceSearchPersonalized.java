package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 查询接口个性化对象 ext_interface_search_personalized
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
public class ExtInterfaceSearchPersonalized extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 接口表id */
    @Excel(name = "接口表id")
    private Long extInterfaceId;

    /** 查询sql设计json */
    @Excel(name = "查询sql设计json")
    private String designSql;

    /** 产生sql */
    @Excel(name = "产生sql")
    private String produceSql;

    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setExtInterfaceId(Long extInterfaceId) 
    {
        this.extInterfaceId = extInterfaceId;
    }

    public Long getExtInterfaceId() 
    {
        return extInterfaceId;
    }
    public void setDesignSql(String designSql) 
    {
        this.designSql = designSql;
    }

    public String getDesignSql() 
    {
        return designSql;
    }
    public void setProduceSql(String produceSql) 
    {
        this.produceSql = produceSql;
    }

    public String getProduceSql() 
    {
        return produceSql;
    }
    public void setEnterpriseCode(String enterpriseCode) 
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode() 
    {
        return enterpriseCode;
    }
    public void setApplicationCode(String applicationCode) 
    {
        this.applicationCode = applicationCode;
    }

    public String getApplicationCode() 
    {
        return applicationCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("extInterfaceId", getExtInterfaceId())
            .append("designSql", getDesignSql())
            .append("produceSql", getProduceSql())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
