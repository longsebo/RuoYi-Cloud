/**
 *
 */
package com.ruoyi.business.domain.sqldesign;

/**
 * @author Administrator
 * 	排序列单项模型
 */
public class SortColumnItemModel extends AvailableColumnItemModel {
	/**
	 * 是否倒排
	 */
	private boolean descending;

	/**
	 * @return the descending
	 */
	public boolean isDescending() {
		return descending;
	}
	/**
	 * @param descending the descending to set
	 */
	public void setDescending(boolean descending) {
		this.descending = descending;
	}


}
