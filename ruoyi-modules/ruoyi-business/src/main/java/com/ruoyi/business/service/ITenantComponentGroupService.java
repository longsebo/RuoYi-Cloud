package com.ruoyi.business.service;

import com.ruoyi.business.domain.TenantComponentGroup;

import java.util.List;

/**
 * 租户级别分组，由各自租户维护Service接口
 *
 * @author ruoyi
 * @date 2025-02-16
 */
public interface ITenantComponentGroupService
{
    /**
     * 查询租户级别分组，由各自租户维护
     *
     * @param groupId 租户级别分组，由各自租户维护主键
     * @return 租户级别分组，由各自租户维护
     */
    public TenantComponentGroup selectTenantComponentGroupByGroupId(Long groupId);

    /**
     * 查询租户级别分组，由各自租户维护列表
     *
     * @param tenantComponentGroup 租户级别分组，由各自租户维护
     * @return 租户级别分组，由各自租户维护集合
     */
    public List<TenantComponentGroup> selectTenantComponentGroupList(TenantComponentGroup tenantComponentGroup);

    /**
     * 新增租户级别分组，由各自租户维护
     *
     * @param tenantComponentGroup 租户级别分组，由各自租户维护
     * @return 结果
     */
    public int insertTenantComponentGroup(TenantComponentGroup tenantComponentGroup);

    /**
     * 修改租户级别分组，由各自租户维护
     *
     * @param tenantComponentGroup 租户级别分组，由各自租户维护
     * @return 结果
     */
    public int updateTenantComponentGroup(TenantComponentGroup tenantComponentGroup);

    /**
     * 批量删除租户级别分组，由各自租户维护
     *
     * @param groupIds 需要删除的租户级别分组，由各自租户维护主键集合
     * @return 结果
     */
    public int deleteTenantComponentGroupByGroupIds(Long[] groupIds);

    /**
     * 删除租户级别分组，由各自租户维护信息
     *
     * @param groupId 租户级别分组，由各自租户维护主键
     * @return 结果
     */
    public int deleteTenantComponentGroupByGroupId(Long groupId);

    /**
     * 查询系统及当前租户的所有组及各组组件
     * @param tenantComponentGroup
     * @return
     */
    List<TenantComponentGroup> selectAllGroup();
}
