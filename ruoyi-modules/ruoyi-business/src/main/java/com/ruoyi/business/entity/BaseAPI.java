package com.ruoyi.business.entity;

import lombok.Data;

/**
 * 基本API公用参数
 */
@Data
public class BaseAPI {
    /** 接口编码 */
    private String interfaceCode;
    /**
     * id
     */
    private Long id;
}
