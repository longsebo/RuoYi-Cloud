package com.ruoyi.business.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.List;

/**
 * 业务功能对象 ext_business_function
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public class ExtBusinessFunction extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 业务编码 */
    @Excel(name = "业务编码")
    private String businessCode;

    /** 业务名称 */
    @Excel(name = "业务名称")
    private String businessName;

    /** 父级编码 */
    @Excel(name = "父级编码")
    private String parentCode;
    /**
     * 第一层
     */
    private String firstTop;
    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;
    public String getFirstTop() {
        return firstTop;
    }

    public void setFirstTop(String firstTop) {
        this.firstTop = firstTop;
    }


    public List<ExtBusinessFunction> getChildren() {
        return children;
    }

    public void setChildren(List<ExtBusinessFunction> children) {
        this.children = children;
    }

    /** 子功能 */
    private List<ExtBusinessFunction> children = new ArrayList<ExtBusinessFunction>();

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setBusinessCode(String businessCode)
    {
        this.businessCode = businessCode;
    }

    public String getBusinessCode()
    {
        return businessCode;
    }
    public void setBusinessName(String businessName)
    {
        this.businessName = businessName;
    }

    public String getBusinessName()
    {
        return businessName;
    }
    public void setParentCode(String parentCode)
    {
        this.parentCode = parentCode;
    }

    public String getParentCode()
    {
        return parentCode;
    }
	public void setEnterpriseCode(String enterpriseCode) 
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode() 
    {
        return enterpriseCode;
    }
    public void setApplicationCode(String applicationCode) 
    {
        this.applicationCode = applicationCode;
    }

    public String getApplicationCode() 
    {
        return applicationCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("businessCode", getBusinessCode())
            .append("businessName", getBusinessName())
            .append("parentCode", getParentCode())
            .append("remark", getRemark())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
