package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtModelFieldRef;
import com.ruoyi.business.service.IExtModelFieldRefService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 模型字段引用Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@RestController
@RequestMapping("/modelFieldRef")
public class ExtModelFieldRefController extends BaseController
{
    @Autowired
    private IExtModelFieldRefService extModelFieldRefService;

    /**
     * 查询模型字段引用列表
     */
    @RequiresPermissions("business:ref:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtModelFieldRef extModelFieldRef)
    {
        startPage();
        List<ExtModelFieldRef> list = extModelFieldRefService.selectExtModelFieldRefList(extModelFieldRef);
        return getDataTable(list);
    }

    /**
     * 导出模型字段引用列表
     */
    @RequiresPermissions("business:ref:export")
    @Log(title = "模型字段引用", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtModelFieldRef extModelFieldRef)
    {
        List<ExtModelFieldRef> list = extModelFieldRefService.selectExtModelFieldRefList(extModelFieldRef);
        ExcelUtil<ExtModelFieldRef> util = new ExcelUtil<ExtModelFieldRef>(ExtModelFieldRef.class);
        util.exportExcel(response, list, "模型字段引用数据");
    }

    /**
     * 获取模型字段引用详细信息
     */
    @RequiresPermissions("business:ref:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extModelFieldRefService.selectExtModelFieldRefById(id));
    }

    /**
     * 新增模型字段引用
     */
    @RequiresPermissions("business:ref:add")
    @Log(title = "模型字段引用", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty ExtModelFieldRef extModelFieldRef)
    {
        return toAjax(extModelFieldRefService.insertExtModelFieldRef(extModelFieldRef));
    }

    /**
     * 修改模型字段引用
     */
    @RequiresPermissions("business:ref:edit")
    @Log(title = "模型字段引用", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty ExtModelFieldRef extModelFieldRef)
    {
        return toAjax(extModelFieldRefService.updateExtModelFieldRef(extModelFieldRef));
    }

    /**
     * 删除模型字段引用
     */
    @RequiresPermissions("business:ref:remove")
    @Log(title = "模型字段引用", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extModelFieldRefService.deleteExtModelFieldRefByIds(ids));
    }
}
