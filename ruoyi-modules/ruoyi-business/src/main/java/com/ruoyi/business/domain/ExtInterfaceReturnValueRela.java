package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 接口返回值关系对象 ext_interface_return_value_rela
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
public class ExtInterfaceReturnValueRela extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 接口编码 */
    @Excel(name = "接口编码")
    private String interfaceCode;

    /** 接口返回值ID */
    @Excel(name = "接口返回值ID")
    private Long extInterfaceReturnValueId;

    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setInterfaceCode(String interfaceCode) 
    {
        this.interfaceCode = interfaceCode;
    }

    public String getInterfaceCode() 
    {
        return interfaceCode;
    }
    public void setExtInterfaceReturnValueId(Long extInterfaceReturnValueId) 
    {
        this.extInterfaceReturnValueId = extInterfaceReturnValueId;
    }

    public Long getExtInterfaceReturnValueId() 
    {
        return extInterfaceReturnValueId;
    }
    public void setEnterpriseCode(String enterpriseCode) 
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode() 
    {
        return enterpriseCode;
    }
    public void setApplicationCode(String applicationCode) 
    {
        this.applicationCode = applicationCode;
    }

    public String getApplicationCode() 
    {
        return applicationCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("interfaceCode", getInterfaceCode())
            .append("extInterfaceReturnValueId", getExtInterfaceReturnValueId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
