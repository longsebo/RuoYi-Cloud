package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.domain.ExtPageBusinessFunctionRela;

/**
 * 业务功能页面关系Service接口
 * 
 * @author ruoyi
 * @date 2024-01-28
 */
public interface IExtPageBusinessFunctionRelaService 
{
    /**
     * 查询业务功能页面关系
     * 
     * @param id 业务功能页面关系主键
     * @return 业务功能页面关系
     */
    public ExtPageBusinessFunctionRela selectExtPageBusinessFunctionRelaById(Long id);

    /**
     * 查询业务功能页面关系列表
     * 
     * @param extPageBusinessFunctionRela 业务功能页面关系
     * @return 业务功能页面关系集合
     */
    public List<ExtPageBusinessFunctionRela> selectExtPageBusinessFunctionRelaList(ExtPageBusinessFunctionRela extPageBusinessFunctionRela);

    /**
     * 新增业务功能页面关系
     * 
     * @param extPageBusinessFunctionRela 业务功能页面关系
     * @return 结果
     */
    public int insertExtPageBusinessFunctionRela(ExtPageBusinessFunctionRela extPageBusinessFunctionRela);

    /**
     * 修改业务功能页面关系
     * 
     * @param extPageBusinessFunctionRela 业务功能页面关系
     * @return 结果
     */
    public int updateExtPageBusinessFunctionRela(ExtPageBusinessFunctionRela extPageBusinessFunctionRela);

    /**
     * 批量删除业务功能页面关系
     * 
     * @param ids 需要删除的业务功能页面关系主键集合
     * @return 结果
     */
    public int deleteExtPageBusinessFunctionRelaByIds(Long[] ids);

    /**
     * 删除业务功能页面关系信息
     * 
     * @param id 业务功能页面关系主键
     * @return 结果
     */
    public int deleteExtPageBusinessFunctionRelaById(Long id);
}
