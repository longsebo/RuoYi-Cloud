package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.domain.SysComponentGroup;

/**
 * 系统级别分组，最基本的组。一般不会变化。除非系统版本升级Mapper接口
 * 
 * @author ruoyi
 * @date 2025-02-16
 */
public interface SysComponentGroupMapper 
{
    /**
     * 查询系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     * 
     * @param groupId 系统级别分组，最基本的组。一般不会变化。除非系统版本升级主键
     * @return 系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     */
    public SysComponentGroup selectSysComponentGroupByGroupId(Long groupId);

    /**
     * 查询系统级别分组，最基本的组。一般不会变化。除非系统版本升级列表
     * 
     * @param sysComponentGroup 系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     * @return 系统级别分组，最基本的组。一般不会变化。除非系统版本升级集合
     */
    public List<SysComponentGroup> selectSysComponentGroupList(SysComponentGroup sysComponentGroup);

    /**
     * 新增系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     * 
     * @param sysComponentGroup 系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     * @return 结果
     */
    public int insertSysComponentGroup(SysComponentGroup sysComponentGroup);

    /**
     * 修改系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     * 
     * @param sysComponentGroup 系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     * @return 结果
     */
    public int updateSysComponentGroup(SysComponentGroup sysComponentGroup);

    /**
     * 删除系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     * 
     * @param groupId 系统级别分组，最基本的组。一般不会变化。除非系统版本升级主键
     * @return 结果
     */
    public int deleteSysComponentGroupByGroupId(Long groupId);

    /**
     * 批量删除系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     * 
     * @param groupIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysComponentGroupByGroupIds(Long[] groupIds);
}
