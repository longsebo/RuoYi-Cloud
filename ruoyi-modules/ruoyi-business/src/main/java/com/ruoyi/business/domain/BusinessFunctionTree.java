package com.ruoyi.business.domain;

import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 业务功能树节点
 */
@Data
public class BusinessFunctionTree implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 业务功能类型
     */
    public static String NODE_TYPE_BUSINESS = "bs";
    /**
     * 页面类型
     */
    public static String NODE_TYPE_PAGE = "page";
    /**
     * 页面控件
     */
    public static String NODE_TYPE_PAGE_CONTROL = "page_control";
    /** 业务编码或末级编码 */
    private String value;

    /** 业务名称或末级名称 */
    private String label;

    /** 父级编码 */
    private String parentCode;
    /**
     * 节点类型
     */
    private String nodeType;
    /**
     * 节点数据
     */
    private String nodeData;
    /** 子功能,可能为业务功能或挂在功能上接口，页面等 */
    private List<BusinessFunctionTree> children;
    /**
     * 是否有子节点
     */
    private boolean hasChildren;
}
