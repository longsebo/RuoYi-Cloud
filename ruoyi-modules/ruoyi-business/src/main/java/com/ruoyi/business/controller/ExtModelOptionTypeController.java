package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtModelOptionType;
import com.ruoyi.business.service.IExtModelOptionTypeService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 选项类型Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@RestController
@RequestMapping("/optionType")
public class ExtModelOptionTypeController extends BaseController
{
    @Autowired
    private IExtModelOptionTypeService extModelOptionTypeService;

    /**
     * 查询选项类型列表
     */
    @RequiresPermissions("business:type:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtModelOptionType extModelOptionType)
    {
        startPage();
        List<ExtModelOptionType> list = extModelOptionTypeService.selectExtModelOptionTypeList(extModelOptionType);
        return getDataTable(list);
    }

    /**
     * 导出选项类型列表
     */
    @RequiresPermissions("business:type:export")
    @Log(title = "选项类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtModelOptionType extModelOptionType)
    {
        List<ExtModelOptionType> list = extModelOptionTypeService.selectExtModelOptionTypeList(extModelOptionType);
        ExcelUtil<ExtModelOptionType> util = new ExcelUtil<ExtModelOptionType>(ExtModelOptionType.class);
        util.exportExcel(response, list, "选项类型数据");
    }

    /**
     * 获取选项类型详细信息
     */
    @RequiresPermissions("business:type:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extModelOptionTypeService.selectExtModelOptionTypeById(id));
    }

    /**
     * 新增选项类型
     */
    @RequiresPermissions("business:type:add")
    @Log(title = "选项类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty ExtModelOptionType extModelOptionType)
    {
        return toAjax(extModelOptionTypeService.insertExtModelOptionType(extModelOptionType));
    }

    /**
     * 修改选项类型
     */
    @RequiresPermissions("business:type:edit")
    @Log(title = "选项类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty ExtModelOptionType extModelOptionType)
    {
        return toAjax(extModelOptionTypeService.updateExtModelOptionType(extModelOptionType));
    }

    /**
     * 删除选项类型
     */
    @RequiresPermissions("business:type:remove")
    @Log(title = "选项类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extModelOptionTypeService.deleteExtModelOptionTypeByIds(ids));
    }
}
