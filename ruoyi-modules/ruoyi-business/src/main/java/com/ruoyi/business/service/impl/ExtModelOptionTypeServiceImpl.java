package com.ruoyi.business.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.ExtModelOptionTypeMapper;
import com.ruoyi.business.domain.ExtModelOptionType;
import com.ruoyi.business.service.IExtModelOptionTypeService;

/**
 * 选项类型Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
@Service
public class ExtModelOptionTypeServiceImpl implements IExtModelOptionTypeService 
{
    @Autowired
    private ExtModelOptionTypeMapper extModelOptionTypeMapper;

    /**
     * 查询选项类型
     * 
     * @param id 选项类型主键
     * @return 选项类型
     */
    @Override
    public ExtModelOptionType selectExtModelOptionTypeById(Long id)
    {
        return extModelOptionTypeMapper.selectExtModelOptionTypeById(id);
    }

    /**
     * 查询选项类型列表
     * 
     * @param extModelOptionType 选项类型
     * @return 选项类型
     */
    @Override
    public List<ExtModelOptionType> selectExtModelOptionTypeList(ExtModelOptionType extModelOptionType)
    {
        return extModelOptionTypeMapper.selectExtModelOptionTypeList(extModelOptionType);
    }

    /**
     * 新增选项类型
     * 
     * @param extModelOptionType 选项类型
     * @return 结果
     */
    @Override
    public int insertExtModelOptionType(ExtModelOptionType extModelOptionType)
    {
        extModelOptionType.setCreateTime(DateUtils.getNowDate());
        return extModelOptionTypeMapper.insertExtModelOptionType(extModelOptionType);
    }

    /**
     * 修改选项类型
     * 
     * @param extModelOptionType 选项类型
     * @return 结果
     */
    @Override
    public int updateExtModelOptionType(ExtModelOptionType extModelOptionType)
    {
        extModelOptionType.setUpdateTime(DateUtils.getNowDate());
        return extModelOptionTypeMapper.updateExtModelOptionType(extModelOptionType);
    }

    /**
     * 批量删除选项类型
     * 
     * @param ids 需要删除的选项类型主键
     * @return 结果
     */
    @Override
    public int deleteExtModelOptionTypeByIds(Long[] ids)
    {
        return extModelOptionTypeMapper.deleteExtModelOptionTypeByIds(ids);
    }

    /**
     * 删除选项类型信息
     * 
     * @param id 选项类型主键
     * @return 结果
     */
    @Override
    public int deleteExtModelOptionTypeById(Long id)
    {
        return extModelOptionTypeMapper.deleteExtModelOptionTypeById(id);
    }
}
