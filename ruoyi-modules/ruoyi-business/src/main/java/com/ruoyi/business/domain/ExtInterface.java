package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 接口对象 ext_interface
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public class ExtInterface extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 接口名称 */
    @Excel(name = "接口名称")
    private String interfaceName;

    /** 接口编码 */
    @Excel(name = "接口编码")
    private String interfaceCode;

    /** 接口URL */
    @Excel(name = "接口URL")
    private String interfaceUrl;

    /** 接口METHOD */
    @Excel(name = "接口METHOD")
    private String interfaceMethod;

    /** 接口类型 */
    @Excel(name = "接口类型")
    private String interfaceType;

    /** 接口数据源名称 */
    @Excel(name = "接口数据源名称")
    private String interfaceDatasourceName;

    /** 是否选数据源 */
    @Excel(name = "是否选数据源")
    private String isSelectDatasource;

    /** 是否通用URL */
    @Excel(name = "是否通用URL")
    private String isCommonUrl;
    /**
     * 查询sql设计json
     */
    private String designSql;
    /**
     * 产生sql
     */
    private String  produceSql;
    /**
     * 业务编码
     */
    private String businessCode;

    /** 是否翻页 */
    @Excel(name = "是否翻页")
    private String isPage;
    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;

    public Long getSearchPersonalizedId() {
        return searchPersonalizedId;
    }

    public void setSearchPersonalizedId(Long searchPersonalizedId) {
        this.searchPersonalizedId = searchPersonalizedId;
    }

    /**
     * 查询个性化接口id
     */
    private Long searchPersonalizedId;


    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setInterfaceName(String interfaceName)
    {
        this.interfaceName = interfaceName;
    }

    public String getInterfaceName()
    {
        return interfaceName;
    }
    public void setInterfaceCode(String interfaceCode)
    {
        this.interfaceCode = interfaceCode;
    }

    public String getInterfaceCode()
    {
        return interfaceCode;
    }
    public void setInterfaceUrl(String interfaceUrl)
    {
        this.interfaceUrl = interfaceUrl;
    }

    public String getInterfaceUrl()
    {
        return interfaceUrl;
    }
    public void setInterfaceMethod(String interfaceMethod)
    {
        this.interfaceMethod = interfaceMethod;
    }

    public String getInterfaceMethod()
    {
        return interfaceMethod;
    }
    public void setInterfaceType(String interfaceType)
    {
        this.interfaceType = interfaceType;
    }

    public String getInterfaceType()
    {
        return interfaceType;
    }
    public void setInterfaceDatasourceName(String interfaceDatasourceName)
    {
        this.interfaceDatasourceName = interfaceDatasourceName;
    }

    public String getInterfaceDatasourceName()
    {
        return interfaceDatasourceName;
    }
    public void setIsSelectDatasource(String isSelectDatasource)
    {
        this.isSelectDatasource = isSelectDatasource;
    }

    public String getIsSelectDatasource()
    {
        return isSelectDatasource;
    }
    public void setIsCommonUrl(String isCommonUrl)
    {
        this.isCommonUrl = isCommonUrl;
    }

    public String getIsCommonUrl()
    {
        return isCommonUrl;
    }

    public String getDesignSql() {
        return designSql;
    }

    public void setDesignSql(String designSql) {
        //
        this.designSql = designSql;
    }

    public String getProduceSql() {

        return produceSql;
    }

    public void setProduceSql(String produceSql) {
        //
        this.produceSql = produceSql;

    }
    public void setIsPage(String isPage)
    {
        this.isPage = isPage;
    }

    public String getIsPage()
    {
        return isPage;
    }
	public void setEnterpriseCode(String enterpriseCode) 
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode() 
    {
        return enterpriseCode;
    }
    public void setApplicationCode(String applicationCode) 
    {
        this.applicationCode = applicationCode;
    }

    public String getApplicationCode() 
    {
        return applicationCode;
    }
    @Override
    public String toString() {
        //
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("interfaceName", getInterfaceName())
            .append("interfaceCode", getInterfaceCode())
            .append("interfaceUrl", getInterfaceUrl())
            .append("interfaceMethod", getInterfaceMethod())
            .append("interfaceType", getInterfaceType())
            .append("interfaceDatasourceName", getInterfaceDatasourceName())
            .append("isSelectDatasource", getIsSelectDatasource())
            .append("isCommonUrl", getIsCommonUrl())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("isPage", getIsPage())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
