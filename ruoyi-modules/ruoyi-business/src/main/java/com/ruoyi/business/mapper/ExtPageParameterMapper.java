package com.ruoyi.business.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.ExtPageParameter;

/**
 * 页面参数Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-28
 */
public interface ExtPageParameterMapper
{
    /**
     * 查询页面参数
     *
     * @param id 页面参数主键
     * @return 页面参数
     */
    public ExtPageParameter selectExtPageParameterById(Long id);

    /**
     * 查询页面参数列表
     *
     * @param extPageParameter 页面参数
     * @return 页面参数集合
     */
    public List<ExtPageParameter> selectExtPageParameterList(ExtPageParameter extPageParameter);

    /**
     * 新增页面参数
     *
     * @param extPageParameter 页面参数
     * @return 结果
     */
    public int insertExtPageParameter(ExtPageParameter extPageParameter);

    /**
     * 修改页面参数
     *
     * @param extPageParameter 页面参数
     * @return 结果
     */
    public int updateExtPageParameter(ExtPageParameter extPageParameter);

    /**
     * 删除页面参数
     *
     * @param id 页面参数主键
     * @return 结果
     */
    public int deleteExtPageParameterById(Long id);

    /**
     * 批量删除页面参数
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtPageParameterByIds(Long[] ids);

    /**
     * 查询满足条件记录数
     * @param searchParams
     * @return
     */
    public int selectCount(Map<String, Object> searchParams);
}
