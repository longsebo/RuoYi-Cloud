package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtPageBusinessFunctionRela;
import com.ruoyi.business.service.IExtPageBusinessFunctionRelaService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 业务功能页面关系Controller
 *
 * @author ruoyi
 * @date 2024-01-28
 */
@RestController
@RequestMapping("/pageBusinessFunctionRela")
public class ExtPageBusinessFunctionRelaController extends BaseController
{
    @Autowired
    private IExtPageBusinessFunctionRelaService extPageBusinessFunctionRelaService;

    /**
     * 查询业务功能页面关系列表
     */
    @RequiresPermissions("business:rela:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtPageBusinessFunctionRela extPageBusinessFunctionRela)
    {
        startPage();
        List<ExtPageBusinessFunctionRela> list = extPageBusinessFunctionRelaService.selectExtPageBusinessFunctionRelaList(extPageBusinessFunctionRela);
        return getDataTable(list);
    }

    /**
     * 导出业务功能页面关系列表
     */
    @RequiresPermissions("business:rela:export")
    @Log(title = "业务功能页面关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtPageBusinessFunctionRela extPageBusinessFunctionRela)
    {
        List<ExtPageBusinessFunctionRela> list = extPageBusinessFunctionRelaService.selectExtPageBusinessFunctionRelaList(extPageBusinessFunctionRela);
        ExcelUtil<ExtPageBusinessFunctionRela> util = new ExcelUtil<ExtPageBusinessFunctionRela>(ExtPageBusinessFunctionRela.class);
        util.exportExcel(response, list, "业务功能页面关系数据");
    }

    /**
     * 获取业务功能页面关系详细信息
     */
    @RequiresPermissions("business:rela:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extPageBusinessFunctionRelaService.selectExtPageBusinessFunctionRelaById(id));
    }

    /**
     * 新增业务功能页面关系
     */
    @RequiresPermissions("business:rela:add")
    @Log(title = "业务功能页面关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty ExtPageBusinessFunctionRela extPageBusinessFunctionRela)
    {
        return toAjax(extPageBusinessFunctionRelaService.insertExtPageBusinessFunctionRela(extPageBusinessFunctionRela));
    }

    /**
     * 修改业务功能页面关系
     */
    @RequiresPermissions("business:rela:edit")
    @Log(title = "业务功能页面关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty ExtPageBusinessFunctionRela extPageBusinessFunctionRela)
    {
        return toAjax(extPageBusinessFunctionRelaService.updateExtPageBusinessFunctionRela(extPageBusinessFunctionRela));
    }

    /**
     * 删除业务功能页面关系
     */
    @RequiresPermissions("business:rela:remove")
    @Log(title = "业务功能页面关系", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extPageBusinessFunctionRelaService.deleteExtPageBusinessFunctionRelaByIds(ids));
    }
}
