package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.domain.ExtPageParameter;

/**
 * 页面参数Service接口
 *
 * @author ruoyi
 * @date 2024-01-28
 */
public interface IExtPageParameterService
{
    /**
     * 查询页面参数
     *
     * @param id 页面参数主键
     * @return 页面参数
     */
    public ExtPageParameter selectExtPageParameterById(Long id);

    /**
     * 查询页面参数列表
     *
     * @param extPageParameter 页面参数
     * @return 页面参数集合
     */
    public List<ExtPageParameter> selectExtPageParameterList(ExtPageParameter extPageParameter);

    /**
     * 新增页面参数
     *
     * @param extPageParameter 页面参数
     * @return 结果
     */
    public int insertExtPageParameter(ExtPageParameter extPageParameter);

    /**
     * 修改页面参数
     *
     * @param extPageParameter 页面参数
     * @return 结果
     */
    public int updateExtPageParameter(ExtPageParameter extPageParameter);

    /**
     * 批量删除页面参数
     *
     * @param ids 需要删除的页面参数主键集合
     * @return 结果
     */
    public int deleteExtPageParameterByIds(Long[] ids);

    /**
     * 删除页面参数信息
     *
     * @param id 页面参数主键
     * @return 结果
     */
    public int deleteExtPageParameterById(Long id);

    /**
     * 返回页面参数树形列表
     * @param extPageParameter
     * @return
     */
    List<ExtPageParameter> selectTree(ExtPageParameter extPageParameter);
}
