package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.domain.ExtBusinessAppBus;

/**
 * 应用业务关系Service接口
 * 
 * @author ruoyi
 * @date 2024-09-23
 */
public interface IExtBusinessAppBusService 
{
    /**
     * 查询应用业务关系
     * 
     * @param id 应用业务关系主键
     * @return 应用业务关系
     */
    public ExtBusinessAppBus selectExtBusinessAppBusById(Long id);

    /**
     * 查询应用业务关系列表
     * 
     * @param extBusinessAppBus 应用业务关系
     * @return 应用业务关系集合
     */
    public List<ExtBusinessAppBus> selectExtBusinessAppBusList(ExtBusinessAppBus extBusinessAppBus);

    /**
     * 新增应用业务关系
     * 
     * @param extBusinessAppBus 应用业务关系
     * @return 结果
     */
    public int insertExtBusinessAppBus(ExtBusinessAppBus extBusinessAppBus);

    /**
     * 修改应用业务关系
     * 
     * @param extBusinessAppBus 应用业务关系
     * @return 结果
     */
    public int updateExtBusinessAppBus(ExtBusinessAppBus extBusinessAppBus);

    /**
     * 批量删除应用业务关系
     * 
     * @param ids 需要删除的应用业务关系主键集合
     * @return 结果
     */
    public int deleteExtBusinessAppBusByIds(Long[] ids);

    /**
     * 删除应用业务关系信息
     * 
     * @param id 应用业务关系主键
     * @return 结果
     */
    public int deleteExtBusinessAppBusById(Long id);
}
