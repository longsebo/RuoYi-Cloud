package com.ruoyi.business.controller;

import com.ruoyi.business.domain.TenantComponent;
import com.ruoyi.business.service.ITenantComponentService;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 租户级别组件，是各个租户自定义组件Controller
 *
 * @author ruoyi
 * @date 2025-02-16
 */
@RestController
@RequestMapping("/tenantComponent")
public class TenantComponentController extends BaseController
{
    @Autowired
    private ITenantComponentService tenantComponentService;

    /**
     * 查询租户级别组件，是各个租户自定义组件列表
     */
    @RequiresPermissions("business:tenantcomponent:list")
    @GetMapping("/list")
    public TableDataInfo list(TenantComponent tenantComponent)
    {
        startPage();
        List<TenantComponent> list = tenantComponentService.selectTenantComponentList(tenantComponent);
        return getDataTable(list);
    }

    /**
     * 导出租户级别组件，是各个租户自定义组件列表
     */
    @RequiresPermissions("business:tenantcomponent:export")
    @Log(title = "租户级别组件，是各个租户自定义组件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TenantComponent tenantComponent)
    {
        List<TenantComponent> list = tenantComponentService.selectTenantComponentList(tenantComponent);
        ExcelUtil<TenantComponent> util = new ExcelUtil<TenantComponent>(TenantComponent.class);
        util.exportExcel(response, list, "租户级别组件，是各个租户自定义组件数据");
    }

    /**
     * 获取租户级别组件，是各个租户自定义组件详细信息
     */
    @RequiresPermissions("business:tenantcomponent:query")
    @GetMapping(value = "/{componentId}")
    public AjaxResult getInfo(@PathVariable("componentId") Long componentId)
    {
        return success(tenantComponentService.selectTenantComponentByComponentId(componentId));
    }

    /**
     * 新增租户级别组件，是各个租户自定义组件
     */
    @RequiresPermissions("business:tenantcomponent:add")
    @Log(title = "租户级别组件，是各个租户自定义组件", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty TenantComponent tenantComponent)
    {
        return toAjax(tenantComponentService.insertTenantComponent(tenantComponent));
    }

    /**
     * 修改租户级别组件，是各个租户自定义组件
     */
    @RequiresPermissions("business:tenantcomponent:edit")
    @Log(title = "租户级别组件，是各个租户自定义组件", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty TenantComponent tenantComponent)
    {
        return toAjax(tenantComponentService.updateTenantComponent(tenantComponent));
    }

    /**
     * 删除租户级别组件，是各个租户自定义组件
     */
    @RequiresPermissions("business:tenantcomponent:remove")
    @Log(title = "租户级别组件，是各个租户自定义组件", businessType = BusinessType.DELETE)
	@DeleteMapping("/{componentIds}")
    public AjaxResult remove(@PathVariable Long[] componentIds)
    {
        return toAjax(tenantComponentService.deleteTenantComponentByComponentIds(componentIds));
    }
}
