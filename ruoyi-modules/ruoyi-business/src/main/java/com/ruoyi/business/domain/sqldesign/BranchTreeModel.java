/**
 *
 */
package com.ruoyi.business.domain.sqldesign;

import java.util.ArrayList;
import java.util.List;

/**
 * 	@author Administrator
 * 	分支树节点模型
 */
public class BranchTreeModel extends AbstractConditionTreeModel {
	//直接下属的条件
	private List<IConditionTreeModel> childConditionTreeModels;
	public BranchTreeModel() {
		this.type = IConditionTreeModel.TYPE_BRANCH;
		//默认为ALL
		this.conditionRelaType=CONDITON_RELA_ALL;
		childConditionTreeModels = new ArrayList<IConditionTreeModel>();
	}
	/**
	 * 	条件关系类型
	 */
	private String conditionRelaType;

	/**
	 * @return the conditionRelaType
	 */
	public String getConditionRelaType() {
		return conditionRelaType;
	}

	/**
	 * @param conditionRelaType the conditionRelaType to set
	 */
	public void setConditionRelaType(String conditionRelaType) {
		this.conditionRelaType = conditionRelaType;
	}
	/**
	 * 获取子节点最大当前级别编号
	 * @return
	 */
	public int getChildConditionMaxCurrentLevel() {
		if(childConditionTreeModels.isEmpty()) {
			return 0;
		}else {
			return childConditionTreeModels.get(childConditionTreeModels.size()-1).getCurrentLevel();
		}
	}
	/**
	 * 	增加子节点
	 * @param childConditonTreeModel
	 */
	public void addChildConditionTreeModel(IConditionTreeModel childConditonTreeModel) {
	    //判断子节点是否已经存在
		if(!isExists(childConditonTreeModel)) {
			//设置当前级别
			childConditonTreeModel.setParentLevel(getLevel());
			childConditonTreeModel.setCurrentLevel(getChildConditionMaxCurrentLevel()+1);
			this.childConditionTreeModels.add(childConditonTreeModel);
		}
	}
	/**
	 *	 判断子节点是否存在,通过级别是否相同
	 * 	@param childConditonTreeModel
	 * 	@return
	 */
	private boolean isExists(IConditionTreeModel childConditonTreeModel) {
		for(int i=0;i<childConditionTreeModels.size();i++) {
			if(childConditionTreeModels.get(i).getLevel().equals(childConditonTreeModel.getLevel())) {
				return true;
			}
		}
		return false;
	}

	public void removeChildConditionTreeModel(IConditionTreeModel childConditionTreeModel) {
		//查找所在位置
		boolean adjustLevel=false;
		int adjustIndex=-1;
		for(int i=0;i<childConditionTreeModels.size();i++) {
			if(childConditionTreeModels.get(i).getLevel().equals(childConditionTreeModel.getLevel())) {
				childConditionTreeModels.remove(i);
				adjustIndex = i;
				adjustLevel = true;
				break;
			}
		}
		if(adjustLevel) {
			adjustLevel(adjustIndex);
		}
	}
	/**
	   *     调整级别
	 * @param adjustIndex
	 */
	private void adjustLevel(int adjustIndex) {
		int currentLevel;
		if(adjustIndex-1<0) {
			currentLevel =0;
		}else {
			currentLevel = childConditionTreeModels.get(adjustIndex-1).getCurrentLevel();
		}
		for(int i=adjustIndex;i<childConditionTreeModels.size();i++) {
			childConditionTreeModels.get(i).setCurrentLevel(++currentLevel);
			//如果是分支树模型，则递归调整下级
			if(childConditionTreeModels.get(i).getType()==IConditionTreeModel.TYPE_BRANCH) {
				((BranchTreeModel)childConditionTreeModels.get(i)).adjustLevel(0);
			}
		}
	}
	/**
	 * 	替换子模型
	 * 	@param oldTreeModel 旧模型
	 * 	@param newTreeModel 新模型
	 */
	public void replaceChildConditionTreeModel(IConditionTreeModel oldTreeModel, IConditionTreeModel newTreeModel) {
		for(int i=0;i<childConditionTreeModels.size();i++) {
			if(childConditionTreeModels.get(i).getLevel().equals(oldTreeModel.getLevel())) {
				childConditionTreeModels.set(i, newTreeModel);
				break;
			}
		}
	}

	public List<IConditionTreeModel> getChildConditionTreeModels() {
		List<IConditionTreeModel> retLists = new ArrayList<IConditionTreeModel>();
		retLists.addAll(childConditionTreeModels);
		return retLists;
	}

	public void setChildConditionTreeModels(List<IConditionTreeModel> childConditionTreeModels) {
		this.childConditionTreeModels = childConditionTreeModels;
	}
}
