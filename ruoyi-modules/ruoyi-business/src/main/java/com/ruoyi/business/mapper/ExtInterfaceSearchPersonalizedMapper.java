package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.domain.ExtInterfaceSearchPersonalized;

/**
 * 查询接口个性化Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
public interface ExtInterfaceSearchPersonalizedMapper 
{
    /**
     * 查询查询接口个性化
     * 
     * @param id 查询接口个性化主键
     * @return 查询接口个性化
     */
    public ExtInterfaceSearchPersonalized selectExtInterfaceSearchPersonalizedById(Long id);

    /**
     * 查询查询接口个性化列表
     * 
     * @param extInterfaceSearchPersonalized 查询接口个性化
     * @return 查询接口个性化集合
     */
    public List<ExtInterfaceSearchPersonalized> selectExtInterfaceSearchPersonalizedList(ExtInterfaceSearchPersonalized extInterfaceSearchPersonalized);

    /**
     * 新增查询接口个性化
     * 
     * @param extInterfaceSearchPersonalized 查询接口个性化
     * @return 结果
     */
    public int insertExtInterfaceSearchPersonalized(ExtInterfaceSearchPersonalized extInterfaceSearchPersonalized);

    /**
     * 修改查询接口个性化
     * 
     * @param extInterfaceSearchPersonalized 查询接口个性化
     * @return 结果
     */
    public int updateExtInterfaceSearchPersonalized(ExtInterfaceSearchPersonalized extInterfaceSearchPersonalized);

    /**
     * 删除查询接口个性化
     * 
     * @param id 查询接口个性化主键
     * @return 结果
     */
    public int deleteExtInterfaceSearchPersonalizedById(Long id);

    /**
     * 批量删除查询接口个性化
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtInterfaceSearchPersonalizedByIds(Long[] ids);
}
