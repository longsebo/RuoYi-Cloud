package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 页面参数关系对象 ext_page_parameter_rela
 * 
 * @author ruoyi
 * @date 2024-01-28
 */
public class ExtPageParameterRela extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 页面参数表id */
    @Excel(name = "页面参数表id")
    private Long extPageParameterId;

    /** 页面编码 */
    @Excel(name = "页面编码")
    private String pageCode;

    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setExtPageParameterId(Long extPageParameterId) 
    {
        this.extPageParameterId = extPageParameterId;
    }

    public Long getExtPageParameterId() 
    {
        return extPageParameterId;
    }
    public void setPageCode(String pageCode) 
    {
        this.pageCode = pageCode;
    }

    public String getPageCode() 
    {
        return pageCode;
    }
    public void setEnterpriseCode(String enterpriseCode) 
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode() 
    {
        return enterpriseCode;
    }
    public void setApplicationCode(String applicationCode) 
    {
        this.applicationCode = applicationCode;
    }

    public String getApplicationCode() 
    {
        return applicationCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("extPageParameterId", getExtPageParameterId())
            .append("pageCode", getPageCode())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
