package com.ruoyi.business.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.core.exception.ServiceException;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.ExtPageInterfaceMapper;
import com.ruoyi.business.domain.ExtPageInterface;
import com.ruoyi.business.service.IExtPageInterfaceService;

/**
 * 页面接口关系Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@Service
public class ExtPageInterfaceServiceImpl implements IExtPageInterfaceService
{
    @Autowired
    private ExtPageInterfaceMapper extPageInterfaceMapper;

    /**
     * 查询页面接口关系
     *
     * @param id 页面接口关系主键
     * @return 页面接口关系
     */
    @Override
    public ExtPageInterface selectExtPageInterfaceById(Long id)
    {
        return extPageInterfaceMapper.selectExtPageInterfaceById(id);
    }

    /**
     * 查询页面接口关系列表
     *
     * @param extPageInterface 页面接口关系
     * @return 页面接口关系
     */
    @Override
    public List<ExtPageInterface> selectExtPageInterfaceList(ExtPageInterface extPageInterface)
    {
        return extPageInterfaceMapper.selectExtPageInterfaceList(extPageInterface);
    }

    /**
     * 新增页面接口关系
     *
     * @param extPageInterface 页面接口关系
     * @return 结果
     */
    @Override
    public int insertExtPageInterface(ExtPageInterface extPageInterface)
    {
        //检验入口参数
        verifyInsert(extPageInterface);
        return extPageInterfaceMapper.insertExtPageInterface(extPageInterface);
    }

    /**
     * 校验新增参数
     * @param extPageInterface
     */
    private void verifyInsert(ExtPageInterface extPageInterface) {
        if(extPageInterface==null)
            throw new ServiceException("页面接口关系不能为空!");
        if(extPageInterface.getPageCode()==null||extPageInterface.getPageCode().isEmpty())
            throw new ServiceException("页面编码不能为空!");
        if(extPageInterface.getInterfaceCode()==null||extPageInterface.getInterfaceCode().isEmpty())
            throw new ServiceException("接口编码不能为空!");
        if(extPageInterface.getInterfaceAlias()==null||extPageInterface.getInterfaceAlias().isEmpty())
            throw new ServiceException("接口别名不能为空!");
        //同一个页面，接口编码不能重复
        Map<String,Object> extPageInterface1=new HashMap<>();
        extPageInterface1.put("pageCode",extPageInterface.getPageCode());
        extPageInterface1.put("interfaceCode",extPageInterface.getInterfaceCode());
        int count=extPageInterfaceMapper.selectCount(extPageInterface1);
        if(count>0)
            throw new ServiceException("同一个页面，接口编码不能重复!");

    }

    /**
     * 修改页面接口关系
     *
     * @param extPageInterface 页面接口关系
     * @return 结果
     */
    @Override
    public int updateExtPageInterface(ExtPageInterface extPageInterface)
    {
        //校验更新参数
        verifyUpdate(extPageInterface);
        return extPageInterfaceMapper.updateExtPageInterface(extPageInterface);
    }

    /**
     * 校验更新参数
     * @param extPageInterface
     */
    private void verifyUpdate(ExtPageInterface extPageInterface) {
        if(extPageInterface==null)
            throw new ServiceException("页面接口关系不能为空!");
        if(extPageInterface.getId()==null)
            throw new ServiceException("id不能为空!");
        if(extPageInterface.getPageCode()==null||extPageInterface.getPageCode().isEmpty())
            throw new ServiceException("页面编码不能为空!");
        if(extPageInterface.getInterfaceCode()==null||extPageInterface.getInterfaceCode().isEmpty())
            throw new ServiceException("接口编码不能为空!");
        if(extPageInterface.getInterfaceAlias()==null||extPageInterface.getInterfaceAlias().isEmpty())
            throw new ServiceException("接口别名不能为空!");
        //同一个页面，接口编码不能重复
        Map<String,Object> extPageInterface1=new HashMap<>();
        extPageInterface1.put("pageCode",extPageInterface.getPageCode());
        extPageInterface1.put("interfaceCode",extPageInterface.getInterfaceCode());
        extPageInterface1.put("notId",extPageInterface.getId());
        int count=extPageInterfaceMapper.selectCount(extPageInterface1);
        if(count>0)
            throw new ServiceException("同一个页面，接口编码不能重复!");
    }

    /**
     * 批量删除页面接口关系
     *
     * @param ids 需要删除的页面接口关系主键
     * @return 结果
     */
    @Override
    public int deleteExtPageInterfaceByIds(Long[] ids)
    {
        return extPageInterfaceMapper.deleteExtPageInterfaceByIds(ids);
    }

    /**
     * 删除页面接口关系信息
     *
     * @param id 页面接口关系主键
     * @return 结果
     */
    @Override
    public int deleteExtPageInterfaceById(Long id)
    {
        return extPageInterfaceMapper.deleteExtPageInterfaceById(id);
    }
}
