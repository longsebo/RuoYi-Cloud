package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.annotation.DefaultCreateProperty;
import com.ruoyi.common.security.annotation.DefaultUpdateProperty;
import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtPageInterface;
import com.ruoyi.business.service.IExtPageInterfaceService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 页面接口关系Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@RestController
@RequestMapping("/pageInterface")
public class ExtPageInterfaceController extends BaseController
{
    @Autowired
    private IExtPageInterfaceService extPageInterfaceService;

    /**
     * 查询页面接口关系列表
     */
    @RequiresPermissions("business:pageinterfacerela:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtPageInterface extPageInterface)
    {
        startPage();
        List<ExtPageInterface> list = extPageInterfaceService.selectExtPageInterfaceList(extPageInterface);
        return getDataTable(list);
    }

    /**
     * 导出页面接口关系列表
     */
    @RequiresPermissions("business:pageinterfacerela:export")
    @Log(title = "页面接口关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtPageInterface extPageInterface)
    {
        List<ExtPageInterface> list = extPageInterfaceService.selectExtPageInterfaceList(extPageInterface);
        ExcelUtil<ExtPageInterface> util = new ExcelUtil<ExtPageInterface>(ExtPageInterface.class);
        util.exportExcel(response, list, "页面接口关系数据");
    }

    /**
     * 获取页面接口关系详细信息
     */
    @RequiresPermissions("business:pageinterfacerela:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extPageInterfaceService.selectExtPageInterfaceById(id));
    }

    /**
     * 新增页面接口关系
     */
    @RequiresPermissions("business:pageinterfacerela:add")
    @Log(title = "页面接口关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@DefaultCreateProperty @EnterpriseAndApplicationCodeProperty ExtPageInterface extPageInterface)
    {
        return toAjax(extPageInterfaceService.insertExtPageInterface(extPageInterface));
    }

    /**
     * 修改页面接口关系
     */
    @RequiresPermissions("business:pageinterfacerela:edit")
    @Log(title = "页面接口关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@DefaultUpdateProperty @EnterpriseAndApplicationCodeProperty ExtPageInterface extPageInterface)
    {
        return toAjax(extPageInterfaceService.updateExtPageInterface(extPageInterface));
    }

    /**
     * 删除页面接口关系
     */
    @RequiresPermissions("business:pageinterfacerela:remove")
    @Log(title = "页面接口关系", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extPageInterfaceService.deleteExtPageInterfaceByIds(ids));
    }
}
