package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.business.domain.ExtInterfaceParameter;
import com.ruoyi.common.security.annotation.DefaultCreateProperty;
import com.ruoyi.common.security.annotation.DefaultUpdateProperty;
import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtPageParameter;
import com.ruoyi.business.service.IExtPageParameterService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 页面参数Controller
 *
 * @author ruoyi
 * @date 2024-01-28
 */
@RestController
@RequestMapping("/pageparameter")
public class ExtPageParameterController extends BaseController
{
    @Autowired
    private IExtPageParameterService extPageParameterService;

    /**
     * 查询页面参数列表
     */
    @RequiresPermissions("business:parameter:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtPageParameter extPageParameter)
    {
        startPage();
        List<ExtPageParameter> list = extPageParameterService.selectExtPageParameterList(extPageParameter);
        return getDataTable(list);
    }

    /**
     * 导出页面参数列表
     */
    @RequiresPermissions("business:parameter:export")
    @Log(title = "页面参数", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtPageParameter extPageParameter)
    {
        List<ExtPageParameter> list = extPageParameterService.selectExtPageParameterList(extPageParameter);
        ExcelUtil<ExtPageParameter> util = new ExcelUtil<ExtPageParameter>(ExtPageParameter.class);
        util.exportExcel(response, list, "页面参数数据");
    }

    /**
     * 获取页面参数详细信息
     */
    @RequiresPermissions("business:parameter:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extPageParameterService.selectExtPageParameterById(id));
    }

    /**
     * 新增页面参数
     */
    @RequiresPermissions("business:parameter:add")
    @Log(title = "页面参数", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@DefaultCreateProperty @EnterpriseAndApplicationCodeProperty ExtPageParameter extPageParameter)
    {
        return toAjax(extPageParameterService.insertExtPageParameter(extPageParameter));
    }

    /**
     * 修改页面参数
     */
    @RequiresPermissions("business:parameter:edit")
    @Log(title = "页面参数", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@DefaultUpdateProperty @EnterpriseAndApplicationCodeProperty ExtPageParameter extPageParameter)
    {
        return toAjax(extPageParameterService.updateExtPageParameter(extPageParameter));
    }

    /**
     * 删除页面参数
     */
    @RequiresPermissions("business:parameter:remove")
    @Log(title = "页面参数", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extPageParameterService.deleteExtPageParameterByIds(ids));
    }
    /**
     * 不翻页查询页面参数列表
     */
    @RequiresPermissions("business:parameter:listAll")
    @GetMapping("/listAll")
    public AjaxResult listAll(ExtPageParameter extPageParameter)
    {
        List<ExtPageParameter> list = extPageParameterService.selectExtPageParameterList(extPageParameter);
        return success(list);
    }
    /**
     * 查询参数树
     * @return
     */
    @RequiresPermissions("business:parameter:tree")
    @PostMapping("/tree")
    public AjaxResult tree(@RequestBody ExtPageParameter extPageParameter)
    {
        List<ExtPageParameter> list = extPageParameterService.selectTree(extPageParameter);
        return success(list);
    }
}
