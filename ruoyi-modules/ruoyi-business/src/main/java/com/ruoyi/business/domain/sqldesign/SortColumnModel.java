/**
 *
 */
package com.ruoyi.business.domain.sqldesign;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

/**
 * 	@author Administrator
 * 	排序列模型
 */
public class SortColumnModel extends AbstractTableModel {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<SortColumnItemModel> list;
	private String groupId;
	final static String columnNames[] = {"排序列","倒排"};
	public SortColumnModel(ArrayList<SortColumnItemModel> list, String groupId) {
		this.list = list;
		this.groupId = groupId;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		SortColumnItemModel item;
		item = list.get(rowIndex);
		if (item != null) {
			switch (columnIndex) {
			case 0:
				return item.getColumAndExp();
			case 1:
				return item.isDescending();
			}
		}
		return null;
	}

	/**
	 * 增加行
	 *
	 * @param item
	 * @return 返回行号
	 */
	public int addRow(SortColumnItemModel item) {
		list.add(item);
		// 更新表格
		int addRow = list.size() - 1;
		fireTableRowsInserted(addRow, addRow);

		return addRow;
	}

	/**
	 * 根据列名/表达式删除行
	 *
	 * @param tableName
	 * @param fieldName
	 * @return 返回删除行号
	 */
	public int removeRowByTabNameAndFieldName(String tableName, String fieldName) {
		for (int i = 0; i < list.size(); i++) {
			SortColumnItemModel model;
			model = list.get(i);
			if (model.getOrgTableName().equals(tableName) && model.getFieldName().equals(fieldName)) {
				list.remove(i);
				// 更新表格
				fireTableRowsDeleted(i, i);
				return i;
			}
		}
		return -1;
	}
	/**
	 * 	根据索引删除行
	 * @param index
	 */
	public void removeRowByIndex(int index) {
		list.remove(index);
		// 更新表格
		fireTableRowsDeleted(index, index);
	}
	/**
	 * 让表格中某些值可修改，但需要setValueAt(Object value, int row, int col)方法配合才能使修改生效
	 */
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if(columnIndex==0) {
			return false;
		}else {
			return true;
		}
	}


	public String getColumnName(int col) {
		return columnNames[col];
	}

	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return list.size();
	}

	/**
	 * 返回数据类型
	 */
	public Class getColumnClass(int col) {
		Object obj = getValueAt(0, col);
		if (obj != null) {
			return obj.getClass();
		} else {
			return String.class;
		}
	}


	public ArrayList<SortColumnItemModel> getList() {
		return list;
	}
	/**
	 * 	根据原表名及字段名查找模型项
	 * @param tableName
	 * @param fieldName
	 * @return
	 */
	public SortColumnItemModel findRowByTabNameAndFieldName(String tableName, String fieldName) {
		for (int i = 0; i < list.size(); i++) {
			SortColumnItemModel model;
			model = list.get(i);
			if (model.getOrgTableName().equals(tableName) && model.getFieldName().equals(fieldName)) {
				return model;
			}
		}
		return null;
	}
	/**
	 * 	交换行
	 * @param onrow
	 * @param otherrow
	 */
	public void exchangeRow(int onrow, int otherrow) {
		SortColumnItemModel oneItem = list.get(onrow);
		SortColumnItemModel otherItem = list.get(otherrow);

		list.set(onrow, otherItem);
		list.set(otherrow, oneItem);
		int firstRow=onrow<otherrow?onrow:otherrow;
		int lastRow=onrow<otherrow?otherrow:onrow;
		this.fireTableRowsUpdated(firstRow, lastRow);
	}
	/**
	 * 使修改的内容生效
	 */
	public void setValueAt(Object value, int row, int col) {
		SortColumnItemModel item = list.get(row);
		if (item != null) {
			switch (col) {
			case 1:
				item.setDescending(value==null?false:Boolean.valueOf(value.toString()));
				this.fireTableCellUpdated(row, col);
				break;
			}

		}
	}
}
