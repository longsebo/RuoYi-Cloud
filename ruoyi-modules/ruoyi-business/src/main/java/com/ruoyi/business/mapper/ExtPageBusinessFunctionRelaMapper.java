package com.ruoyi.business.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.ExtPage;
import com.ruoyi.business.domain.ExtPageBusinessFunctionRela;

/**
 * 业务功能页面关系Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-28
 */
public interface ExtPageBusinessFunctionRelaMapper
{
    /**
     * 查询业务功能页面关系
     *
     * @param id 业务功能页面关系主键
     * @return 业务功能页面关系
     */
    public ExtPageBusinessFunctionRela selectExtPageBusinessFunctionRelaById(Long id);

    /**
     * 查询业务功能页面关系列表
     *
     * @param extPageBusinessFunctionRela 业务功能页面关系
     * @return 业务功能页面关系集合
     */
    public List<ExtPageBusinessFunctionRela> selectExtPageBusinessFunctionRelaList(ExtPageBusinessFunctionRela extPageBusinessFunctionRela);

    /**
     * 新增业务功能页面关系
     *
     * @param extPageBusinessFunctionRela 业务功能页面关系
     * @return 结果
     */
    public int insertExtPageBusinessFunctionRela(ExtPageBusinessFunctionRela extPageBusinessFunctionRela);

    /**
     * 修改业务功能页面关系
     *
     * @param extPageBusinessFunctionRela 业务功能页面关系
     * @return 结果
     */
    public int updateExtPageBusinessFunctionRela(ExtPageBusinessFunctionRela extPageBusinessFunctionRela);

    /**
     * 删除业务功能页面关系
     *
     * @param id 业务功能页面关系主键
     * @return 结果
     */
    public int deleteExtPageBusinessFunctionRelaById(Long id);

    /**
     * 批量删除业务功能页面关系
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtPageBusinessFunctionRelaByIds(Long[] ids);

    /**
     * 更新页面编码
     * @param updateMap
     * @return
     */
    public int updatePageCode(Map<String, Object> updateMap);

    /**
     * 根据业务编码和页面编码删除关系表
     * @param extPage
     * @return
     */
    public int  deleteByBusinessCodePageCode(ExtPage extPage);
}
