package com.ruoyi.business.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 应用定义对象 ext_business_application
 *
 * @author ruoyi
 * @date 2024-09-25
 */
public class ExtBusinessApplication extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 应用编码1 */
    @Excel(name = "应用编码1")
    private String applicationCode;

    /** 应用名称 */
    @Excel(name = "应用名称")
    private String applicationName;

    /** 图标 */
    @Excel(name = "图标")
    private String icon;

    /** 最后访问时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后访问时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lastAccessTime;

    /** 命名空间 */
    @Excel(name = "命名空间")
    private String nameSpace;
    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setApplicationCode(String applicationCode)
    {
        this.applicationCode = applicationCode;
    }

    public String getApplicationCode()
    {
        return applicationCode;
    }
    public void setApplicationName(String applicationName)
    {
        this.applicationName = applicationName;
    }

    public String getApplicationName()
    {
        return applicationName;
    }
    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    public String getIcon()
    {
        return icon;
    }
    public void setLastAccessTime(Date lastAccessTime)
    {
        this.lastAccessTime = lastAccessTime;
    }

    public Date getLastAccessTime()
    {
        return lastAccessTime;
    }
    public void setNameSpace(String nameSpace)
    {
        this.nameSpace = nameSpace;
    }

    public String getNameSpace()
    {
        return nameSpace;
    }

    public String getEnterpriseCode() {
        return enterpriseCode;
    }

    public void setEnterpriseCode(String enterpriseCode) {
        this.enterpriseCode = enterpriseCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("applicationCode", getApplicationCode())
            .append("applicationName", getApplicationName())
            .append("remark", getRemark())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("icon", getIcon())
            .append("lastAccessTime", getLastAccessTime())
            .append("nameSpace", getNameSpace())
            .append("enterpriseCode", getEnterpriseCode())
            .toString();
    }
}
