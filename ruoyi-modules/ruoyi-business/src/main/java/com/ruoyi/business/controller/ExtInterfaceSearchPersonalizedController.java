package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtInterfaceSearchPersonalized;
import com.ruoyi.business.service.IExtInterfaceSearchPersonalizedService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 查询接口个性化Controller
 *
 * @author ruoyi
 * @date 2024-04-24
 */
@RestController
@RequestMapping("/personalized")
public class ExtInterfaceSearchPersonalizedController extends BaseController
{
    @Autowired
    private IExtInterfaceSearchPersonalizedService extInterfaceSearchPersonalizedService;

    /**
     * 查询查询接口个性化列表
     */
    @RequiresPermissions("business:personalized:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtInterfaceSearchPersonalized extInterfaceSearchPersonalized)
    {
        startPage();
        List<ExtInterfaceSearchPersonalized> list = extInterfaceSearchPersonalizedService.selectExtInterfaceSearchPersonalizedList(extInterfaceSearchPersonalized);
        return getDataTable(list);
    }

    /**
     * 导出查询接口个性化列表
     */
    @RequiresPermissions("business:personalized:export")
    @Log(title = "查询接口个性化", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtInterfaceSearchPersonalized extInterfaceSearchPersonalized)
    {
        List<ExtInterfaceSearchPersonalized> list = extInterfaceSearchPersonalizedService.selectExtInterfaceSearchPersonalizedList(extInterfaceSearchPersonalized);
        ExcelUtil<ExtInterfaceSearchPersonalized> util = new ExcelUtil<ExtInterfaceSearchPersonalized>(ExtInterfaceSearchPersonalized.class);
        util.exportExcel(response, list, "查询接口个性化数据");
    }

    /**
     * 获取查询接口个性化详细信息
     */
    @RequiresPermissions("business:personalized:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extInterfaceSearchPersonalizedService.selectExtInterfaceSearchPersonalizedById(id));
    }

    /**
     * 新增查询接口个性化
     */
    @RequiresPermissions("business:personalized:add")
    @Log(title = "查询接口个性化", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty ExtInterfaceSearchPersonalized extInterfaceSearchPersonalized)
    {
        return toAjax(extInterfaceSearchPersonalizedService.insertExtInterfaceSearchPersonalized(extInterfaceSearchPersonalized));
    }

    /**
     * 修改查询接口个性化
     */
    @RequiresPermissions("business:personalized:edit")
    @Log(title = "查询接口个性化", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty ExtInterfaceSearchPersonalized extInterfaceSearchPersonalized)
    {
        return toAjax(extInterfaceSearchPersonalizedService.updateExtInterfaceSearchPersonalized(extInterfaceSearchPersonalized));
    }

    /**
     * 删除查询接口个性化
     */
    @RequiresPermissions("business:personalized:remove")
    @Log(title = "查询接口个性化", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extInterfaceSearchPersonalizedService.deleteExtInterfaceSearchPersonalizedByIds(ids));
    }
}
