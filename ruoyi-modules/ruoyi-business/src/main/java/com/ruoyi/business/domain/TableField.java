package com.ruoyi.business.domain;

import lombok.Data;

/**
 * 数据库表字段定义
 */
@Data
public class TableField {
    /**
     * 字段名
     */
    private String fieldName;
    /**
     * 字段类型
     */
    private String fieldType;
    /**
     * 是否允许为空
     */
    private String nullLabel;
    /**
     * 主键标识
     */
    private String primaryKey;
    /**
     * 是否自增 仅适用数值型
     */
    private String autoIncrement;
    /**
     * 注释
     */
    private String comment;
}
