package com.ruoyi.business.domain;

import com.ruoyi.business.domain.field.FieldScheme;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 模型字段对象 ext_model_field
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public class ExtModelField extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 字段名 */
    @Excel(name = "字段名")
    private String fieldEnName;

    /** 字段中文名 */
    @Excel(name = "字段中文名")
    private String fieldCnName;

    /** 字段说明 */
    @Excel(name = "字段说明")
    private String ffieldRemark;

    /** 字段宽度 */
    @Excel(name = "字段宽度")
    private Long fieldWidth;

    /** 字段类型 */
    @Excel(name = "字段类型")
    private String fieldType;

    /** 字段控件定义 */
    @Excel(name = "字段控件定义")
    private FieldScheme scheme;

    /** 信息范围 */
    @Excel(name = "信息范围")
    private String scope;
    /** 模型表英文名 */
    private String enName;

    /** 数据源名称 */
    private String datasourceName;
    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;    
    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setFieldEnName(String fieldEnName)
    {
        this.fieldEnName = fieldEnName;
    }

    public String getFieldEnName()
    {
        return fieldEnName;
    }
    public void setFieldCnName(String fieldCnName)
    {
        this.fieldCnName = fieldCnName;
    }

    public String getFieldCnName()
    {
        return fieldCnName;
    }
    public void setFfieldRemark(String ffieldRemark)
    {
        this.ffieldRemark = ffieldRemark;
    }

    public String getFfieldRemark()
    {
        return ffieldRemark;
    }
    public void setFieldWidth(Long fieldWidth)
    {
        this.fieldWidth = fieldWidth;
    }

    public Long getFieldWidth()
    {
        return fieldWidth;
    }
    public void setFieldType(String fieldType)
    {
        this.fieldType = fieldType;
    }

    public String getFieldType()
    {
        return fieldType;
    }
    public void setScheme(FieldScheme scheme)
    {
        this.scheme = scheme;
    }

    public FieldScheme getScheme()
    {
        return scheme;
    }
    public void setScope(String scope)
    {
        this.scope = scope;
    }

    public String getScope()
    {
        return scope;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getDatasourceName() {
        return datasourceName;
    }

    public void setDatasourceName(String datasourceName) {
        this.datasourceName = datasourceName;
    }
public void setEnterpriseCode(String enterpriseCode) 
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode() 
    {
        return enterpriseCode;
    }
    public void setApplicationCode(String applicationCode) 
    {
        this.applicationCode = applicationCode;
    }

    public String getApplicationCode() 
    {
        return applicationCode;
    }    

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("fieldEnName", getFieldEnName())
            .append("fieldCnName", getFieldCnName())
            .append("ffieldRemark", getFfieldRemark())
            .append("fieldWidth", getFieldWidth())
            .append("fieldType", getFieldType())
            .append("scheme", getScheme())
            .append("scope", getScope())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
