package com.ruoyi.business.service;

import java.util.List;

import com.ruoyi.business.domain.ExtModelDef;
import com.ruoyi.business.domain.ExtModelFieldRef;
import org.apache.ibatis.annotations.Param;

/**
 * 模型字段引用Service接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface IExtModelFieldRefService
{
    /**
     * 查询模型字段引用
     *
     * @param id 模型字段引用主键
     * @return 模型字段引用
     */
    public ExtModelFieldRef selectExtModelFieldRefById(Long id);

    /**
     * 查询模型字段引用列表
     *
     * @param extModelFieldRef 模型字段引用
     * @return 模型字段引用集合
     */
    public List<ExtModelFieldRef> selectExtModelFieldRefList(ExtModelFieldRef extModelFieldRef);

    /**
     * 新增模型字段引用
     *
     * @param extModelFieldRef 模型字段引用
     * @return 结果
     */
    public int insertExtModelFieldRef(ExtModelFieldRef extModelFieldRef);

    /**
     * 修改模型字段引用
     *
     * @param extModelFieldRef 模型字段引用
     * @return 结果
     */
    public int updateExtModelFieldRef(ExtModelFieldRef extModelFieldRef);

    /**
     * 批量删除模型字段引用
     *
     * @param ids 需要删除的模型字段引用主键集合
     * @return 结果
     */
    public int deleteExtModelFieldRefByIds(Long[] ids);

    /**
     * 删除模型字段引用信息
     *
     * @param id 模型字段引用主键
     * @return 结果
     */
    public int deleteExtModelFieldRefById(Long id);

    /**
     * 使用表英文名删除字段引用
     * @param enName 表英文名
     * @return 返回删除记录数
     */
    public int deleteByTableName(ExtModelDef enName);
}
