package com.ruoyi.business.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 通用URL对象 ext_inteface_url
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
public class ExtIntefaceUrl extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** URL */
    @Excel(name = "URL")
    private String url;

    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setEnterpriseCode(String enterpriseCode) 
    {
        this.enterpriseCode = enterpriseCode;
    }

    public String getEnterpriseCode() 
    {
        return enterpriseCode;
    }
    public void setApplicationCode(String applicationCode) 
    {
        this.applicationCode = applicationCode;
    }

    public String getApplicationCode() 
    {
        return applicationCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("url", getUrl())
            .append("remark", getRemark())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
