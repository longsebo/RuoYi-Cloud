package com.ruoyi.business.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.ExtInterfaceParameter;
import org.apache.ibatis.annotations.Param;

/**
 * 接口参数Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-22
 */
public interface ExtInterfaceParameterMapper
{
    /**
     * 查询接口参数
     *
     * @param id 接口参数主键
     * @return 接口参数
     */
    public ExtInterfaceParameter selectExtInterfaceParameterById(Long id);

    /**
     * 查询接口参数列表
     *
     * @param extInterfaceParameter 接口参数
     * @return 接口参数集合
     */
    public List<ExtInterfaceParameter> selectExtInterfaceParameterList(ExtInterfaceParameter extInterfaceParameter);

    /**
     * 新增接口参数
     *
     * @param extInterfaceParameter 接口参数
     * @return 结果
     */
    public int insertExtInterfaceParameter(ExtInterfaceParameter extInterfaceParameter);

    /**
     * 修改接口参数
     *
     * @param extInterfaceParameter 接口参数
     * @return 结果
     */
    public int updateExtInterfaceParameter(ExtInterfaceParameter extInterfaceParameter);

    /**
     * 删除接口参数
     *
     * @param id 接口参数主键
     * @return 结果
     */
    public int deleteExtInterfaceParameterById(Long id);

    /**
     * 批量删除接口参数
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtInterfaceParameterByIds(Long[] ids);

    /**
     * 根据接口编码删除接口参数
     * @param interfaceCode
     * @return
     */
    public int  deleteByInterfaceCode(@Param("interfaceCode") String interfaceCode);

    /**
     * 查询满足条件记录数
     * @param searchMap
     * @return
     */
    public int selectCount(Map<String, Object> searchMap);
}
