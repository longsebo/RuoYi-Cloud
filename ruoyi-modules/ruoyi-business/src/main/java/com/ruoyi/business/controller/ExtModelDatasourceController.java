package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtModelDatasource;
import com.ruoyi.business.service.IExtModelDatasourceService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 数据源定义Controller
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@RestController
@RequestMapping("/datasource")
public class ExtModelDatasourceController extends BaseController
{
    @Autowired
    private IExtModelDatasourceService extModelDatasourceService;

    /**
     * 查询数据源定义列表
     */
    @RequiresPermissions("business:datasource:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtModelDatasource extModelDatasource)
    {
        startPage();
        List<ExtModelDatasource> list = extModelDatasourceService.selectExtModelDatasourceList(extModelDatasource);
        return getDataTable(list);
    }

    /**
     * 导出数据源定义列表
     */
    @RequiresPermissions("business:datasource:export")
    @Log(title = "数据源定义", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtModelDatasource extModelDatasource)
    {
        List<ExtModelDatasource> list = extModelDatasourceService.selectExtModelDatasourceList(extModelDatasource);
        ExcelUtil<ExtModelDatasource> util = new ExcelUtil<ExtModelDatasource>(ExtModelDatasource.class);
        util.exportExcel(response, list, "数据源定义数据");
    }

    /**
     * 获取数据源定义详细信息
     */
    @RequiresPermissions("business:datasource:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extModelDatasourceService.selectExtModelDatasourceById(id));
    }

    /**
     * 新增数据源定义
     */
    @RequiresPermissions("business:datasource:add")
    @Log(title = "数据源定义", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty  ExtModelDatasource extModelDatasource)
    {
        return toAjax(extModelDatasourceService.insertExtModelDatasource(extModelDatasource));
    }

    /**
     * 修改数据源定义
     */
    @RequiresPermissions("business:datasource:edit")
    @Log(title = "数据源定义", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty ExtModelDatasource extModelDatasource)
    {
        return toAjax(extModelDatasourceService.updateExtModelDatasource(extModelDatasource));
    }

    /**
     * 删除数据源定义
     */
    @RequiresPermissions("business:datasource:remove")
    @Log(title = "数据源定义", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extModelDatasourceService.deleteExtModelDatasourceByIds(ids));
    }
    /**
     * 校验数据库连接
     */
    @RequiresPermissions("business:datasource:checkconnect")
    @GetMapping(value = "/checkconnect/{id}")
    public AjaxResult checkconect(@PathVariable("id") Long id)
    {
        extModelDatasourceService.checkConnect(id);
        return success();
    }
    /**
     * 查询所有数据源定义列表
     */
    @RequiresPermissions("business:datasource:listAll")
    @GetMapping("/listAll")
    public AjaxResult listAll()
    {
        ExtModelDatasource extModelDatasource = new ExtModelDatasource();
        List<ExtModelDatasource> list = extModelDatasourceService.selectExtModelDatasourceList(extModelDatasource);
        return success(list);
    }
}
