package com.ruoyi.business.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.core.exception.ServiceException;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.security.utils.SecurityUtils;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.ExtModelDatasourceMapper;
import com.ruoyi.business.domain.ExtModelDatasource;
import com.ruoyi.business.service.IExtModelDatasourceService;
import lombok.extern.slf4j.Slf4j;
/**
 * 数据源定义Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@Slf4j
@Service
public class ExtModelDatasourceServiceImpl implements IExtModelDatasourceService
{
    @Autowired
    private ExtModelDatasourceMapper extModelDatasourceMapper;

    /**
     * 查询数据源定义
     *
     * @param id 数据源定义主键
     * @return 数据源定义
     */
    @Override
    public ExtModelDatasource selectExtModelDatasourceById(Long id)
    {
        return extModelDatasourceMapper.selectExtModelDatasourceById(id);
    }

    /**
     * 查询数据源定义列表
     *
     * @param extModelDatasource 数据源定义
     * @return 数据源定义
     */
    @Override
    public List<ExtModelDatasource> selectExtModelDatasourceList(ExtModelDatasource extModelDatasource)
    {
        return extModelDatasourceMapper.selectExtModelDatasourceList(extModelDatasource);
    }

    /**
     * 新增数据源定义
     *
     * @param extModelDatasource 数据源定义
     * @return 结果
     */
    @Override
    public int insertExtModelDatasource(ExtModelDatasource extModelDatasource)
    {
        //插入前检查
        verifyData(extModelDatasource);
        extModelDatasource.setCreateTime(DateUtils.getNowDate());
        extModelDatasource.setCreateBy(SecurityUtils.getUsername());
        return extModelDatasourceMapper.insertExtModelDatasource(extModelDatasource);
    }

    /**
     * 校验数据
     * @param extModelDatasource
     */
    private void verifyData(ExtModelDatasource extModelDatasource) {
        //数据源名称不能重复
        Map<String,Object> searchMap = new HashMap<>();
        searchMap.put("dataSourceName",extModelDatasource.getDatasourceName());
        if(extModelDatasource.getId()!=null && extModelDatasource.getId()!=0L) {
            searchMap.put("notId", extModelDatasource.getId());
        }
        if(extModelDatasourceMapper.selectCount(searchMap)>0)
            throw new ServiceException("数据源名称:"+extModelDatasource.getDatasourceName()+"已经存在!");
    }

    /**
     * 修改数据源定义
     *
     * @param extModelDatasource 数据源定义
     * @return 结果
     */
    @Override
    public int updateExtModelDatasource(ExtModelDatasource extModelDatasource)
    {
        //更新前校验
        verifyData(extModelDatasource);
        if(extModelDatasource.getId()==null||extModelDatasource.getId()==0L)
            throw new ServiceException("更新id不能为空！");
        extModelDatasource.setUpdateTime(DateUtils.getNowDate());
        extModelDatasource.setUpdateBy(SecurityUtils.getUsername());
        return extModelDatasourceMapper.updateExtModelDatasource(extModelDatasource);
    }

    /**
     * 批量删除数据源定义
     *
     * @param ids 需要删除的数据源定义主键
     * @return 结果
     */
    @Override
    public int deleteExtModelDatasourceByIds(Long[] ids)
    {
        return extModelDatasourceMapper.deleteExtModelDatasourceByIds(ids);
    }

    /**
     * 删除数据源定义信息
     *
     * @param id 数据源定义主键
     * @return 结果
     */
    @Override
    public int deleteExtModelDatasourceById(Long id)
    {
        return extModelDatasourceMapper.deleteExtModelDatasourceById(id);
    }

    /**
     * 校验数据库连接
     *
     * @param id
     */
    @Override
    public void checkConnect(Long id) {
        //获取配置
        ExtModelDatasource ds = extModelDatasourceMapper.selectExtModelDatasourceById(id);
        if(ds == null)
            throw new ServiceException("记录找不到!");
        //执行jdbc连接
        Connection con = null;
        try {
            Class.forName(ds.getDriverClass());
        }catch(Exception e) {
            log.error("测试数据源连接数据报错",e);
            throw new ServiceException("测试数据源连接失败！无法装载驱动类:"+ds.getDriverClass());
        }
        try{
            if(!StringUtils.isEmpty(ds.getUserName())) {
                con = DriverManager.getConnection(ds.getUrl(), ds.getUserName(), ds.getPassword());
            }else{
                con = DriverManager.getConnection(ds.getUrl());
            }

        }catch(Exception e){
            log.error("测试数据源连接数据报错",e);
            throw new ServiceException("测试数据源连接失败！"+e.getMessage());
        }finally {
            if(con!=null){
                try {
                    con.close();
                } catch (SQLException e) {
                    log.error("关闭数据库连接失败",e);
                }
            }
        }
    }

    /**
     * 根据条件查询满足条数
     *
     * @param searchMap 查询条件
     * @return 条数
     */
    @Override
    public int selectCount(Map<String, Object> searchMap) {
        return extModelDatasourceMapper.selectCount(searchMap);
    }
}
