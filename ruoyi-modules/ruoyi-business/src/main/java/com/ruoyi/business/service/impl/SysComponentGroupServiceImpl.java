package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.SysComponent;
import com.ruoyi.business.domain.SysComponentGroup;
import com.ruoyi.business.mapper.SysComponentGroupMapper;
import com.ruoyi.business.mapper.SysComponentMapper;
import com.ruoyi.business.service.ISysComponentGroupService;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 系统级别分组，最基本的组。一般不会变化。除非系统版本升级Service业务层处理
 *
 * @author ruoyi
 * @date 2025-02-16
 */
@Service
public class SysComponentGroupServiceImpl implements ISysComponentGroupService
{
    @Autowired
    private SysComponentGroupMapper sysComponentGroupMapper;
    @Autowired
    private SysComponentMapper sysComponentMapper;
    /**
     * 查询系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     *
     * @param groupId 系统级别分组，最基本的组。一般不会变化。除非系统版本升级主键
     * @return 系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     */
    @Override
    public SysComponentGroup selectSysComponentGroupByGroupId(Long groupId)
    {
        return sysComponentGroupMapper.selectSysComponentGroupByGroupId(groupId);
    }

    /**
     * 查询系统级别分组，最基本的组。一般不会变化。除非系统版本升级列表
     *
     * @param sysComponentGroup 系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     * @return 系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     */
    @Override
    public List<SysComponentGroup> selectSysComponentGroupList(SysComponentGroup sysComponentGroup)
    {
        return sysComponentGroupMapper.selectSysComponentGroupList(sysComponentGroup);
    }

    /**
     * 新增系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     *
     * @param sysComponentGroup 系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     * @return 结果
     */
    @Override
    public int insertSysComponentGroup(SysComponentGroup sysComponentGroup)
    {
        sysComponentGroup.setCreateTime(DateUtils.getNowDate());
        return sysComponentGroupMapper.insertSysComponentGroup(sysComponentGroup);
    }

    /**
     * 修改系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     *
     * @param sysComponentGroup 系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     * @return 结果
     */
    @Override
    public int updateSysComponentGroup(SysComponentGroup sysComponentGroup)
    {
        sysComponentGroup.setUpdateTime(DateUtils.getNowDate());
        return sysComponentGroupMapper.updateSysComponentGroup(sysComponentGroup);
    }

    /**
     * 批量删除系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     *
     * @param groupIds 需要删除的系统级别分组，最基本的组。一般不会变化。除非系统版本升级主键
     * @return 结果
     */
    @Override
    public int deleteSysComponentGroupByGroupIds(Long[] groupIds)
    {
        return sysComponentGroupMapper.deleteSysComponentGroupByGroupIds(groupIds);
    }

    /**
     * 删除系统级别分组，最基本的组。一般不会变化。除非系统版本升级信息
     *
     * @param groupId 系统级别分组，最基本的组。一般不会变化。除非系统版本升级主键
     * @return 结果
     */
    @Override
    public int deleteSysComponentGroupByGroupId(Long groupId)
    {
        return sysComponentGroupMapper.deleteSysComponentGroupByGroupId(groupId);
    }

    @Override
    public List<SysComponentGroup> selectAllGroup() {
        SysComponentGroup sysComponentGroup;
        sysComponentGroup = new SysComponentGroup();

        sysComponentGroup.setStatus("01");
        List<SysComponentGroup> returnGroups = sysComponentGroupMapper.selectSysComponentGroupList(sysComponentGroup);
        SysComponent sysComponent= new SysComponent();
        sysComponent.setStatus("01");
        List<SysComponent> allSysComponents = sysComponentMapper.selectSysComponentList(sysComponent);
        //循环returnGroups,根据组编码过滤allSysComponents
        for (SysComponentGroup sysComponentGroup1 : returnGroups) {
           //使用jdk8 的stream流过滤
            sysComponentGroup1.setSysComponents(allSysComponents.stream().filter(sysComponent1 -> sysComponent1.getGroupCode().equals(sysComponentGroup1.getGroupCode())).toList());
        }
        return returnGroups;
    }
}
