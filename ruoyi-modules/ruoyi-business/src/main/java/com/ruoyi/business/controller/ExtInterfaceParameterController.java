package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.business.domain.ExtBusinessFunction;
import com.ruoyi.common.security.annotation.DefaultCreateProperty;
import com.ruoyi.common.security.annotation.DefaultUpdateProperty;
import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtInterfaceParameter;
import com.ruoyi.business.service.IExtInterfaceParameterService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 接口参数Controller
 *
 * @author ruoyi
 * @date 2024-01-22
 */
@RestController
@RequestMapping("/parameter")
public class ExtInterfaceParameterController extends BaseController
{
    @Autowired
    private IExtInterfaceParameterService extInterfaceParameterService;

    /**
     * 查询接口参数列表
     */
    @RequiresPermissions("business:parameter:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtInterfaceParameter extInterfaceParameter)
    {
        startPage();
        List<ExtInterfaceParameter> list = extInterfaceParameterService.selectExtInterfaceParameterList(extInterfaceParameter);
        return getDataTable(list);
    }

    /**
     * 导出接口参数列表
     */
    @RequiresPermissions("business:parameter:export")
    @Log(title = "接口参数", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtInterfaceParameter extInterfaceParameter)
    {
        List<ExtInterfaceParameter> list = extInterfaceParameterService.selectExtInterfaceParameterList(extInterfaceParameter);
        ExcelUtil<ExtInterfaceParameter> util = new ExcelUtil<ExtInterfaceParameter>(ExtInterfaceParameter.class);
        util.exportExcel(response, list, "接口参数数据");
    }

    /**
     * 获取接口参数详细信息
     */
    @RequiresPermissions("business:parameter:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extInterfaceParameterService.selectExtInterfaceParameterById(id));
    }

    /**
     * 新增接口参数
     */
    @RequiresPermissions("business:parameter:add")
    @Log(title = "接口参数", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@DefaultCreateProperty @EnterpriseAndApplicationCodeProperty ExtInterfaceParameter extInterfaceParameter)
    {
        return toAjax(extInterfaceParameterService.insertExtInterfaceParameter(extInterfaceParameter));
    }

    /**
     * 修改接口参数
     */
    @RequiresPermissions("business:parameter:edit")
    @Log(title = "接口参数", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@DefaultUpdateProperty @EnterpriseAndApplicationCodeProperty ExtInterfaceParameter extInterfaceParameter)
    {
        return toAjax(extInterfaceParameterService.updateExtInterfaceParameter(extInterfaceParameter));
    }

    /**
     * 删除接口参数
     */
    @RequiresPermissions("business:parameter:remove")
    @Log(title = "接口参数", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extInterfaceParameterService.deleteExtInterfaceParameterByIds(ids));
    }
    /**
     * 查询参数树
     * @return
     */
    @RequiresPermissions("business:parameter:tree")
    @PostMapping("/tree")
    public TableDataInfo tree(@RequestBody ExtInterfaceParameter extInterfaceParameter)
    {
        startPage();
        List<ExtInterfaceParameter> list = extInterfaceParameterService.selectTree(extInterfaceParameter);
        return getDataTable(list);
    }
}
