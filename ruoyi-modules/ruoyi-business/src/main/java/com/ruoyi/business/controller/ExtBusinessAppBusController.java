package com.ruoyi.business.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.business.domain.ExtBusinessAppBus;
import com.ruoyi.business.service.IExtBusinessAppBusService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 应用业务关系Controller
 *
 * @author ruoyi
 * @date 2024-09-23
 */
@RestController
@RequestMapping("/bus")
public class ExtBusinessAppBusController extends BaseController
{
    @Autowired
    private IExtBusinessAppBusService extBusinessAppBusService;

    /**
     * 查询应用业务关系列表
     */
//    @RequiresPermissions("business:bus:list")
    @GetMapping("/list")
    public TableDataInfo list(ExtBusinessAppBus extBusinessAppBus)
    {
        startPage();
        List<ExtBusinessAppBus> list = extBusinessAppBusService.selectExtBusinessAppBusList(extBusinessAppBus);
        return getDataTable(list);
    }

    /**
     * 导出应用业务关系列表
     */
    @RequiresPermissions("business:bus:export")
    @Log(title = "应用业务关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExtBusinessAppBus extBusinessAppBus)
    {
        List<ExtBusinessAppBus> list = extBusinessAppBusService.selectExtBusinessAppBusList(extBusinessAppBus);
        ExcelUtil<ExtBusinessAppBus> util = new ExcelUtil<ExtBusinessAppBus>(ExtBusinessAppBus.class);
        util.exportExcel(response, list, "应用业务关系数据");
    }

    /**
     * 获取应用业务关系详细信息
     */
    @RequiresPermissions("business:bus:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(extBusinessAppBusService.selectExtBusinessAppBusById(id));
    }

    /**
     * 新增应用业务关系
     */
    @RequiresPermissions("business:bus:add")
    @Log(title = "应用业务关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty ExtBusinessAppBus extBusinessAppBus)
    {
        return toAjax(extBusinessAppBusService.insertExtBusinessAppBus(extBusinessAppBus));
    }

    /**
     * 修改应用业务关系
     */
    @RequiresPermissions("business:bus:edit")
    @Log(title = "应用业务关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty ExtBusinessAppBus extBusinessAppBus)
    {
        return toAjax(extBusinessAppBusService.updateExtBusinessAppBus(extBusinessAppBus));
    }

    /**
     * 删除应用业务关系
     */
    @RequiresPermissions("business:bus:remove")
    @Log(title = "应用业务关系", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(extBusinessAppBusService.deleteExtBusinessAppBusByIds(ids));
    }
}
