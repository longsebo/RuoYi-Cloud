package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.TenantComponent;
import com.ruoyi.business.mapper.TenantComponentMapper;
import com.ruoyi.business.service.ITenantComponentService;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.UniqueIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 租户级别组件，是各个租户自定义组件Service业务层处理
 *
 * @author ruoyi
 * @date 2025-02-16
 */
@Service
public class TenantComponentServiceImpl implements ITenantComponentService
{
    @Autowired
    private TenantComponentMapper tenantComponentMapper;

    /**
     * 查询租户级别组件，是各个租户自定义组件
     *
     * @param componentId 租户级别组件，是各个租户自定义组件主键
     * @return 租户级别组件，是各个租户自定义组件
     */
    @Override
    public TenantComponent selectTenantComponentByComponentId(Long componentId)
    {
        return tenantComponentMapper.selectTenantComponentByComponentId(componentId);
    }

    /**
     * 查询租户级别组件，是各个租户自定义组件列表
     *
     * @param tenantComponent 租户级别组件，是各个租户自定义组件
     * @return 租户级别组件，是各个租户自定义组件
     */
    @Override
    public List<TenantComponent> selectTenantComponentList(TenantComponent tenantComponent)
    {
        return tenantComponentMapper.selectTenantComponentList(tenantComponent);
    }

    /**
     * 新增租户级别组件，是各个租户自定义组件
     *
     * @param tenantComponent 租户级别组件，是各个租户自定义组件
     * @return 结果
     */
    @Override
    public int insertTenantComponent(TenantComponent tenantComponent)
    {
        tenantComponent.setCreateTime(DateUtils.getNowDate());
        //自动产生组件类名，并保证唯一
        String className;
        TenantComponent searchTenantComponent = new TenantComponent();
        while (true) {
            className = UniqueIdGenerator.getUniqueClassName(10);
            searchTenantComponent.setClassName(className);
            List<TenantComponent> list = tenantComponentMapper.selectTenantComponentList(searchTenantComponent);
            if (list.size() == 0) {
                tenantComponent.setClassName(className);
                break;
            }
        }
        return tenantComponentMapper.insertTenantComponent(tenantComponent);
    }

    /**
     * 修改租户级别组件，是各个租户自定义组件
     *
     * @param tenantComponent 租户级别组件，是各个租户自定义组件
     * @return 结果
     */
    @Override
    public int updateTenantComponent(TenantComponent tenantComponent)
    {
        tenantComponent.setUpdateTime(DateUtils.getNowDate());
        return tenantComponentMapper.updateTenantComponent(tenantComponent);
    }

    /**
     * 批量删除租户级别组件，是各个租户自定义组件
     *
     * @param componentIds 需要删除的租户级别组件，是各个租户自定义组件主键
     * @return 结果
     */
    @Override
    public int deleteTenantComponentByComponentIds(Long[] componentIds)
    {
        return tenantComponentMapper.deleteTenantComponentByComponentIds(componentIds);
    }

    /**
     * 删除租户级别组件，是各个租户自定义组件信息
     *
     * @param componentId 租户级别组件，是各个租户自定义组件主键
     * @return 结果
     */
    @Override
    public int deleteTenantComponentByComponentId(Long componentId)
    {
        return tenantComponentMapper.deleteTenantComponentByComponentId(componentId);
    }
}
