package com.ruoyi.business.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.domain.ExtModelDef;
import com.ruoyi.business.domain.ExtModelField;
import com.ruoyi.business.domain.ExtModelFieldRef;
import org.apache.ibatis.annotations.Param;

/**
 * 模型字段Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface ExtModelFieldMapper
{
    /**
     * 查询模型字段
     *
     * @param id 模型字段主键
     * @return 模型字段
     */
    public ExtModelField selectExtModelFieldById(Long id);

    /**
     * 查询模型字段列表
     *
     * @param extModelField 模型字段
     * @return 模型字段集合
     */
    public List<ExtModelField> selectExtModelFieldList(ExtModelField extModelField);

    /**
     * 新增模型字段
     *
     * @param extModelField 模型字段
     * @return 结果
     */
    public int insertExtModelField(ExtModelField extModelField);

    /**
     * 修改模型字段
     *
     * @param extModelField 模型字段
     * @return 结果
     */
    public int updateExtModelField(ExtModelField extModelField);

    /**
     * 删除模型字段
     *
     * @param id 模型字段主键
     * @return 结果
     */
    public int deleteExtModelFieldById(Long id);

    /**
     * 批量删除模型字段
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtModelFieldByIds(Long[] ids);

    /**
     * 根据表英文名删除字段（仅限于范围为私有的)
     * @param extModelDef 表英文名
     * @return 返回删除记录数
     */
    public int deleteExtModelFieldByTableName(@Param("extModelDef") ExtModelDef extModelDef);
    /**
     * 通过数据源名称和表英文名非翻页查询模型字段列表(模型字段和字段引用表关联)
     *
     * @param extModelFieldRef 模型字段引用
     * @param scope 信息范围
     */
    public List<ExtModelField> selectExtModelFieldListRef(@Param("ref") ExtModelFieldRef extModelFieldRef, @Param("scope") String scope);

    /**
     * 查询某模型实体字段列表（含私有，缺省，全局)
     *
     * @param extModelField 必须有数据源+表英文名
     * @return 模型实体字段列表
     */
    public List<ExtModelField> selectModelField(ExtModelField extModelField);
    /**
     * 根据引用字段id，数据源名称，表英文名 删除模型字段
     *
     * @param extModelField
     * @return
     */
    public int removeByExtModelField(ExtModelField extModelField);

    /**
     * 根据数据源名称+表英文名查询所有字段
     * @param extModelDef
     * @return
     */
    public List<ExtModelField> selectAllFieldsByDsAndEnName(ExtModelDef extModelDef);

    /**
     * 根据数据源名称+表英文名+信息范围查询所有字段
     * @param extModelFieldRef
     * @return
     */
    public List<ExtModelField> selectExtModelFieldRefByModel(ExtModelFieldRef extModelFieldRef);

    /**
     * 查询满足条件记录数
     * @param searchMap 查询条件
     * @return 返回满足条件数
     */
    public int  selectCount(Map<String, Object> searchMap);
}
