package com.ruoyi.business.service.impl;

import com.ruoyi.business.constant.IBusinessConstant;
import com.ruoyi.business.domain.ExtModelDatasource;
import com.ruoyi.business.service.IDataSourceService;
import com.ruoyi.business.service.IExtModelDatasourceService;
import com.ruoyi.business.utils.MyBatisUtils;
import com.ruoyi.business.utils.SqlSessionFactoryCache;
import com.ruoyi.common.core.exception.ServiceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Properties;

/**
 * 切换数据源
 */
@Service
public class DataSourceService implements IDataSourceService {
    @Autowired
    private IExtModelDatasourceService extModelDatasourceService;
    @Autowired
    private SqlSessionFactoryCache sqlSessionFactoryCache;
    /**
     * 切换数据源
     *
     * @param dataSourceName
     * @return
     */
    @Override
    public SqlSession switchDataSource(String dataSourceName) {
        //根据表英文名获取数据源名称，然后根据数据源名称获取配置
        ExtModelDatasource extModelDataSource = new ExtModelDatasource();
        //
        extModelDataSource.setDatasourceName(dataSourceName);
        List<ExtModelDatasource> dsList = extModelDatasourceService.selectExtModelDatasourceList(extModelDataSource);
        //
        if(CollectionUtils.isEmpty(dsList))
            throw new ServiceException("数据源:"+dataSourceName+"没找到!");
        //
        ExtModelDatasource ds = dsList.get(0);
        

        //
        SqlSessionFactory sqlSessionFactory = sqlSessionFactoryCache.get(ds.getDatasourceName());
        if(sqlSessionFactory==null){
            //构造一个新的SqlSessionFactory
            Properties properties = new Properties();
            properties.put(IBusinessConstant.URL,ds.getUrl());
            properties.put(IBusinessConstant.DRIVER,ds.getDriverClass());
            properties.put(IBusinessConstant.USER_NAME,ds.getUserName());
            properties.put(IBusinessConstant.PASSWORD,ds.getPassword());
            sqlSessionFactory = MyBatisUtils.buildSessionFactory(properties);
            sqlSessionFactoryCache.put(ds.getDatasourceName(),sqlSessionFactory);
        }
        //打开连接
        return sqlSessionFactory.openSession();
    }
}
