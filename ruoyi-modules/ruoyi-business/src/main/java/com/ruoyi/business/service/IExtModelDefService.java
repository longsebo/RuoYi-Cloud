package com.ruoyi.business.service;

import com.ruoyi.business.domain.BusinessFunctionTree;
import com.ruoyi.business.domain.ExtModelDef;
import com.ruoyi.business.entity.ExtModelDefInstanceAdd;
import com.ruoyi.business.entity.ExtModelDefInstanceList;
import com.ruoyi.business.entity.ExtModelDefInstanceOne;
import com.ruoyi.business.entity.ExtModelDefInstanceUpdate;
import com.ruoyi.common.core.web.page.PageDomain;

import java.util.List;
import java.util.Map;

/**
 * 模型定义Service接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface IExtModelDefService
{
    /**
     * 查询模型定义
     *
     * @param id 模型定义主键
     * @return 模型定义
     */
    public ExtModelDef selectExtModelDefById(Long id);

    /**
     * 查询模型定义列表
     *
     * @param extModelDef 模型定义
     * @return 模型定义集合
     */
    public List<ExtModelDef> selectExtModelDefList(ExtModelDef extModelDef);

    /**
     * 新增模型定义
     *
     * @param extModelDef 模型定义
     * @return 结果
     */
    public int insertExtModelDef(ExtModelDef extModelDef);

    /**
     * 修改模型定义
     *
     * @param extModelDef 模型定义
     * @return 结果
     */
    public int updateExtModelDef(ExtModelDef extModelDef);

    /**
     * 批量删除模型定义
     *
     * @param ids 需要删除的模型定义主键集合
     * @return 结果
     */
    public int deleteExtModelDefByIds(Long[] ids);

    /**
     * 删除模型定义信息
     *
     * @param id 模型定义主键
     * @return 结果
     */
    public int deleteExtModelDefById(Long id);

    /**
     * 根据数据源+表英文名+id，查询实例记录
     * @param instanceFindParam
     * @return
     */
    public Map<String, Object> getInstance(ExtModelDefInstanceOne instanceFindParam);

    /**
     * 插入实例记录
     * @param param
     * @return
     */
    ExtModelDefInstanceAdd createInstance(ExtModelDefInstanceAdd param);

    /**
     * 更新实例记录
     * @param param
     * @return
     */
    ExtModelDefInstanceUpdate updateInstance(ExtModelDefInstanceUpdate param);

    /**
     * 根据数据源+表英文名+id,删除实例记录
     * @param param
     */
    void deleteInstance(ExtModelDefInstanceOne param);

    /**
     * 获取模型（表）树列表
     * @param extModelDef
     * @return
     */
    List<BusinessFunctionTree> treeList(ExtModelDef extModelDef);

    /**
     * 查询实例表
     * @param extModelDefInstanceList
     * @param pageDomain
     * @return
     */
    List<Map<String, Object>> listInstance(ExtModelDefInstanceList extModelDefInstanceList, PageDomain pageDomain);

    /**
     * 查询所有记录
     * @param extModelDefInstanceList
     * @return
     */
    List<Map<String, Object>> listAllInstance(ExtModelDefInstanceList extModelDefInstanceList);

    /**
     * 查询实例条数
     * @param extModelDefInstanceList
     * @return
     */
    int countInstance(ExtModelDefInstanceList extModelDefInstanceList);
}
