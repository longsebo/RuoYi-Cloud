package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.domain.ExtModelOptionType;

/**
 * 选项类型Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
public interface ExtModelOptionTypeMapper 
{
    /**
     * 查询选项类型
     * 
     * @param id 选项类型主键
     * @return 选项类型
     */
    public ExtModelOptionType selectExtModelOptionTypeById(Long id);

    /**
     * 查询选项类型列表
     * 
     * @param extModelOptionType 选项类型
     * @return 选项类型集合
     */
    public List<ExtModelOptionType> selectExtModelOptionTypeList(ExtModelOptionType extModelOptionType);

    /**
     * 新增选项类型
     * 
     * @param extModelOptionType 选项类型
     * @return 结果
     */
    public int insertExtModelOptionType(ExtModelOptionType extModelOptionType);

    /**
     * 修改选项类型
     * 
     * @param extModelOptionType 选项类型
     * @return 结果
     */
    public int updateExtModelOptionType(ExtModelOptionType extModelOptionType);

    /**
     * 删除选项类型
     * 
     * @param id 选项类型主键
     * @return 结果
     */
    public int deleteExtModelOptionTypeById(Long id);

    /**
     * 批量删除选项类型
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExtModelOptionTypeByIds(Long[] ids);
}
