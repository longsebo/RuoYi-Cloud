package com.ruoyi.business.controller;

import com.ruoyi.business.domain.TenantComponentGroup;
import com.ruoyi.business.service.ITenantComponentGroupService;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 租户级别分组，由各自租户维护Controller
 *
 * @author ruoyi
 * @date 2025-02-16
 */
@RestController
@RequestMapping("/tenantComponentGroup")
public class TenantComponentGroupController extends BaseController
{
    @Autowired
    private ITenantComponentGroupService tenantComponentGroupService;

    /**
     * 查询租户级别分组，由各自租户维护列表
     */
    @RequiresPermissions("business:tenantcomponentgroup:list")
    @GetMapping("/list")
    public TableDataInfo list(TenantComponentGroup tenantComponentGroup)
    {
        startPage();
        List<TenantComponentGroup> list = tenantComponentGroupService.selectTenantComponentGroupList(tenantComponentGroup);
        return getDataTable(list);
    }

    /**
     * 导出租户级别分组，由各自租户维护列表
     */
    @RequiresPermissions("business:tenantcomponentgroup:export")
    @Log(title = "租户级别分组，由各自租户维护", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TenantComponentGroup tenantComponentGroup)
    {
        List<TenantComponentGroup> list = tenantComponentGroupService.selectTenantComponentGroupList(tenantComponentGroup);
        ExcelUtil<TenantComponentGroup> util = new ExcelUtil<TenantComponentGroup>(TenantComponentGroup.class);
        util.exportExcel(response, list, "租户级别分组，由各自租户维护数据");
    }

    /**
     * 获取租户级别分组，由各自租户维护详细信息
     */
    @RequiresPermissions("business:tenantcomponentgroup:query")
    @GetMapping(value = "/{groupId}")
    public AjaxResult getInfo(@PathVariable("groupId") Long groupId)
    {
        return success(tenantComponentGroupService.selectTenantComponentGroupByGroupId(groupId));
    }

    /**
     * 新增租户级别分组，由各自租户维护
     */
    @RequiresPermissions("business:tenantcomponentgroup:add")
    @Log(title = "租户级别分组，由各自租户维护", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty TenantComponentGroup tenantComponentGroup)
    {
        return toAjax(tenantComponentGroupService.insertTenantComponentGroup(tenantComponentGroup));
    }

    /**
     * 修改租户级别分组，由各自租户维护
     */
    @RequiresPermissions("business:tenantcomponentgroup:edit")
    @Log(title = "租户级别分组，由各自租户维护", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty TenantComponentGroup tenantComponentGroup)
    {
        return toAjax(tenantComponentGroupService.updateTenantComponentGroup(tenantComponentGroup));
    }

    /**
     * 删除租户级别分组，由各自租户维护
     */
    @RequiresPermissions("business:tenantcomponentgroup:remove")
    @Log(title = "租户级别分组，由各自租户维护", businessType = BusinessType.DELETE)
	@DeleteMapping("/{groupIds}")
    public AjaxResult remove(@PathVariable Long[] groupIds)
    {
        return toAjax(tenantComponentGroupService.deleteTenantComponentGroupByGroupIds(groupIds));
    }
    /**
     * 查询系统+租户级别分组，以及每个分组下的组件
     */
    @RequiresPermissions("business:tenantcomponentgroup:listAllGroup")
    @GetMapping("/listAllGroup")
    public AjaxResult listAllGroup()
    {
        List<TenantComponentGroup> list = tenantComponentGroupService.selectAllGroup();
        return success(list);
    }
}
