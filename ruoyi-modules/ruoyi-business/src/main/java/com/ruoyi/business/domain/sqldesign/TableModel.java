package com.ruoyi.business.domain.sqldesign;

import lombok.Data;

/**
 * sql 预览表模型
 * @author Administrator
 *
 */
@Data
public class TableModel  {
	/**
	 * 	原始表名
	 */
	private String enName;
	/**
	 * 	表别名
	 */
	private String alias;
	/**
	 * 表中文名
	 */
	private String cnName;

}
