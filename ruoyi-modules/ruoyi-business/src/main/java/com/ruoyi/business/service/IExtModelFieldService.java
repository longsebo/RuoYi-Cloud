package com.ruoyi.business.service;

import java.util.List;

import com.ruoyi.business.domain.ExtModelDef;
import com.ruoyi.business.domain.ExtModelField;
import com.ruoyi.business.domain.ExtModelFieldRef;
import com.ruoyi.business.domain.field.FieldScheme;

/**
 * 模型字段Service接口
 *
 * @author ruoyi
 * @date 2024-01-08
 */
public interface IExtModelFieldService
{
    /**
     * 查询模型字段
     *
     * @param id 模型字段主键
     * @return 模型字段
     */
    public ExtModelField selectExtModelFieldById(Long id);

    /**
     * 查询模型字段列表
     *
     * @param extModelField 模型字段
     * @return 模型字段集合
     */
    public List<ExtModelField> selectExtModelFieldList(ExtModelField extModelField);

    /**
     * 新增模型字段
     *
     * @param extModelField 模型字段
     * @return 结果
     */
    public int insertExtModelField(ExtModelField extModelField);

    /**
     * 修改模型字段
     *
     * @param extModelField 模型字段
     * @return 结果
     */
    public int updateExtModelField(ExtModelField extModelField);

    /**
     * 批量删除模型字段
     *
     * @param ids 需要删除的模型字段主键集合
     * @return 结果
     */
    public int deleteExtModelFieldByIds(Long[] ids);

    /**
     * 删除模型字段信息
     *
     * @param id 模型字段主键
     * @return 结果
     */
    public int deleteExtModelFieldById(Long id);

    /**
     * 根据模型表英文名删除
     * @param enName 表英文名
     */
    public int deleteExtModelFieldByTableName(ExtModelDef enName);
    /**
     * 通过数据源名称和表英文名非翻页查询模型字段列表(模型字段和字段引用表关联)
     */
    List<ExtModelField> selectExtModelFieldListRef(ExtModelFieldRef extModelFieldRef);
    /**
     * 获取字段类型
     */
    public String getDbType(long width, FieldScheme fieldScheme);

    /**
     * 查询某模型实体字段列表（含私有，缺省，全局)
     * @param extModelField  必须有数据源+表英文名
     * @return 模型实体字段列表
     */
    public List<ExtModelField> selectModelField(ExtModelField extModelField);

    /**
     * 根据引用字段id，数据源名称，表英文名 删除模型字段
     * @param extModelField
     * @return
     */
    public int removeByExtModelField(ExtModelField extModelField);

    /**
     * 根据数据源名称+表英文名查询所有字段
     * @param extModelDef
     * @return
     */
    public List<ExtModelField> selectAllFieldsByDsAndEnName(ExtModelDef extModelDef);

    /**
     * 根据数据源名称+表英文名+信息范围查询字段定义
     * @param extModelFieldRef
     * @return
     */
    public List<ExtModelField> selectExtModelFieldRefByModel(ExtModelFieldRef extModelFieldRef);

}
