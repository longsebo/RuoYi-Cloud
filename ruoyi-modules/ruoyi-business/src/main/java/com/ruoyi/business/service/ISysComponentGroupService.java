package com.ruoyi.business.service;

import com.ruoyi.business.domain.SysComponentGroup;

import java.util.List;

/**
 * 系统级别分组，最基本的组。一般不会变化。除非系统版本升级Service接口
 *
 * @author ruoyi
 * @date 2025-02-16
 */
public interface ISysComponentGroupService
{
    /**
     * 查询系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     *
     * @param groupId 系统级别分组，最基本的组。一般不会变化。除非系统版本升级主键
     * @return 系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     */
    public SysComponentGroup selectSysComponentGroupByGroupId(Long groupId);

    /**
     * 查询系统级别分组，最基本的组。一般不会变化。除非系统版本升级列表
     *
     * @param sysComponentGroup 系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     * @return 系统级别分组，最基本的组。一般不会变化。除非系统版本升级集合
     */
    public List<SysComponentGroup> selectSysComponentGroupList(SysComponentGroup sysComponentGroup);

    /**
     * 新增系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     *
     * @param sysComponentGroup 系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     * @return 结果
     */
    public int insertSysComponentGroup(SysComponentGroup sysComponentGroup);

    /**
     * 修改系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     *
     * @param sysComponentGroup 系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     * @return 结果
     */
    public int updateSysComponentGroup(SysComponentGroup sysComponentGroup);

    /**
     * 批量删除系统级别分组，最基本的组。一般不会变化。除非系统版本升级
     *
     * @param groupIds 需要删除的系统级别分组，最基本的组。一般不会变化。除非系统版本升级主键集合
     * @return 结果
     */
    public int deleteSysComponentGroupByGroupIds(Long[] groupIds);

    /**
     * 删除系统级别分组，最基本的组。一般不会变化。除非系统版本升级信息
     *
     * @param groupId 系统级别分组，最基本的组。一般不会变化。除非系统版本升级主键
     * @return 结果
     */
    public int deleteSysComponentGroupByGroupId(Long groupId);

    List<SysComponentGroup> selectAllGroup();
}
