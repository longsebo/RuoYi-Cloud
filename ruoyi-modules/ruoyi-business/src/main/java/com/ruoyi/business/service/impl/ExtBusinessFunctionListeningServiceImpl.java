package com.ruoyi.business.service.impl;

import com.ruoyi.business.constant.IBusinessConstant;
import com.ruoyi.business.domain.ExtBusinessFunctionListening;
import com.ruoyi.business.domain.ExtBusinessMqConf;
import com.ruoyi.business.mapper.ExtBusinessFunctionListeningMapper;
import com.ruoyi.business.service.IExtBusinessApplicationService;
import com.ruoyi.business.service.IExtBusinessFunctionListeningService;
import com.ruoyi.business.service.IExtBusinessMqConfService;
import com.ruoyi.common.core.exception.ServiceException;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.security.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 功能侦听Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-08
 */
@Service
public class ExtBusinessFunctionListeningServiceImpl implements IExtBusinessFunctionListeningService
{
    @Autowired
    private ExtBusinessFunctionListeningMapper extBusinessFunctionListeningMapper;
    @Autowired
    private IExtBusinessApplicationService extBusinessApplicationService;
    @Autowired
    private IExtBusinessMqConfService extBusinessMqConfService;
    /**
     * 查询功能侦听
     *
     * @param id 功能侦听主键
     * @return 功能侦听
     */
    @Override
    public ExtBusinessFunctionListening selectExtBusinessFunctionListeningById(Long id)
    {
        return extBusinessFunctionListeningMapper.selectExtBusinessFunctionListeningById(id);
    }

    /**
     * 查询功能侦听列表
     *
     * @param extBusinessFunctionListening 功能侦听
     * @return 功能侦听
     */
    @Override
    public List<ExtBusinessFunctionListening> selectExtBusinessFunctionListeningList(ExtBusinessFunctionListening extBusinessFunctionListening)
    {
        return extBusinessFunctionListeningMapper.selectExtBusinessFunctionListeningList(extBusinessFunctionListening);
    }

    /**
     * 新增功能侦听
     *
     * @param extBusinessFunctionListening 功能侦听
     * @return 结果
     */
    @Override
    public int insertExtBusinessFunctionListening(ExtBusinessFunctionListening extBusinessFunctionListening)
    {
        //插入前校验
        verifyData(extBusinessFunctionListening);
        extBusinessFunctionListening.setCreateTime(DateUtils.getNowDate());
        extBusinessFunctionListening.setCreateBy(SecurityUtils.getUsername());
        return extBusinessFunctionListeningMapper.insertExtBusinessFunctionListening(extBusinessFunctionListening);
    }

    /**
     * 校验参数
     * @param extBusinessFunctionListening
     */
    private void verifyData(ExtBusinessFunctionListening extBusinessFunctionListening) {
        if(extBusinessFunctionListening==null)
            throw new ServiceException("插入功能侦听对象为空!");
        //检查应用编码是否存在
        Map<String,Object> searchMap = new HashMap<>();
        searchMap.put("applicationCode", extBusinessFunctionListening.getApplicationCode());
        if(extBusinessApplicationService.selectCount(searchMap)==0)
            throw new ServiceException("应用编码："+ extBusinessFunctionListening.getApplicationCode()+"不存在!");
        //mq 名称是否存在
        searchMap.clear();
        searchMap.put("mqName", extBusinessFunctionListening.getMqName());
        if(extBusinessMqConfService.selectCount(searchMap)==0)
            throw new ServiceException("mq名称:"+ extBusinessFunctionListening.getMqName()+"不存在！");
        //如果是rocketmq,标签名称必填
        ExtBusinessMqConf extBusinessMqConf = new ExtBusinessMqConf();
        extBusinessMqConf.setMqName(extBusinessFunctionListening.getMqName());
        List<ExtBusinessMqConf> list = extBusinessMqConfService.selectExtBusinessMqConfList(extBusinessMqConf);
        extBusinessMqConf = list.get(0);
        if(IBusinessConstant.MQ_TYPE_ROCKETMQ.equals(extBusinessMqConf.getMqType())){
            if(StringUtils.isEmpty(extBusinessFunctionListening.getTag()))
                throw new ServiceException("标签不能为空！");
        }
    }

    /**
     * 修改功能侦听
     *
     * @param extBusinessFunctionListening 功能侦听
     * @return 结果
     */
    @Override
    public int updateExtBusinessFunctionListening(ExtBusinessFunctionListening extBusinessFunctionListening)
    {
        //更新前校验
        verifyData(extBusinessFunctionListening);
        if(extBusinessFunctionListening.getId()==null||extBusinessFunctionListening.getId()==0L)
            throw new ServiceException("id不能为空!");
        extBusinessFunctionListening.setUpdateTime(DateUtils.getNowDate());
        extBusinessFunctionListening.setUpdateBy(SecurityUtils.getUsername());
        return extBusinessFunctionListeningMapper.updateExtBusinessFunctionListening(extBusinessFunctionListening);
    }

    /**
     * 批量删除功能侦听
     *
     * @param ids 需要删除的功能侦听主键
     * @return 结果
     */
    @Override
    public int deleteExtBusinessFunctionListeningByIds(Long[] ids)
    {
        return extBusinessFunctionListeningMapper.deleteExtBusinessFunctionListeningByIds(ids);
    }

    /**
     * 删除功能侦听信息
     *
     * @param id 功能侦听主键
     * @return 结果
     */
    @Override
    public int deleteExtBusinessFunctionListeningById(Long id)
    {
        return extBusinessFunctionListeningMapper.deleteExtBusinessFunctionListeningById(id);
    }

    /**
     * 查询满足条件个数
     *
     * @param map 条件map
     * @return 返回条数
     */
    @Override
    public int selectCount(Map<String, Object> map) {
        return extBusinessFunctionListeningMapper.selectCount(map);
    }
}
