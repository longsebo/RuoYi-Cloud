package com.ruoyi.business.domain.sqldesign;
import java.util.ArrayList;
import java.util.List;

//import com.autoscript.ui.core.customsqlcomp.select.component.JCheckListBox;
/**
 *
 * @author Administrator
 * 数据库表关联模型
 */
//import com.autoscript.ui.core.customsqlcomp.select.component.TableDefineFrame;

public class DbTableJoinModel {
	//表内关联
	public static final String JOINT_INNER_TYPE="inner";
	//左关联
	public static final String JOINT_LEFT_TYPE="left";
	//右关联
	public static final String JOINT_RIGHT_TYPE="right";
	//全关联
	public static final String JOINT_FULL_TYPE="full";

//	/**
//	 * 开始表defineframe
//	 */
//	private TableDefineFrame startTableDefineFrame;
//	/**
//	 * 结束表defineframe
//	 */
//	private TableDefineFrame endTableDefineFrame;
	/**
	 * 数据库列关联模型列表
	 */
	private List<DbColumnJoinModel> dbColumnJoinModels;
	/**
	 * 表关联类型
	 */
	private String joinType;
	/**
	 * 结束CheckListBox
	 */
//	private JCheckListBox endCheckListBox;
	/**
	 *
	 */
//	private JCheckListBox startCheckListBox;
	public DbTableJoinModel() {
		dbColumnJoinModels = new ArrayList<DbColumnJoinModel>();
	}


//	public TableDefineFrame getStartTableDefineFrame() {
//		return startTableDefineFrame;
//	}
//	public void setStartTableDefineFrame(TableDefineFrame startTableDefineFrame) {
//		this.startTableDefineFrame = startTableDefineFrame;
//	}
//	public TableDefineFrame getEndTableDefineFrame() {
//		return endTableDefineFrame;
//	}
//	public void setEndTableDefineFrame(TableDefineFrame endTableDefineFrame) {
//		this.endTableDefineFrame = endTableDefineFrame;
//	}


	public List<DbColumnJoinModel> getDbColumnJoinModels() {
		return dbColumnJoinModels;
	}


	public void setDbColumnJoinModels(List<DbColumnJoinModel> dbColumnJoinModels) {
		this.dbColumnJoinModels = dbColumnJoinModels;
	}


	public String getJoinType() {
		return joinType;
	}


	public void setJoinType(String joinType) {
		this.joinType = joinType;
	}

	/**
	 * 增加列模型
	 * @param dbColumnJoinModel
	 */
	public void addColumnJoin(DbColumnJoinModel dbColumnJoinModel) {
		dbColumnJoinModels.add(dbColumnJoinModel);
	}
	/**
	 * 判断列模型是否存在
	 * @param startIndex
	 * @param endIndex
	 * @return
	 */
	public boolean isExistsColumnJoin(int startIndex, int endIndex) {
		for(DbColumnJoinModel dbColumnJoinModel:dbColumnJoinModels) {
			if(dbColumnJoinModel.getEndIndex() == endIndex && dbColumnJoinModel.getStartIndex() == startIndex) {
				return true;
			}
		}
		return false;
	}

//
//	public JCheckListBox getEndCheckListBox() {
//		return endCheckListBox;
//	}
//
//
//	public void setEndCheckListBox(JCheckListBox endCheckListBox) {
//		this.endCheckListBox = endCheckListBox;
//	}
//
//
//	public JCheckListBox getStartCheckListBox() {
//		return startCheckListBox;
//	}
//
//
//	public void setStartCheckListBox(JCheckListBox startCheckListBox) {
//		this.startCheckListBox = startCheckListBox;
//	}
	/**
	 * 	查找列join模型
	 * @param startIndex 开始索引
	 * @param endIndex 结束索引
	 * @return
	 */
	public DbColumnJoinModel getColumnJoin(int startIndex, int endIndex) {
		for(DbColumnJoinModel dbColumnJoinModel:dbColumnJoinModels) {
			if(dbColumnJoinModel.getEndIndex() == endIndex && dbColumnJoinModel.getStartIndex() == startIndex) {
				return dbColumnJoinModel;
			}
		}
		return null;
	}



}
