package com.ruoyi.business.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.ExtInterfaceBusinessFunctionRelaMapper;
import com.ruoyi.business.domain.ExtInterfaceBusinessFunctionRela;
import com.ruoyi.business.service.IExtInterfaceBusinessFunctionRelaService;

/**
 * 业务功能接口关系Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-08
 */
@Service
public class ExtInterfaceBusinessFunctionRelaServiceImpl implements IExtInterfaceBusinessFunctionRelaService 
{
    @Autowired
    private ExtInterfaceBusinessFunctionRelaMapper extInterfaceBusinessFunctionRelaMapper;

    /**
     * 查询业务功能接口关系
     * 
     * @param id 业务功能接口关系主键
     * @return 业务功能接口关系
     */
    @Override
    public ExtInterfaceBusinessFunctionRela selectExtInterfaceBusinessFunctionRelaById(Long id)
    {
        return extInterfaceBusinessFunctionRelaMapper.selectExtInterfaceBusinessFunctionRelaById(id);
    }

    /**
     * 查询业务功能接口关系列表
     * 
     * @param extInterfaceBusinessFunctionRela 业务功能接口关系
     * @return 业务功能接口关系
     */
    @Override
    public List<ExtInterfaceBusinessFunctionRela> selectExtInterfaceBusinessFunctionRelaList(ExtInterfaceBusinessFunctionRela extInterfaceBusinessFunctionRela)
    {
        return extInterfaceBusinessFunctionRelaMapper.selectExtInterfaceBusinessFunctionRelaList(extInterfaceBusinessFunctionRela);
    }

    /**
     * 新增业务功能接口关系
     * 
     * @param extInterfaceBusinessFunctionRela 业务功能接口关系
     * @return 结果
     */
    @Override
    public int insertExtInterfaceBusinessFunctionRela(ExtInterfaceBusinessFunctionRela extInterfaceBusinessFunctionRela)
    {
        extInterfaceBusinessFunctionRela.setCreateTime(DateUtils.getNowDate());
        return extInterfaceBusinessFunctionRelaMapper.insertExtInterfaceBusinessFunctionRela(extInterfaceBusinessFunctionRela);
    }

    /**
     * 修改业务功能接口关系
     * 
     * @param extInterfaceBusinessFunctionRela 业务功能接口关系
     * @return 结果
     */
    @Override
    public int updateExtInterfaceBusinessFunctionRela(ExtInterfaceBusinessFunctionRela extInterfaceBusinessFunctionRela)
    {
        extInterfaceBusinessFunctionRela.setUpdateTime(DateUtils.getNowDate());
        return extInterfaceBusinessFunctionRelaMapper.updateExtInterfaceBusinessFunctionRela(extInterfaceBusinessFunctionRela);
    }

    /**
     * 批量删除业务功能接口关系
     * 
     * @param ids 需要删除的业务功能接口关系主键
     * @return 结果
     */
    @Override
    public int deleteExtInterfaceBusinessFunctionRelaByIds(Long[] ids)
    {
        return extInterfaceBusinessFunctionRelaMapper.deleteExtInterfaceBusinessFunctionRelaByIds(ids);
    }

    /**
     * 删除业务功能接口关系信息
     * 
     * @param id 业务功能接口关系主键
     * @return 结果
     */
    @Override
    public int deleteExtInterfaceBusinessFunctionRelaById(Long id)
    {
        return extInterfaceBusinessFunctionRelaMapper.deleteExtInterfaceBusinessFunctionRelaById(id);
    }
}
