package com.ruoyi.business.domain.sqldesign;

import lombok.Data;

/**
 *
 * 	@author Administrator
 *	有效列项模型
 */
@Data
public class AvailableColumnItemModel {
	/**
	 * 	列/表达式
	 */
	protected String columAndExp;
	/**
	 * 	列别名
	 */
	protected String alias;
	/**
	 * 	原始表名
	 */
	protected String orgTableName;
	/**
	 * 	表别名
	 */
	protected String tableAlias;
	/**
	 * 字段名
	 */
	protected String fieldName;
	protected String fullFieldName;

}
