package com.ruoyi.business.entity;
import lombok.Data;

import java.util.Map;

/**
 * 更新模型实例
 */
@Data
public class ExtModelDefInstanceUpdate extends BaseAPI {
    /**
     * 数据源名称
     */
    private String dataSourceName;
    /**
     * 表英文名
     */
    private String enName;
    /**
     * 字段数据
     */
    private Map<String, Object> data;
    /**
     * 模型实例ID
     */
    private Long id;
}
