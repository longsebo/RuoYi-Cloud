/**
 *
 */
package com.ruoyi.business.domain.sqldesign;

/**
 * 	@author Administrator
 * 	条件树模型
 */
public interface IConditionTreeModel {
	//类型常量
	/**
	 * 	分支类型
	 */
	public final int TYPE_BRANCH=1;
	/**
	 * 	条件类型
	 */
	public final int TYPE_CONDITION=2;
	//条件关系类型常量
	/**
	 * 	All of the conditions in branch must match
	 * 	分支内所有条件必须匹配(即使用 and 连接)
	 */
	public final String CONDITON_RELA_ALL="All";
	/**
	 * 	Any of the conditions in branch must match
	 * 	分支内所有条件任意一个匹配(即使用 or 连接)
	 */
	public final String CONDITON_RELA_ANY="Any";
	/**
	 * 	None of the conditions in branch must match
	 * 	分支内没有条件必须匹配(第一项用not ,后面用or 连接)
	 */
	public final String CONDITON_RELA_NONE="None";
	/**
	 * 	NotAll of the conditions in branch must match
	 * 	分支内不是所有条件必须匹配(第一项用not,后面使用 and 连接)
	 */
	public final String CONDITON_RELA_NOTALL="NotAll";

	/**
	 * 	获取模型类型
	 *  @return
	 */
	public int getType();
	/**
	 * 	获取级别
	 * 	@return
	 */
	public String getLevel();
	/**
	 * 	设置父级别
	 * 	@param value 父级别有多层，所以用点分级，所以为字符串
	 */
	public void setParentLevel(String value);
	/**
	 * 	设置当前级别
	 * 	@param value 当前级别为数字，便于计算兄弟级别
	 */
	public void setCurrentLevel(int value);
	/**
	 * 	获取当前级别
	 * 	@return
	 */
	public int  getCurrentLevel();
	/**
	 * 	获取父级别
	 * 	@return
	 */
	public String getParentLevel();
}
