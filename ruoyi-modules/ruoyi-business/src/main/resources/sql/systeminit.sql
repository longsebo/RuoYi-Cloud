-- 后台系统组及组件
INSERT INTO sys_component_group (group_code, group_name, group_icon_url, remark, status, sort_number, create_by, create_time, update_by, update_time)
 VALUES('sys_common', '常用', '/statics/2025/02/08/common.svg', 'jdk lang包', '1', 0, 'admin', now(), null, null);
-- 后台常用组组件
INSERT INTO sys_component (group_code, class_name, component_name, component_icon_url, remark, status, sort_number, create_by, create_time, update_by, update_time)
VALUES('sys_common', 'GString', '字符串型', '/statics/2025/02/07/string.svg', 'groovy 字符串型类', '1', 1, 'admin', now(), null, null);

INSERT INTO sys_component (group_code, class_name, component_name, component_icon_url, remark, status, sort_number, create_by, create_time, update_by, update_time)
VALUES('sys_common', 'GInteger', '整型', '/statics/2025/02/07/integer.svg', 'groovy 整型类', '1', 2, 'admin', now(), null, null);

INSERT INTO sys_component (group_code, class_name, component_name, component_icon_url, remark, status, sort_number, create_by, create_time, update_by, update_time)
VALUES('sys_common', 'GLong', '长整型', '/statics/2025/02/07/long.svg', 'groovy 长整型类', '1', 3, 'admin', now(), null, null);

INSERT INTO sys_component (group_code, class_name, component_name, component_icon_url, remark, status, sort_number, create_by, create_time, update_by, update_time)
 VALUES('sys_common', 'GBoolean', '布尔型', '/statics/2025/02/07/boolean.svg', 'groovy 布尔型类', '1', 4, 'admin', now(), null, null);

INSERT INTO sys_component (group_code, class_name, component_name, component_icon_url, remark, status, sort_number, create_by, create_time, update_by, update_time)
VALUES('sys_common', 'GByte', '字节型', '/statics/2025/02/07/byte.svg', 'groovy 字节型类', '1', 5, 'admin', now(), null, null);

INSERT INTO sys_component (group_code, class_name, component_name, component_icon_url, remark, status, sort_number, create_by, create_time, update_by, update_time)
VALUES('sys_common', 'GCharacter', '单字符型', '/statics/2025/02/07/character.svg', 'groovy 单字符型类', '1', 6, 'admin', now(), null, null);

INSERT INTO sys_component (group_code, class_name, component_name, component_icon_url, remark, status, sort_number, create_by, create_time, update_by, update_time)
VALUES('sys_common', 'GDouble', '双精度浮点型', '/statics/2025/02/07/double.svg', 'groovy 双精度浮点型', '1', 7, 'admin', now(), null, null);

INSERT INTO sys_component (group_code, class_name, component_name, component_icon_url, remark, status, sort_number, create_by, create_time, update_by, update_time)
VALUES('sys_common', 'GFloat', '浮点型', '/statics/2025/02/07/float.svg', 'groovy 浮点型类', '1', 8, 'admin', now(), null, null);

INSERT INTO sys_component (group_code, class_name, component_name, component_icon_url, remark, status, sort_number, create_by, create_time, update_by, update_time)
VALUES('sys_common', 'GShort', '短整型', '/statics/2025/02/07/short.svg', 'groovy 短整型类', '1', 9, 'admin', now(), null, null);

INSERT INTO sys_component (group_code, class_name, component_name, component_icon_url, remark, status, sort_number, create_by, create_time, update_by, update_time)
VALUES('sys_common', 'GMath', '数学型', '/statics/2025/02/07/math.svg', 'groovy 数学类', '1', 10, 'admin', now(), null, null);
