<#-- 提取#{和}中间的值 -->
<#macro pickMiddleVal exp>
<#assign retVal=''/>
<#assign startPos>${exp?index_of('#{')?c}</#assign>
<#if (startPos?number==-1)>
 <#assign startPos>${exp?index_of('${')?c}</#assign>
</#if>
<#if (startPos?number>=0)>
 <#assign endPos>${exp?index_of('}',startPos?number)?c}</#assign>
 <#if (endPos?number>=0)>
 <#assign retVal='${exp?substring(startPos?number+2,endPos?number)}'/>
 </#if>
</#if>
<#-- 去掉点 -->
<#assign startPos=retVal?index_of('.')?c/>
<#if (startPos?number>=0)>
<#assign retVal=retVal?substring(startPos?number+1)/>
</#if>
${retVal}<#rt>
</#macro>
<#-- 判断是否是Mybatis 表达式-->
<#macro isMybatisExp exp>
<#assign flag='0'/>
<#assign startPos>${exp?index_of('#{')?c}</#assign>
<#if (startPos?number==-1)>
 <#assign startPos>${exp?index_of('${')?c}</#assign>
</#if>
<#if (startPos?number>=0)>
 <#assign endPos>${exp?index_of('}',startPos?number)?c}</#assign>
 <#if (endPos?number>=0)>
 <#assign flag='1'/>
 </#if>
</#if>
${flag}<#rt>
</#macro>

<#-- 获取逻辑表达式-->
<#macro getLogicExp exp>
<#if exp??>
 			<#if (exp=="All")>
 			<#lt>AND<#t>
 			<#elseif (exp=="Any")>
 			<#lt>OR<#t>
 			<#elseif (exp=="None")>
			<#lt>OR<#t>
			<#elseif (exp=="NotAll")>
			<#lt>AND<#t>
			</#if>
		   <#else>
		     <#lt>AND<#t>
</#if>
</#macro>

<#-- 递归产生条件宏 -->
<#macro makeCondition branchTreeModel>
 <#if branchTreeModel.childConditionTreeModels?has_content>
 	<#-- 产生外层括号(第一层除外) -->
	<#if (branchTreeModel.getLevel()!="1")>
 	${"( "}
 	</#if>
 	<#-- 循环输出每个条件 -->
 	<#list branchTreeModel.childConditionTreeModels as conditionModel>
 		<#-- 如果条件值包含#{和}，则生成mybatis if或foreach -->
 		<#assign mybatisif=''/>
 		<#assign operator=''/>
 		<#assign logicExp><@getLogicExp exp='${branchTreeModel.conditionRelaType}'/></#assign>
 		<#if (conditionModel.type == 2)>
 		<#assign operator=conditionModel.operator/>
 		<#assign flag><@isMybatisExp exp='${conditionModel.right!""}'/></#assign>
 		<#if (flag=='1')>
 	    <#assign mybatisif><@pickMiddleVal exp='${conditionModel.right!""}'/></#assign>
 	    <#else>
 	    <#assign flag><@isMybatisExp exp='${conditionModel.left!""}'/></#assign>
 	     <#if (flag=='1')>
 		<#assign mybatisif><@pickMiddleVal exp='${conditionModel.left!""}'/></#assign>
 		 </#if>
 	    </#if>
 	    </#if>
        <#if (conditionModel.isOptional??)>
 	    <#if (mybatisif!='' && conditionModel.isOptional=='1' )>
 	     <#noparse><#if (</#noparse>${mybatisif}<#noparse>?? && </#noparse> ${mybatisif}<#noparse>!='')></#noparse>
		</#if>
		</#if>
 	 	<#-- 条件连接符号 -->
 		<#if (conditionModel_index>0)>
 		${logicExp}
 		</#if>
 		<#--  取反前缀 -->
 		<#if conditionModel_index==0>
 		   <#if (branchTreeModel.conditionRelaType=="None" || branchTreeModel.conditionRelaType=="NotAll")>
			NOT(<#t>
		   </#if>
		 </#if>
 		<#-- 如果是分支模型，则递归 -->
 		<#if conditionModel.type==1>
 		<#list conditionModel as conditionModelItem>
 			<@makeCondition branchTreeModel=conditionModelItem/>
 		</#list>
 		<#else>
 		<#if (operator!='IN')>
 		<#lt> ${conditionModel.left!''} ${conditionModel.operator!''} ${conditionModel.right!''}<#lt>
 		<#else>
 		<#-- <#lt> ${conditionModel.left!''} ${conditionModel.operator!''}
 		<foreach collection="${mybatisif!''}" item="item" open="(" close=")" separator=",">
 		<#noparse>#{item}</#noparse>
		</foreach> -->
 		</#if>
		<#if (conditionModel.isOptional??)>
 		<#if (mybatisif!='' && conditionModel.isOptional=='1' )>
 		 <#noparse></#if></#noparse>
 		</#if>
		</#if>
 		</#if>
		<#--  取反前缀 -->
 		<#if conditionModel_has_next>
 		<#else>
 		  <#if (branchTreeModel.conditionRelaType=="None" || branchTreeModel.conditionRelaType=="NotAll")>
			)<#t>
		   </#if>
		 </#if>
 	</#list>
 	 <#-- 产生外层括号(第一层除外)  -->
 	 <#if (branchTreeModel.getLevel()!="1")>
 	 ${" )"}
 	 </#if>
 </#if>
</#macro>
<#-- 检查表名是否在关联模型列表 -->
<#macro isExistInJoinTableModel tableJoinModels1,tableName>
<#assign found='0'/>
<#-- <#list tableJoinModels1 as tableJoinModel >
  <#if (tableJoinModel.startTableDefineFrame.tableModel.code==tableName ||
       tableJoinModel.endTableDefineFrame.tableModel.code==tableName)>
  	<#assign found='1'/>
  	<#break>
  </#if>
</#list> -->
${found}<#rt>
</#macro>
<#-- 生成单个表关联模型 -->
<#macro printTableJoinModel model,firstFlag >
<#-- <#assign startOutputTabName><@getTableAlias tableName='${model.startTableDefineFrame.tableModel.code}'/></#assign>
<#assign endOutputTabName><@getTableAlias tableName='${model.endTableDefineFrame.tableModel.code}'/></#assign>
-->
<#assign startOutputTabName></#assign>
<#assign endOutputTabName></#assign>
<#-- <#if (startOutputTabName=='')>
<#assign startOutputTabName>${model.startTableDefineFrame.tableModel.code}</#assign>
</#if>

<#if (endOutputTabName=='')>
<#assign endOutputTabName>${model.endTableDefineFrame.tableModel.code}</#assign>
</#if> -->
<#-- 如果两个都没有输出过  -->
<#if (listMakedTab?index_of(startOutputTabName+',')==-1 && listMakedTab?index_of(endOutputTabName+',')==-1)>
<#-- 如果不是第一次（最靠近from)，则输出，-->
<#if firstFlag=='0'>
,
</#if>
<#-- 输出源表 -->
<#-- ${model.startTableDefineFrame.tableModel.code} <@getTableAlias tableName='${model.startTableDefineFrame.tableModel.code}'/> -->
<#-- 关联类型 -->
<#-- <#if (model.joinType=='inner')>
INNER JOIN
<#elseif (model.joinType=='left')>
LEFT OUTER JOIN
<#elseif (model.joinType=='right')>
RIGHT OUTER JOIN
<#else>
FULL  OUTER JOIN
</#if>
-->
<#-- 输出目标表 -->
<#-- ${model.endTableDefineFrame.tableModel.code} <@getTableAlias tableName='${model.endTableDefineFrame.tableModel.code}'/> -->
<#-- 输出关联条件-->
<#-- ON
<#list model.dbColumnJoinModels as columnJoinModel>
	<#if (columnJoinModel_index>0)>
	AND<#rt>
	</#if>
	 (${startOutputTabName}.${columnJoinModel.startColumnName} ${columnJoinModel.operatorSign} ${endOutputTabName}.${columnJoinModel.endColumnName})
</#list> -->
<#--记录已经输出-->
<#assign listMakedTab='${listMakedTab}${startOutputTabName},${endOutputTabName},'/>
<#--其中一个表已经输出过 -->
<#else>
<#-- 关联类型 -->
<#if (model.joinType=='inner')>
INNER JOIN
<#elseif (model.joinType=='left')>
LEFT OUTER JOIN
<#elseif (model.joinType=='right')>
RIGHT OUTER JOIN
<#else>
FULL  OUTER JOIN
</#if>
<#-- 输出目标表 -->
<#if (listMakedTab?index_of(endOutputTabName+',')==-1)>
<#--记录已经输出-->
<#assign listMakedTab>${listMakedTab}${endOutputTabName},</#assign>
<#-- ${model.endTableDefineFrame.tableModel.code} <@getTableAlias tableName='${model.endTableDefineFrame.tableModel.code}'/> -->
<#else>
<#--记录已经输出-->
<#assign listMakedTab>${listMakedTab}${startOutputTabName},</#assign>
<#-- ${model.startTableDefineFrame.tableModel.code} <@getTableAlias tableName='${model.startTableDefineFrame.tableModel.code}'/> -->
</#if>
<#-- 输出关联条件-->
ON
<#list model.dbColumnJoinModels as columnJoinModel>
	<#if (columnJoinModel_index>0)>
	AND<#rt>
	</#if>
	 (${startOutputTabName}.${columnJoinModel.startColumnName} ${columnJoinModel.operatorSign} ${endOutputTabName}.${columnJoinModel.endColumnName})
</#list>
</#if>
</#macro>

<#--  获取表别名 -->
<#macro getTableAlias tableName>
<#assign tableAlias1=''/>
	<#list tablesModel as tableModel>
    	<#if (tableModel.enName == tableName && tableModel.alias??)>
    		<#assign tableAlias1='${tableModel.alias}'/>
    		<#break>
		</#if>
    </#list>
${tableAlias1}<#rt>
</#macro>
<#-----********************* 以上为宏函数 ********************** -->
	SELECT
	<#--排重 -->
	<#if distinct>
	DISTINCT
	</#if>
    <#-- 循环选择列 -->
    <#list selectColumnTabModel as columnModel1 >
		  <#if !columnModel1.aggregation??>
		  	${columnModel1.columAndExp} <#if columnModel1.alias??>${columnModel1.alias}</#if><#if columnModel1_has_next>,</#if>
		  <#else>
		    <#if (columnModel1.aggregation?length>0)>
		  	${columnModel1.aggregation +"("+columnModel1.columAndExp+")"} <#if columnModel1.alias??>${columnModel1.alias}</#if><#if columnModel1_has_next>,</#if>
		  	<#else>
		  	${columnModel1.columAndExp} <#if columnModel1.alias??>${columnModel1.alias}</#if> <#if columnModel1_has_next>,</#if>
		  	</#if>
		  </#if>
    </#list>
FROM
    <#-- 循环表名列 -->
    <#list tablesModel as tableModel>
        <#--排除已经在表关联模型 -->
        <#--<#if tableJoinModels??>
        <#assign existFlag><@isExistInJoinTableModel tableJoinModels1=tableJoinModels tableName='${tableModel.enName}'/></#assign>

        <#else>
        <#assign existFlag='0'/>
        </#if> -->
        <#assign existFlag='0'/>
        <#if (existFlag=='0')>
    	<#if tableModel.alias??>
    		${tableModel.enName} ${tableModel.alias}
    	<#else>
    		${tableModel.enName}
    	</#if>
    	<#if tableModel_has_next>,</#if>
		</#if>
    </#list>
    <#--拼接表关联模型 -->
    <#if tableJoinModels??>
    <#-- 记录已经生成的表 -->
    <#assign listMakedTab=''/>
    <#assign tableJoinBuff=''/>
    <#assign firstFlag1='0'/>
    <#if (existFlag=='0')>
      <#assign firstFlag1='0'/>
    <#else>
      <#assign firstFlag1='1'/>
    </#if>
    <#list tableJoinModels as tableJoinModel >
      <#assign tableJoinBuff>${tableJoinBuff}<@printTableJoinModel model=tableJoinModel firstFlag=firstFlag1 /></#assign>
      <#if (firstFlag1=='1')>
        <#assign firstFlag1='0'/>
      </#if>
    </#list>
    <#-- 去掉最后逗号-->
    <#if (tableJoinBuff?length>0)>
    <#if (tableJoinBuff?substring(tableJoinBuff?length-1)==',')>
  	${tableJoinBuff?substring(0,tableJoinBuff?length-1)}
    <#else>
    ${tableJoinBuff}
    </#if>
    </#if>

    </#if>
    <#-- 列条件 -->
    <#list conditionTreeModel as conditionModelItem>
	<#assign where><@makeCondition branchTreeModel=conditionModelItem/><#t></#assign>
	</#list>
	<#if (where?length>0)>
WHERE
	${where}
	</#if>
    <#-- 分组列表 -->
    <#assign groups=""/>
	<#list tablesModel as tableModel>
    	<#if tableModel.group??>
    	    <#if tableModel.group>
    		<#assign groups>${groups}#{tableModel.columAndExp},</#assign>
    		</#if>
    	</#if>
    </#list>
    <#if (groups?length>0)>
    GROUP BY
    ${groups?substring(0,groups.length-1)}
    </#if>
    <#-- 组条件 -->
    <#assign groupCondition><@makeCondition branchTreeModel=groupConditionTreeModel/><#t></#assign>
	<#if (groupCondition?length>0)>
HAVING
     ${groupCondition}
	</#if>
<#if sortColumnModel?has_content>
ORDER BY
</#if>
    <#-- 排序列表 -->
    <#list sortColumnModel as columnModel >
    	${columnModel.fullFieldName} <#if columnModel.descending>DESC<#else>ASC</#if><#if columnModel_has_next>,</#if>
    </#list>
