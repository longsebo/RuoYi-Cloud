package com.ruoyi.job.config;

import com.ruoyi.common.security.aspect.RequestDefaultPropertyMethodArgumentResolver;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
public class ResourcesConfig implements WebMvcConfigurer{
    /**
     * 添加自定义参数绑定的
     *
     * @param resolvers
     */
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(new RequestDefaultPropertyMethodArgumentResolver(false));
    }

}
