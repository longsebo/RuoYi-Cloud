package com.ruoyi.system.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;

/**
 * 导入菜单
 */
@Data
public class ImpMenu extends SysMenu {
    /** 页面编码 */
    private String pageCode;
}
