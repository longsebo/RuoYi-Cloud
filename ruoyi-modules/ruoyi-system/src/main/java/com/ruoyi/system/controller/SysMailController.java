package com.ruoyi.system.controller;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.system.service.ISysMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 邮件control
 */
@RestController
@RequestMapping("/mail")
public class SysMailController extends BaseController {
    @Autowired
    private ISysMailService sysMailService;
    /**
     * 发送短信验证码
     * @param mail
     * @return
     */
    @PostMapping("/sendSmsVerifyCode/{mail}")
    public R<Boolean> sendSmsVerifyCode(@PathVariable("mail") String mail) {
        try{
            sysMailService.sendMailVerifyCode(mail);
            return R.ok(true);
        }catch(Exception e){
            return R.fail(false);
        }
    }
    /**
     * 验证短信验证码
     * @param mail
     * @return
     */
    @PostMapping("/verifyCode/{mail}/{code}")
    public R<Boolean> verifyCode(@PathVariable("mail") String mail,@PathVariable("code") String code) {
        try{
            sysMailService.verifyCode(mail,code);
            return R.ok(true);
        }catch(Exception e){
            return R.fail(false);
        }
    }
}
