package com.ruoyi.system.domain;


import lombok.Data;

@Data
public class NoPermissionButton {
    /** 菜单id */
    private long menuId;
    /** 页面id */
    private long pageId;
    /** 企业编码 */
    private String enterpriseCode;

    /** 应用编码 */
    private String applicationCode;
}
