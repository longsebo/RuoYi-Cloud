package com.ruoyi.system.service;

/**
 * 发送邮件接口
 */
public interface ISysMailService {
    /**
     * 发送邮件验证码
     * @param mail
     */
    public void sendMailVerifyCode(String mail);

    /**
     * 验证邮件验证码
     * @param mail 邮箱
     * @param code 验证码
     */
    public   void verifyCode(String mail, String code);
}
