package com.ruoyi.system.controller;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.domain.DynamicSql;
import com.ruoyi.system.service.IDynamicSqlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class SysDynamicSqlController {
    @Autowired
    private IDynamicSqlService dynamicSqlService;

    /**
     * 执行更新sql
     * @param dynamicSql  动态sql
     * @return
     */
    @PostMapping("dynamicsql/doupdate")
    public R<Integer> doUpdate(@RequestBody DynamicSql dynamicSql) {
        try{
            int rowNum = dynamicSqlService.executeSql(dynamicSql.getSql());
            return R.ok(rowNum);
        }catch(Exception e){
            return R.fail(-1);
        }
    }
}
