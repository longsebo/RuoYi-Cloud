package com.ruoyi.system.service;

/**
 * 发送短信接口
 */
public interface ISysSmsService {
    /**
     * 发送短信验证码
     * @param mobile
     */
    public void sendSmsVerifyCode(String mobile);

    /**
     * 验证短信验证码
     * @param mobile 手机号
     * @param code 验证码
     */
    public   void verifyCode(String mobile, String code);
}
