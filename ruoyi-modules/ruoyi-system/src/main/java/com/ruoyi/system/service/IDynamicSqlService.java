package com.ruoyi.system.service;

/**
 * 执行动态sql服务接口
 */
public interface IDynamicSqlService {
    public int executeSql(String sql);
}
