package com.ruoyi.system.domain.pageparse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Schema {
    private String id;
    private String name;
    private String category;
    private String component;
    private JSONObject attrs;
    private JSONObject formItemAttrs;
    private List<Schema> children;

    // Getters and setters
    public String getId() { return id; }
    public void setId(String id) { this.id = id; }
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public String getCategory() { return category; }
    public void setCategory(String category) { this.category = category; }
    public String getComponent() { return component; }
    public void setComponent(String component) { this.component = component; }
    public JSONObject getAttrs() { return attrs; }
    public void setAttrs(JSONObject attrs) { this.attrs = attrs; }
    public JSONObject getFormItemAttrs() { return formItemAttrs; }
    public void setFormItemAttrs(JSONObject formItemAttrs) { this.formItemAttrs = formItemAttrs; }
    public List<Schema> getChildren() { return children; }
    public void setChildren(List<Schema> children) { this.children = children; }
}



