package com.ruoyi.system.domain.pageparse;

import java.util.List;

public class TempNode {
    private String id;
    private String value;
    private String label;
    private String nodeType;
    private boolean hasChildren;
    private List<TempNode> children;

    public TempNode() {
        this.id = "";
        this.label = "";
        this.nodeType = "";
        this.hasChildren = false;
        this.children = null;
    }
    public TempNode(String id, String label, String nodeType, boolean hasChildren) {
        this.id = id;
        this.label = label;
        this.nodeType = nodeType;
        this.hasChildren = hasChildren;
        this.children = null;

    }

    // Getters and setters
    public String getId() { return id; }
    public void setId(String id) { this.id = id; }
    public String getValue() { return value; }
    public void setValue(String value) { this.value = value; }
    public String getLabel() { return label; }
    public void setLabel(String label) { this.label = label; }
    public String getNodeType() { return nodeType; }
    public void setNodeType(String nodeType) { this.nodeType = nodeType; }
    public boolean isHasChildren() { return hasChildren; }
    public void setHasChildren(boolean hasChildren) { this.hasChildren = hasChildren; }
    public List<TempNode> getChildren() { return children; }
    public void setChildren(List<TempNode> children) { this.children = children; }
}
