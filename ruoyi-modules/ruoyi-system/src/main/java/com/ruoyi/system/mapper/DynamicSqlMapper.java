package com.ruoyi.system.mapper;

public interface DynamicSqlMapper {
    int executeSql(String sql);
}
