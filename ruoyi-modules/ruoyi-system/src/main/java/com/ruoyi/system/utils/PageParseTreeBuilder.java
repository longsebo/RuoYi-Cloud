package com.ruoyi.system.utils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.business.domain.ExtButton;
import com.ruoyi.system.domain.pageparse.TempNode;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
public class PageParseTreeBuilder {
    /**
     * 从页面内容，递归提取想要对象
     * @param schema
     * @param pickCategory
     * @return
     */
    public static List<TempNode> buildTree(JSONObject schema, String pickCategory) {
        List<TempNode> returnData = new ArrayList<>();
        JSONArray childrens = schema.getJSONArray("children");

        if (childrens != null) {
            for (int i = 0; i < childrens.size(); i++) {
                JSONObject child = childrens.getJSONObject(i);
                String category = child.getString("category");
                String component = child.getString("component");

                if (!"layout".equals(category) && !"display".equals(category) ) {
                    TempNode tempNode = new TempNode(child.getString("id"),
                            child.getJSONObject("attrs").getString("label"),component, false);
                        List<TempNode> childReturnData = new ArrayList<>();
                        if ("ruoyi-ag-grid".equals(component)) {

                        } else {
                            childReturnData = buildTree(child, pickCategory);
                        }
                        if (!childReturnData.isEmpty()) {
                            tempNode.setChildren(childReturnData);
                            tempNode.setHasChildren(true);
                            returnData.add(tempNode);
                        } else {
                            tempNode.setHasChildren(false);
                            if(pickCategory.equals(category)) {
                                returnData.add(tempNode);
                            }
                        }

                } else {
                    List<TempNode> childReturnData = buildTree(child, pickCategory);
                    TempNode tempNode = new TempNode();
                    tempNode.setId(child.getString("id"));
                    tempNode.setLabel(child.getJSONObject("formItemAttrs") != null ? child.getJSONObject("formItemAttrs").getString("label") : category);
                    tempNode.setNodeType(component);
                    tempNode.setChildren(new ArrayList<>());

                    if (!childReturnData.isEmpty()) {
                        tempNode.setChildren(childReturnData);
                        tempNode.setHasChildren(true);
                         returnData.add(tempNode);
                    } else {
                        tempNode.setHasChildren(false);
                        if(pickCategory.equals(category)) {
                            returnData.add(tempNode);
                        }

                    }
                }
            }
        }
        return returnData;
    }

    /**
     * 从节点列表获取按钮列表
     * @param nodes
     * @return
     */
    public static List<ExtButton> getButtons(List<TempNode> nodes){
        List<ExtButton> retButtons = new ArrayList<>();
        for(TempNode node : nodes){
            if(node.isHasChildren()){
                retButtons.addAll(getButtons(node.getChildren()));
            }else{
                ExtButton extButton = new ExtButton();
                extButton.setId(node.getId());
                extButton.setName(node.getLabel());
                retButtons.add(extButton);
            }
        }
        return retButtons;
    }
}
