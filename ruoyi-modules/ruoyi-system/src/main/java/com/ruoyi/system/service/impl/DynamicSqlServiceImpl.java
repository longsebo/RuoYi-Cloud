package com.ruoyi.system.service.impl;

import com.ruoyi.system.mapper.DynamicSqlMapper;
import com.ruoyi.system.service.IDynamicSqlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 执行动态sql服务
 */
@Service
public class DynamicSqlServiceImpl implements IDynamicSqlService {
    @Autowired
    private DynamicSqlMapper dynamicSqlMapper;
    @Override
    public int executeSql(String sql) {
        return dynamicSqlMapper.executeSql(sql);
    }
}
