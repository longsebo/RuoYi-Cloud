package com.ruoyi.system.controller;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.system.service.ISysSmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sms")
public class SysSmsController extends BaseController {
    @Autowired
    private ISysSmsService sysSmsService;
    /**
     * 发送短信验证码
     * @param mobile
     * @return
     */
    @PostMapping("/sendSmsVerifyCode/{mobile}")
    public R<Boolean> sendSmsVerifyCode(@PathVariable("mobile") String mobile) {
        try{
            sysSmsService.sendSmsVerifyCode(mobile);
            return R.ok(true);
        }catch(Exception e){
            return R.fail(false);
        }
    }
    /**
     * 验证短信验证码
     * @param mobile
     * @return
     */
    @PostMapping("/verifyCode/{mobile}/{code}")
    public R<Boolean> verifyCode(@PathVariable("mobile") String mobile,@PathVariable("code") String code) {
        try{
            sysSmsService.verifyCode(mobile,code);
            return R.ok(true);
        }catch(Exception e){
            return R.fail(false,"验证码错误");
        }
    }
}
