package com.base64;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class TestBase64 {
    public static void main(String[] args) {
        //
        Base64.Decoder decoder = Base64.getDecoder();
        String contentTemp="5oKo55qE6aqM6K+B56CB5Li677yaJXPvvIzmnInmlYjml7bpl7TkuLolc+WIhumSn++8jOivt+WwveW/q+mqjOivge+8jOWIh+WLv+WwhumqjOivgeeggeazhOmcsuS6juS7luS6uuOAgg==";
        //
        byte[] decodedBytes = decoder.decode(contentTemp);

        //
        String decoderContentTemp = new String(decodedBytes, StandardCharsets.UTF_8);
        System.out.println(decoderContentTemp);
    }
}
