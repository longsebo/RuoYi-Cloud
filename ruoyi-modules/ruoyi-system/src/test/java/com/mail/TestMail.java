package com.mail;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

public class TestMail {
    public static void main(String[] args) {
        String userName = "ruoyi@163.com";
        String receiver = "ruoyi@qq.com";
//        System.out.println("配置文件中的信息 userName：{},receiver:{}",userName,receiver);
        SimpleMailMessage message = new SimpleMailMessage();
        // 发送着
        message.setFrom(userName);
        message.setSubject("测试发送邮箱的主题");
        message.setText("我是文本信息 测试是否发送成功");
        JavaMailSender javaMailSender;

        javaMailSender = new JavaMailSenderImpl();
        javaMailSender.send(message);

    }
}
