package com.ruoyi.system.utils;


import com.alibaba.fastjson.JSONObject;
import com.ruoyi.business.domain.ExtButton;
import com.ruoyi.system.domain.pageparse.TempNode;
import org.apache.commons.lang3.builder.ToStringExclude;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestPageParse {
    @Test
    public void testPageParse(){
       String pageContent="{\"mode\": \"read\", \"size\": \"default\", \"style\": \"\", \"children\": [{\"id\": \"search-form_7nfsylqkquo\", \"key\": 1, \"attrs\": {\"size\": \"default\", \"labelWidth\": \"120px\", \"labelPosition\": \"left\"}, \"category\": \"container\", \"children\": [{\"id\": \"el-row_6vxigat4vtc\", \"key\": 1, \"attrs\": {\"tag\": \"div\", \"align\": \"top\", \"gutter\": 4, \"justify\": \"start\"}, \"category\": \"layout\", \"children\": [{\"id\": \"el-col_1t9yafi1o0r\", \"key\": 1, \"attrs\": {\"span\": 12}, \"category\": \"layout\", \"children\": [{\"id\": \"text-input_1ro7j79b9yt\", \"key\": 1, \"attrs\": {\"style\": \"width: 100%\", \"runShow\": true, \"autofocus\": false, \"clearable\": false, \"placeholder\": \"输入字符串\", \"showWordLimit\": false, \"validateEvent\": false}, \"category\": \"form-item\", \"children\": [], \"component\": \"text-input\", \"formItemAttrs\": {\"label\": \"名称\"}}], \"component\": \"el-col\"}, {\"id\": \"field_6u44nq35iy\", \"key\": 1, \"attrs\": {\"span\": 12}, \"category\": \"layout\", \"children\": [{\"id\": \"el-row_1u11msjea7h\", \"key\": 1, \"attrs\": {\"tag\": \"div\", \"align\": \"top\", \"gutter\": 4, \"justify\": \"start\"}, \"category\": \"layout\", \"children\": [{\"id\": \"el-col_482qdutlte8\", \"key\": 1, \"attrs\": {\"span\": 12}, \"category\": \"layout\", \"children\": [{\"id\": \"search-design-button_4vq6ol3gdpc\", \"key\": 1, \"attrs\": {\"bg\": false, \"link\": false, \"size\": \"default\", \"text\": true, \"type\": \"primary\", \"label\": \"查询\", \"plain\": false, \"round\": false, \"circle\": false, \"loading\": false, \"disabled\": false, \"autofocus\": false, \"operationdata\": {\"interfaceCode\": \"\", \"parameterList\": [], \"returnValueList\": []}}, \"category\": \"button\", \"children\": [], \"component\": \"search-design-button\"}], \"component\": \"el-col\"}, {\"id\": \"field_3ek84vj20rk\", \"key\": 1, \"attrs\": {\"span\": 12}, \"category\": \"layout\", \"children\": [{\"id\": \"reset-design-button_efiuagbb71\", \"key\": 1, \"attrs\": {\"bg\": false, \"link\": false, \"size\": \"default\", \"text\": true, \"type\": \"primary\", \"label\": \"重置\", \"plain\": false, \"round\": false, \"circle\": false, \"loading\": false, \"disabled\": false, \"autofocus\": false, \"resetComponents\": \"\"}, \"category\": \"button\", \"children\": [], \"component\": \"reset-design-button\"}], \"component\": \"el-col\"}], \"component\": \"el-row\"}], \"component\": \"el-col\"}], \"component\": \"el-row\"}], \"component\": \"search-form\", \"formItemAttrs\": {\"label\": \"\", \"required\": false, \"labelWidth\": \"120px\"}}], \"labelWidth\": \"120px\", \"labelPosition\": \"auto\"}";
        JSONObject schema = JSONObject.parseObject(pageContent);
        List<TempNode> nodes = PageParseTreeBuilder.buildTree(schema, "button");
        List<ExtButton> buttons = PageParseTreeBuilder.getButtons(nodes);
        System.out.println(JSONObject.toJSONString(buttons));
    }
}
