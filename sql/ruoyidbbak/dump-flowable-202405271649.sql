-- MySQL dump 10.13  Distrib 8.2.0, for Win64 (x86_64)
--
-- Host: localhost    Database: flowable
-- ------------------------------------------------------
-- Server version	8.2.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `act_evt_log`
--

DROP TABLE IF EXISTS `act_evt_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_evt_log` (
  `LOG_NR_` bigint NOT NULL AUTO_INCREMENT,
  `TYPE_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `TIME_STAMP_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `USER_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `DATA_` longblob,
  `LOCK_OWNER_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  `IS_PROCESSED_` tinyint DEFAULT '0',
  PRIMARY KEY (`LOG_NR_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_evt_log`
--

LOCK TABLES `act_evt_log` WRITE;
/*!40000 ALTER TABLE `act_evt_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_evt_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ge_bytearray`
--

DROP TABLE IF EXISTS `act_ge_bytearray`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ge_bytearray` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `BYTES_` longblob,
  `GENERATED_` tinyint DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_BYTEARR_DEPL` (`DEPLOYMENT_ID_`),
  CONSTRAINT `ACT_FK_BYTEARR_DEPL` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_re_deployment` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ge_bytearray`
--

LOCK TABLES `act_ge_bytearray` WRITE;
/*!40000 ALTER TABLE `act_ge_bytearray` DISABLE KEYS */;
INSERT INTO `act_ge_bytearray` VALUES ('0f4c89d5-a45a-11ee-b377-005056c00001',1,'source',NULL,_binary '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:flowable=\"http://flowable.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:bpmn2=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://flowable.org/bpmn\" id=\"diagram_Process_1701854918145\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd\">\n  <process id=\"Process_1701854918145\" name=\"业务流程_1701854918145\" isExecutable=\"true\">\n    <startEvent id=\"Event_13w76qi\" name=\"开始\"></startEvent>\n    <userTask id=\"Activity_1e348q9\" name=\"124\"></userTask>\n    <sequenceFlow id=\"Flow_0btbv9z\" sourceRef=\"Event_13w76qi\" targetRef=\"Activity_1e348q9\"></sequenceFlow>\n    <exclusiveGateway id=\"Gateway_0rpylue\"></exclusiveGateway>\n    <sequenceFlow id=\"Flow_1papq8h\" sourceRef=\"Activity_1e348q9\" targetRef=\"Gateway_0rpylue\"></sequenceFlow>\n    <userTask id=\"Activity_1s82xkw\" name=\"135\"></userTask>\n    <sequenceFlow id=\"Flow_0r31rfh\" sourceRef=\"Gateway_0rpylue\" targetRef=\"Activity_1s82xkw\"></sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_Process_1701854918145\">\n    <bpmndi:BPMNPlane bpmnElement=\"Process_1701854918145\" id=\"BPMNPlane_Process_1701854918145\">\n      <bpmndi:BPMNShape bpmnElement=\"Event_13w76qi\" id=\"BPMNShape_Event_13w76qi\">\n        <omgdc:Bounds height=\"36.0\" width=\"36.0\" x=\"-148.0\" y=\"12.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_1e348q9\" id=\"BPMNShape_Activity_1e348q9\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"-60.0\" y=\"-10.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Gateway_0rpylue\" id=\"BPMNShape_Gateway_0rpylue\">\n        <omgdc:Bounds height=\"50.0\" width=\"50.0\" x=\"95.0\" y=\"5.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_1s82xkw\" id=\"BPMNShape_Activity_1s82xkw\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"200.0\" y=\"-10.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_0btbv9z\" id=\"BPMNEdge_Flow_0btbv9z\">\n        <omgdi:waypoint x=\"-112.0\" y=\"30.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"-60.0\" y=\"30.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_1papq8h\" id=\"BPMNEdge_Flow_1papq8h\">\n        <omgdi:waypoint x=\"40.0\" y=\"30.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"95.0\" y=\"30.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_0r31rfh\" id=\"BPMNEdge_Flow_0r31rfh\">\n        <omgdi:waypoint x=\"145.0\" y=\"30.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"200.0\" y=\"30.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>',NULL),('2be004bb-9d50-11ee-b541-005056c00001',2,'source',NULL,_binary '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:flowable=\"http://flowable.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:bpmn2=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://flowable.org/bpmn\" id=\"diagram_Process_1701854918145\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd\">\n  <process id=\"Process_1701854918145\" name=\"业务流程_1701854918145\" isExecutable=\"true\">\n    <startEvent id=\"Event_13w76qi\" name=\"开始\"></startEvent>\n    <userTask id=\"Activity_1e348q9\" name=\"124\"></userTask>\n    <sequenceFlow id=\"Flow_0btbv9z\" sourceRef=\"Event_13w76qi\" targetRef=\"Activity_1e348q9\"></sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_Process_1701854918145\">\n    <bpmndi:BPMNPlane bpmnElement=\"Process_1701854918145\" id=\"BPMNPlane_Process_1701854918145\">\n      <bpmndi:BPMNShape bpmnElement=\"Event_13w76qi\" id=\"BPMNShape_Event_13w76qi\">\n        <omgdc:Bounds height=\"36.0\" width=\"36.0\" x=\"-148.0\" y=\"12.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_1e348q9\" id=\"BPMNShape_Activity_1e348q9\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"-60.0\" y=\"-10.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_0btbv9z\" id=\"BPMNEdge_Flow_0btbv9z\">\n        <omgdi:waypoint x=\"-112.0\" y=\"30.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"-60.0\" y=\"30.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>',NULL),('2df3253d-a45c-11ee-ba71-005056c00001',1,'source',NULL,_binary '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:flowable=\"http://flowable.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:bpmn2=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://flowable.org/bpmn\" id=\"diagram_Process_1701854918145\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd\">\n  <process id=\"Process_1701854918145\" name=\"业务流程_1701854918145\" isExecutable=\"true\">\n    <startEvent id=\"Event_13w76qi\" name=\"开始\"></startEvent>\n    <userTask id=\"Activity_1e348q9\" name=\"124\"></userTask>\n    <sequenceFlow id=\"Flow_0btbv9z\" sourceRef=\"Event_13w76qi\" targetRef=\"Activity_1e348q9\"></sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_Process_1701854918145\">\n    <bpmndi:BPMNPlane bpmnElement=\"Process_1701854918145\" id=\"BPMNPlane_Process_1701854918145\">\n      <bpmndi:BPMNShape bpmnElement=\"Event_13w76qi\" id=\"BPMNShape_Event_13w76qi\">\n        <omgdc:Bounds height=\"36.0\" width=\"36.0\" x=\"-148.0\" y=\"12.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_1e348q9\" id=\"BPMNShape_Activity_1e348q9\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"-60.0\" y=\"-10.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_0btbv9z\" id=\"BPMNEdge_Flow_0btbv9z\">\n        <omgdi:waypoint x=\"-112.0\" y=\"30.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"-60.0\" y=\"30.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>',NULL),('322fc00a-9fd3-11ee-8a8a-005056c00001',2,'source',NULL,_binary '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:flowable=\"http://flowable.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:bpmn2=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://flowable.org/bpmn\" id=\"diagram_process_1703143742179\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd\">\n  <process id=\"process_1703143742179\" name=\"OA_1703143742179\" isExecutable=\"true\">\n    <documentation>测试新建流程202312211529</documentation>\n    <startEvent id=\"StartEvent_de5ekdnxz46\" name=\"开始\" flowable:formKey=\"1\"></startEvent>\n    <userTask id=\"Activity_0dtdyif\" name=\"202312211529\"></userTask>\n    <sequenceFlow id=\"Flow_0oe32pj\" sourceRef=\"StartEvent_de5ekdnxz46\" targetRef=\"Activity_0dtdyif\"></sequenceFlow>\n    <exclusiveGateway id=\"Gateway_10mwbrh\"></exclusiveGateway>\n    <sequenceFlow id=\"Flow_1xd9921\" sourceRef=\"Activity_0dtdyif\" targetRef=\"Gateway_10mwbrh\"></sequenceFlow>\n    <userTask id=\"Activity_1pzmdnl\" name=\"1\"></userTask>\n    <sequenceFlow id=\"Flow_11g97ig\" sourceRef=\"Gateway_10mwbrh\" targetRef=\"Activity_1pzmdnl\"></sequenceFlow>\n    <userTask id=\"Activity_18o5pof\" name=\"2\"></userTask>\n    <sequenceFlow id=\"Flow_1uks9sk\" sourceRef=\"Gateway_10mwbrh\" targetRef=\"Activity_18o5pof\"></sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_process_1703143742179\">\n    <bpmndi:BPMNPlane bpmnElement=\"process_1703143742179\" id=\"BPMNPlane_process_1703143742179\">\n      <bpmndi:BPMNShape bpmnElement=\"StartEvent_de5ekdnxz46\" id=\"BPMNShape_StartEvent_de5ekdnxz46\">\n        <omgdc:Bounds height=\"36.0\" width=\"36.0\" x=\"-28.0\" y=\"22.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_0dtdyif\" id=\"BPMNShape_Activity_0dtdyif\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"60.0\" y=\"0.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Gateway_10mwbrh\" id=\"BPMNShape_Gateway_10mwbrh\">\n        <omgdc:Bounds height=\"50.0\" width=\"50.0\" x=\"215.0\" y=\"15.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_1pzmdnl\" id=\"BPMNShape_Activity_1pzmdnl\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"320.0\" y=\"-50.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_18o5pof\" id=\"BPMNShape_Activity_18o5pof\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"320.0\" y=\"110.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_0oe32pj\" id=\"BPMNEdge_Flow_0oe32pj\">\n        <omgdi:waypoint x=\"8.0\" y=\"40.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"60.0\" y=\"40.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_1xd9921\" id=\"BPMNEdge_Flow_1xd9921\">\n        <omgdi:waypoint x=\"160.0\" y=\"40.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"215.0\" y=\"40.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_11g97ig\" id=\"BPMNEdge_Flow_11g97ig\">\n        <omgdi:waypoint x=\"265.0\" y=\"40.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"293.0\" y=\"40.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"293.0\" y=\"-10.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"320.0\" y=\"-10.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_1uks9sk\" id=\"BPMNEdge_Flow_1uks9sk\">\n        <omgdi:waypoint x=\"240.0\" y=\"65.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"240.0\" y=\"150.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"320.0\" y=\"150.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>',NULL),('4a23ccbc-9fd3-11ee-8a8a-005056c00001',1,'process_1703143742179.bpmn20.xml','4a23a5ab-9fd3-11ee-8a8a-005056c00001',_binary '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:flowable=\"http://flowable.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:bpmn2=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://flowable.org/bpmn\" id=\"diagram_process_1703143742179\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd\">\n  <process id=\"process_1703143742179\" name=\"OA_1703143742179\" isExecutable=\"true\">\n    <documentation>测试新建流程202312211529</documentation>\n    <startEvent id=\"StartEvent_de5ekdnxz46\" name=\"开始\"></startEvent>\n    <userTask id=\"Activity_0dtdyif\" name=\"202312211529\"></userTask>\n    <sequenceFlow id=\"Flow_0oe32pj\" sourceRef=\"StartEvent_de5ekdnxz46\" targetRef=\"Activity_0dtdyif\"></sequenceFlow>\n    <exclusiveGateway id=\"Gateway_10mwbrh\"></exclusiveGateway>\n    <sequenceFlow id=\"Flow_1xd9921\" sourceRef=\"Activity_0dtdyif\" targetRef=\"Gateway_10mwbrh\"></sequenceFlow>\n    <userTask id=\"Activity_1pzmdnl\" name=\"1\"></userTask>\n    <sequenceFlow id=\"Flow_11g97ig\" sourceRef=\"Gateway_10mwbrh\" targetRef=\"Activity_1pzmdnl\"></sequenceFlow>\n    <userTask id=\"Activity_18o5pof\" name=\"2\"></userTask>\n    <sequenceFlow id=\"Flow_1uks9sk\" sourceRef=\"Gateway_10mwbrh\" targetRef=\"Activity_18o5pof\"></sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_process_1703143742179\">\n    <bpmndi:BPMNPlane bpmnElement=\"process_1703143742179\" id=\"BPMNPlane_process_1703143742179\">\n      <bpmndi:BPMNShape bpmnElement=\"StartEvent_de5ekdnxz46\" id=\"BPMNShape_StartEvent_de5ekdnxz46\">\n        <omgdc:Bounds height=\"36.0\" width=\"36.0\" x=\"-28.0\" y=\"22.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_0dtdyif\" id=\"BPMNShape_Activity_0dtdyif\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"60.0\" y=\"0.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Gateway_10mwbrh\" id=\"BPMNShape_Gateway_10mwbrh\">\n        <omgdc:Bounds height=\"50.0\" width=\"50.0\" x=\"215.0\" y=\"15.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_1pzmdnl\" id=\"BPMNShape_Activity_1pzmdnl\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"320.0\" y=\"-50.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_18o5pof\" id=\"BPMNShape_Activity_18o5pof\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"320.0\" y=\"110.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_0oe32pj\" id=\"BPMNEdge_Flow_0oe32pj\">\n        <omgdi:waypoint x=\"8.0\" y=\"40.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"60.0\" y=\"40.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_1xd9921\" id=\"BPMNEdge_Flow_1xd9921\">\n        <omgdi:waypoint x=\"160.0\" y=\"40.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"215.0\" y=\"40.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_11g97ig\" id=\"BPMNEdge_Flow_11g97ig\">\n        <omgdi:waypoint x=\"265.0\" y=\"40.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"293.0\" y=\"40.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"293.0\" y=\"-10.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"320.0\" y=\"-10.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_1uks9sk\" id=\"BPMNEdge_Flow_1uks9sk\">\n        <omgdi:waypoint x=\"240.0\" y=\"65.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"240.0\" y=\"150.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"320.0\" y=\"150.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>',0),('4a3e0b7d-9fd3-11ee-8a8a-005056c00001',1,'process_1703143742179.process_1703143742179.png','4a23a5ab-9fd3-11ee-8a8a-005056c00001',_binary '�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\�\0\0\0�\0\0\0!�\�\0\0IDATx^\�\�m�T\���q��VoӴ(\��/JҤI\�_\�k\�h�7�,,�\0�Uij] ��m\�5M��X�ͽ�\�\�^�O�O<̰w��+�uq\�2첸,Pv��\��N\�lw�\�̙3眙�\�\�\����{\�\�3\�̞\�\�\��&\0\0*\�sYww\�K;w\��2�ɘ-[�0\�q��\�\�e�\�{\�\�\0PeN$_vNҦ��\�\r\r�\�\�\�3y\�\�\�ooot\�9\�~�\0\0U$W�r��O\�L\�\'�\�\�;�\�m?G\0�*�o�r%�c\�ypB9l?G\0�*���\�\'l�z#χ�\0�(j(Ϟʛ\�֙[W�#�-\�\�\�dC(@�(�<3\�c\�o���\�%\�F�\�:{{&�J\0P&J(?;\�拤7�\�\�۞�?�\0��ʃ۞\�\�Ygo\�\�B	\0\�D	\��-��@z#\�\�\��C(@B�k%\0(%�\�*W;�\�\�:{{&�J\0P&J(���\�\�Ygo\�\�B	\0\�D	\�ɞ�f�\�\'|��e�\�ޞ�?�\0��J��v�\��R�\�\�1ɆP�2�B9<l�v�\�R�\�:\�\�L\�!�\0�L�P\�;\�|��_$��u�;OzC(@���t�\�]Y�\�\�\�|q�G��m��L>�\0�	\ne��\�B\�\�e\�!�\0�LP(�\\E\Z�\\{L\�!�\0�LP(\���:\���\�C(@��P2\�B	\0\�J]C(@B�k%\0(C(u\r�\0e��!�\0��\�5�\0�!���P�2�R\�J\0P�P\�\ZB	\0\�J]C(@B�k%\0(C(u\r�\0e��!�\0��\�5�\0*dƌۜ�\�^n#���P@�8�4#\ZLB�k%\0TȘP��P\�\ZB	\0\��`J]C(�Bi�LB�k%\0D��M�����\�:tȷ�ֆP@0{Y�\� ��m3\�\�[�\�Y�f�I�&�\r6�\�\�\�J\0��L�HO��r߾}榛n�ǋP@�H9���\�\�z(��@(�q�\�\�@z��!�\0A\Z���P\�\ZB	\0\�\�g��\�A(�\�\�\�\�ͳ\�>\�r\�̙fǎ�mji%\0D@(���\��k�\�}\�\�\�J\0��P6\�J%�1�uww��s\�\�/3���\�0�\�q��\�\�e�\�{\�\� ��;\�|\�\����\�\�\�I\�\�\�\����!\�Ŕ\�q�ǿ��}\�90�\�\�\Z�l\�!�Jȕ���\�\'���\�\��~\��\�m?Ghl��q�P*!\�n\�JR\�\�\�\�\�\�s�\�F(w���6Bٸ\��@��\�\�Sy\���:s`\�Jw\�e���l80`#��;���r`�\�1�7�\�|\�֒q#\�d��=80`#��;���r`|v�\�Io>?�ѷ=80`#��;���r`\�\��/�\�\�:{{&�p`�F(w8(\��ؿ�\�Hod��=80`#��;���r`\�\�\rl��q�\�Qy��Hod��=80`#��;���r`tm_\��7�\�ޞ�?�\�\�\�JD90N\�\�5�7?ዤ,�u\�\�L�\����P6\�p>P\"\�\�׎\��P\�2{;&\�p`$\�\�\��ZG(w8(\��6];\��B)\�d�o{&\�p`$2řޑ?\��l\�\�|�D�C\�y\�\�\��\"鍬\�\�y\��ؼH���Y7�$��;��(x`8W����\�\�\�c�8\�#\�ȶ\\]&�X\�+I�\�F(w8(t`��,4\\]&���b�\�5�P6\�p>P\"\���rYh\�s\��1ч�$\�bXl}M ��;��:0\���:\���\�ÁY\�F\�N-Bٸ\��@	]ÁI�\�+u{Ue\�N\�\����W����\\�z\�G+V�\�_�l\�\�O~\�\�\�\�\�\�|���\�\�\�\�ˏ�\\�r\�%K�\���K\�}4]S��~č^\�ϫ:Bٸ���`׮]Ϳ�\�o{-Zd�8��_~\��\�/1�~��9y\�\�|,\�e�ҥK/Ο?��\�MMMM�\�}6]���Q_�\�.\�\�W�l\�I\�|�w\�\��\�\�~w\���\�7��\�9q\�Ũd{�<\'�_>�\�\�/͜9s�}\��Cפv`ԟ�\"�\�~*�P6\�$>8��\�\��\�n�@�\�\�\�̙3vK\"�/��7o޹ٳg�Ⱦ�zŁ�k\�)\������\"��;�\�N׾�aÆO|\�A\�\�\�m7/ٟ\�!\�\ns�}�\��C\�$:0\�S��V����P6\�\�>8-�֪U��q300`w.�\�e˖��;w\�z�\�\�\r���}`ԧrǬ\��OE%C�\�d.\r\r��.�ʏ\�<s\�\�\�sT�ӰK�+\�.��쨜d�---�f͚�ܾ\��P\�\ZB9jl\�\�\�\����D\�~\�ǲ��\�\�r\�z{{}_�L\�\�ȑ#r\��\�\�(��\�C=T�+I�\�΂N;_�w\�\��^J]C(]v$�\�y\\\�ϸ�ۏ\�XV2�\�l\��\�\�\��|>\�ϕeu\�y\�\�~\�9uf���ڳg\�Ty\�N\�?�,Fno\�\�\�ӧO�ھO\��P\�\ZB鋖\\\�c\��(\�Fқ�\�\�߮\Z�����\\\�8s^����<\�\���I!�D^�Z\rk֬9\�|�>mߧz O�}�f�7\�|\�\�Q	�UP\�J�e\�\��\�\�r[\�\�W]�C�\Z%o& W��O��Vr�s\�\��\�7% ����C��\�E�e\�\���\'\�~T�D$\�;����ݯ�Z�~��\�\�\�\��\�J]Ӡ����\�\�2h�b�\�D�?C(Q�s5wUss�\�\�\�\�UQ\�\���sE\�\�\�\��O\�}�e�R\�4`(K�RP��b�]\�HzJ�_eE(Qԛo��B޻U�v�hh\�G����͙[\�\�6B�kj1�Q�\�ĉQP\�\�2h}��\�Ĺ�#�(\�g�\�\'o`��ڵk�\����w�>j$\�Ȅ�\����\�PF�Z�$�PP\�c�n��<n$=I\�g*%��_�%�\�C�\\.�\�\�?�q�}5\Zs\�\n=�J]S\��Z#��Ų\��8i$=i\�\�\�%�Z�t\�P��\�d!���\�\�\���Q�\�+\�$F(uM��2\�kmD�\�	�eڑ\��y�K\"�������\�_|a7�*N�<y\���\��\�F\'-{ܓ�\�5u\�q_k\��v�}%)�\�Ew\��E\�ch/ƙ3g��p\�ݬ�p��}�\�Iʎ�\�\�\�f���\�-߷o�y\�w\�\�\�p�\�er���9gϞ����\�:tȷ}ؾ\n�+��B\�6m\�d6l\�0:c\��\r�\�\�믿n>��c\�mD�����>}���\�\�\�\�b\�	�]Q�\Z6\r-\�߭c\�	`�E�]\�rEy\�ĉC3j��rی�\�\�\�\�:zR�<y��\�H�o߾\�\\v\�e\�\���\�E���\��F\�M�2E\�\�ЍҬY�̤I�\�HE�\�B\�\�\��\��\�w\�ݼys�ې\�\�o}Gb`.�\�\�\��f\���L�]Q��Z��\�YP$\�+˴c�\��\�\�\�#��\�\�3J\�*�cF\���\�>i�\�r\�ĉ&�͚L&㞔\�x\�\rwySS���\�\�\�\�\�_��9u\�T\�\�2\��\�7\�k��\�^�\�:	�\\�\�t\�M\�\�v�\�\��.l_a\�\��\�q�݆,��\��o\�\�^��y\�\�ܿ����NB��6\"�\�ER~&\��״b�\��\�\�\�\��L˫^�nݚ��W����b�R\�\�\�?t�\�\�Ƽ\�\�{\�\�\�|\�;橧�2G�u�����n/s\�\�A\�m�+vw�\�[\'\�\�Wغ�}���뮻ܐ��\�fpp�\�m\\q\�\�\�����\�A�(S\��Z#It\nE2\�\�Q&�e���_kk\�F-��\�W��ի3j\�\�QF:i\�\r�7�V�2w\�y\�\�\�_�\�\�̺u\�\�\��\�7\�$%\�]\�\�\�\�\�\��\�n\�\�q?۔}\�+��B\�\�\��\�\�+�=\�\�p\�\r\�n\�\�sG�.�\�v��\�W�j��\�?�n+lj4���\�,q\���W�m7�q\�\'PY˖-�c\�ҥ\�hU������3j\�y�JJy�|�U��e\��\�7ݫ*����{�q\�\�\�\���me~�ӟ��\�+��B\�\n\��к\'�|\�\�\�\��/\�\�c��nc\�֭\�{\�����\�x����;w�n+lj1�	����E\��}��,\��\�#ﭺ`��\�\�~�\�\�?�|�\�<\�\��\��o�.^�؜;w\�}\�]�\�\�?��̣�>j\�\�\�\�\�\���\�\�O?\��Jt$f۶m�\�\�c;`a�\n[���ur�:;;G�/?G-vެ\\�ҽ\"\�~Fu\Z,�\"J���IO\�\�E�e��\�dɒM\��\�!��\�/_��o�iӦ����[o�u\�jK\"y\��׺/j�\�w�[t{����\��\�g?3˗/wOf\���9~��y\�\�gݏgΜiv\�\�i_A\�\�\��N\��}\�g��^z\�\�?	�\rY./���϶����|\�+\�c�}E���RP\�D\�\��\�bv\0�\�\�@Ο?�\�j�>ʁ��\�N$��}���	\�O<,�Wy\�x\�nd\�~۵\�\�2�\�\�5\�\\\���\'�x\�]&�\Z��#\�+h]ؾ\�\��\�4\��\��\�\�W_m~�\�_�ކ,��\�+]\�\�\�\�#��W�\�\�D�\r��2!8N\�M�x�IOP,\�\�\�\n\�@��~�%���jpn���P>mߧz7�Ly��C)�\"56rq\"鉲��\�j\�\�\�ӯ�7o޹J��ʎ���k�ܾ}�\��\�5\rJ+��\\ŭa�	�]�\�̞=�G�-\Z\Z�{V}}}�\�D\�3w\�\��^J]C(]��V�o(��\�\�\�/r>\'\'�r\Z����郙3g>j߇zB(u\r�U�xU\�v�ʚ;w\�����S庲t�$\�%�Ε��,��J]C(\�)w\�ʽ��f͚�ܹ�L�}`G~&y�ޯ$=�R\�J�rŬ\\�t��Ξ={`͚5\�Μ9c7�$�N�\�5\�\�־z����P\�\ZB(\����?@7\�\�o��Ӎ9s\�>��\�8q��_^y���g��+\�W�\�~d\�m\�3B�keAi\�-��\0�G\�@\�9\�	]\���]�vO.�\�\�\�\�y�����\�3}}}\�\�pNF[\�\r\�G޻\��|^=��@�R\�\�PI#�\�\�qɽ\�\��/N0��_�\�DP\�U\��5@\�\�nY.\�e;\�\�\�A#!���P7vq?@�#���PFRj\�J\�\0��P\�\ZBY\��E\�\0�J]C(KR,�\�\�@q�R\�ʒ�a�\�\0P\ZB�ke,v\� >B�kel^[F�$�\0\�A(u\r�Ld��30�\�^\0�J]C(�	%\0��P\�\ZB���:B�ke2�@\���!�\�J\0�#���P&C(��P\�\ZB���:B�ke2�@\���!�\�J\0�#���P&C(��P\�\ZB���:B�ke2�@\�2�\�š�!\�	���8\�\�1\'�\�\�s�\�%�\�\�r�c�����6S�9r\�ȟ�P#DG(�.�\�\�\�\�\�>�\�\�����\�8�{�\�\�\�/:�<\�\�T�9Bt�@Y\�\�Y�d�9/?#c*>\�\�\�O$\"�\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\0\0� �\�c.\�\�\�~i\�Ν_f2�e\��\�\�<\�s�ܱl6{���\0@b�2\'�/;\'i\�\�\�k���\��\�\�\n�<\�\�����:\�j?G\0��LF�$\�$m���\�O>�\�wB�\�~�\0 B��|��+I#σ\�a�9�De2\�32��\�To\����#\0H�P&5�gO\�M\�\�́�+ݑ��e\�vL�!�\0RG(��\�3=f��_��\�Z2nd����g\���:B�L�P~v�\�Io>?�ѷ=%�\�\�d��\�ට|�\�F\�\�\�3\�PH�L&J(\�oi\�\�Ygo\�\�B	 u�2B�k%�\�\�d��R^\�j\�Ygo\�\�B	 u�2�(�\�ھ\�Hod��=%�\�\�d��\�d\�^�\��H\�2Ygo\�\�B	 u�2�(���k\�z_(e���l%�\�\�d\"�rx\�t\�X\��,�u�\�\�C(��P&S,�\�\�;�l\�Iod\�Γ\�J\0�#�\��s��\�ʚ}\�>拣=��l\�\�e\�!�\0RG(�	\ne��\�B\�\�e\�!�\0RG(�	\ne��\�B#�k>�@\�e2A��\�W\�\��c���:B�LP(�\�\r��:B��\�5�@\�e2�R\�J\0�#�\�J]C(��P&C(u\r��:B��\�5�@\�e2�R\�J\0�#�\�J]C(��P&C(u\r��:B��\�5�@\�e2�R\�J\0�#�\�J]C(��P&C(u\r��:B��\�5�@\�e0\�q\�\�\�-\�r�\�5�@\�e0y\\F&4��R\�J\0�#��Ƅ24��R\�J\0�#��BLB�k%�\�\�`��\�\r&�\�5�@\���0\�ʎ�\�\�\�f���}\�ji%\0T�\�1�mF�}뵵�U\�\�\�\�ɓ\�_|\�ۦV�P@���B9q\�D�\�fM&�qc�\�o����!�\0P!\�驇P677�?�М<y\�\r\�{\�\�ۦV�P@���B\�ͪU�̝w\�\�[^KC(@�z	e.�3MMM\�\�\�Ӿu�4�\0���P<x\�,^�؜;w\�}\�\�]�|\�\�\�J\0P�B9m\�4\�\�/�\r6�[o�\�}Q��M��\0e\�!�F�i�7����mje%\0(S���!�\0��\�5�\0�!���P�2�R\�J\0P�P\�\ZB	\0\�J]C(@B�k%\0(C(u\r�\0e��!�\0��\�5�\0�!���P�2�R\�J\0P�P\�\ZB	\0\�J]C(@B�k%\0(C(u\r�\0e��!�\0��\�5�\0�!���P�2�R\�J\0P�P\�\ZB	\0\�J]C(@B�k%\0(C(u\r�\0e��!�\0��\�5�\0�!���P�2�R\�J\0P&�\�\\\Z\Z򝰙ʏ\�<sB9l?G\0�*\�\�r\�z{{}\'m�\�s\�ȑ?9�\�m?G\0�*\�f�\����\�\��~�,�3\�\�?|�\��N$�:3\�~�\0\0U&\'g��q\������\�\�.�?�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0��?�	?{�h\r\0\0\0\0IEND�B`�',1),('672db81f-a45c-11ee-ba71-005056c00001',1,'process_1702865697042.bpmn20.xml','672db81e-a45c-11ee-ba71-005056c00001',_binary '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:flowable=\"http://flowable.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:bpmn2=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://flowable.org/bpmn\" id=\"diagram_Process_1701854918145\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd\">\n  <process id=\"Process_1701854918145\" name=\"业务流程_1701854918145\" isExecutable=\"true\">\n    <startEvent id=\"Event_13w76qi\" name=\"开始\"></startEvent>\n    <userTask id=\"Activity_1e348q9\" name=\"124\"></userTask>\n    <sequenceFlow id=\"Flow_0btbv9z\" sourceRef=\"Event_13w76qi\" targetRef=\"Activity_1e348q9\"></sequenceFlow>\n    <exclusiveGateway id=\"Gateway_0rpylue\"></exclusiveGateway>\n    <sequenceFlow id=\"Flow_1papq8h\" sourceRef=\"Activity_1e348q9\" targetRef=\"Gateway_0rpylue\"></sequenceFlow>\n    <userTask id=\"Activity_1s82xkw\" name=\"135\"></userTask>\n    <sequenceFlow id=\"Flow_0r31rfh\" sourceRef=\"Gateway_0rpylue\" targetRef=\"Activity_1s82xkw\"></sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_Process_1701854918145\">\n    <bpmndi:BPMNPlane bpmnElement=\"Process_1701854918145\" id=\"BPMNPlane_Process_1701854918145\">\n      <bpmndi:BPMNShape bpmnElement=\"Event_13w76qi\" id=\"BPMNShape_Event_13w76qi\">\n        <omgdc:Bounds height=\"36.0\" width=\"36.0\" x=\"-148.0\" y=\"12.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_1e348q9\" id=\"BPMNShape_Activity_1e348q9\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"-60.0\" y=\"-10.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Gateway_0rpylue\" id=\"BPMNShape_Gateway_0rpylue\">\n        <omgdc:Bounds height=\"50.0\" width=\"50.0\" x=\"95.0\" y=\"5.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_1s82xkw\" id=\"BPMNShape_Activity_1s82xkw\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"200.0\" y=\"-10.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_0btbv9z\" id=\"BPMNEdge_Flow_0btbv9z\">\n        <omgdi:waypoint x=\"-112.0\" y=\"30.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"-60.0\" y=\"30.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_1papq8h\" id=\"BPMNEdge_Flow_1papq8h\">\n        <omgdi:waypoint x=\"40.0\" y=\"30.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"95.0\" y=\"30.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_0r31rfh\" id=\"BPMNEdge_Flow_0r31rfh\">\n        <omgdi:waypoint x=\"145.0\" y=\"30.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"200.0\" y=\"30.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>',0),('67ab4c90-a45c-11ee-ba71-005056c00001',1,'process_1702865697042.Process_1701854918145.png','672db81e-a45c-11ee-ba71-005056c00001',_binary '�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\�\0\0\0Z\0\0\08�+\0\0XIDATx^\��oUU\�+$5\�(���\��41fƄ��a2*��վ!i�5��\�G�vB�\Zf\�/\�\�v\�LF�\�D��\�\�\�ҹ\n�X�\�B�R��-%m�\�\�;�}wo\�y\�{�ww}��\�\�\�\�\�s\�Z\�\�ǹ99 �!~\�\�\��\�\�_�sKK�8x\� �aɿ��X,v���\�)\��\0�I�\�y�i�=򠈁�155%�_�eX\�w��[[۸4\�\�\�1 S �\�!\�ȑ�z��̫��X\Z\�z�\0\�\�>B0��^�#G�\� �1�#\02򀏐��\�\�\�E\':\�1 S x	y�Sc\\\��\��Ut\�\��M\�\�\�`�1@� x	y�cL�\��S�/��G\�M�i4O]\�/D	򀗐L01Ə]\�I�pu�볤\�!��1@� x	y�c�9�\�$C��y\�\�� J���<`��1N�O2�+��.���y�K\�&��c�(A\�\�	&Ơw���pE\�\�\�!��1@� x	y�c\�ٙdW4O]\�/D	򀗐L01\�H_�8u`s�)h\Z\�S���\�\0Q�<\�%\�L�A�wǮ$c\�4u9(�`��ܮN\0\� x	y�#cLO����IƠi4/iyȷ`�@��\Z��	|�<\�%\�\�3\�i\�#;�L\�\�\�n\�	\�\��[���?Q�>@\�\�	s\ZC>2\�\�i\'���$3��ehY<�.\�ꕤ�;0y�K\�&\�1ߣƹ�G��cxf�R�k:H򀗐L\�\�\�Q\�\\�u\�\� s����\����<`�\�\�\�\�U\�x��`cLK\�t9��<\�&\�tƀ��a�\�\�\���y�Ki˃\�\�\�%\�\�\�[v\�\��}CC\�pMM\�\�/�p#//OTTT\�TVV^�����e˖Ϫ��~#W�Ec!c\�Rڌa~K\�\�z\n\�/��\�\�\�o��f_yy��\�(\�\�\�#��\�[q\�\�9122\"�I�\�t�_]]}���\��,\��+\�10/�n�ZvA׷\�/������}�\��/?��\�\�\�?CCCN)�B\�\�z�0޸q\�G���\�\�m\���K�\�>\�*��Ʊ\�/\�\�q�?��\�f*\�\�\�{OLLL�\�	Z�\�Y�v\�dQQ\�\�\�\�l\�\��ư��\�-\�\�y�K�\�@\�\�]�w\��a��\����W\��@\�x�|�\�f��]�1x)�1\�$]���q�\Z\�/�\�\�ewm߾}���N����=\n4nMM\�dII\�.u��c\�oc\�I�\�,\�\�g\�^\��\�\�+\�*I\Z$�\�����c����\�~\���K��a\'�%F\�L_3�3�qP�	 x\�W\�k�/��bڮ$Uh;eeeW\�\�\�V��b0/�2�}�%�)��\�O�e�j�\�,\�^\�Ǐ�޸\�k�\�A\�+**\Z\�\�\�]�\�\r��\�\�\���]\�\�ĕZr&$������H\�\�y�K�\�>B\�J��\�\�\�\�\�r��O6\0c\�gc؅��t%\�,u\��yv��n�\n\�/y\���\0]M^�zU\���@\�-..�\�0/y2�]�*)]ٙ��n��J\�%\�~X\�<\�\�qg\�޽je�]�v��W�R\�-ہ1xɓ1\����t���,u\�\�W�.&�c%\�^2\�y5����B�ݕQ\�\��#�(\�\�\����\���dl{\�RJ�\�ӕ�n9Ӓt\�_ր<\�%\�<طo_ݻ�eee\�\�U\�/\�}\�\�\�\�R�V����dlF��k\Z����\�R7\�kI��\�?��#\�/\���;N\�\r\�9\�\�\�tB�lP\��#\�m)�Ji�����\�sM!H	\�ʐ~�g{\�\�~K\�%\�~��\�!x\�8諲\�\�>8�\�:�{\�fu9�`���1x\�\��0=\��|te٭��$]\�\�\�H1=F\�^2΃\�\�\�Lvr.���\�\�o\�}\�\�Z���dlFh\�1\��6K���+˰K\�%\��\�8�c�=F\�^2΃���?�\��\�Y�0222!O�>u9�1�*\� 0/��sK�s�大l\�\�V\�J�~OǛ\�ұ�AsLT9\�y�K\�yP\\\\,fff\�Ί�t;;\�\�je�1zzz\�ٳg��wtt�\�\�f1<<�4���M�G�\�\�\�\�\�[o��L_�z<\0��(u\�\r�J\��f�lɃX,&��\�q\�ڵ����\��ݻwǥ[�������|�\�\�\�\�\�ټ쿢<���O�\�I]XX(�-[\�\�\�\�\�\�\��b\�\nA\�;\�ԩS\�\�nKZ�����\�9�=\�r½\"ӕ�zevY���Esl�\�Ȇ<X�fM�X�r����r�?\���\�8p\�@Ҹ\�d���\�\�U.�Q�>}�#/{_�TC\�![�q\�\�I\�\�#�8\'�j��\�[������g���~\�L���?��v�26#LϵY\�(]I\�k��w��U�a\�wd�#\�\��;\��|\�\�\�wq\�\�\�I\�p�q\�\�\��\�\�]��j\�\�w�j\r\�-\�p��1ݐ\�ﾣא��_~��3�\�\�_\�\�w�cu�26#Lϵ��\�\\%�\�s�A\�2\����\�ِgΜqn;:44\�̧\�4�\�\'�t\nv߾}b||<i,�2΃���ϸ|�\�\�^ۛ�=��Li�\�j�\�\�\�\'�p�}\�\�	q��\����.q\�wι7��皂�\�ѕ�\�ݭ�\������d�\�1�%���\�c�=&V�Z%����iK�,��=\�P�)Y\�2΃���U\�\�\�7\�Ҋ�����μ,�3�)��^��\�\�y$I�?�\��B�7β�-\�ׯw�:\�\��\�\�vॄt\�+I\�\�^\�\�\��Y�-y@\�I/�\�R|\�֭[E__�\��\�^ir�qнU\�\�ʮG}�\�K�.�%ُ{�F��1詖\r68�Iһ_\�\�\�\�1�����\�\�\�\�`RF�\�KU�.�\�L\�\�d��Æ<ضm��\�\��8|�p|zww�s�\�\�\�x\�\�)���\�G�\�!���\�y�\��\�t\�\�\�\�[o9\'x~~�8z\�h|\�3\�<#\�J\�0�>�h��\"��\�:\�:&�D��<\�R���\�LJ\�E��|e�j�Ɔ<X�t�x�\�Emm�3�����Oߴi���/�\�\�!}dii\�\�Q}\�\�\�\�1Y���>\�\�DF\�I�u\�\�\�\�%N\'��H\�N��\�;\�$�\�M��as�Ӛ������tѕe\�MK���\�cA`C����\�{\�q>:�y\�\��\�?�P\�{\�b�\�\�\�7\�H\Z��<\��ƍ?���(�\���,\�m\�>\�@�c�ȳ1\�BWR�%\�$]L\�\�mA�<\�%\�y����|\�ڵ���LeGG=\�:H\�W\�\�`^\�l�Е�\Z]\�\�\��\�\�mw��<\�%_yPTT\�tyy�\�\�\�\�giapp\�_�$/H�V\�\�`^\�e�\�tiez{lA\�\�<(--�/r��A\�\�\�\�\�邂�o\�\�\�7��`0/�6�}d��2���\0y�K�򠤤dWee\�X��,\�d�����\�2�\�\�@ư�t�X�\�\�:��8\nk\�\�e\�\���}M\�\�W�.0/6�}��\�\�5nV�<\�P\�^;,**\Zmll�<11�v�\'\�\�\�\�g\�\�:h\�k�*0/�b��\�\�\�\Z��Z\�+�e\�э\�\�\�\�w\�}\�\�\�А�/��\�\�\�;\�\�\�༳�ơ\�\�m\���K�\�>\�*��Ʊ\�/��t3\0�s�,��u\�\�]hjj:�\�:���/_��8188\�\�\�\�\�!7~�np>{\�\�+���70\�\�ЍaAK.\��փ<\�t\��-\�>�\�dan��Ē%Hw\��1\�_1C?�\�t�O\�\�\�\�\0	��\�iK\�[v~\�[P x	y���`#���\�\�,\�^B0\�\�%\�\�\�3]\� �	y���`O\�W�\�\�\n\�^B0\�\�%\�3s�\�\\\�A\n���<`��K0�/\�RT� x	y���`߸\�X9�%\�\�/!�\0c\��\\Iy�K\�&��c\�vu0y�K\�&��c�(A\�\�	0/� J���<`��K0�\�/!�\0c\���y�K\�&��c�(A\�\�	0/� J���<`��K0�\�/!�\0c\���y�K\�&��c�(A\�\�	0/� J���<`BKKˍ����e^\�8\\�ƘV�\0�y�G\�F\�b�\�I	ʼ.\\�\�7i�c\�1 S �y��\�\�֧\�\�\�\�����\�H2\Zɿ{��\�\�?���(\��z�\0\�ȃ\�<`\nz\�\"u���2.��\�\��\0�C\�\�\���<�F\�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\�\��X�\�9�\0\0\0\0IEND�B`�',1),('7b8eef18-9f0d-11ee-9459-005056c00001',1,'business_1702865697042.bpmn','7b8eef17-9f0d-11ee-9459-005056c00001',_binary '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:flowable=\"http://flowable.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:bpmn2=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://flowable.org/bpmn\" id=\"diagram_Process_1701854918145\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd\">\n  <process id=\"Process_1701854918145\" name=\"业务流程_1701854918145\" isExecutable=\"true\">\n    <startEvent id=\"Event_13w76qi\" name=\"开始\"></startEvent>\n    <userTask id=\"Activity_1e348q9\" name=\"124\"></userTask>\n    <sequenceFlow id=\"Flow_0btbv9z\" sourceRef=\"Event_13w76qi\" targetRef=\"Activity_1e348q9\"></sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_Process_1701854918145\">\n    <bpmndi:BPMNPlane bpmnElement=\"Process_1701854918145\" id=\"BPMNPlane_Process_1701854918145\">\n      <bpmndi:BPMNShape bpmnElement=\"Event_13w76qi\" id=\"BPMNShape_Event_13w76qi\">\n        <omgdc:Bounds height=\"36.0\" width=\"36.0\" x=\"-148.0\" y=\"12.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_1e348q9\" id=\"BPMNShape_Activity_1e348q9\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"-60.0\" y=\"-10.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_0btbv9z\" id=\"BPMNEdge_Flow_0btbv9z\">\n        <omgdi:waypoint x=\"-112.0\" y=\"30.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"-60.0\" y=\"30.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>',0),('7c070549-9f0d-11ee-9459-005056c00001',1,'business_1702865697042.Process_1701854918145.png','7b8eef17-9f0d-11ee-9459-005056c00001',_binary '�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0\�\0\0\0Z\0\0\0\�?)e\0\0\�IDATx^\�\�O�W\�Q�%^hts�z��\��ټX�\�2Ͷ_\n�1�X�zaD\�&\�(j�\�9|cq\��I$�:3�ԮF\r��@!���\nZlCA\�\�\�5x��O_\�\�\�\�\��|#>\�\�9N\�\�琓\�!\�{���go޼����Q444 ��ܧ�^\�\�\��R-`R�:Y(bppPD�Q199�d8\��\�\�\�\�xF�(k\�2&@-�ZXH\�BR�[j��Oh)x�\�A�1��0���b^�<\�2&`T�瑀\���\�]�\�B?\�2u;$�@&c,\�/\�/+\���\�\�2Z�n�$��#b<\��tR\�\�\�wA�=�| ��\�y\�G��\�:u{$�@&���B\'D,�N\�I>�	�W ��Aw�T!b�u\�\�H\�L0\"F\�\��:!b�u\�\�H\�L0\"\�p�h�\�;���֩\�#\�b0����ktb\�2u;$�@&cbBt\�8���\�:\�\�HҁLx�\�d��\�#:)b�ux���@&\�*�l	]n\�\�\�7:\�\�6�-Z�\�1�O�����G\�L�\'��Vb�о\�\�\�L�\'�Z\��z<\�x ≁��9cdd\�}�\�Uy\�\�\��N�3TVVݱcǴ\�f\�\�\�S%%%��\�\�TYYya߾}�\�]\�\�x����v1����>\�\�p8��A\�\�Չ;w�1<<,���O\�i}ii\�\��m\�&� �\�\�\�s\�c�@^I�����?~|`�\�\�\�ܹs\"j����� /\�\�\�s6//o�z+1x%e1d�^p\�\�%	q�\�i166�\�����\�8[�n߼y\�W\���\n\�\���Đ\�xImm택;w�\�\�^���O\��-�S=������\�.���\n����p8�\�\�@Ǖ��\�-[�Ԩ\�\Z�W�C\�\���\�\")\� s	���$RPP�_�+1x%)1�O�k׮9k)T\�<v�}\�f�}�^�U����\�\�\�[K\�t\�)\��Ov\�\�6l�P�&+\01x%a1\�,\�52�\'N\�V\�\'\���\0\�\���Ġ�w\�Z����u6#\�yG��b\�JBb\�\���z��f����\�j��^[�1xŰ\�\�\�\�\�bhhH��E��_\�bVTT\�W�1���bX��/:i\�\�v�C\�j|�^#G\�u^��D]�1xŰG�i����n��\�\�\Z9B��_捂@^1,\r�Ѱ\�z���6mr�\�ȑb�Q��+�\�(--�f�\�\�l��~�*u[�F�\�#� �W�!;\�\�Ϟ=S\�)�\�Jկ^#G\��Fb\��a1\n\�\�ԔZGMA^\rQ+XV\'\�\�\�\�\�\�\�ݺ\�\�\�\�\�\�r�P(�[\�\�x\�\�\�1�\�p8S\\Z�`0\�m\��\�-�JQ\�.((˖-���������ʤ%77WP}��koo.\�\�\�5�\�ػw\�(�>FGGG�-{��	#[\�hkk�W�\�*�Zɗ.]*\�n��?\�L\�ϟ?�-�V���\�����1��RW�\\i\�»Rq���-bĒ��\�\�w\�j\�\�\��k׮i\�:$�/_.-Z�ۇk�!�\�\\�c<x�ޖ=\�1\�(D+�KUU�X�n�\�sKK�X�b�\��|b\�\�ų\�\�-�\�(++��f\�P+�	L\�\�\�ڲ\�ɷQ�\"�\�\��|��\���+W\�,0ڶ\�\�\�\�\Z4�*\�~\�bX\Z�d�\�\'\�+\�\�ɓ\�R�\0\�J���8btvv�ݻwk}\n�;\�\�\�\��3>3�7?9ǰ\��d\�\�\���at�iy�\��8v\�V�\�\�\�č7^�[�~�6\�	�f\�\Z�N\�#����\��~�\�\�\��[�ރ�y�\�z#ߒR\�}\�B\"\�\��\�_TT\�j\�\�唘\0t7s\�\�\'O\�\�-	�A\�dhf��\'\���\ro\�!H\�b\�;\�4Z��i\�\�\�j\�|#�H\�b4C�\�\�fj�\�\��[J\�g\�,!H���\�H��\�A撉��٧�-;z_�\�`% �$-A3\�dhs\�rȖ\�CRȖ\�\�\�Vb\�JJb4C�l=\�>�\�e�\�\�[��WR��\��4\Z\���\�l\�H�\�\�ݧ!+\�)T ��E���\�J�y�N�:\�yyc�����\�\�\�\�<\�q\�\�13�61b\�\�7z2-+\�PQQQ_uu\�=zG\�\�\�?\n�Bϥc�\�\�\�\���\�\�h@\�˱OOi?+>�3\�\����1�y7n�X\n\�=\r����Z?��?���E\�i=mG۫x���2�b���1�\01xb0b\�\n\�`\�\�����+�	�W  �@&@^�L��1�\01xb0b\�\n\�`\�\�����+�	�W  �@&@^�L��1�\01xb0���q:\Z�\�\n\�|d9H1&\�2&\�\�zu��d>}}}�I1n�eL�\�v\�\�xF�@-�9��{\�\�Çg��d֪eL�\n�~S\�L\�w\\$\�ϝ>H\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0��T\� �i�\0\0\0\0IEND�B`�',1),('ae2cc4a3-a45c-11ee-ba71-005056c00001',1,'source',NULL,_binary '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:flowable=\"http://flowable.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:bpmn2=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://flowable.org/bpmn\" id=\"diagram_Process_1701854918145\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd\">\n  <process id=\"Process_1701854918145\" name=\"业务流程_1701854918145\" isExecutable=\"true\">\n    <startEvent id=\"Event_13w76qi\" name=\"开始\"></startEvent>\n    <userTask id=\"Activity_1e348q9\" name=\"124\"></userTask>\n    <sequenceFlow id=\"Flow_0btbv9z\" sourceRef=\"Event_13w76qi\" targetRef=\"Activity_1e348q9\"></sequenceFlow>\n    <exclusiveGateway id=\"Gateway_0rpylue\"></exclusiveGateway>\n    <sequenceFlow id=\"Flow_1papq8h\" sourceRef=\"Activity_1e348q9\" targetRef=\"Gateway_0rpylue\"></sequenceFlow>\n    <userTask id=\"Activity_1s82xkw\" name=\"135\"></userTask>\n    <sequenceFlow id=\"Flow_0r31rfh\" sourceRef=\"Gateway_0rpylue\" targetRef=\"Activity_1s82xkw\"></sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_Process_1701854918145\">\n    <bpmndi:BPMNPlane bpmnElement=\"Process_1701854918145\" id=\"BPMNPlane_Process_1701854918145\">\n      <bpmndi:BPMNShape bpmnElement=\"Event_13w76qi\" id=\"BPMNShape_Event_13w76qi\">\n        <omgdc:Bounds height=\"36.0\" width=\"36.0\" x=\"-148.0\" y=\"12.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_1e348q9\" id=\"BPMNShape_Activity_1e348q9\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"-60.0\" y=\"-10.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Gateway_0rpylue\" id=\"BPMNShape_Gateway_0rpylue\">\n        <omgdc:Bounds height=\"50.0\" width=\"50.0\" x=\"95.0\" y=\"5.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_1s82xkw\" id=\"BPMNShape_Activity_1s82xkw\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"200.0\" y=\"-10.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_0btbv9z\" id=\"BPMNEdge_Flow_0btbv9z\">\n        <omgdi:waypoint x=\"-112.0\" y=\"30.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"-60.0\" y=\"30.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_1papq8h\" id=\"BPMNEdge_Flow_1papq8h\">\n        <omgdi:waypoint x=\"40.0\" y=\"30.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"95.0\" y=\"30.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_0r31rfh\" id=\"BPMNEdge_Flow_0r31rfh\">\n        <omgdi:waypoint x=\"145.0\" y=\"30.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"200.0\" y=\"30.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>',NULL),('b88a591f-9f18-11ee-8c1f-005056c00001',1,'business_1702865697042.bpmn','b88a591e-9f18-11ee-8c1f-005056c00001',_binary '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:flowable=\"http://flowable.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:bpmn2=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://flowable.org/bpmn\" id=\"diagram_Process_1701854918145\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd\">\n  <process id=\"Process_1701854918145\" name=\"业务流程_1701854918145\" isExecutable=\"true\">\n    <startEvent id=\"Event_13w76qi\" name=\"开始\"></startEvent>\n    <userTask id=\"Activity_1e348q9\" name=\"124\"></userTask>\n    <sequenceFlow id=\"Flow_0btbv9z\" sourceRef=\"Event_13w76qi\" targetRef=\"Activity_1e348q9\"></sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_Process_1701854918145\">\n    <bpmndi:BPMNPlane bpmnElement=\"Process_1701854918145\" id=\"BPMNPlane_Process_1701854918145\">\n      <bpmndi:BPMNShape bpmnElement=\"Event_13w76qi\" id=\"BPMNShape_Event_13w76qi\">\n        <omgdc:Bounds height=\"36.0\" width=\"36.0\" x=\"-148.0\" y=\"12.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_1e348q9\" id=\"BPMNShape_Activity_1e348q9\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"-60.0\" y=\"-10.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_0btbv9z\" id=\"BPMNEdge_Flow_0btbv9z\">\n        <omgdi:waypoint x=\"-112.0\" y=\"30.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"-60.0\" y=\"30.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>',0),('bb0cbe40-9f18-11ee-8c1f-005056c00001',1,'business_1702865697042.Process_1701854918145.png','b88a591e-9f18-11ee-8c1f-005056c00001',_binary '�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0\�\0\0\0Z\0\0\0\�?)e\0\0\�IDATx^\�\�O�W\�Q�%^hts�z��\��ټX�\�2Ͷ_\n�1�X�zaD\�&\�(j�\�9|cq\��I$�:3�ԮF\r��@!���\nZlCA\�\�\�5x��O_\�\�\�\�\��|#>\�\�9N\�\�琓\�!\�{���go޼����Q444 ��ܧ�^\�\�\��R-`R�:Y(bppPD�Q199�d8\��\�\�\�\�xF�(k\�2&@-�ZXH\�BR�[j��Oh)x�\�A�1��0���b^�<\�2&`T�瑀\���\�]�\�B?\�2u;$�@&c,\�/\�/+\���\�\�2Z�n�$��#b<\��tR\�\�\�wA�=�| ��\�y\�G��\�:u{$�@&���B\'D,�N\�I>�	�W ��Aw�T!b�u\�\�H\�L0\"F\�\��:!b�u\�\�H\�L0\"\�p�h�\�;���֩\�#\�b0����ktb\�2u;$�@&cbBt\�8���\�:\�\�HҁLx�\�d��\�#:)b�ux���@&\�*�l	]n\�\�\�7:\�\�6�-Z�\�1�O�����G\�L�\'��Vb�о\�\�\�L�\'�Z\��z<\�x ≁��9cdd\�}�\�Uy\�\�\��N�3TVVݱcǴ\�f\�\�\�S%%%��\�\�TYYya߾}�\�]\�\�x����v1����>\�\�p8��A\�\�Չ;w�1<<,���O\�i}ii\�\��m\�&� �\�\�\�s\�c�@^I�����?~|`�\�\�\�ܹs\"j����� /\�\�\�s6//o�z+1x%e1d�^p\�\�%	q�\�i166�\�����\�8[�n߼y\�W\���\n\�\���Đ\�xImm택;w�\�\�^���O\��-�S=������\�.���\n����p8�\�\�@Ǖ��\�-[�Ԩ\�\Z�W�C\�\���\�\")\� s	���$RPP�_�+1x%)1�O�k׮9k)T\�<v�}\�f�}�^�U����\�\�\�[K\�t\�)\��Ov\�\�6l�P�&+\01x%a1\�,\�52�\'N\�V\�\'\���\0\�\���Ġ�w\�Z����u6#\�yG��b\�JBb\�\���z��f����\�j��^[�1xŰ\�\�\�\�\�bhhH��E��_\�bVTT\�W�1���bX��/:i\�\�v�C\�j|�^#G\�u^��D]�1xŰG�i����n��\�\�\Z9B��_捂@^1,\r�Ѱ\�z���6mr�\�ȑb�Q��+�\�(--�f�\�\�l��~�*u[�F�\�#� �W�!;\�\�Ϟ=S\�)�\�Jկ^#G\��Fb\��a1\n\�\�ԔZGMA^\rQ+XV\'\�\�\�\�\�\�\�ݺ\�\�\�\�\�\�r�P(�[\�\�x\�\�\�1�\�p8S\\Z�`0\�m\��\�-�JQ\�.((˖-���������ʤ%77WP}��koo.\�\�\�5�\�ػw\�(�>FGGG�-{��	#[\�hkk�W�\�*�Zɗ.]*\�n��?\�L\�ϟ?�-�V���\�����1��RW�\\i\�»Rq���-bĒ��\�\�w\�j\�\�\��k׮i\�:$�/_.-Z�ۇk�!�\�\\�c<x�ޖ=\�1\�(D+�KUU�X�n�\�sKK�X�b�\��|b\�\�ų\�\�-�\�(++��f\�P+�	L\�\�\�ڲ\�ɷQ�\"�\�\��|��\���+W\�,0ڶ\�\�\�\�\Z4�*\�~\�bX\Z�d�\�\'\�+\�\�ɓ\�R�\0\�J���8btvv�ݻwk}\n�;\�\�\�\��3>3�7?9ǰ\��d\�\�\���at�iy�\��8v\�V�\�\�\�č7^�[�~�6\�	�f\�\Z�N\�#����\��~�\�\�\��[�ރ�y�\�z#ߒR\�}\�B\"\�\��\�_TT\�j\�\�唘\0t7s\�\�\'O\�\�-	�A\�dhf��\'\���\ro\�!H\�b\�;\�4Z��i\�\�\�j\�|#�H\�b4C�\�\�fj�\�\��[J\�g\�,!H���\�H��\�A撉��٧�-;z_�\�`% �$-A3\�dhs\�rȖ\�CRȖ\�\�\�Vb\�JJb4C�l=\�>�\�e�\�\�[��WR��\��4\Z\���\�l\�H�\�\�ݧ!+\�)T ��E���\�J�y�N�:\�yyc�����\�\�\�\�<\�q\�\�13�61b\�\�7z2-+\�PQQQ_uu\�=zG\�\�\�?\n�Bϥc�\�\�\�\���\�\�h@\�˱OOi?+>�3\�\����1�y7n�X\n\�=\r����Z?��?���E\�i=mG۫x���2�b���1�\01xb0b\�\n\�`\�\�����+�	�W  �@&@^�L��1�\01xb0b\�\n\�`\�\�����+�	�W  �@&@^�L��1�\01xb0���q:\Z�\�\n\�|d9H1&\�2&\�\�zu��d>}}}�I1n�eL�\�v\�\�xF�@-�9��{\�\�Çg��d֪eL�\n�~S\�L\�w\\$\�ϝ>H\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0��T\� �i�\0\0\0\0IEND�B`�',1),('c9f1f584-9fd2-11ee-8a8a-005056c00001',1,'source',NULL,_binary '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:flowable=\"http://flowable.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:bpmn2=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://flowable.org/bpmn\" id=\"diagram_process_1703143742179\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd\">\n  <process id=\"process_1703143742179\" name=\"OA_1703143742179\" isExecutable=\"true\">\n    <documentation>测试新建流程202312211529</documentation>\n    <startEvent id=\"StartEvent_de5ekdnxz46\" name=\"开始\"></startEvent>\n    <userTask id=\"Activity_0dtdyif\" name=\"202312211529\"></userTask>\n    <sequenceFlow id=\"Flow_0oe32pj\" sourceRef=\"StartEvent_de5ekdnxz46\" targetRef=\"Activity_0dtdyif\"></sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_process_1703143742179\">\n    <bpmndi:BPMNPlane bpmnElement=\"process_1703143742179\" id=\"BPMNPlane_process_1703143742179\">\n      <bpmndi:BPMNShape bpmnElement=\"StartEvent_de5ekdnxz46\" id=\"BPMNShape_StartEvent_de5ekdnxz46\">\n        <omgdc:Bounds height=\"36.0\" width=\"36.0\" x=\"-28.0\" y=\"22.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_0dtdyif\" id=\"BPMNShape_Activity_0dtdyif\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"60.0\" y=\"0.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_0oe32pj\" id=\"BPMNEdge_Flow_0oe32pj\">\n        <omgdi:waypoint x=\"8.0\" y=\"40.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"60.0\" y=\"40.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>',NULL),('d4d2660b-9fcb-11ee-b189-005056c00001',1,'source',NULL,_binary '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:flowable=\"http://flowable.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:bpmn2=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://flowable.org/bpmn\" id=\"diagram_Process_1701854918145\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd\">\n  <process id=\"Process_1701854918145\" name=\"Process_1701854918145\" isExecutable=\"true\">\n    <startEvent id=\"Event_13w76qi\" name=\"开始\"></startEvent>\n    <userTask id=\"Activity_0kzptkw\" name=\"1223\"></userTask>\n    <sequenceFlow id=\"Flow_0tbwghm\" sourceRef=\"Event_13w76qi\" targetRef=\"Activity_0kzptkw\"></sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_Process_1701854918145\">\n    <bpmndi:BPMNPlane bpmnElement=\"Process_1701854918145\" id=\"BPMNPlane_Process_1701854918145\">\n      <bpmndi:BPMNShape bpmnElement=\"Event_13w76qi\" id=\"BPMNShape_Event_13w76qi\">\n        <omgdc:Bounds height=\"36.0\" width=\"36.0\" x=\"192.0\" y=\"92.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_0kzptkw\" id=\"BPMNShape_Activity_0kzptkw\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"280.0\" y=\"70.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_0tbwghm\" id=\"BPMNEdge_Flow_0tbwghm\">\n        <omgdi:waypoint x=\"228.0\" y=\"110.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"280.0\" y=\"110.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>',NULL),('dba55d70-9fc7-11ee-90eb-005056c00001',1,'process_1702865697042.bpmn20.xml','dba55d6f-9fc7-11ee-90eb-005056c00001',_binary '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:flowable=\"http://flowable.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:bpmn2=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://flowable.org/bpmn\" id=\"diagram_Process_1701854918145\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd\">\n  <process id=\"Process_1701854918145\" name=\"业务流程_1701854918145\" isExecutable=\"true\">\n    <startEvent id=\"Event_13w76qi\" name=\"开始\"></startEvent>\n    <userTask id=\"Activity_1e348q9\" name=\"124\"></userTask>\n    <sequenceFlow id=\"Flow_0btbv9z\" sourceRef=\"Event_13w76qi\" targetRef=\"Activity_1e348q9\"></sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_Process_1701854918145\">\n    <bpmndi:BPMNPlane bpmnElement=\"Process_1701854918145\" id=\"BPMNPlane_Process_1701854918145\">\n      <bpmndi:BPMNShape bpmnElement=\"Event_13w76qi\" id=\"BPMNShape_Event_13w76qi\">\n        <omgdc:Bounds height=\"36.0\" width=\"36.0\" x=\"-148.0\" y=\"12.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_1e348q9\" id=\"BPMNShape_Activity_1e348q9\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"-60.0\" y=\"-10.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_0btbv9z\" id=\"BPMNEdge_Flow_0btbv9z\">\n        <omgdi:waypoint x=\"-112.0\" y=\"30.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"-60.0\" y=\"30.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>',0),('de57d341-9fc7-11ee-90eb-005056c00001',1,'process_1702865697042.Process_1701854918145.png','dba55d6f-9fc7-11ee-90eb-005056c00001',_binary '�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0\�\0\0\0Z\0\0\0\�?)e\0\0\�IDATx^\�\�O�W\�Q�%^hts�z��\��ټX�\�2Ͷ_\n�1�X�zaD\�&\�(j�\�9|cq\��I$�:3�ԮF\r��@!���\nZlCA\�\�\�5x��O_\�\�\�\�\��|#>\�\�9N\�\�琓\�!\�{���go޼����Q444 ��ܧ�^\�\�\��R-`R�:Y(bppPD�Q199�d8\��\�\�\�\�xF�(k\�2&@-�ZXH\�BR�[j��Oh)x�\�A�1��0���b^�<\�2&`T�瑀\���\�]�\�B?\�2u;$�@&c,\�/\�/+\���\�\�2Z�n�$��#b<\��tR\�\�\�wA�=�| ��\�y\�G��\�:u{$�@&���B\'D,�N\�I>�	�W ��Aw�T!b�u\�\�H\�L0\"F\�\��:!b�u\�\�H\�L0\"\�p�h�\�;���֩\�#\�b0����ktb\�2u;$�@&cbBt\�8���\�:\�\�HҁLx�\�d��\�#:)b�ux���@&\�*�l	]n\�\�\�7:\�\�6�-Z�\�1�O�����G\�L�\'��Vb�о\�\�\�L�\'�Z\��z<\�x ≁��9cdd\�}�\�Uy\�\�\��N�3TVVݱcǴ\�f\�\�\�S%%%��\�\�TYYya߾}�\�]\�\�x����v1����>\�\�p8��A\�\�Չ;w�1<<,���O\�i}ii\�\��m\�&� �\�\�\�s\�c�@^I�����?~|`�\�\�\�ܹs\"j����� /\�\�\�s6//o�z+1x%e1d�^p\�\�%	q�\�i166�\�����\�8[�n߼y\�W\���\n\�\���Đ\�xImm택;w�\�\�^���O\��-�S=������\�.���\n����p8�\�\�@Ǖ��\�-[�Ԩ\�\Z�W�C\�\���\�\")\� s	���$RPP�_�+1x%)1�O�k׮9k)T\�<v�}\�f�}�^�U����\�\�\�[K\�t\�)\��Ov\�\�6l�P�&+\01x%a1\�,\�52�\'N\�V\�\'\���\0\�\���Ġ�w\�Z����u6#\�yG��b\�JBb\�\���z��f����\�j��^[�1xŰ\�\�\�\�\�bhhH��E��_\�bVTT\�W�1���bX��/:i\�\�v�C\�j|�^#G\�u^��D]�1xŰG�i����n��\�\�\Z9B��_捂@^1,\r�Ѱ\�z���6mr�\�ȑb�Q��+�\�(--�f�\�\�l��~�*u[�F�\�#� �W�!;\�\�Ϟ=S\�)�\�Jկ^#G\��Fb\��a1\n\�\�ԔZGMA^\rQ+XV\'\�\�\�\�\�\�\�ݺ\�\�\�\�\�\�r�P(�[\�\�x\�\�\�1�\�p8S\\Z�`0\�m\��\�-�JQ\�.((˖-���������ʤ%77WP}��koo.\�\�\�5�\�ػw\�(�>FGGG�-{��	#[\�hkk�W�\�*�Zɗ.]*\�n��?\�L\�ϟ?�-�V���\�����1��RW�\\i\�»Rq���-bĒ��\�\�w\�j\�\�\��k׮i\�:$�/_.-Z�ۇk�!�\�\\�c<x�ޖ=\�1\�(D+�KUU�X�n�\�sKK�X�b�\��|b\�\�ų\�\�-�\�(++��f\�P+�	L\�\�\�ڲ\�ɷQ�\"�\�\��|��\���+W\�,0ڶ\�\�\�\�\Z4�*\�~\�bX\Z�d�\�\'\�+\�\�ɓ\�R�\0\�J���8btvv�ݻwk}\n�;\�\�\�\��3>3�7?9ǰ\��d\�\�\���at�iy�\��8v\�V�\�\�\�č7^�[�~�6\�	�f\�\Z�N\�#����\��~�\�\�\��[�ރ�y�\�z#ߒR\�}\�B\"\�\��\�_TT\�j\�\�唘\0t7s\�\�\'O\�\�-	�A\�dhf��\'\���\ro\�!H\�b\�;\�4Z��i\�\�\�j\�|#�H\�b4C�\�\�fj�\�\��[J\�g\�,!H���\�H��\�A撉��٧�-;z_�\�`% �$-A3\�dhs\�rȖ\�CRȖ\�\�\�Vb\�JJb4C�l=\�>�\�e�\�\�[��WR��\��4\Z\���\�l\�H�\�\�ݧ!+\�)T ��E���\�J�y�N�:\�yyc�����\�\�\�\�<\�q\�\�13�61b\�\�7z2-+\�PQQQ_uu\�=zG\�\�\�?\n�Bϥc�\�\�\�\���\�\�h@\�˱OOi?+>�3\�\����1�y7n�X\n\�=\r����Z?��?���E\�i=mG۫x���2�b���1�\01xb0b\�\n\�`\�\�����+�	�W  �@&@^�L��1�\01xb0b\�\n\�`\�\�����+�	�W  �@&@^�L��1�\01xb0���q:\Z�\�\n\�|d9H1&\�2&\�\�zu��d>}}}�I1n�eL�\�v\�\�xF�@-�9��{\�\�Çg��d֪eL�\n�~S\�L\�w\\$\�ϝ>H\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0��T\� �i�\0\0\0\0IEND�B`�',1),('e3d7aaee-9fd0-11ee-8a8a-005056c00001',1,'source',NULL,_binary '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:flowable=\"http://flowable.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:bpmn2=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://flowable.org/bpmn\" id=\"diagram_b219e72d-9fd0-11ee-8a8a-005056c00001\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd\">\n  <process id=\"b219e72d-9fd0-11ee-8a8a-005056c00001\" name=\"business_1703142898591\" isExecutable=\"true\">\n    <startEvent id=\"StartEvent_ztyoyovkq8d\" name=\"开始\">\n      <documentation>测试流程名称和部署名称1</documentation>\n    </startEvent>\n    <userTask id=\"Activity_10vodc0\" name=\"测试流程名称20231221\"></userTask>\n    <sequenceFlow id=\"Flow_041lywc\" sourceRef=\"StartEvent_ztyoyovkq8d\" targetRef=\"Activity_10vodc0\"></sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_b219e72d-9fd0-11ee-8a8a-005056c00001\">\n    <bpmndi:BPMNPlane bpmnElement=\"b219e72d-9fd0-11ee-8a8a-005056c00001\" id=\"BPMNPlane_b219e72d-9fd0-11ee-8a8a-005056c00001\">\n      <bpmndi:BPMNShape bpmnElement=\"StartEvent_ztyoyovkq8d\" id=\"BPMNShape_StartEvent_ztyoyovkq8d\">\n        <omgdc:Bounds height=\"36.0\" width=\"36.0\" x=\"112.0\" y=\"12.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_10vodc0\" id=\"BPMNShape_Activity_10vodc0\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"200.0\" y=\"-10.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_041lywc\" id=\"BPMNEdge_Flow_041lywc\">\n        <omgdi:waypoint x=\"148.0\" y=\"30.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"200.0\" y=\"30.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>',NULL),('eb06c15d-9fcb-11ee-b189-005056c00001',1,'process_1703140711137.bpmn20.xml','eb06c15c-9fcb-11ee-b189-005056c00001',_binary '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:flowable=\"http://flowable.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:bpmn2=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://flowable.org/bpmn\" id=\"diagram_Process_1701854918145\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd\">\n  <process id=\"Process_1701854918145\" name=\"Process_1701854918145\" isExecutable=\"true\">\n    <startEvent id=\"Event_13w76qi\" name=\"开始\"></startEvent>\n    <userTask id=\"Activity_0kzptkw\" name=\"1223\"></userTask>\n    <sequenceFlow id=\"Flow_0tbwghm\" sourceRef=\"Event_13w76qi\" targetRef=\"Activity_0kzptkw\"></sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_Process_1701854918145\">\n    <bpmndi:BPMNPlane bpmnElement=\"Process_1701854918145\" id=\"BPMNPlane_Process_1701854918145\">\n      <bpmndi:BPMNShape bpmnElement=\"Event_13w76qi\" id=\"BPMNShape_Event_13w76qi\">\n        <omgdc:Bounds height=\"36.0\" width=\"36.0\" x=\"192.0\" y=\"92.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_0kzptkw\" id=\"BPMNShape_Activity_0kzptkw\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"280.0\" y=\"70.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_0tbwghm\" id=\"BPMNEdge_Flow_0tbwghm\">\n        <omgdi:waypoint x=\"228.0\" y=\"110.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"280.0\" y=\"110.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>',0),('eb8f042e-9fcb-11ee-b189-005056c00001',1,'process_1703140711137.Process_1701854918145.png','eb06c15c-9fcb-11ee-b189-005056c00001',_binary '�PNG\r\n\Z\n\0\0\0\rIHDR\0\0�\0\0\0�\0\0\0\�z\0\0\niIDATx^\�\��OT\�\�q��\�7��\���Fkbcbl�\�\��0 	 /�\�%-4����M\�M�T�lZ�\Z\�j�p\�x�\�EѰ(\��\�\0À4<}�SǬ\� �\�a|��Ov\��\�9\'\��||f\�3o\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�$���iGGǱ7n����S�.]\"	������\�\�\�ߘ�\0$�.�\�zPR\�\�\�*�\�\�Q�\�\�\�]~�����(V��\0$�\�dP2+��tuu\�\�b�i�F\0�P\�\�37\"��.�\�5���\��\��؋�\�k\0	\�\�\�w��[U��wE\"�.\�\�\�\�\�B1\0�\�K1�:\���W\���\��\�2YgnO\�\�\0�:/\�\�6��y\�z&f{(\0\�y)��\r�)�hd��=�?\0\�\��Ke1���3�\'\�b\0`\�\�V(\0\�y)��Y\�\�:s{(\0\�y)�\�kb\n!\ZYgnO\�\�\0�:/\�\�\�٢\�_\�S\n�L֙ۓ�C1\0�\�K1H�\�\\S�\�܎L/\0\�<\�Ȉj�^S�L\�\�lO\�\�\0��O�\\\��õ�1���\�\�\�\�\0�����	t�׫{��]L��md[f\�\�\0����\�S����\�\�\�C1\0�n�b\�2K�X\�g\��\�\0X7Q1���Tc\�x\�\0�����\�\�\0̂����\�\�\�\�:p\��\�\�\�\�\�\�\�\�M�6��|>���7VPP0XRR\�l׮]g\n�\�s����P�jjj\�ۻwognn�\�e��?�n߾�?~������\�\�rY_TT4���5�\�Bjj\�s�����P�hii�͡C�^nܸQ�8qB\�\�\�DJ�+\�^~N\�\����c)))�\�c$3���P�4\�1�\�s\�\�\�J!9rD\r\r\r�c��\�\�\�~233��~��\�\�\�\�V( Nz����\�͛7���s��ٟ.���A��\�MF�[��8\������Pii�\n�B\�>#d�\�\�\�\�\�\�\�\r\�\�V(`�\�����)�K)\��\�$�/((\�OKK+1\�#�Pn�b\0�H>Sزeˬ\�Lr�\�\�\�A�\�\��y.ɂbp+0w\�\�]%4\�\�g\n�\"\�\�����k\��\�<�d@1����J�|kȆ\�\�ʗzְ\�<�d@1��\�H.^�\�\�\�\�9f\'�7==} /��\�\n\�\0x$W4�<y\��������5�\�<���bp+\���\��¼�<ͱ:�\�\���g\�eee?1\�q.�\�\n\�\0xp\�\�\�r�\������\�Y\�/\�st�>\��_�\�M�[�\0\�\�\�Onx炪��\��\��\�9�H\�&�.�\�\�V(��u�\�\r\��@�e��\��\�9�\�G\�0iAPn�b\0<(**\n\'�څ�ikk���n�\�\�	�a�\�\n\�\0x���7��\�ks�����oH��\�9�h�B0)���P�\�\�\�jll\���\�\�!�\�0\�9��P\�\�\�\�ѣG1˛��Umm�\�\�\�\��\\\��\�\�՛7obֹ�\� 77w̕COO\�#\�ܟ14�\�\�[IRiiijѢE���\�ueee2�F�d\�%��L�\\�aÆ\�\�-[�\�\�p\�1m�b\0<رcǠ+�1<x\�\�7w?c���\\.�{\�K�Fr�,X�\�\�\�U]]]d�\�ӧ\'].�?�:u\�Td6!\�.^�sLۡ\0JKK��\�˗/\�\��o%MXQ.C4\�&(�\�\�Ν;�\�x\��+W�L�\\\�\�\�\�\�\�M\�q��N\�\�<�\�P�eeeg\\��aϞ=\'}s\�:�I!j�C4j\�\�՞�WUU�+V��+W�����\��C1\0�,**\Z7i\�RSS[|s\�\�g�\�r1\�\��5�\��,�\�K�m۶�u.�b\0<�{egg�ھWҋ/�\�R\�\�^I�ϼ	�A\�ںu�\Z�|���i\�\�}�\�\�\�D\��W544\�\�v(��\�\�\��﮺{\�\�\�|\�]5�y\�\�:x\�`dOIIQׯ_�n͚5J�\�!��|�\�\��|l�d\�j�\�\����$�ϫW�\�\�v(�#yBVV\�[[\�c�B7u)yCb#E0\�\�[?����\�\�~�\\\�\�\�6\�r\�ѣG\�\�ŋ#_ݹsg\�\�\\\�\0LA~~�1[Op\�\����\'���b\0�@�����9�\�k\Z���\�-� \�|&�\�\0L�\�\��6777\n�\�\�{V��\�Rx�\�y.ɂbp+���,ypϰ�!�M###RSSo����\�<�dB1���SFFFuAAA�l\�\�L�QJA\��b;\�Pn�b\0�!--�D\�f�>J\�>Sx�\�3�(���P�4\�{�~�?TYY�rhh\�㧤���\�ݷ��\�����bp+0\�\�\�\�WI\�\�\�>����g*o\�\�\�V^^�\�\�G\�g#�Qn�b\0f�\\|&W&\�=���󴪪\�<����\�Yoo\�]C�`�����Y�\�$7\�{w\�W\�s\�x\��[��\�\�źu\�~�\�r�l=\�\�U\�\�}����\�,�\���lo\�\�sB1���u�[�\0XG1���u�[�\0XG1���u�[�\0XG1���u�[�\0XG1���u�[�\0XG1���u�[�\0XG1���u�[�\0XG1���u�[�\0XG1���u�[�\0XG1���uuuu\�\�p8f�\"��~^\�b1_#\0H�@ 𲻻;f�\"�\�ӧO���\��\Z@B\�\�\�\�\�\�8\�\�\�\�\�\��N\�\�\�ɓ\'\�\�Rx��\�|�\0 \�d0���\�\�{\�$\�߻��)\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�\���\�@n\�x\�\0\0\0\0IEND�B`�',1),('fc2bb1f0-9fd0-11ee-8a8a-005056c00001',1,'process_1703142898591.bpmn20.xml','fc2bb1ef-9fd0-11ee-8a8a-005056c00001',_binary '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:flowable=\"http://flowable.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:bpmn2=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://flowable.org/bpmn\" id=\"diagram_b219e72d-9fd0-11ee-8a8a-005056c00001\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd\">\n  <process id=\"b219e72d-9fd0-11ee-8a8a-005056c00001\" name=\"business_1703142898591\" isExecutable=\"true\">\n    <startEvent id=\"StartEvent_ztyoyovkq8d\" name=\"开始\">\n      <documentation>测试流程名称和部署名称1</documentation>\n    </startEvent>\n    <userTask id=\"Activity_10vodc0\" name=\"测试流程名称20231221\"></userTask>\n    <sequenceFlow id=\"Flow_041lywc\" sourceRef=\"StartEvent_ztyoyovkq8d\" targetRef=\"Activity_10vodc0\"></sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_b219e72d-9fd0-11ee-8a8a-005056c00001\">\n    <bpmndi:BPMNPlane bpmnElement=\"b219e72d-9fd0-11ee-8a8a-005056c00001\" id=\"BPMNPlane_b219e72d-9fd0-11ee-8a8a-005056c00001\">\n      <bpmndi:BPMNShape bpmnElement=\"StartEvent_ztyoyovkq8d\" id=\"BPMNShape_StartEvent_ztyoyovkq8d\">\n        <omgdc:Bounds height=\"36.0\" width=\"36.0\" x=\"112.0\" y=\"12.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"Activity_10vodc0\" id=\"BPMNShape_Activity_10vodc0\">\n        <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"200.0\" y=\"-10.0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"Flow_041lywc\" id=\"BPMNEdge_Flow_041lywc\">\n        <omgdi:waypoint x=\"148.0\" y=\"30.0\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"200.0\" y=\"30.0\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>',0),('fcad8c21-9fd0-11ee-8a8a-005056c00001',1,'process_1703142898591.b219e72d-9fd0-11ee-8a8a-005056c00001.png','fc2bb1ef-9fd0-11ee-8a8a-005056c00001',_binary '�PNG\r\n\Z\n\0\0\0\rIHDR\0\06\0\0\0Z\0\0\02�z<\0\0\�IDATx^\�	lT\�\�\����qcqI\\q�[\�`�\n�P�)\\����`�\�P�,�\\���� B[J���\�(�RJ�Y\nH��p\��\�\�L\�,�\�\�\�v\�\�%_�sϙsOg\��\�\�{\�)Ey��Kii\�6l�7\'\'G�Y����\��~2??�*77w���P�$ԖʋJ\�\�Ԉ\�\�fq\�\�	:\�\���\�?//�I�\��3�(*@!R\�E�_lt\�]]]]/�V�FE(4?�Y\��$؎\�EQ\n}<�Fw�\�y\�EQ\�_�=T-J7\�;�S�\�7�\��\�\�L�Q�	\�lG\Z+\�\�_&�-+Ƶ1�!M\�Oo���L�?`;�#\�\rj�+v,w\�Oo���L�?`۵v�\�#M\�Oo���L�?`۾&\�\rh���秃7�FQ&�`��	6�2A��\r��:\�#M\�Oo���L�?`+)Hs�a�\��\�\�M�Q�	\�l\r�[\�\�Փݠ�cH\�\�\���`�(\�\�\�}�2\���cz>:4le��\�\�\�d}�\�pin�\�M�Q�	jlXY��`�\�#��\�3�FQ&\�+\�d$V]�+���\�3\�ȃ��\�B7�FQ&\�\�ڋҼ�\�[\�&\�(\�y�?Q�7\�zy��&\�(\�y��@��G�o���L�\'�\�g��򨦦��YYY�iii[RRR\��G�}\�\�p������c\�NJJ*OMM]>nܸ�\�%�\�e\�I��L�QmTXX�0cƌ\���x!a&�.]*~��w�w\�^\�\�\�  <\�9�#�\��\�>9jԨp�6��^�D�Y\���u\�\�s\�̩z\�\�\�\�\�/\�\�\�\��\�\�up��3\�ۘ��\��9�Y��L�\�\\�I��\\�2@�\�\�/ő#Gtf$�\�<�\�\�bcc\�\�Vl\�2�fcI��p\�\�\�/���(--\��P��e��\�R\�\�F�6k�`��${Ν5kV\�ĉEcc�\�%S�r��92S?��`��	6J2\�4��\0j��S(\�ر����\�#�D�Y\��\r�>�W^y%l��.�\'..\�\�\�x\\�K��`��	6������ݧ֞p�\�\�\�\�!C�\�\�\�\r\"جe�\�f�Zv�\�\�ӫd\�6S�S4�`��	6	�o�>|XgND�\�1�)\Z\'\�l\�2�f#aE��e\�t\�DT���{e\�6W�[g�`�\�\�?\�\�c\�\�\�\�\r6�\�䢢\"\�\�\�\�v\�\�X9R]]\�v\�յ��b˖-^\�\�\�۶mG�u;\�\���9�?&\�l\"-uKHHP_֎�<�e\�V�������Yf�m\�̙\�g����:&8�^xA\�\�\�[lڴI�\�y\�\�\�v\���\�\�\�o��\�V\�E]$�\�\�#\�|\�Mq�u\�\�\��N�1\�\0lW\\q��\�\�\�걬�L�?^<�\�\�\�\�+�O?��x\�\'T]\�r\�-���\�\��0΋V�\�\�\�~�\�\'\�\��C���\�>[,\\�\�g�@M�\�D򋔂��VP\\\\\\��\�z\�u��d=\�J\�Տ\�2l��\�\�ի��GZZ�hjj7\�|��Rjj��뮻\�Ν;��w\�\�%\�;\�<q\�\��6\�\\z\�j�\�\�/P\�\0�=z��s\�*\�\��]v�\�߿���{\�\�bŊ꜈\�\�y\��$y\��Uh��r\��\�\�\�ـ��F	\�=\��\�UW]%\�ϟ�ұ�Ǯ��zq\�M7��\�zK|\�\�\'\�\�F�;\�C��zف�`��fϞ�\r֭����\�\�\�?M�����\�>*\�V�\\)f͚����\�]\�z\�\�n\�|\�4h���+\�`DSK�,iQ�s\�9b�\�\�\�\�k�U\�\0&\�N�6ML�>]�[�N�\�\�\�?���jn\Z\�-Z�H\�\�\�oWF\�v\�5׈�:\�s\�g�\�k�\�\�\�&�\�\��\�\�\�\�\�Ǌ�\�D�v\�}\��\�q\�\�\�\n\�\0\0���	6�[\�c\�\�\�o�M�,��V�\�|.T��\���9��\��\�\�\"6� \�\�Ĵ���\�u\�]\'V�Z��p���Mԯ��Z\�Xbcc\�5�\�0\"<Dk\�\�\�\�(\�\0�\0\�\�ɓEEE��<\�\�h\��\�N�2E\\~�\�b\�ƍ\�\�EA\ne��/_��p\�\�\�S�P�\�4�\�Ӓ��\�&l6���5Gz\�7�\�\�_h�n\�\�hEy\0�G��\n6x�\�\�\�\�W_U\�\�K/����\r����aNN��yy\�7*`�\�\�m\�Fs\�\�\�S�w\�\�B$8j\�(1c\�$\�~1DNh\�~8�\�\�?\�\�`�ѥ�?==]\�uZ�z��\�\0����[�\�\0\0y\�m���;�]YY��\�]�vU\�\�ߓ`M�\�D\�\�\�$�hVPCC\�	�J��V��\�V�3l\0\0�0z\�hY<\0��:\�l\�\�\�\n=\��\�\�\�\�����k>\n`@���@<�\�\�/�P\�B3�\�{F^@�\�ԩS\�o��\���!\�BDh4mu\�h�v\�\�\�	�\�{L:ԙ\'//O�Q\�=\�\�#>��c�.\0���k�\�&\Z1b�hii\�\�!�\��\�*�ڡ�-++K]\�\�C䅋`A\�_}\����7�!:�4i�l�\�\�\�\�٣F>1)yq\���\��*\�\Z\�\�9`�\0թ�~0\0Q\�H\�\�p\�5X@��\\�̵~h\��I����_oȐ!�l���A\�[F\�\0�k��`��\�\�\�[��\�\�\�\�qt��m�\�\�(\0����`ؠ\�\�\�\�j���<�\r��h2VUU9��\�F�:>(M=�\�D䅑Jtܻ�\r}aȏHM[DR��p���7#b��f㯿��\�����4\�\�!��\��܌&�1\�\np\�=\�ߛ@M�\�D\�\�~\�*}l\�b\�\�\�}lm�f\��\n�ą?a\�\�G�)���y\�\�\�W_-v\�\�\�\��`\�\�>R\�?����P�a��V8�\�cƌQ\�J\�5@�`C��\�Jc\��\�P\�JDl#G�}�\��>��[��B\�:B��xD:��G�\�\�w\�U\���`�UW�\��\�\�s{o5�fM�8\�UFE���s:ᨨG�2l�~\�6g\�\�1�~\�B\�SMt��1\��h\�\�\�eff�����\�Y0�Y�\0\ZF4#D~\�`@�f1ڊ�lzٞ�\�4&�Cާ�~\�\�\�a>���ѩ`�L�\�D\�\�\�˭2�M^4\��g�O�2l���r0K��}M{˽\�0�f%&&\�\�ݤt\�t�Z�\r�\�\�IV�+��F�c�\�&\�\�̸���V����@B��kE\�p�`��p\�ώ\�\�cʔ)�\�\�#dc�}Z\�\�u\�\�]��z�\�[~8??_�F_��U\0�\�\��*\�[���\�e�\�F\�>h�\�gG\�\�\�\�\�X$�V\��\�B��v\�$WS0���|ۥKuܵ\�[~��i}�\�Q�_\0:\�8\�\�uß�<��*+�&\�l&\�̸�vЕ\�^\�\��!�\�1\�\�\�\�\�Bv�?jbnFF�8\�\�3\�\\5_�\�\�:KM\�E��4,�B\�w\�\��\�:�|�\�)\�WY\�4�f3\���q�\�\�&\n����\�A\�ƴ\�;ê\0yj5M\�/�\�5K�p˗|\�1\���;�d�\�#_e�J\�TV8M�\�P�C{|||s�\�RU[[�NBm��w�2՘����\�\�\�O?]\�\�\�\�YY%�x\�W~È\����\�͡o0\�V��4_e�\��M�;�\�f\���S\�B\�)�Hcbb\�\�u�&E\Zl\�\�G\�\�uGl��\nh�\�*�m�|\�7��\�k��\�v\��|�\�-\�[Y\�2�fc\�\���q�\"7�\�j2R�L?w�)�`C\�\�aB,F!�eTإ\�zc!Y%\�\�\�\�\�\�\�[\r˪ZA ֮]\�L\�sF�\�\�橬p�`��p�v������O�<\�#5C�\�SO=�v\�\0(x\�g��v\�\��m�p?�\�\�\�Xc�5�III\n>ؠ\�E�\��c��\�\�\��U��4_e�\��eC�\�fƸ\�\'\��\"\�*l���\�>5]�\�)�MG\�\�:Q\�Ёm�\0+\�f���0�D�\��\�t씋c��k~c{{eyJ\�UV8M�QJ\�״;�bྟ,\�[WW\�\�m-���)))j\�\�<�ѬH��n\�\�F�<��L�\�\�uFFF1\�Q }\�\�\�\�J����-ݱc\�&�\�Y�\��k?\�u\�8�\�l\�2�FyөC�\�-7[Iha\��\���\��c�#��_/�N\"جe���L�f-le�6k�`�(D�Y\�E� �\�Z&\�(\�l\�2�FQ&�`��	6�2A��L�Q�	\"جe���L�f-le�6k�`�(D�Y\�E� �\�Z&\�(\�l\�2�FQ&�`��	6�2A��L�Q�	\"جe���L�f-le�6k�`�(���s���\�\��#o�9TI�\�?#��T~~~n�_dt\���\�lE�gDQT�\�\�\����\�T]]]\�ȭc,\�\�겲�Ej\�ҏ\�EQA\"\�\�\�#n�\�x�	5��(��(��(��(��(��(��(��(��(��(�\��\�+\�̰��W\0\0\0\0IEND�B`�',1);
/*!40000 ALTER TABLE `act_ge_bytearray` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ge_property`
--

DROP TABLE IF EXISTS `act_ge_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ge_property` (
  `NAME_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `VALUE_` varchar(300) COLLATE utf8mb3_bin DEFAULT NULL,
  `REV_` int DEFAULT NULL,
  PRIMARY KEY (`NAME_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ge_property`
--

LOCK TABLES `act_ge_property` WRITE;
/*!40000 ALTER TABLE `act_ge_property` DISABLE KEYS */;
INSERT INTO `act_ge_property` VALUES ('batch.schema.version','7.0.0.0',1),('cfg.execution-related-entities-count','true',1),('cfg.task-related-entities-count','true',1),('common.schema.version','7.0.0.0',1),('entitylink.schema.version','7.0.0.0',1),('eventsubscription.schema.version','7.0.0.0',1),('identitylink.schema.version','7.0.0.0',1),('job.schema.version','7.0.0.0',1),('next.dbid','1',1),('schema.history','create(7.0.0.0)',1),('schema.version','7.0.0.0',1),('task.schema.version','7.0.0.0',1),('variable.schema.version','7.0.0.0',1);
/*!40000 ALTER TABLE `act_ge_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_actinst`
--

DROP TABLE IF EXISTS `act_hi_actinst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_actinst` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT '1',
  `PROC_DEF_ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `ACT_ID_` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `CALL_PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `ACT_NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `ACT_TYPE_` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `ASSIGNEE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) DEFAULT NULL,
  `TRANSACTION_ORDER_` int DEFAULT NULL,
  `DURATION_` bigint DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_ACT_INST_START` (`START_TIME_`),
  KEY `ACT_IDX_HI_ACT_INST_END` (`END_TIME_`),
  KEY `ACT_IDX_HI_ACT_INST_PROCINST` (`PROC_INST_ID_`,`ACT_ID_`),
  KEY `ACT_IDX_HI_ACT_INST_EXEC` (`EXECUTION_ID_`,`ACT_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_actinst`
--

LOCK TABLES `act_hi_actinst` WRITE;
/*!40000 ALTER TABLE `act_hi_actinst` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_hi_actinst` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_attachment`
--

DROP TABLE IF EXISTS `act_hi_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_attachment` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `USER_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `URL_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `CONTENT_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `TIME_` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_attachment`
--

LOCK TABLES `act_hi_attachment` WRITE;
/*!40000 ALTER TABLE `act_hi_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_hi_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_comment`
--

DROP TABLE IF EXISTS `act_hi_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_comment` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `USER_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `ACTION_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `MESSAGE_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `FULL_MSG_` longblob,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_comment`
--

LOCK TABLES `act_hi_comment` WRITE;
/*!40000 ALTER TABLE `act_hi_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_hi_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_detail`
--

DROP TABLE IF EXISTS `act_hi_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_detail` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `TYPE_` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `ACT_INST_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `VAR_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `REV_` int DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `BYTEARRAY_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `DOUBLE_` double DEFAULT NULL,
  `LONG_` bigint DEFAULT NULL,
  `TEXT_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `TEXT2_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_DETAIL_PROC_INST` (`PROC_INST_ID_`),
  KEY `ACT_IDX_HI_DETAIL_ACT_INST` (`ACT_INST_ID_`),
  KEY `ACT_IDX_HI_DETAIL_TIME` (`TIME_`),
  KEY `ACT_IDX_HI_DETAIL_NAME` (`NAME_`),
  KEY `ACT_IDX_HI_DETAIL_TASK_ID` (`TASK_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_detail`
--

LOCK TABLES `act_hi_detail` WRITE;
/*!40000 ALTER TABLE `act_hi_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_hi_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_entitylink`
--

DROP TABLE IF EXISTS `act_hi_entitylink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_entitylink` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `LINK_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `CREATE_TIME_` datetime(3) DEFAULT NULL,
  `SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `PARENT_ELEMENT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `REF_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `REF_SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `REF_SCOPE_DEFINITION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `ROOT_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `ROOT_SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `HIERARCHY_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_ENT_LNK_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`,`LINK_TYPE_`),
  KEY `ACT_IDX_HI_ENT_LNK_REF_SCOPE` (`REF_SCOPE_ID_`,`REF_SCOPE_TYPE_`,`LINK_TYPE_`),
  KEY `ACT_IDX_HI_ENT_LNK_ROOT_SCOPE` (`ROOT_SCOPE_ID_`,`ROOT_SCOPE_TYPE_`,`LINK_TYPE_`),
  KEY `ACT_IDX_HI_ENT_LNK_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`,`LINK_TYPE_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_entitylink`
--

LOCK TABLES `act_hi_entitylink` WRITE;
/*!40000 ALTER TABLE `act_hi_entitylink` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_hi_entitylink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_identitylink`
--

DROP TABLE IF EXISTS `act_hi_identitylink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_identitylink` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `GROUP_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `USER_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `CREATE_TIME_` datetime(3) DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_IDENT_LNK_USER` (`USER_ID_`),
  KEY `ACT_IDX_HI_IDENT_LNK_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_HI_IDENT_LNK_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_HI_IDENT_LNK_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_HI_IDENT_LNK_TASK` (`TASK_ID_`),
  KEY `ACT_IDX_HI_IDENT_LNK_PROCINST` (`PROC_INST_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_identitylink`
--

LOCK TABLES `act_hi_identitylink` WRITE;
/*!40000 ALTER TABLE `act_hi_identitylink` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_hi_identitylink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_procinst`
--

DROP TABLE IF EXISTS `act_hi_procinst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_procinst` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT '1',
  `PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `BUSINESS_KEY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) DEFAULT NULL,
  `DURATION_` bigint DEFAULT NULL,
  `START_USER_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `START_ACT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `END_ACT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUPER_PROCESS_INSTANCE_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  `NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `CALLBACK_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `CALLBACK_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `REFERENCE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `BUSINESS_STATUS_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `PROC_INST_ID_` (`PROC_INST_ID_`),
  KEY `ACT_IDX_HI_PRO_INST_END` (`END_TIME_`),
  KEY `ACT_IDX_HI_PRO_I_BUSKEY` (`BUSINESS_KEY_`),
  KEY `ACT_IDX_HI_PRO_SUPER_PROCINST` (`SUPER_PROCESS_INSTANCE_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_procinst`
--

LOCK TABLES `act_hi_procinst` WRITE;
/*!40000 ALTER TABLE `act_hi_procinst` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_hi_procinst` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_taskinst`
--

DROP TABLE IF EXISTS `act_hi_taskinst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_taskinst` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT '1',
  `PROC_DEF_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `TASK_DEF_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `OWNER_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `ASSIGNEE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `CLAIM_TIME_` datetime(3) DEFAULT NULL,
  `END_TIME_` datetime(3) DEFAULT NULL,
  `DURATION_` bigint DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `PRIORITY_` int DEFAULT NULL,
  `DUE_DATE_` datetime(3) DEFAULT NULL,
  `FORM_KEY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  `LAST_UPDATED_TIME_` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_TASK_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_HI_TASK_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_HI_TASK_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_HI_TASK_INST_PROCINST` (`PROC_INST_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_taskinst`
--

LOCK TABLES `act_hi_taskinst` WRITE;
/*!40000 ALTER TABLE `act_hi_taskinst` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_hi_taskinst` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_tsk_log`
--

DROP TABLE IF EXISTS `act_hi_tsk_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_tsk_log` (
  `ID_` bigint NOT NULL AUTO_INCREMENT,
  `TYPE_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `TIME_STAMP_` timestamp(3) NOT NULL,
  `USER_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `DATA_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_tsk_log`
--

LOCK TABLES `act_hi_tsk_log` WRITE;
/*!40000 ALTER TABLE `act_hi_tsk_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_hi_tsk_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_varinst`
--

DROP TABLE IF EXISTS `act_hi_varinst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_varinst` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT '1',
  `PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `VAR_TYPE_` varchar(100) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `DOUBLE_` double DEFAULT NULL,
  `LONG_` bigint DEFAULT NULL,
  `TEXT_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `TEXT2_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `META_INFO_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `CREATE_TIME_` datetime(3) DEFAULT NULL,
  `LAST_UPDATED_TIME_` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_PROCVAR_NAME_TYPE` (`NAME_`,`VAR_TYPE_`),
  KEY `ACT_IDX_HI_VAR_SCOPE_ID_TYPE` (`SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_HI_VAR_SUB_ID_TYPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_HI_PROCVAR_PROC_INST` (`PROC_INST_ID_`),
  KEY `ACT_IDX_HI_PROCVAR_TASK_ID` (`TASK_ID_`),
  KEY `ACT_IDX_HI_PROCVAR_EXE` (`EXECUTION_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_varinst`
--

LOCK TABLES `act_hi_varinst` WRITE;
/*!40000 ALTER TABLE `act_hi_varinst` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_hi_varinst` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_bytearray`
--

DROP TABLE IF EXISTS `act_id_bytearray`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_bytearray` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `BYTES_` longblob,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_bytearray`
--

LOCK TABLES `act_id_bytearray` WRITE;
/*!40000 ALTER TABLE `act_id_bytearray` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_id_bytearray` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_group`
--

DROP TABLE IF EXISTS `act_id_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_group` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_group`
--

LOCK TABLES `act_id_group` WRITE;
/*!40000 ALTER TABLE `act_id_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_id_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_info`
--

DROP TABLE IF EXISTS `act_id_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_info` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `USER_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `TYPE_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `KEY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `VALUE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `PASSWORD_` longblob,
  `PARENT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_info`
--

LOCK TABLES `act_id_info` WRITE;
/*!40000 ALTER TABLE `act_id_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_id_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_membership`
--

DROP TABLE IF EXISTS `act_id_membership`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_membership` (
  `USER_ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `GROUP_ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  PRIMARY KEY (`USER_ID_`,`GROUP_ID_`),
  KEY `ACT_FK_MEMB_GROUP` (`GROUP_ID_`),
  CONSTRAINT `ACT_FK_MEMB_GROUP` FOREIGN KEY (`GROUP_ID_`) REFERENCES `act_id_group` (`ID_`),
  CONSTRAINT `ACT_FK_MEMB_USER` FOREIGN KEY (`USER_ID_`) REFERENCES `act_id_user` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_membership`
--

LOCK TABLES `act_id_membership` WRITE;
/*!40000 ALTER TABLE `act_id_membership` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_id_membership` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_priv`
--

DROP TABLE IF EXISTS `act_id_priv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_priv` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `NAME_` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `ACT_UNIQ_PRIV_NAME` (`NAME_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_priv`
--

LOCK TABLES `act_id_priv` WRITE;
/*!40000 ALTER TABLE `act_id_priv` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_id_priv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_priv_mapping`
--

DROP TABLE IF EXISTS `act_id_priv_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_priv_mapping` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `PRIV_ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `USER_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `GROUP_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_PRIV_MAPPING` (`PRIV_ID_`),
  KEY `ACT_IDX_PRIV_USER` (`USER_ID_`),
  KEY `ACT_IDX_PRIV_GROUP` (`GROUP_ID_`),
  CONSTRAINT `ACT_FK_PRIV_MAPPING` FOREIGN KEY (`PRIV_ID_`) REFERENCES `act_id_priv` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_priv_mapping`
--

LOCK TABLES `act_id_priv_mapping` WRITE;
/*!40000 ALTER TABLE `act_id_priv_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_id_priv_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_property`
--

DROP TABLE IF EXISTS `act_id_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_property` (
  `NAME_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `VALUE_` varchar(300) COLLATE utf8mb3_bin DEFAULT NULL,
  `REV_` int DEFAULT NULL,
  PRIMARY KEY (`NAME_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_property`
--

LOCK TABLES `act_id_property` WRITE;
/*!40000 ALTER TABLE `act_id_property` DISABLE KEYS */;
INSERT INTO `act_id_property` VALUES ('schema.version','7.0.0.0',1);
/*!40000 ALTER TABLE `act_id_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_token`
--

DROP TABLE IF EXISTS `act_id_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_token` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `TOKEN_VALUE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TOKEN_DATE_` timestamp(3) NULL DEFAULT NULL,
  `IP_ADDRESS_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `USER_AGENT_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `USER_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TOKEN_DATA_` varchar(2000) COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_token`
--

LOCK TABLES `act_id_token` WRITE;
/*!40000 ALTER TABLE `act_id_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_id_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_user`
--

DROP TABLE IF EXISTS `act_id_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_user` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `FIRST_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `LAST_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `DISPLAY_NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `EMAIL_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `PWD_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `PICTURE_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_user`
--

LOCK TABLES `act_id_user` WRITE;
/*!40000 ALTER TABLE `act_id_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_id_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_procdef_info`
--

DROP TABLE IF EXISTS `act_procdef_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_procdef_info` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `INFO_JSON_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `ACT_UNIQ_INFO_PROCDEF` (`PROC_DEF_ID_`),
  KEY `ACT_IDX_INFO_PROCDEF` (`PROC_DEF_ID_`),
  KEY `ACT_FK_INFO_JSON_BA` (`INFO_JSON_ID_`),
  CONSTRAINT `ACT_FK_INFO_JSON_BA` FOREIGN KEY (`INFO_JSON_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_INFO_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_procdef_info`
--

LOCK TABLES `act_procdef_info` WRITE;
/*!40000 ALTER TABLE `act_procdef_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_procdef_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_re_deployment`
--

DROP TABLE IF EXISTS `act_re_deployment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_re_deployment` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `KEY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  `DEPLOY_TIME_` timestamp(3) NULL DEFAULT NULL,
  `DERIVED_FROM_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `DERIVED_FROM_ROOT_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PARENT_DEPLOYMENT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `ENGINE_VERSION_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_re_deployment`
--

LOCK TABLES `act_re_deployment` WRITE;
/*!40000 ALTER TABLE `act_re_deployment` DISABLE KEYS */;
INSERT INTO `act_re_deployment` VALUES ('4a23a5ab-9fd3-11ee-8a8a-005056c00001','OA_1703143742179','001','process_1703143742179','','2023-12-21 07:33:52.583',NULL,NULL,'4a23a5ab-9fd3-11ee-8a8a-005056c00001',NULL),('672db81e-a45c-11ee-ba71-005056c00001','business_1702865697042','001','process_1702865697042','','2023-12-27 02:05:27.006',NULL,NULL,'672db81e-a45c-11ee-ba71-005056c00001',NULL),('7b8eef17-9f0d-11ee-9459-005056c00001','business_1702865697042','001','process_1702865697042','','2023-12-20 07:57:55.142',NULL,NULL,'7b8eef17-9f0d-11ee-9459-005056c00001',NULL),('b88a591e-9f18-11ee-8c1f-005056c00001','business_1702865697042','001','process_1702865697042','','2023-12-20 09:18:21.911',NULL,NULL,'b88a591e-9f18-11ee-8c1f-005056c00001',NULL),('dba55d6f-9fc7-11ee-90eb-005056c00001','business_1702865697042','001','process_1702865697042','','2023-12-21 06:12:02.742',NULL,NULL,'dba55d6f-9fc7-11ee-90eb-005056c00001',NULL),('eb06c15c-9fcb-11ee-b189-005056c00001','business_1703140711137','001','process_1703140711137','','2023-12-21 06:41:06.533',NULL,NULL,'eb06c15c-9fcb-11ee-b189-005056c00001',NULL),('fc2bb1ef-9fd0-11ee-8a8a-005056c00001','business_1703142898591','001','process_1703142898591','','2023-12-21 07:17:22.780',NULL,NULL,'fc2bb1ef-9fd0-11ee-8a8a-005056c00001',NULL);
/*!40000 ALTER TABLE `act_re_deployment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_re_model`
--

DROP TABLE IF EXISTS `act_re_model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_re_model` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `KEY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LAST_UPDATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `VERSION_` int DEFAULT NULL,
  `META_INFO_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `EDITOR_SOURCE_VALUE_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `EDITOR_SOURCE_EXTRA_VALUE_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_MODEL_SOURCE` (`EDITOR_SOURCE_VALUE_ID_`),
  KEY `ACT_FK_MODEL_SOURCE_EXTRA` (`EDITOR_SOURCE_EXTRA_VALUE_ID_`),
  KEY `ACT_FK_MODEL_DEPLOYMENT` (`DEPLOYMENT_ID_`),
  CONSTRAINT `ACT_FK_MODEL_DEPLOYMENT` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_re_deployment` (`ID_`),
  CONSTRAINT `ACT_FK_MODEL_SOURCE` FOREIGN KEY (`EDITOR_SOURCE_VALUE_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_MODEL_SOURCE_EXTRA` FOREIGN KEY (`EDITOR_SOURCE_EXTRA_VALUE_ID_`) REFERENCES `act_ge_bytearray` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_re_model`
--

LOCK TABLES `act_re_model` WRITE;
/*!40000 ALTER TABLE `act_re_model` DISABLE KEYS */;
INSERT INTO `act_re_model` VALUES ('0f481d04-a45a-11ee-b377-005056c00001',2,'business_1702865697042','process_1702865697042','001','2023-12-27 01:48:40.546','2023-12-27 01:48:40.576',3,'{\"description\":\"测试模型key\"}',NULL,'0f4c89d5-a45a-11ee-b377-005056c00001',NULL,''),('2bc836fa-9d50-11ee-b541-005056c00001',3,'business_1702865697042','process_1702865697042','001','2023-12-18 02:50:15.255','2023-12-18 06:52:52.062',2,'{\"description\":\"测试模型key\"}',NULL,'2be004bb-9d50-11ee-b541-005056c00001',NULL,''),('2de4cd5c-a45c-11ee-ba71-005056c00001',2,'business_1702865697042','process_1702865697042','001','2023-12-27 02:03:50.898','2023-12-27 02:03:50.994',4,'{\"description\":\"测试模型key\"}',NULL,'2df3253d-a45c-11ee-ba71-005056c00001',NULL,''),('322e6079-9fd3-11ee-8a8a-005056c00001',4,'OA_1703143742179','process_1703143742179','001','2023-12-21 07:33:12.388','2023-12-26 06:39:04.951',2,'{\"description\":\"测试202312211529\"}',NULL,'322fc00a-9fd3-11ee-8a8a-005056c00001',NULL,''),('46ce30ae-9d4b-11ee-a04d-005056c00001',1,'business_1702865697042','process_1702865697042','001','2023-12-18 02:15:13.109','2023-12-18 02:15:13.109',1,'{\"description\":\"测试模型key\"}',NULL,NULL,NULL,''),('4f6a1089-9e4f-11ee-b219-005056c00001',1,'business_1702977383506','process_1702977383506','001','2023-12-19 09:16:36.702','2023-12-19 09:16:36.702',1,'{\"description\":\"测试空流程图\"}',NULL,NULL,NULL,''),('986a39ea-9fcb-11ee-b189-005056c00001',3,'business_1703140711137','process_1703140711137','001','2023-12-21 06:38:47.934','2023-12-21 06:40:29.284',1,'{\"description\":\"测试流程名称\"}',NULL,'d4d2660b-9fcb-11ee-b189-005056c00001',NULL,''),('ac00dbe3-9fd2-11ee-8a8a-005056c00001',3,'OA_1703143742179','process_1703143742179','001','2023-12-21 07:29:27.275','2023-12-21 07:30:17.509',1,'{\"description\":\"测试202312211529\"}',NULL,'c9f1f584-9fd2-11ee-8a8a-005056c00001',NULL,''),('ae2a7ab2-a45c-11ee-ba71-005056c00001',2,'business_1702865697042','process_1702865697042','001','2023-12-27 02:07:26.103','2023-12-27 02:07:26.118',5,'{\"description\":\"测试模型key\"}',NULL,'ae2cc4a3-a45c-11ee-ba71-005056c00001',NULL,''),('b219e72d-9fd0-11ee-8a8a-005056c00001',3,'business_1703142898591','process_1703142898591','001','2023-12-21 07:15:18.511','2023-12-21 07:16:41.967',1,'{\"description\":\"测试新流程20231221\"}',NULL,'e3d7aaee-9fd0-11ee-8a8a-005056c00001',NULL,'');
/*!40000 ALTER TABLE `act_re_model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_re_procdef`
--

DROP TABLE IF EXISTS `act_re_procdef`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_re_procdef` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `KEY_` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `VERSION_` int NOT NULL,
  `DEPLOYMENT_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `RESOURCE_NAME_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `DGRM_RESOURCE_NAME_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `HAS_START_FORM_KEY_` tinyint DEFAULT NULL,
  `HAS_GRAPHICAL_NOTATION_` tinyint DEFAULT NULL,
  `SUSPENSION_STATE_` int DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  `ENGINE_VERSION_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `DERIVED_FROM_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `DERIVED_FROM_ROOT_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `DERIVED_VERSION_` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `ACT_UNIQ_PROCDEF` (`KEY_`,`VERSION_`,`DERIVED_VERSION_`,`TENANT_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_re_procdef`
--

LOCK TABLES `act_re_procdef` WRITE;
/*!40000 ALTER TABLE `act_re_procdef` DISABLE KEYS */;
INSERT INTO `act_re_procdef` VALUES ('Process_1701854918145:1:7c072c5a-9f0d-11ee-9459-005056c00001',1,'http://flowable.org/bpmn','业务流程_1701854918145','Process_1701854918145',1,'7b8eef17-9f0d-11ee-9459-005056c00001','business_1702865697042.bpmn','business_1702865697042.Process_1701854918145.png',NULL,0,1,1,'',NULL,NULL,NULL,0),('Process_1701854918145:2:bb1067c1-9f18-11ee-8c1f-005056c00001',1,'http://flowable.org/bpmn','业务流程_1701854918145','Process_1701854918145',2,'b88a591e-9f18-11ee-8c1f-005056c00001','business_1702865697042.bpmn','business_1702865697042.Process_1701854918145.png',NULL,0,1,1,'',NULL,NULL,NULL,0),('Process_1701854918145:3:de5b7cc2-9fc7-11ee-90eb-005056c00001',1,'http://flowable.org/bpmn','业务流程_1701854918145','Process_1701854918145',3,'dba55d6f-9fc7-11ee-90eb-005056c00001','process_1702865697042.bpmn20.xml','process_1702865697042.Process_1701854918145.png',NULL,0,1,1,'',NULL,NULL,NULL,0),('Process_1701854918145:4:eb8fa06f-9fcb-11ee-b189-005056c00001',4,'001','Process_1701854918145','Process_1701854918145',4,'eb06c15c-9fcb-11ee-b189-005056c00001','process_1703140711137.bpmn20.xml','process_1703140711137.Process_1701854918145.png',NULL,0,1,1,'',NULL,NULL,NULL,0),('Process_1701854918145:5:67ac0fe1-a45c-11ee-ba71-005056c00001',2,'001','业务流程_1701854918145','Process_1701854918145',5,'672db81e-a45c-11ee-ba71-005056c00001','process_1702865697042.bpmn20.xml','process_1702865697042.Process_1701854918145.png',NULL,0,1,1,'',NULL,NULL,NULL,0),('fcae0152-9fd0-11ee-8a8a-005056c00001',2,'001','business_1703142898591','b219e72d-9fd0-11ee-8a8a-005056c00001',1,'fc2bb1ef-9fd0-11ee-8a8a-005056c00001','process_1703142898591.bpmn20.xml','process_1703142898591.b219e72d-9fd0-11ee-8a8a-005056c00001.png',NULL,0,1,1,'',NULL,NULL,NULL,0),('process_1703143742179:2:4a3ecece-9fd3-11ee-8a8a-005056c00001',9,'001','OA_1703143742179','process_1703143742179',2,'4a23a5ab-9fd3-11ee-8a8a-005056c00001','process_1703143742179.bpmn20.xml','process_1703143742179.process_1703143742179.png','测试新建流程202312211529',0,1,2,'',NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `act_re_procdef` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_actinst`
--

DROP TABLE IF EXISTS `act_ru_actinst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_actinst` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT '1',
  `PROC_DEF_ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `ACT_ID_` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `CALL_PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `ACT_NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `ACT_TYPE_` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `ASSIGNEE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) DEFAULT NULL,
  `DURATION_` bigint DEFAULT NULL,
  `TRANSACTION_ORDER_` int DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_RU_ACTI_START` (`START_TIME_`),
  KEY `ACT_IDX_RU_ACTI_END` (`END_TIME_`),
  KEY `ACT_IDX_RU_ACTI_PROC` (`PROC_INST_ID_`),
  KEY `ACT_IDX_RU_ACTI_PROC_ACT` (`PROC_INST_ID_`,`ACT_ID_`),
  KEY `ACT_IDX_RU_ACTI_EXEC` (`EXECUTION_ID_`),
  KEY `ACT_IDX_RU_ACTI_EXEC_ACT` (`EXECUTION_ID_`,`ACT_ID_`),
  KEY `ACT_IDX_RU_ACTI_TASK` (`TASK_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_actinst`
--

LOCK TABLES `act_ru_actinst` WRITE;
/*!40000 ALTER TABLE `act_ru_actinst` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_actinst` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_deadletter_job`
--

DROP TABLE IF EXISTS `act_ru_deadletter_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_deadletter_job` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_DEADLETTER_JOB_EXCEPTION_STACK_ID` (`EXCEPTION_STACK_ID_`),
  KEY `ACT_IDX_DEADLETTER_JOB_CUSTOM_VALUES_ID` (`CUSTOM_VALUES_ID_`),
  KEY `ACT_IDX_DEADLETTER_JOB_CORRELATION_ID` (`CORRELATION_ID_`),
  KEY `ACT_IDX_DJOB_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_DJOB_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_DJOB_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_FK_DEADLETTER_JOB_EXECUTION` (`EXECUTION_ID_`),
  KEY `ACT_FK_DEADLETTER_JOB_PROCESS_INSTANCE` (`PROCESS_INSTANCE_ID_`),
  KEY `ACT_FK_DEADLETTER_JOB_PROC_DEF` (`PROC_DEF_ID_`),
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`),
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_deadletter_job`
--

LOCK TABLES `act_ru_deadletter_job` WRITE;
/*!40000 ALTER TABLE `act_ru_deadletter_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_deadletter_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_entitylink`
--

DROP TABLE IF EXISTS `act_ru_entitylink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_entitylink` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `CREATE_TIME_` datetime(3) DEFAULT NULL,
  `LINK_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `PARENT_ELEMENT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `REF_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `REF_SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `REF_SCOPE_DEFINITION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `ROOT_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `ROOT_SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `HIERARCHY_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_ENT_LNK_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`,`LINK_TYPE_`),
  KEY `ACT_IDX_ENT_LNK_REF_SCOPE` (`REF_SCOPE_ID_`,`REF_SCOPE_TYPE_`,`LINK_TYPE_`),
  KEY `ACT_IDX_ENT_LNK_ROOT_SCOPE` (`ROOT_SCOPE_ID_`,`ROOT_SCOPE_TYPE_`,`LINK_TYPE_`),
  KEY `ACT_IDX_ENT_LNK_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`,`LINK_TYPE_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_entitylink`
--

LOCK TABLES `act_ru_entitylink` WRITE;
/*!40000 ALTER TABLE `act_ru_entitylink` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_entitylink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_event_subscr`
--

DROP TABLE IF EXISTS `act_ru_event_subscr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_event_subscr` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `EVENT_TYPE_` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `EVENT_NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `ACTIVITY_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `CONFIGURATION_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `CREATED_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `PROC_DEF_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_EVENT_SUBSCR_CONFIG_` (`CONFIGURATION_`),
  KEY `ACT_IDX_EVENT_SUBSCR_SCOPEREF_` (`SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_FK_EVENT_EXEC` (`EXECUTION_ID_`),
  CONSTRAINT `ACT_FK_EVENT_EXEC` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_event_subscr`
--

LOCK TABLES `act_ru_event_subscr` WRITE;
/*!40000 ALTER TABLE `act_ru_event_subscr` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_event_subscr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_execution`
--

DROP TABLE IF EXISTS `act_ru_execution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_execution` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `BUSINESS_KEY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `PARENT_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUPER_EXEC_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `ROOT_PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `ACT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `IS_ACTIVE_` tinyint DEFAULT NULL,
  `IS_CONCURRENT_` tinyint DEFAULT NULL,
  `IS_SCOPE_` tinyint DEFAULT NULL,
  `IS_EVENT_SCOPE_` tinyint DEFAULT NULL,
  `IS_MI_ROOT_` tinyint DEFAULT NULL,
  `SUSPENSION_STATE_` int DEFAULT NULL,
  `CACHED_ENT_STATE_` int DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  `NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `START_ACT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `START_TIME_` datetime(3) DEFAULT NULL,
  `START_USER_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `IS_COUNT_ENABLED_` tinyint DEFAULT NULL,
  `EVT_SUBSCR_COUNT_` int DEFAULT NULL,
  `TASK_COUNT_` int DEFAULT NULL,
  `JOB_COUNT_` int DEFAULT NULL,
  `TIMER_JOB_COUNT_` int DEFAULT NULL,
  `SUSP_JOB_COUNT_` int DEFAULT NULL,
  `DEADLETTER_JOB_COUNT_` int DEFAULT NULL,
  `EXTERNAL_WORKER_JOB_COUNT_` int DEFAULT NULL,
  `VAR_COUNT_` int DEFAULT NULL,
  `ID_LINK_COUNT_` int DEFAULT NULL,
  `CALLBACK_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `CALLBACK_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `REFERENCE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `BUSINESS_STATUS_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_EXEC_BUSKEY` (`BUSINESS_KEY_`),
  KEY `ACT_IDC_EXEC_ROOT` (`ROOT_PROC_INST_ID_`),
  KEY `ACT_IDX_EXEC_REF_ID_` (`REFERENCE_ID_`),
  KEY `ACT_FK_EXE_PROCINST` (`PROC_INST_ID_`),
  KEY `ACT_FK_EXE_PARENT` (`PARENT_ID_`),
  KEY `ACT_FK_EXE_SUPER` (`SUPER_EXEC_`),
  KEY `ACT_FK_EXE_PROCDEF` (`PROC_DEF_ID_`),
  CONSTRAINT `ACT_FK_EXE_PARENT` FOREIGN KEY (`PARENT_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE,
  CONSTRAINT `ACT_FK_EXE_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`),
  CONSTRAINT `ACT_FK_EXE_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ACT_FK_EXE_SUPER` FOREIGN KEY (`SUPER_EXEC_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_execution`
--

LOCK TABLES `act_ru_execution` WRITE;
/*!40000 ALTER TABLE `act_ru_execution` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_execution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_external_job`
--

DROP TABLE IF EXISTS `act_ru_external_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_external_job` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `RETRIES_` int DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_EXTERNAL_JOB_EXCEPTION_STACK_ID` (`EXCEPTION_STACK_ID_`),
  KEY `ACT_IDX_EXTERNAL_JOB_CUSTOM_VALUES_ID` (`CUSTOM_VALUES_ID_`),
  KEY `ACT_IDX_EXTERNAL_JOB_CORRELATION_ID` (`CORRELATION_ID_`),
  KEY `ACT_IDX_EJOB_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_EJOB_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_EJOB_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`),
  CONSTRAINT `ACT_FK_EXTERNAL_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_EXTERNAL_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_external_job`
--

LOCK TABLES `act_ru_external_job` WRITE;
/*!40000 ALTER TABLE `act_ru_external_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_external_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_history_job`
--

DROP TABLE IF EXISTS `act_ru_history_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_history_job` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `RETRIES_` int DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `ADV_HANDLER_CFG_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_history_job`
--

LOCK TABLES `act_ru_history_job` WRITE;
/*!40000 ALTER TABLE `act_ru_history_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_history_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_identitylink`
--

DROP TABLE IF EXISTS `act_ru_identitylink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_identitylink` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `GROUP_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `USER_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_IDENT_LNK_USER` (`USER_ID_`),
  KEY `ACT_IDX_IDENT_LNK_GROUP` (`GROUP_ID_`),
  KEY `ACT_IDX_IDENT_LNK_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_IDENT_LNK_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_IDENT_LNK_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_ATHRZ_PROCEDEF` (`PROC_DEF_ID_`),
  KEY `ACT_FK_TSKASS_TASK` (`TASK_ID_`),
  KEY `ACT_FK_IDL_PROCINST` (`PROC_INST_ID_`),
  CONSTRAINT `ACT_FK_ATHRZ_PROCEDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`),
  CONSTRAINT `ACT_FK_IDL_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_TSKASS_TASK` FOREIGN KEY (`TASK_ID_`) REFERENCES `act_ru_task` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_identitylink`
--

LOCK TABLES `act_ru_identitylink` WRITE;
/*!40000 ALTER TABLE `act_ru_identitylink` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_identitylink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_job`
--

DROP TABLE IF EXISTS `act_ru_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_job` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `RETRIES_` int DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_JOB_EXCEPTION_STACK_ID` (`EXCEPTION_STACK_ID_`),
  KEY `ACT_IDX_JOB_CUSTOM_VALUES_ID` (`CUSTOM_VALUES_ID_`),
  KEY `ACT_IDX_JOB_CORRELATION_ID` (`CORRELATION_ID_`),
  KEY `ACT_IDX_JOB_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_JOB_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_JOB_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_FK_JOB_EXECUTION` (`EXECUTION_ID_`),
  KEY `ACT_FK_JOB_PROCESS_INSTANCE` (`PROCESS_INSTANCE_ID_`),
  KEY `ACT_FK_JOB_PROC_DEF` (`PROC_DEF_ID_`),
  CONSTRAINT `ACT_FK_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`),
  CONSTRAINT `ACT_FK_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_job`
--

LOCK TABLES `act_ru_job` WRITE;
/*!40000 ALTER TABLE `act_ru_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_suspended_job`
--

DROP TABLE IF EXISTS `act_ru_suspended_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_suspended_job` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `RETRIES_` int DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_SUSPENDED_JOB_EXCEPTION_STACK_ID` (`EXCEPTION_STACK_ID_`),
  KEY `ACT_IDX_SUSPENDED_JOB_CUSTOM_VALUES_ID` (`CUSTOM_VALUES_ID_`),
  KEY `ACT_IDX_SUSPENDED_JOB_CORRELATION_ID` (`CORRELATION_ID_`),
  KEY `ACT_IDX_SJOB_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_SJOB_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_SJOB_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_FK_SUSPENDED_JOB_EXECUTION` (`EXECUTION_ID_`),
  KEY `ACT_FK_SUSPENDED_JOB_PROCESS_INSTANCE` (`PROCESS_INSTANCE_ID_`),
  KEY `ACT_FK_SUSPENDED_JOB_PROC_DEF` (`PROC_DEF_ID_`),
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`),
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_suspended_job`
--

LOCK TABLES `act_ru_suspended_job` WRITE;
/*!40000 ALTER TABLE `act_ru_suspended_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_suspended_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_task`
--

DROP TABLE IF EXISTS `act_ru_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_task` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `TASK_DEF_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `OWNER_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `ASSIGNEE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `DELEGATION_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PRIORITY_` int DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `DUE_DATE_` datetime(3) DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUSPENSION_STATE_` int DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  `FORM_KEY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `CLAIM_TIME_` datetime(3) DEFAULT NULL,
  `IS_COUNT_ENABLED_` tinyint DEFAULT NULL,
  `VAR_COUNT_` int DEFAULT NULL,
  `ID_LINK_COUNT_` int DEFAULT NULL,
  `SUB_TASK_COUNT_` int DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_TASK_CREATE` (`CREATE_TIME_`),
  KEY `ACT_IDX_TASK_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_TASK_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_TASK_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_FK_TASK_EXE` (`EXECUTION_ID_`),
  KEY `ACT_FK_TASK_PROCINST` (`PROC_INST_ID_`),
  KEY `ACT_FK_TASK_PROCDEF` (`PROC_DEF_ID_`),
  CONSTRAINT `ACT_FK_TASK_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_TASK_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`),
  CONSTRAINT `ACT_FK_TASK_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_task`
--

LOCK TABLES `act_ru_task` WRITE;
/*!40000 ALTER TABLE `act_ru_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_timer_job`
--

DROP TABLE IF EXISTS `act_ru_timer_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_timer_job` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `RETRIES_` int DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_TIMER_JOB_EXCEPTION_STACK_ID` (`EXCEPTION_STACK_ID_`),
  KEY `ACT_IDX_TIMER_JOB_CUSTOM_VALUES_ID` (`CUSTOM_VALUES_ID_`),
  KEY `ACT_IDX_TIMER_JOB_CORRELATION_ID` (`CORRELATION_ID_`),
  KEY `ACT_IDX_TIMER_JOB_DUEDATE` (`DUEDATE_`),
  KEY `ACT_IDX_TJOB_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_TJOB_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_TJOB_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_FK_TIMER_JOB_EXECUTION` (`EXECUTION_ID_`),
  KEY `ACT_FK_TIMER_JOB_PROCESS_INSTANCE` (`PROCESS_INSTANCE_ID_`),
  KEY `ACT_FK_TIMER_JOB_PROC_DEF` (`PROC_DEF_ID_`),
  CONSTRAINT `ACT_FK_TIMER_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_TIMER_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_TIMER_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_TIMER_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`),
  CONSTRAINT `ACT_FK_TIMER_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_timer_job`
--

LOCK TABLES `act_ru_timer_job` WRITE;
/*!40000 ALTER TABLE `act_ru_timer_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_timer_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_variable`
--

DROP TABLE IF EXISTS `act_ru_variable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_variable` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `NAME_` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `DOUBLE_` double DEFAULT NULL,
  `LONG_` bigint DEFAULT NULL,
  `TEXT_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `TEXT2_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  `META_INFO_` varchar(4000) COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_RU_VAR_SCOPE_ID_TYPE` (`SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_IDX_RU_VAR_SUB_ID_TYPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`),
  KEY `ACT_FK_VAR_BYTEARRAY` (`BYTEARRAY_ID_`),
  KEY `ACT_IDX_VARIABLE_TASK_ID` (`TASK_ID_`),
  KEY `ACT_FK_VAR_EXE` (`EXECUTION_ID_`),
  KEY `ACT_FK_VAR_PROCINST` (`PROC_INST_ID_`),
  CONSTRAINT `ACT_FK_VAR_BYTEARRAY` FOREIGN KEY (`BYTEARRAY_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_VAR_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_VAR_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_variable`
--

LOCK TABLES `act_ru_variable` WRITE;
/*!40000 ALTER TABLE `act_ru_variable` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_variable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `business_custom`
--

DROP TABLE IF EXISTS `business_custom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `business_custom` (
  `custom_name` varchar(32) DEFAULT NULL COMMENT '客户姓名',
  `orgid` bigint unsigned DEFAULT NULL COMMENT '机构id',
  `id` bigint unsigned DEFAULT NULL COMMENT '编号',
  `update_by` bigint unsigned DEFAULT NULL COMMENT '更新人',
  `update_time` varchar(16) DEFAULT NULL COMMENT '更新时间',
  `create_by` bigint unsigned DEFAULT NULL COMMENT '创建人',
  `create_time` varchar(16) DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='客户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `business_custom`
--

LOCK TABLES `business_custom` WRITE;
/*!40000 ALTER TABLE `business_custom` DISABLE KEYS */;
/*!40000 ALTER TABLE `business_custom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flowable_category`
--

DROP TABLE IF EXISTS `flowable_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flowable_category` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '流程分类id',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '流程分类名称',
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '分类编码',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '备注',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='流程分类表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flowable_category`
--

LOCK TABLES `flowable_category` WRITE;
/*!40000 ALTER TABLE `flowable_category` DISABLE KEYS */;
INSERT INTO `flowable_category` VALUES (2,'测试','001','123','','2023-11-28 09:56:09','','2023-12-22 11:26:27','0');
/*!40000 ALTER TABLE `flowable_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flowable_field_def`
--

DROP TABLE IF EXISTS `flowable_field_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flowable_field_def` (
  `id` bigint unsigned NOT NULL COMMENT '主键ID',
  `field` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '数据库字段/表单字段',
  `label` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '字段名/表单字段名',
  `remark` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '字段说明',
  `width` int NOT NULL DEFAULT '0' COMMENT '字段宽度',
  `type` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '字段组件类型',
  `scheme` json NOT NULL COMMENT '字段定义',
  `scope` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '字段范围',
  `update_by` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_by` varchar(64) COLLATE utf8mb3_bin NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `modeling_field_def_field_index` (`field`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='流程字段定义';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flowable_field_def`
--

LOCK TABLES `flowable_field_def` WRITE;
/*!40000 ALTER TABLE `flowable_field_def` DISABLE KEYS */;
INSERT INTO `flowable_field_def` VALUES (1583622160878735361,'id','编号','主键',8,'number','{\"type\": \"number\", \"precision\": 0}','WORKFLOW_DEFAULT','1','2022-11-20 21:39:36','1','2022-10-22 08:52:00'),(1584160647990243329,'process_instance_id','流程实例ID','流程引擎实例ID',36,'text','{\"type\": \"text\", \"defaultValue\": null}','WORKFLOW_DEFAULT','1','2022-11-20 21:39:36','1','2022-10-23 20:32:04'),(1584161060399378434,'code','流水号','流程唯一编号',32,'text','{\"type\": \"text\", \"defaultValue\": null}','WORKFLOW_DEFAULT','1','2022-11-22 21:57:54','1','2022-10-23 20:33:24'),(1584161141097787394,'name','标题','流程标题',128,'text','{\"type\": \"text\", \"defaultValue\": null}','WORKFLOW_DEFAULT','1','2022-11-20 21:39:36','1','2022-10-23 20:33:43'),(1584161976175300609,'dept_id','所属部门','',32,'dept','{\"type\": \"dept\", \"multiple\": false}','WORKFLOW_DEFAULT','1','2022-11-20 21:39:36','1','2022-10-23 20:37:02'),(1584181666134118402,'create_by','创建人','发起流程的人',16,'user','{\"type\": \"user\", \"multiple\": false}','WORKFLOW_DEFAULT','1','2022-11-20 21:39:36','1','2022-10-23 21:55:17'),(1584181794765033473,'create_time','创建时间','发起流程的时间',16,'date','{\"type\": \"date\", \"format\": \"YYYY-MM-DD HH:mm:ss\", \"dateType\": \"datetime\", \"valueFormat\": \"YYYY-MM-DD HH:mm:ss\", \"defaultValue\": \"\"}','WORKFLOW_DEFAULT','1','2022-11-20 21:39:36','1','2022-10-23 21:55:47'),(1584182223049609217,'update_by','更新人','更新数据的人',16,'user','{\"type\": \"user\", \"multiple\": false}','WORKFLOW_DEFAULT','1','2022-11-20 21:39:36','1','2022-10-23 21:57:29'),(1584182320063860737,'update_time','更新时间','最后更新流程数据的时间',16,'date','{\"type\": \"date\", \"format\": \"YYYY-MM-DD HH:mm:ss\", \"dateType\": \"datetime\", \"valueFormat\": \"YYYY-MM-DD HH:mm:ss\", \"defaultValue\": \"\"}','WORKFLOW_DEFAULT','1','2022-11-20 21:39:36','1','2022-10-23 21:57:52'),(1584339977915265025,'cost_center','成本中心','成本中心下拉单选字段',16,'option','{\"type\": \"option\", \"multiple\": false, \"optionTypeId\": \"1583739849538342914\", \"optionComponent\": \"single-select\", \"optionDefaultValue\": [\"1583824814787584001\"]}','GLOBAL','1','2022-11-21 16:00:21','1','2022-10-24 08:24:21'),(1589631844999802881,'cost_center_multiple','多选成本中心','多选成本中心',16,'option','{\"type\": \"option\", \"multiple\": true, \"optionTypeId\": \"1583739849538342914\", \"optionComponent\": \"multi-select\", \"optionDefaultValue\": [\"1584347991602769921\", \"1584348027585703938\"]}','GLOBAL','1','2022-11-21 16:00:22','1','2022-11-07 22:52:20');
/*!40000 ALTER TABLE `flowable_field_def` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flowable_field_ref`
--

DROP TABLE IF EXISTS `flowable_field_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flowable_field_ref` (
  `id` bigint unsigned NOT NULL COMMENT '主键ID',
  `module` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '模块',
  `mkey` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '模型标识',
  `field_id` bigint unsigned NOT NULL COMMENT '字段ID',
  `create_by` varchar(64) COLLATE utf8mb3_bin NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `version` int NOT NULL COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `key_unidx` (`module`,`mkey`,`field_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='流程字段引用关系表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flowable_field_ref`
--

LOCK TABLES `flowable_field_ref` WRITE;
/*!40000 ALTER TABLE `flowable_field_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `flowable_field_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flowable_model_page`
--

DROP TABLE IF EXISTS `flowable_model_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flowable_model_page` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `module` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '模块',
  `mkey` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '模型标识',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '页面名称',
  `page_scheme` json NOT NULL COMMENT '页面定义',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_by` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `page_uidx` (`module`,`mkey`,`name`) COMMENT '唯一标识'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='建模页面绑定';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flowable_model_page`
--

LOCK TABLES `flowable_model_page` WRITE;
/*!40000 ALTER TABLE `flowable_model_page` DISABLE KEYS */;
INSERT INTO `flowable_model_page` VALUES (1,'WORKFLOW','process_1703143742179','开始页面','{\"mode\": \"design\", \"size\": \"default\", \"style\": \"\", \"children\": [{\"id\": \"number-input_kwfihqh98\", \"key\": 1, \"attrs\": {\"max\": 100, \"min\": 1, \"style\": \"width: 100%\", \"clearable\": false, \"placeholder\": \"输入数字\"}, \"category\": \"form-item\", \"children\": [], \"component\": \"number-input\", \"formItemAttrs\": {\"label\": \"数字\"}}, {\"id\": \"text-input_yikedv67bi\", \"key\": 1, \"attrs\": {\"style\": \"width: 100%\", \"autofocus\": false, \"clearable\": false, \"placeholder\": \"输入字符串\", \"showWordLimit\": false, \"validateEvent\": false}, \"category\": \"form-item\", \"children\": [], \"component\": \"text-input\", \"formItemAttrs\": {\"label\": \"文本框\"}}, {\"id\": \"date-picker_1g31qgv0nhy\", \"key\": 1, \"attrs\": {\"style\": \"width: 100%\", \"format\": \"YYYY-MM-DD\", \"dateType\": \"date\", \"placeholder\": \"输入日期\", \"valueFormat\": \"YYYY-MM-DD\"}, \"category\": \"form-item\", \"children\": [], \"component\": \"date-picker\", \"formItemAttrs\": {\"label\": \"日期\"}}], \"labelWidth\": \"120px\", \"labelPosition\": \"auto\"}','admin','2023-12-26 10:56:13','admin','2023-12-26 10:55:25');
/*!40000 ALTER TABLE `flowable_model_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flowable_process_operate_trigger`
--

DROP TABLE IF EXISTS `flowable_process_operate_trigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flowable_process_operate_trigger` (
  `ID` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `BUSINESS_CODE` varchar(255) NOT NULL COMMENT '业务编码',
  `PROCESS_DEFINE_ID` varchar(64) DEFAULT NULL COMMENT '流程定义ID',
  `INTERFACE_CODE` varchar(100) DEFAULT NULL COMMENT '接口编码',
  `MQ_TYPE` char(2) DEFAULT NULL COMMENT 'MQ类型',
  `TOPIC` varchar(255) DEFAULT NULL COMMENT '主题',
  `TAG` varchar(255) DEFAULT NULL COMMENT '标签',
  `TRIGER_BEFORE_FLAG` char(2) DEFAULT NULL COMMENT '触发前标志',
  `CREATE_BY` varchar(64) DEFAULT NULL COMMENT '创建者',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` varchar(64) DEFAULT NULL COMMENT '更新者',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='流程操作触发';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flowable_process_operate_trigger`
--

LOCK TABLES `flowable_process_operate_trigger` WRITE;
/*!40000 ALTER TABLE `flowable_process_operate_trigger` DISABLE KEYS */;
/*!40000 ALTER TABLE `flowable_process_operate_trigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flw_channel_definition`
--

DROP TABLE IF EXISTS `flw_channel_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flw_channel_definition` (
  `ID_` varchar(255) NOT NULL,
  `NAME_` varchar(255) DEFAULT NULL,
  `VERSION_` int DEFAULT NULL,
  `KEY_` varchar(255) DEFAULT NULL,
  `CATEGORY_` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) DEFAULT NULL,
  `CREATE_TIME_` datetime(3) DEFAULT NULL,
  `TENANT_ID_` varchar(255) DEFAULT NULL,
  `RESOURCE_NAME_` varchar(255) DEFAULT NULL,
  `DESCRIPTION_` varchar(255) DEFAULT NULL,
  `TYPE_` varchar(255) DEFAULT NULL,
  `IMPLEMENTATION_` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `ACT_IDX_CHANNEL_DEF_UNIQ` (`KEY_`,`VERSION_`,`TENANT_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flw_channel_definition`
--

LOCK TABLES `flw_channel_definition` WRITE;
/*!40000 ALTER TABLE `flw_channel_definition` DISABLE KEYS */;
/*!40000 ALTER TABLE `flw_channel_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flw_ev_databasechangelog`
--

DROP TABLE IF EXISTS `flw_ev_databasechangelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flw_ev_databasechangelog` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flw_ev_databasechangelog`
--

LOCK TABLES `flw_ev_databasechangelog` WRITE;
/*!40000 ALTER TABLE `flw_ev_databasechangelog` DISABLE KEYS */;
INSERT INTO `flw_ev_databasechangelog` VALUES ('1','flowable','org/flowable/eventregistry/db/liquibase/flowable-eventregistry-db-changelog.xml','2023-12-18 10:00:17',1,'EXECUTED','8:1b0c48c9cf7945be799d868a2626d687','createTable tableName=FLW_EVENT_DEPLOYMENT; createTable tableName=FLW_EVENT_RESOURCE; createTable tableName=FLW_EVENT_DEFINITION; createIndex indexName=ACT_IDX_EVENT_DEF_UNIQ, tableName=FLW_EVENT_DEFINITION; createTable tableName=FLW_CHANNEL_DEFIN...','',NULL,'4.9.1',NULL,NULL,'2864817596'),('2','flowable','org/flowable/eventregistry/db/liquibase/flowable-eventregistry-db-changelog.xml','2023-12-18 10:00:17',2,'EXECUTED','8:0ea825feb8e470558f0b5754352b9cda','addColumn tableName=FLW_CHANNEL_DEFINITION; addColumn tableName=FLW_CHANNEL_DEFINITION','',NULL,'4.9.1',NULL,NULL,'2864817596'),('3','flowable','org/flowable/eventregistry/db/liquibase/flowable-eventregistry-db-changelog.xml','2023-12-18 10:00:17',3,'EXECUTED','8:3c2bb293350b5cbe6504331980c9dcee','customChange','',NULL,'4.9.1',NULL,NULL,'2864817596');
/*!40000 ALTER TABLE `flw_ev_databasechangelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flw_ev_databasechangeloglock`
--

DROP TABLE IF EXISTS `flw_ev_databasechangeloglock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flw_ev_databasechangeloglock` (
  `ID` int NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flw_ev_databasechangeloglock`
--

LOCK TABLES `flw_ev_databasechangeloglock` WRITE;
/*!40000 ALTER TABLE `flw_ev_databasechangeloglock` DISABLE KEYS */;
INSERT INTO `flw_ev_databasechangeloglock` VALUES (1,_binary '\0',NULL,NULL);
/*!40000 ALTER TABLE `flw_ev_databasechangeloglock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flw_event_definition`
--

DROP TABLE IF EXISTS `flw_event_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flw_event_definition` (
  `ID_` varchar(255) NOT NULL,
  `NAME_` varchar(255) DEFAULT NULL,
  `VERSION_` int DEFAULT NULL,
  `KEY_` varchar(255) DEFAULT NULL,
  `CATEGORY_` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) DEFAULT NULL,
  `TENANT_ID_` varchar(255) DEFAULT NULL,
  `RESOURCE_NAME_` varchar(255) DEFAULT NULL,
  `DESCRIPTION_` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `ACT_IDX_EVENT_DEF_UNIQ` (`KEY_`,`VERSION_`,`TENANT_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flw_event_definition`
--

LOCK TABLES `flw_event_definition` WRITE;
/*!40000 ALTER TABLE `flw_event_definition` DISABLE KEYS */;
/*!40000 ALTER TABLE `flw_event_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flw_event_deployment`
--

DROP TABLE IF EXISTS `flw_event_deployment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flw_event_deployment` (
  `ID_` varchar(255) NOT NULL,
  `NAME_` varchar(255) DEFAULT NULL,
  `CATEGORY_` varchar(255) DEFAULT NULL,
  `DEPLOY_TIME_` datetime(3) DEFAULT NULL,
  `TENANT_ID_` varchar(255) DEFAULT NULL,
  `PARENT_DEPLOYMENT_ID_` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flw_event_deployment`
--

LOCK TABLES `flw_event_deployment` WRITE;
/*!40000 ALTER TABLE `flw_event_deployment` DISABLE KEYS */;
/*!40000 ALTER TABLE `flw_event_deployment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flw_event_resource`
--

DROP TABLE IF EXISTS `flw_event_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flw_event_resource` (
  `ID_` varchar(255) NOT NULL,
  `NAME_` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) DEFAULT NULL,
  `RESOURCE_BYTES_` longblob,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flw_event_resource`
--

LOCK TABLES `flw_event_resource` WRITE;
/*!40000 ALTER TABLE `flw_event_resource` DISABLE KEYS */;
/*!40000 ALTER TABLE `flw_event_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flw_ru_batch`
--

DROP TABLE IF EXISTS `flw_ru_batch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flw_ru_batch` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `TYPE_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `SEARCH_KEY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SEARCH_KEY2_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NOT NULL,
  `COMPLETE_TIME_` datetime(3) DEFAULT NULL,
  `STATUS_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `BATCH_DOC_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flw_ru_batch`
--

LOCK TABLES `flw_ru_batch` WRITE;
/*!40000 ALTER TABLE `flw_ru_batch` DISABLE KEYS */;
/*!40000 ALTER TABLE `flw_ru_batch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flw_ru_batch_part`
--

DROP TABLE IF EXISTS `flw_ru_batch_part`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flw_ru_batch_part` (
  `ID_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `BATCH_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `TYPE_` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `SCOPE_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `SEARCH_KEY_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `SEARCH_KEY2_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NOT NULL,
  `COMPLETE_TIME_` datetime(3) DEFAULT NULL,
  `STATUS_` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `RESULT_DOC_ID_` varchar(64) COLLATE utf8mb3_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `FLW_IDX_BATCH_PART` (`BATCH_ID_`),
  CONSTRAINT `FLW_FK_BATCH_PART_PARENT` FOREIGN KEY (`BATCH_ID_`) REFERENCES `flw_ru_batch` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flw_ru_batch_part`
--

LOCK TABLES `flw_ru_batch_part` WRITE;
/*!40000 ALTER TABLE `flw_ru_batch_part` DISABLE KEYS */;
/*!40000 ALTER TABLE `flw_ru_batch_part` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'flowable'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-27 16:49:59
