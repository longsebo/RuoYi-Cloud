--作流增加企业编码,应用编码
ALTER TABLE flowable_field_def
    ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE flowable_field_ref
    ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE flowable_model_page
    ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';
