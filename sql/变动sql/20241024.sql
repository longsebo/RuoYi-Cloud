--工作流增加企业编码,应用编码
ALTER TABLE flowable_process_operate_trigger
    ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE flowable_category
    ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';
