

drop table if exists sys_component;

drop table if exists sys_component_group;




drop table if exists sys_function;




drop table if exists sys_function_parameters;


drop table if exists tenant_component;

drop table if exists tenant_component_group;




drop table if exists tenant_function;




drop table if exists tenant_function_parameters;

/*==============================================================*/
/* Table: sys_component                                         */
/*==============================================================*/
create table sys_component
(
    component_id         bigint not null auto_increment  comment '',
    group_code           varchar(10)  comment '',
    class_name           varchar(10)  comment '',
    component_name       varchar(20)  comment '',
    component_icon_url   VARCHAR(255)  comment '',
    remark               VARCHAR(255)  comment '',
    status               CHAR(2)  comment '启用禁用状态：00--禁用 01--启用',
    sort_number          integer  comment '',
    create_by            varchar(64) default ''  comment '创建者',
    create_time          datetime default NULL  comment '创建时间',
    update_by            varchar(64) default ''  comment '更新者',
    update_time          datetime default NULL  comment '更新时间',
    primary key (component_id)
)
;

alter table sys_component comment '系统级别组件，最基本最底层的组件。一般不会变化。除非系统版本升级
可以由系统级别组件，组装成自定义组件';

/*==============================================================*/
/* Index: index_2                                               */
/*==============================================================*/
create unique index index_2 on sys_component
    (
     class_name
        );

/*==============================================================*/
/* Table: sys_component_group                                   */
/*==============================================================*/
create table sys_component_group
(
    group_id             bigint not null auto_increment  comment '',
    group_code           varchar(10)  comment '',
    group_name           varchar(20)  comment '',
    group_icon_url       VARCHAR(255)  comment '',
    remark               VARCHAR(255)  comment '',
    status               CHAR(2)  comment '启用禁用状态：00--禁用 01--启用',
    sort_number          integer  comment '',
    create_by            varchar(64) default ''  comment '创建者',
    create_time          datetime default NULL  comment '创建时间',
    update_by            varchar(64) default ''  comment '更新者',
    update_time          datetime default NULL  comment '更新时间',
    primary key (group_id)
)
;

alter table sys_component_group comment '系统级别分组，最基本的组。一般不会变化。除非系统版本升级';

/*==============================================================*/
/* Index: index_1                                               */
/*==============================================================*/
create unique index index_1 on sys_component_group
    (
     group_code
        );

/*==============================================================*/
/* Table: sys_function                                          */
/*==============================================================*/
create table sys_function
(
    function_id          bigint not null auto_increment  comment '',
    component_id         bigint  comment '',
    class_name           varchar(10)  comment '',
    function_code        varchar(10)  comment '',
    function_name        VARCHAR(255)  comment '',
    function_desc        VARCHAR(255)  comment '',
    status               CHAR(2)  comment '启用禁用状态：00--禁用 01--启用',
    sort_number          integer  comment '',
    is_returnvalue       CHAR(1)  comment '是否有返回值：N---否，Y---是',
    return_type          VARCHAR(255)  comment '',
    create_by            varchar(64) default ''  comment '创建者',
    create_time          datetime default NULL  comment '创建时间',
    update_by            varchar(64) default ''  comment '更新者',
    update_time          datetime default NULL  comment '更新时间',
    primary key (function_id)
)
;

alter table sys_function comment '对外宣传为功能点，即开发人员的函数';

/*==============================================================*/
/* Index: index_1                                               */
/*==============================================================*/
create unique index index_1 on sys_function
    (
     function_code
        );

/*==============================================================*/
/* Table: sys_function_parameters                               */
/*==============================================================*/
create table sys_function_parameters
(
    parameter_id         bigint not null auto_increment  comment '',
    function_code        varchar(10)  comment '',
    parameter_code       varchar(10)  comment '',
    parameter_name       VARCHAR(255)  comment '',
    parameter_type       VARCHAR(255)  comment '',
    sort_number          integer  comment '',
    create_by            varchar(64) default ''  comment '创建者',
    create_time          datetime default NULL  comment '创建时间',
    update_by            varchar(64) default ''  comment '更新者',
    update_time          datetime default NULL  comment '更新时间',
    primary key (parameter_id)
)
;

/*==============================================================*/
/* Index: index_1                                               */
/*==============================================================*/
create unique index index_1 on sys_function_parameters
    (
     parameter_code
        );

/*==============================================================*/
/* Table: tenant_component                                      */
/*==============================================================*/
create table tenant_component
(
    enterprise_code      varchar(10) not null  comment '',
    component_id         bigint not null auto_increment  comment '',
    group_code           varchar(10)  comment '',
    class_name           varchar(10)  comment '',
    component_name       varchar(20)  comment '',
    component_icon_url   VARCHAR(255)  comment '',
    remark               VARCHAR(255)  comment '',
    status               CHAR(2)  comment '启用禁用状态：00--禁用 01--启用',
    sort_number          integer  comment '',
    create_by            varchar(64) default ''  comment '创建者',
    create_time          datetime default NULL  comment '创建时间',
    update_by            varchar(64) default ''  comment '更新者',
    update_time          datetime default NULL  comment '更新时间',
    primary key (component_id)
)
;

alter table tenant_component comment '租户级别组件，是各个租户自定义组件';

/*==============================================================*/
/* Index: index_1                                               */
/*==============================================================*/
create unique index index_1 on tenant_component
    (
     class_name
        );

/*==============================================================*/
/* Table: tenant_component_group                                */
/*==============================================================*/
create table tenant_component_group
(
    group_id             bigint not null auto_increment  comment '',
    enterprise_code      varchar(10) not null  comment '',
    group_code           varchar(10)  comment '',
    group_name           varchar(20)  comment '',
    group_icon_url       VARCHAR(255)  comment '',
    remark               VARCHAR(255)  comment '',
    status               CHAR(2)  comment '启用禁用状态：00--禁用 01--启用',
    sort_number          integer  comment '',
    create_by            varchar(64) default ''  comment '创建者',
    create_time          datetime default NULL  comment '创建时间',
    update_by            varchar(64) default ''  comment '更新者',
    update_time          datetime default NULL  comment '更新时间',
    primary key (group_id)
)
;

alter table tenant_component_group comment '租户级别分组，由各自租户维护';

/*==============================================================*/
/* Index: index_1                                               */
/*==============================================================*/
create unique index index_1 on tenant_component_group
    (
     group_code
        );

/*==============================================================*/
/* Table: tenant_function                                       */
/*==============================================================*/
create table tenant_function
(
    function_id          bigint not null auto_increment  comment '',
    enterprise_code      varchar(10) not null  comment '',
    class_name           varchar(10)  comment '',
    function_code        varchar(10)  comment '',
    function_name        VARCHAR(255)  comment '',
    function_desc        VARCHAR(255)  comment '',
    groovy_source        mediumtext  comment '',
    design_script        mediumtext  comment '',
    full_groovy_source   mediumtext  comment '这个字段意思是所有依赖的源码都在这里。比如A->B 则B的类及方法在这里，加上A实现也在',
    status               CHAR(2)  comment '启用禁用状态：00--禁用 01--启用',
    is_returnvalue       CHAR(1)  comment '是否有返回值：N---否，Y---是',
    return_type          VARCHAR(255)  comment '',
    sort_number          integer  comment '',
    create_by            varchar(64) default ''  comment '创建者',
    create_time          datetime default NULL  comment '创建时间',
    update_by            varchar(64) default ''  comment '更新者',
    update_time          datetime default NULL  comment '更新时间',
    primary key (function_id)
)
;

alter table tenant_function comment '对外宣传为功能点，即开发人员的函数';

/*==============================================================*/
/* Index: index_1                                               */
/*==============================================================*/
create unique index index_1 on tenant_function
    (
     function_code
        );

/*==============================================================*/
/* Table: tenant_function_parameters                            */
/*==============================================================*/
create table tenant_function_parameters
(
    parameter_id         bigint not null auto_increment  comment '',
    enterprise_code      varchar(10) not null  comment '',
    function_code        varchar(10)  comment '',
    parameter_code       varchar(10)  comment '',
    parameter_name       VARCHAR(255)  comment '',
    parameter_type       VARCHAR(255)  comment '',
    sort_number          integer  comment '',
    create_by            varchar(64) default ''  comment '创建者',
    create_time          datetime default NULL  comment '创建时间',
    update_by            varchar(64) default ''  comment '更新者',
    update_time          datetime default NULL  comment '更新时间',
    primary key (parameter_id)
)
;

/*==============================================================*/
/* Index: index_1                                               */
/*==============================================================*/
create unique index index_1 on tenant_function_parameters
    (
     parameter_code
        );
