ALTER TABLE `ry-cloud`.sys_dict_type DROP KEY dict_type;
ALTER TABLE `ry-cloud`.sys_dict_type ADD CONSTRAINT dict_type_enterprise_code UNIQUE KEY (enterprise_code,dict_type);
