create table sys_tenant
(
   id                   bigint not null auto_increment  comment '',
   enterprise_name      VARCHAR(255) not null  comment '企业名称',
   enterprise_code      varchar(10) not null  comment '企业编码',
   industry             VARCHAR(255) not null  comment '行业',
   enterprise_employees_num VARCHAR(255) not null  comment '企业人数',
   create_by            varchar(64) default ''  comment '创建者',
   create_time          datetime default NULL  comment '创建时间',
   update_by            varchar(64) default ''  comment '更新者',
   update_time          datetime default NULL  comment '更新时间',
   primary key (id)
) comment '租户';


ALTER TABLE sys_config
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码';

ALTER TABLE sys_dept
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码';

ALTER TABLE sys_dict_data
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码';

ALTER TABLE sys_dict_type
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码';

ALTER TABLE sys_job
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE sys_job_log
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE sys_logininfor
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE sys_menu
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE sys_notice
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE sys_oper_log
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE sys_post
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE sys_role
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码';

ALTER TABLE sys_role_dept
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码';

ALTER TABLE sys_role_menu
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE sys_user
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码';

ALTER TABLE sys_user_post
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码';

ALTER TABLE sys_user_role
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码';

ALTER TABLE ext_business_app_bus
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_interface_search_personalized
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_page_parameter_rela
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_page_parameter
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_page_business_function_rela
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_business_mq_conf
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_model_option_value
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_model_field_ref
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_model_option_type
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';


ALTER TABLE ext_business_function_listening
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码';

ALTER TABLE ext_page_interface
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_page
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_model_business_function_rela
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_interface_business_function_rela
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_interface_return_value_rela
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_interface_parameter_rela
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_interface_return_value
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_interface_parameter
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_inteface_url
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_interface
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_model_field
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_model_def
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_model_datasource
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

ALTER TABLE ext_business_function
ADD COLUMN  `enterprise_code` varchar(10) not NULL COMMENT '企业编码',
ADD COLUMN  `application_code`  varchar(100)  COMMENT '应用编码';

