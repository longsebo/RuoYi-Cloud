package com.ruoyi.common.core.constant;

/**
 * 权限相关通用常量
 *
 * @author ruoyi
 */
public class SecurityConstants
{
    /**
     * 用户ID字段
     */
    public static final String DETAILS_USER_ID = "user_id";

    /**
     * 用户名字段
     */
    public static final String DETAILS_USERNAME = "username";

    /**
     * 授权信息字段
     */
    public static final String AUTHORIZATION_HEADER = "authorization";

    /**
     * 请求来源
     */
    public static final String FROM_SOURCE = "from-source";

    /**
     * 内部请求
     */
    public static final String INNER = "inner";

    /**
     * 用户标识
     */
    public static final String USER_KEY = "user_key";

    /**
     * 登录用户
     */
    public static final String LOGIN_USER = "login_user";

    /**
     * 角色权限
     */
    public static final String ROLE_PERMISSION = "role_permission";
    /**
     * 租户编码
     */
    public static final String ENTERPRISE_CODE = "Enterprise_code";
    /**
     * 应用编码
     */
    public static final String APPLICATION_CODE = "Application_code";
    /**
     * 忽略的应用编码
     */
    public static final String IGNORE_APPLICATION_CODE = "-";
    /**
     * 租户管理员用户类型
     */
    public static final String TENANT_ADMIN_USER_TYPE = "98";
}
