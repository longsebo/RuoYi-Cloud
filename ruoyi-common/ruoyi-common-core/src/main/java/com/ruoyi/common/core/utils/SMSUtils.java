/**
 *
 */
package com.ruoyi.common.core.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;

/**
 * @author lsb
 * 	短信工具类
 */
@Slf4j
public class SMSUtils {
	/*
	 * 发送方法
	 */
	public static int sendSMS(String cropId,String pwd,String smsgatewayDomainUrl,String Mobile,String Content,String send_time) throws Exception {
		URL url = null;
		String send_content=URLEncoder.encode(Content.replaceAll("<br/>", " "), "GBK");//发送内容
		url = new URL("https://"+smsgatewayDomainUrl+"/BatchSend2.aspx?CorpID="+cropId+"&Pwd="+pwd+"&Mobile="+Mobile+"&Content="+send_content+"&Cell=&SendTime="+send_time);
		BufferedReader in = null;
		int inputLine = 0;
		try {
			System.out.println("开始发送短信手机号码为 ："+Mobile+",内容为:"+Content);
			log.info("开始发送短信手机号码为 ："+Mobile+",内容为:"+Content);
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			inputLine = Integer.valueOf(in.readLine());

		} catch (Exception e) {
			System.out.println("网络异常,发送短信失败！");
			log.error("网络异常,发送短信失败！", e);
			inputLine=-2;
		}
		System.out.println("结束发送短信返回值：  "+inputLine);
		log.info("结束发送短信返回值：  "+inputLine);
		if(inputLine<=0)
			throw new Exception("发送短信失败!");
		return inputLine;
	}

}
