package com.ruoyi.common.core.utils;

import java.util.Random;

/**
 * 生成短信验证码随机数
 */
public class SmsCodeGenerator {
    public static String generateSmsCode() {
        Random random = new Random();
        int code = 1000 + random.nextInt(9000); // 生成4位随机数
        return String.valueOf(code);
    }
}
