package com.ruoyi.common.core.utils;

import java.util.Random;

/**
 * 获取唯一标识
 */
public class UniqueIdGenerator {
    private static final String CHARACTERS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    // 字母常量
    private static final String CHARACTERS_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    public static String getUniqueId(int length){

        // 创建 Random 对象
        Random random = new Random();
        StringBuffer returnValue = new StringBuffer();
        for(int i=0;i<length;i++) {
            // 生成100以内的随机整数
            int randomNumber = random.nextInt(CHARACTERS.length());
            returnValue.append(CHARACTERS.charAt(randomNumber));
        }
        return returnValue.toString();
    }

    /**
     * 获取唯一类名
     * @param length
     * @return
     */
    public static String getUniqueClassName(int length){

        // 创建 Random 对象
        Random random = new Random();
        StringBuffer returnValue = new StringBuffer();
        for(int i=0;i<length;i++) {
            // 生成100以内的随机整数
            int randomNumber = random.nextInt(CHARACTERS_LETTERS.length());
            returnValue.append(CHARACTERS_LETTERS.charAt(randomNumber));
        }
        return returnValue.toString();
    }
}
