package com.ruoyi.common.security.utils;

import org.junit.Test;

public class TestSecurityUtils {
    @Test
    public void testPasswordEncode()
    {
        String encode = SecurityUtils.encryptPassword("1234567");
//        String encode = "$2a$10$LSfgFBY1/NjtxcIXcFOpkOU75PPPkQYzBuFSIJLo6zSC74DqJpYei";

        System.out.println(encode);
        System.out.println(SecurityUtils.matchesPassword("1234567",encode));
    }
}
