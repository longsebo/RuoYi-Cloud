package com.ruoyi.common.security.aspect;


import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.reflect.ReflectUtils;
import com.ruoyi.common.security.annotation.*;
import com.ruoyi.common.security.utils.SecurityUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import com.ruoyi.common.security.annotation.DefaultCreateProperty;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

/**
 * 基于 Spring Aop 的缺省属性注解
 *
 * @author longsebo
 */
@Aspect
@Component
public class PreDefaultPropertyAspect
{
    /**
     * 构建
     */
    public PreDefaultPropertyAspect()
    {
    }



    /**
     * 环绕切入
     *
     * @param joinPoint 切面对象
     * @return 底层方法执行后的返回值
     * @throws Throwable 底层方法抛出的异常
     */
    @Around(value="execution(public * *(..))")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable
    {
        // 注解鉴权
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        assignmentDefaultPropertyVal(signature.getMethod(), joinPoint.getArgs());
        try {
            return joinPoint.proceed();
        } catch (Throwable e) {
            throw e;
        }
    }
    private void assignmentDefaultPropertyVal(Method method, Object[] args) {
        Parameter[] parameters = method.getParameters();
        if (parameters != null && parameters.length > 0) {
            for (int i = 0; i < parameters.length; i++) {
                Parameter parameter = parameters[i];
                Object argValue = args[i];

                // 检查并处理 DefaultCreateProperty 注解
                handleAnnotation(DefaultCreateProperty.class, parameter, argValue);

                // 检查并处理 DefaultUpdateProperty 注解
                handleAnnotation(DefaultUpdateProperty.class, parameter, argValue);

                // 检查并处理 EnterpriseAndApplicationCodeProperty 注解
                handleAnnotation(EnterpriseAndApplicationCodeProperty.class, parameter, argValue);
            }
        }
    }

    private void handleAnnotation(Class<? extends Annotation> annotationClass, Parameter parameter, Object argValue) {
        if (parameter.isAnnotationPresent(annotationClass)) {
            // 这里你可以根据不同的注解类型来执行相应的逻辑
            if (annotationClass == DefaultCreateProperty.class) {
                ReflectUtils.setFieldValue(argValue, "createBy", SecurityUtils.getUsername());
                ReflectUtils.setFieldValue(argValue, "createTime", DateUtils.getNowDate());
            } else if (annotationClass == DefaultUpdateProperty.class) {
                ReflectUtils.setFieldValue(argValue, "updateBy", SecurityUtils.getUsername());
                ReflectUtils.setFieldValue(argValue, "updateTime", DateUtils.getNowDate());
            } else if (annotationClass == EnterpriseAndApplicationCodeProperty.class) {
                ReflectUtils.setFieldValue(argValue, "enterpriseCode", SecurityUtils.getEnterpriseCode());
                if (ReflectUtils.getAccessibleField(argValue, "applicationCode") != null) {
                    ReflectUtils.setFieldValue(argValue, "applicationCode", SecurityUtils.getApplicationCode());
                }
            }
        }
    }
//    /**
//     * 赋值缺省值
//     * @param method
//     */
//    private void assignmentDefaultPropertyVal(Method method) {
//        //获取方法所有参数，循环是否有合适注解
//        Parameter[] parametres = method.getParameters();
//        if(parametres!=null && parametres.length>0){
//            for(Parameter p:parametres){
//                DefaultCreateProperty defaultCreateProperty = p.getAnnotation(DefaultCreateProperty.class);
//                if(defaultCreateProperty!=null){
//                    ReflectUtils.setFieldValue(p,"createBy", SecurityUtils.getUsername());
//                    ReflectUtils.setFieldValue(p,"createTime",DateUtils.getNowDate());
//                }
//                DefaultUpdateProperty defaultUpdateProperty = p.getAnnotation(DefaultUpdateProperty.class);
//                if(defaultUpdateProperty!=null){
//                    ReflectUtils.setFieldValue(p,"updateBy", SecurityUtils.getUsername());
//                    ReflectUtils.setFieldValue(p,"updateTime",DateUtils.getNowDate());
//                }
//                EnterpriseAndApplicationCodeProperty enterpriseAndApplicationCodeProperty = p.getAnnotation(EnterpriseAndApplicationCodeProperty.class);
//                if(enterpriseAndApplicationCodeProperty!=null){
//                    ReflectUtils.setFieldValue(p,"enterpriseCode", SecurityUtils.getEnterpriseCode());
//                    if(ReflectUtils.getAccessibleField(p,"applicationCode")!=null) {
//                        ReflectUtils.setFieldValue(p, "applicationCode", SecurityUtils.getApplicationCode());
//                    }
//                }
//            }
//        }
//    }


}
