package com.ruoyi.flowable.enums;

/**
 * 缓存key
 */
public enum CacheType {
    FLOWCATEGORY("flowcategory","流程分类");
    public String getCode() {
        return code;
    }

    public String getRemark() {
        return remark;
    }

    /**
     * 编码
     */
    private String code;
    /**
     * 说明
     */
    private String remark;
    CacheType(String code,String remark){
        this.code = code;
        this.remark = remark;
    }
}
