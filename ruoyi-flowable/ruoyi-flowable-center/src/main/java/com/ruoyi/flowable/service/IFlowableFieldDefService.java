package com.ruoyi.flowable.service;

import java.util.List;
import com.ruoyi.flowable.domain.FlowableFieldDef;
import com.ruoyi.flowable.domain.FlowableFieldRef;
import com.ruoyi.flowable.domain.FlowableFieldSearch;

/**
 * 流程字段定义Service接口
 *
 * @author ruoyi
 * @date 2023-12-26
 */
public interface IFlowableFieldDefService
{
    /**
     * 查询流程字段定义
     *
     * @param id 流程字段定义主键
     * @return 流程字段定义
     */
    public FlowableFieldDef selectFlowableFieldDefById(String id);

    /**
     * 查询流程字段定义列表
     *
     * @param flowableFieldDef 流程字段定义
     * @return 流程字段定义集合
     */
    public List<FlowableFieldDef> selectFlowableFieldDefList(FlowableFieldDef flowableFieldDef);

    /**
     * 新增流程字段定义
     *
     * @param flowableFieldDef 流程字段定义
     * @return 结果
     */
    public int insertFlowableFieldDef(FlowableFieldDef flowableFieldDef);

    /**
     * 修改流程字段定义
     *
     * @param flowableFieldDef 流程字段定义
     * @return 结果
     */
    public int updateFlowableFieldDef(FlowableFieldDef flowableFieldDef);

    /**
     * 批量删除流程字段定义
     *
     * @param ids 需要删除的流程字段定义主键集合
     * @return 结果
     */
    public int deleteFlowableFieldDefByIds(String[] ids);

    /**
     * 删除流程字段定义信息
     *
     * @param id 流程字段定义主键
     * @return 结果
     */
    public int deleteFlowableFieldDefById(String id);

    /**
     * 查询流程字段引用关系列表(不翻页,关联字段定义表查询)
     * @param flowableFieldSearch
     * @return
     */
    List<FlowableFieldDef> listCombination(FlowableFieldSearch flowableFieldSearch);
}
