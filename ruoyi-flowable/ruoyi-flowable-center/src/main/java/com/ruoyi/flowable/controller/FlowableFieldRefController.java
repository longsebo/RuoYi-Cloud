package com.ruoyi.flowable.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import com.ruoyi.flowable.domain.FlowableFieldDef;
import com.ruoyi.flowable.domain.FlowableFieldSearch;
import com.ruoyi.flowable.service.IFlowableFieldDefService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.flowable.domain.FlowableFieldRef;
import com.ruoyi.flowable.service.IFlowableFieldRefService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 流程字段引用关系Controller
 *
 * @author ruoyi
 * @date 2023-12-26
 */
@RestController
@RequestMapping("/ref")
public class FlowableFieldRefController extends BaseController
{
    @Autowired
    private IFlowableFieldRefService flowableFieldRefService;
    @Autowired
    private IFlowableFieldDefService flowableFieldDefService;
    /**
     * 查询流程字段引用关系列表
     */
    @RequiresPermissions("flowable:ref:list")
    @GetMapping("/list")
    public TableDataInfo list(FlowableFieldRef flowableFieldRef)
    {
        startPage();
        List<FlowableFieldRef> list = flowableFieldRefService.selectFlowableFieldRefList(flowableFieldRef);
        return getDataTable(list);
    }

    /**
     * 导出流程字段引用关系列表
     */
    @RequiresPermissions("flowable:ref:export")
    @Log(title = "流程字段引用关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FlowableFieldRef flowableFieldRef)
    {
        List<FlowableFieldRef> list = flowableFieldRefService.selectFlowableFieldRefList(flowableFieldRef);
        ExcelUtil<FlowableFieldRef> util = new ExcelUtil<FlowableFieldRef>(FlowableFieldRef.class);
        util.exportExcel(response, list, "流程字段引用关系数据");
    }

    /**
     * 获取流程字段引用关系详细信息
     */
    @RequiresPermissions("flowable:ref:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(flowableFieldRefService.selectFlowableFieldRefById(id));
    }

    /**
     * 新增流程字段引用关系
     */
    @RequiresPermissions("flowable:ref:add")
    @Log(title = "流程字段引用关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty FlowableFieldRef flowableFieldRef)
    {
        return toAjax(flowableFieldRefService.insertFlowableFieldRef(flowableFieldRef));
    }

    /**
     * 修改流程字段引用关系
     */
    @RequiresPermissions("flowable:ref:edit")
    @Log(title = "流程字段引用关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty FlowableFieldRef flowableFieldRef)
    {
        return toAjax(flowableFieldRefService.updateFlowableFieldRef(flowableFieldRef));
    }

    /**
     * 删除流程字段引用关系
     */
    @RequiresPermissions("flowable:ref:remove")
    @Log(title = "流程字段引用关系", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(flowableFieldRefService.deleteFlowableFieldRefByIds(ids));
    }
    /**
     * 查询流程字段引用关系列表(不翻页,关联字段定义表查询)
     */
    @RequiresPermissions("flowable:ref:listCombination")
    @GetMapping("/listCombination")
    public AjaxResult listCombination(FlowableFieldSearch flowableFieldSearch)
    {
        List<FlowableFieldDef> list = flowableFieldDefService.listCombination(flowableFieldSearch);
        return success(list);
    }
}
