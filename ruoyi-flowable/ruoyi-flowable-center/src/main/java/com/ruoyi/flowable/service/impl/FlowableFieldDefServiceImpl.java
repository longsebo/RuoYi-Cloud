package com.ruoyi.flowable.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.flowable.domain.FlowableFieldSearch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.flowable.mapper.FlowableFieldDefMapper;
import com.ruoyi.flowable.domain.FlowableFieldDef;
import com.ruoyi.flowable.service.IFlowableFieldDefService;

/**
 * 流程字段定义Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-26
 */
@Service
public class FlowableFieldDefServiceImpl implements IFlowableFieldDefService
{
    @Autowired
    private FlowableFieldDefMapper flowableFieldDefMapper;

    /**
     * 查询流程字段定义
     *
     * @param id 流程字段定义主键
     * @return 流程字段定义
     */
    @Override
    public FlowableFieldDef selectFlowableFieldDefById(String id)
    {
        return flowableFieldDefMapper.selectFlowableFieldDefById(id);
    }

    /**
     * 查询流程字段定义列表
     *
     * @param flowableFieldDef 流程字段定义
     * @return 流程字段定义
     */
    @Override
    public List<FlowableFieldDef> selectFlowableFieldDefList(FlowableFieldDef flowableFieldDef)
    {
        return flowableFieldDefMapper.selectFlowableFieldDefList(flowableFieldDef);
    }

    /**
     * 新增流程字段定义
     *
     * @param flowableFieldDef 流程字段定义
     * @return 结果
     */
    @Override
    public int insertFlowableFieldDef(FlowableFieldDef flowableFieldDef)
    {
        flowableFieldDef.setCreateTime(DateUtils.getNowDate());
        return flowableFieldDefMapper.insertFlowableFieldDef(flowableFieldDef);
    }

    /**
     * 修改流程字段定义
     *
     * @param flowableFieldDef 流程字段定义
     * @return 结果
     */
    @Override
    public int updateFlowableFieldDef(FlowableFieldDef flowableFieldDef)
    {
        flowableFieldDef.setUpdateTime(DateUtils.getNowDate());
        return flowableFieldDefMapper.updateFlowableFieldDef(flowableFieldDef);
    }

    /**
     * 批量删除流程字段定义
     *
     * @param ids 需要删除的流程字段定义主键
     * @return 结果
     */
    @Override
    public int deleteFlowableFieldDefByIds(String[] ids)
    {
        return flowableFieldDefMapper.deleteFlowableFieldDefByIds(ids);
    }

    /**
     * 删除流程字段定义信息
     *
     * @param id 流程字段定义主键
     * @return 结果
     */
    @Override
    public int deleteFlowableFieldDefById(String id)
    {
        return flowableFieldDefMapper.deleteFlowableFieldDefById(id);
    }

    /**
     * 查询流程字段引用关系列表(不翻页,关联字段定义表查询)
     *
     * @param flowableFieldSearch
     * @return
     */
    @Override
    public List<FlowableFieldDef> listCombination(FlowableFieldSearch flowableFieldSearch) {
        return flowableFieldDefMapper.listCombination(flowableFieldSearch);
    }
}
