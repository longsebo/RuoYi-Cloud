package com.ruoyi.flowable.domain;

import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

/**
 * 为了方便查询，提供一个组合类
 */
@Data
public class FlowableFieldSearch extends FlowableFieldRef {
    /** 字段范围 */
    private String scope;
}
