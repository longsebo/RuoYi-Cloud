package com.ruoyi.flowable.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.flowable.domain.FlowableFieldDef;
import com.ruoyi.flowable.service.IFlowableFieldDefService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 流程字段定义Controller
 *
 * @author ruoyi
 * @date 2023-12-26
 */
@RestController
@RequestMapping("/def")
public class FlowableFieldDefController extends BaseController
{
    @Autowired
    private IFlowableFieldDefService flowableFieldDefService;

    /**
     * 查询流程字段定义列表
     */
    @RequiresPermissions("flowable:def:list")
    @GetMapping("/list")
    public TableDataInfo list(FlowableFieldDef flowableFieldDef)
    {
        startPage();
        List<FlowableFieldDef> list = flowableFieldDefService.selectFlowableFieldDefList(flowableFieldDef);
        return getDataTable(list);
    }

    /**
     * 导出流程字段定义列表
     */
    @RequiresPermissions("flowable:def:export")
    @Log(title = "流程字段定义", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FlowableFieldDef flowableFieldDef)
    {
        List<FlowableFieldDef> list = flowableFieldDefService.selectFlowableFieldDefList(flowableFieldDef);
        ExcelUtil<FlowableFieldDef> util = new ExcelUtil<FlowableFieldDef>(FlowableFieldDef.class);
        util.exportExcel(response, list, "流程字段定义数据");
    }

    /**
     * 获取流程字段定义详细信息
     */
    @RequiresPermissions("flowable:def:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(flowableFieldDefService.selectFlowableFieldDefById(id));
    }

    /**
     * 新增流程字段定义
     */
    @RequiresPermissions("flowable:def:add")
    @Log(title = "流程字段定义", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty FlowableFieldDef flowableFieldDef)
    {
        return toAjax(flowableFieldDefService.insertFlowableFieldDef(flowableFieldDef));
    }

    /**
     * 修改流程字段定义
     */
    @RequiresPermissions("flowable:def:edit")
    @Log(title = "流程字段定义", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FlowableFieldDef flowableFieldDef)
    {
        return toAjax(flowableFieldDefService.updateFlowableFieldDef(flowableFieldDef));
    }

    /**
     * 删除流程字段定义
     */
    @RequiresPermissions("flowable:def:remove")
    @Log(title = "流程字段定义", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(flowableFieldDefService.deleteFlowableFieldDefByIds(ids));
    }
    /**
     * 查询流程字段定义列表(不翻页)
     */
    @RequiresPermissions("flowable:def:listAll")
    @GetMapping("/listAll")
    public AjaxResult listAll(FlowableFieldDef flowableFieldDef)
    {
        List<FlowableFieldDef> list = flowableFieldDefService.selectFlowableFieldDefList(flowableFieldDef);
        return success(list);
    }

}
