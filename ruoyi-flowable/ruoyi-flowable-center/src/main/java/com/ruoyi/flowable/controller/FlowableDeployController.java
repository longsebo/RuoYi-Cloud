package com.ruoyi.flowable.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.flowable.domain.FlowableDeploy;
import com.ruoyi.flowable.service.IFlowableDeployService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 流程部署Controller
 *
 * @author ruoyi
 * @date 2023-12-18
 */
@RestController
@RequestMapping("/deploy")
public class FlowableDeployController extends BaseController
{
    @Autowired
    private IFlowableDeployService flowableDeployService;

    /**
     * 查询流程部署列表
     */
    @RequiresPermissions("flowable:deploy:list")
    @GetMapping("/list")
    public TableDataInfo list(FlowableDeploy flowableDeploy)
    {
        startPage();
        List<FlowableDeploy> list = flowableDeployService.selectFlowableDeployList(flowableDeploy);
        return getDataTable(list);
    }

    /**
     * 获取流程部署详细信息
     */
    @RequiresPermissions("flowable:deploy:query")
    @GetMapping(value = "/{definitionId}")
    public AjaxResult getInfo(@PathVariable("definitionId") String definitionId)
    {
        return success(flowableDeployService.selectFlowableDeployByDefinitionId(definitionId));
    }


    /**
     * 删除流程部署
     */
    @RequiresPermissions("flowable:deploy:remove")
    @Log(title = "流程部署", businessType = BusinessType.DELETE)
	@DeleteMapping("/{definitionIds}")
    public AjaxResult remove(@PathVariable String[] definitionIds)
    {
        return toAjax(flowableDeployService.deleteFlowableDeployByDefinitionIds(definitionIds));
    }
    /**
     * 查询流程部署版本列表
     */
    @RequiresPermissions("flowable:deploy:publishList")
    @GetMapping("/publishList")
    public TableDataInfo publishList(@RequestParam String processKey) {
        startPage();

        List<FlowableDeploy> list = flowableDeployService.queryPublishList(processKey);
        return getDataTable(list);
    }

    /**
     * 激活或挂起流程
     *
     * @param state 状态（active:激活 suspended:挂起）
     * @param definitionId 流程定义ID
     */
    @RequiresPermissions("flowable:deploy:state")
    @PutMapping(value = "/changeState")
    public AjaxResult changeState(@RequestParam String state, @RequestParam String definitionId) {
        flowableDeployService.updateState(definitionId,state);
        return success();
    }

    /**
     * 读取xml文件
     * @param definitionId 流程定义ID
     * @return
     */
    @RequiresPermissions("flowable:deploy:bpmnXml")
    @GetMapping("/bpmnXml/{definitionId}")
    public AjaxResult getBpmnXml(@PathVariable(value = "definitionId") String definitionId) {
        return AjaxResult.success("查询成功", flowableDeployService.queryBpmnXmlById(definitionId));
    }
}
