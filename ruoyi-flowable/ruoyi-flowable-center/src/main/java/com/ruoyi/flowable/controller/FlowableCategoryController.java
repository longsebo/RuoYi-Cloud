package com.ruoyi.flowable.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.flowable.domain.FlowableCategory;
import com.ruoyi.flowable.service.IFlowableCategoryService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 流程分类Controller
 *
 * @author ruoyi
 * @date 2023-11-27
 */
@RestController
@RequestMapping("/category")
public class FlowableCategoryController extends BaseController
{
    @Autowired
    private IFlowableCategoryService flowableCategoryService;

    /**
     * 查询流程分类列表
     */
    @RequiresPermissions("flow:flow_classify:list")
    @GetMapping("/list")
    public TableDataInfo list(FlowableCategory flowableCategory)
    {
        startPage();
        List<FlowableCategory> list = flowableCategoryService.selectFlowableCategoryList(flowableCategory);
        return getDataTable(list);
    }

    /**
     * 导出流程分类列表
     */
    @RequiresPermissions("flow:flow_classify:export")
    @Log(title = "流程分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FlowableCategory flowableCategory)
    {
        List<FlowableCategory> list = flowableCategoryService.selectFlowableCategoryList(flowableCategory);
        ExcelUtil<FlowableCategory> util = new ExcelUtil<FlowableCategory>(FlowableCategory.class);
        util.exportExcel(response, list, "流程分类数据");
    }

    /**
     * 获取流程分类详细信息
     */
    @RequiresPermissions("flow:flow_classify:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(flowableCategoryService.selectFlowableCategoryById(id));
    }

    /**
     * 新增流程分类
     */
    @RequiresPermissions("flow:flow_classify:add")
    @Log(title = "流程分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty FlowableCategory flowableCategory)
    {
        return toAjax(flowableCategoryService.insertFlowableCategory(flowableCategory));
    }

    /**
     * 修改流程分类
     */
    @RequiresPermissions("flow:flow_classify:update")
    @Log(title = "流程分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty FlowableCategory flowableCategory)
    {
        return toAjax(flowableCategoryService.updateFlowableCategory(flowableCategory));
    }

    /**
     * 删除流程分类
     */
    @RequiresPermissions("flow:flow_classify:delete")
    @Log(title = "流程分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(flowableCategoryService.deleteFlowableCategoryByIds(ids));
    }
}
