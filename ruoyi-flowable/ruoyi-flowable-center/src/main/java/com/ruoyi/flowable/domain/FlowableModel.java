package com.ruoyi.flowable.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 流程模型对象 flowable_model
 * 
 * @author ruoyi
 * @date 2023-11-28
 */
public class FlowableModel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 模型ID */
    @Excel(name = "模型ID")
    private String modelId;

    /** 模型名称 */
    @Excel(name = "模型名称")
    private String modelName;

    /** 模型Key */
    @Excel(name = "模型Key")
    private String modelKey;

    /** 分类编码 */
    @Excel(name = "分类编码")
    private String category;

    /** 版本 */
    @Excel(name = "版本")
    private Integer version;

    /** 表单类型 */
    @Excel(name = "表单类型")
    private Integer formType;

    /** 表单ID */
    @Excel(name = "表单ID")
    private Long formId;

    /** 模型描述 */
    @Excel(name = "模型描述")
    private String description;

    /** 流程xm */
    @Excel(name = "流程xm")
    private String bpmnXml;

    /** 表单内容 */
    @Excel(name = "表单内容")
    private String content;

    public void setModelId(String modelId) 
    {
        this.modelId = modelId;
    }

    public String getModelId() 
    {
        return modelId;
    }
    public void setModelName(String modelName) 
    {
        this.modelName = modelName;
    }

    public String getModelName() 
    {
        return modelName;
    }
    public void setModelKey(String modelKey) 
    {
        this.modelKey = modelKey;
    }

    public String getModelKey() 
    {
        return modelKey;
    }
    public void setCategory(String category) 
    {
        this.category = category;
    }

    public String getCategory() 
    {
        return category;
    }
    public void setVersion(Integer version)
    {
        this.version = version;
    }

    public Integer getVersion()
    {
        return version;
    }
    public void setFormType(Integer formType)
    {
        this.formType = formType;
    }

    public Integer getFormType()
    {
        return formType;
    }
    public void setFormId(Long formId) 
    {
        this.formId = formId;
    }

    public Long getFormId() 
    {
        return formId;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setBpmnXml(String bpmnXml) 
    {
        this.bpmnXml = bpmnXml;
    }

    public String getBpmnXml() 
    {
        return bpmnXml;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("modelId", getModelId())
            .append("modelName", getModelName())
            .append("modelKey", getModelKey())
            .append("category", getCategory())
            .append("version", getVersion())
            .append("formType", getFormType())
            .append("formId", getFormId())
            .append("description", getDescription())
            .append("createTime", getCreateTime())
            .append("bpmnXml", getBpmnXml())
            .append("content", getContent())
            .toString();
    }
}
