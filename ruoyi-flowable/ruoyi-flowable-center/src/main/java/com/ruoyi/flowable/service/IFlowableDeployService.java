package com.ruoyi.flowable.service;

import java.util.List;
import com.ruoyi.flowable.domain.FlowableDeploy;

/**
 * 流程部署Service接口
 *
 * @author ruoyi
 * @date 2023-12-18
 */
public interface IFlowableDeployService
{
    /**
     * 查询流程部署
     *
     * @param definitionId 流程部署主键
     * @return 流程部署
     */
    public FlowableDeploy selectFlowableDeployByDefinitionId(String definitionId);

    /**
     * 查询流程部署列表
     *
     * @param flowableDeploy 流程部署
     * @return 流程部署集合
     */
    public List<FlowableDeploy> selectFlowableDeployList(FlowableDeploy flowableDeploy);


    /**
     * 批量删除流程部署
     *
     * @param definitionIds 需要删除的流程部署主键集合
     * @return 结果
     */
    public int deleteFlowableDeployByDefinitionIds(String[] definitionIds);

    /**
     * 删除流程部署信息
     *
     * @param definitionId 流程部署主键
     * @return 结果
     */
    public int deleteFlowableDeployByDefinitionId(String definitionId);

    /**
     * 查询所有部署的历史版本
     * @param processKey  流程key
     * @return
     */
    public List<FlowableDeploy> queryPublishList(String processKey);

    /**
     * 改变部署状态
     * @param definitionId
     * @param stateCode
     */
    public void updateState(String definitionId, String stateCode);

    /**
     * 查询流程图
     * @param definitionId
     * @return
     */
    public String queryBpmnXmlById(String definitionId);
}
