package com.ruoyi.flowable.service;

import java.util.List;
import com.ruoyi.flowable.domain.FlowableFieldRef;

/**
 * 流程字段引用关系Service接口
 * 
 * @author ruoyi
 * @date 2023-12-26
 */
public interface IFlowableFieldRefService 
{
    /**
     * 查询流程字段引用关系
     * 
     * @param id 流程字段引用关系主键
     * @return 流程字段引用关系
     */
    public FlowableFieldRef selectFlowableFieldRefById(String id);

    /**
     * 查询流程字段引用关系列表
     * 
     * @param flowableFieldRef 流程字段引用关系
     * @return 流程字段引用关系集合
     */
    public List<FlowableFieldRef> selectFlowableFieldRefList(FlowableFieldRef flowableFieldRef);

    /**
     * 新增流程字段引用关系
     * 
     * @param flowableFieldRef 流程字段引用关系
     * @return 结果
     */
    public int insertFlowableFieldRef(FlowableFieldRef flowableFieldRef);

    /**
     * 修改流程字段引用关系
     * 
     * @param flowableFieldRef 流程字段引用关系
     * @return 结果
     */
    public int updateFlowableFieldRef(FlowableFieldRef flowableFieldRef);

    /**
     * 批量删除流程字段引用关系
     * 
     * @param ids 需要删除的流程字段引用关系主键集合
     * @return 结果
     */
    public int deleteFlowableFieldRefByIds(String[] ids);

    /**
     * 删除流程字段引用关系信息
     * 
     * @param id 流程字段引用关系主键
     * @return 结果
     */
    public int deleteFlowableFieldRefById(String id);
}
