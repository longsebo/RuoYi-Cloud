package com.ruoyi.flowable.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.security.annotation.EnterpriseAndApplicationCodeProperty;
import io.jsonwebtoken.lang.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.flowable.domain.FlowableModelPage;
import com.ruoyi.flowable.service.IFlowableModelPageService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 建模页面绑定Controller
 *
 * @author ruoyi
 * @date 2023-12-25
 */
@RestController
@RequestMapping("/page")
public class FlowableModelPageController extends BaseController
{
    @Autowired
    private IFlowableModelPageService flowableModelPageService;

    /**
     * 查询建模页面绑定列表
     */
    @RequiresPermissions("flowable:page:list")
    @GetMapping("/list")
    public TableDataInfo list(FlowableModelPage flowableModelPage)
    {
        startPage();
        List<FlowableModelPage> list = flowableModelPageService.selectFlowableModelPageList(flowableModelPage);
        return getDataTable(list);
    }

    /**
     * 导出建模页面绑定列表
     */
    @RequiresPermissions("flowable:page:export")
    @Log(title = "建模页面绑定", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FlowableModelPage flowableModelPage)
    {
        List<FlowableModelPage> list = flowableModelPageService.selectFlowableModelPageList(flowableModelPage);
        ExcelUtil<FlowableModelPage> util = new ExcelUtil<FlowableModelPage>(FlowableModelPage.class);
        util.exportExcel(response, list, "建模页面绑定数据");
    }

    /**
     * 获取建模页面绑定详细信息
     */
    @RequiresPermissions("flowable:page:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(flowableModelPageService.selectFlowableModelPageById(id));
    }

    /**
     * 新增建模页面绑定
     */
    @RequiresPermissions("flowable:page:add")
    @Log(title = "建模页面绑定", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@EnterpriseAndApplicationCodeProperty FlowableModelPage flowableModelPage)
    {
        return toAjax(flowableModelPageService.insertFlowableModelPage(flowableModelPage));
    }

    /**
     * 修改建模页面绑定
     */
    @RequiresPermissions("flowable:page:edit")
    @Log(title = "建模页面绑定", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@EnterpriseAndApplicationCodeProperty FlowableModelPage flowableModelPage)
    {
        return toAjax(flowableModelPageService.updateFlowableModelPage(flowableModelPage));
    }

    /**
     * 删除建模页面绑定
     */
    @RequiresPermissions("flowable:page:remove")
    @Log(title = "建模页面绑定", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(flowableModelPageService.deleteFlowableModelPageByIds(ids));
    }
    /**
     * 建模页面绑定
     */
    @RequiresPermissions("flowable:page:bind")
    @Log(title = "建模页面绑定", businessType = BusinessType.UPDATE)
    @PutMapping(value="/bind")
    public AjaxResult bind(@RequestBody FlowableModelPage flowableModelPage)
    {
        List<FlowableModelPage> list = flowableModelPageService.selectFlowableModelPageListByBind(flowableModelPage);
        if(Collections.isEmpty(list)){
            return toAjax(flowableModelPageService.insertFlowableModelPage(flowableModelPage));
        }else {
            flowableModelPage.setId(list.get(0).getId());
            return toAjax(flowableModelPageService.updateFlowableModelPage(flowableModelPage));
        }
    }
    /**
     * 建模页面单页面查询（根据模块，流程标识，页面名称查询)
     */
    @RequiresPermissions("flowable:page:findPage")
    @Log(title = "建模页面单页面查询", businessType = BusinessType.UPDATE)
    @PostMapping(value="/findPage")
    public AjaxResult findPage(@RequestBody FlowableModelPage flowableModelPage)
    {
        FlowableModelPage page = flowableModelPageService.selectFlowableModelPageSingle(flowableModelPage);
        return success(page);
    }
    /**
     * 建模页面模块页面查询（按模块，流程标识查询)
     */
    @RequiresPermissions("flowable:page:findModulePage")
    @Log(title = "建模页面模块页面查询", businessType = BusinessType.UPDATE)
    @PostMapping(value="/findModulePage")
    public AjaxResult findModulePage(@RequestBody FlowableModelPage flowableModelPage)
    {
        List<FlowableModelPage> list = flowableModelPageService.selectFlowableModelPage(flowableModelPage);
        return success(list);
    }
    /**
     * 建模页面解绑
     */
    @RequiresPermissions("flowable:page:unbind")
    @Log(title = "建模页面解绑", businessType = BusinessType.UPDATE)
    @PutMapping(value="/unbind")
    public AjaxResult unbind(@RequestBody FlowableModelPage flowableModelPage)
    {
        List<FlowableModelPage> list = flowableModelPageService.selectFlowableModelPageListByBind(flowableModelPage);
        if(Collections.isEmpty(list)){
            return error("页面没找到!");
        }else {
            return toAjax(flowableModelPageService.deleteFlowableModelPageById(list.get(0).getId()));
        }
    }
}
