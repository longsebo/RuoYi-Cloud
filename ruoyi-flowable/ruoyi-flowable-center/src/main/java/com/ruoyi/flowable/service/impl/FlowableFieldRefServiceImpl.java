package com.ruoyi.flowable.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.flowable.mapper.FlowableFieldRefMapper;
import com.ruoyi.flowable.domain.FlowableFieldRef;
import com.ruoyi.flowable.service.IFlowableFieldRefService;

/**
 * 流程字段引用关系Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-12-26
 */
@Service
public class FlowableFieldRefServiceImpl implements IFlowableFieldRefService 
{
    @Autowired
    private FlowableFieldRefMapper flowableFieldRefMapper;

    /**
     * 查询流程字段引用关系
     * 
     * @param id 流程字段引用关系主键
     * @return 流程字段引用关系
     */
    @Override
    public FlowableFieldRef selectFlowableFieldRefById(String id)
    {
        return flowableFieldRefMapper.selectFlowableFieldRefById(id);
    }

    /**
     * 查询流程字段引用关系列表
     * 
     * @param flowableFieldRef 流程字段引用关系
     * @return 流程字段引用关系
     */
    @Override
    public List<FlowableFieldRef> selectFlowableFieldRefList(FlowableFieldRef flowableFieldRef)
    {
        return flowableFieldRefMapper.selectFlowableFieldRefList(flowableFieldRef);
    }

    /**
     * 新增流程字段引用关系
     * 
     * @param flowableFieldRef 流程字段引用关系
     * @return 结果
     */
    @Override
    public int insertFlowableFieldRef(FlowableFieldRef flowableFieldRef)
    {
        flowableFieldRef.setCreateTime(DateUtils.getNowDate());
        return flowableFieldRefMapper.insertFlowableFieldRef(flowableFieldRef);
    }

    /**
     * 修改流程字段引用关系
     * 
     * @param flowableFieldRef 流程字段引用关系
     * @return 结果
     */
    @Override
    public int updateFlowableFieldRef(FlowableFieldRef flowableFieldRef)
    {
        flowableFieldRef.setUpdateTime(DateUtils.getNowDate());
        return flowableFieldRefMapper.updateFlowableFieldRef(flowableFieldRef);
    }

    /**
     * 批量删除流程字段引用关系
     * 
     * @param ids 需要删除的流程字段引用关系主键
     * @return 结果
     */
    @Override
    public int deleteFlowableFieldRefByIds(String[] ids)
    {
        return flowableFieldRefMapper.deleteFlowableFieldRefByIds(ids);
    }

    /**
     * 删除流程字段引用关系信息
     * 
     * @param id 流程字段引用关系主键
     * @return 结果
     */
    @Override
    public int deleteFlowableFieldRefById(String id)
    {
        return flowableFieldRefMapper.deleteFlowableFieldRefById(id);
    }
}
