package com.ruoyi.flowable.domain.bo;

import lombok.Data;

/**
 * @author ruoyi
 * @createTime 2023/11/28
 */
@Data
public class FlowableMetaInfoBo {

    /**
     * 创建者（username）
     */
    private String createUser;

    /**
     * 流程描述
     */
    private String description;
    /**
     * 表单类型
     */
    private Integer formType;
    /**
     * 表单编号
     */
    private Long formId;

}
