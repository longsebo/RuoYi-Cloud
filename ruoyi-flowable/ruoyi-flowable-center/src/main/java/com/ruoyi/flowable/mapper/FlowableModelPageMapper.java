package com.ruoyi.flowable.mapper;

import java.util.List;
import com.ruoyi.flowable.domain.FlowableModelPage;

/**
 * 建模页面绑定Mapper接口
 * 
 * @author ruoyi
 * @date 2023-12-25
 */
public interface FlowableModelPageMapper 
{
    /**
     * 查询建模页面绑定
     * 
     * @param id 建模页面绑定主键
     * @return 建模页面绑定
     */
    public FlowableModelPage selectFlowableModelPageById(String id);

    /**
     * 查询建模页面绑定列表
     * 
     * @param flowableModelPage 建模页面绑定
     * @return 建模页面绑定集合
     */
    public List<FlowableModelPage> selectFlowableModelPageList(FlowableModelPage flowableModelPage);

    /**
     * 新增建模页面绑定
     * 
     * @param flowableModelPage 建模页面绑定
     * @return 结果
     */
    public int insertFlowableModelPage(FlowableModelPage flowableModelPage);

    /**
     * 修改建模页面绑定
     * 
     * @param flowableModelPage 建模页面绑定
     * @return 结果
     */
    public int updateFlowableModelPage(FlowableModelPage flowableModelPage);

    /**
     * 删除建模页面绑定
     * 
     * @param id 建模页面绑定主键
     * @return 结果
     */
    public int deleteFlowableModelPageById(String id);

    /**
     * 批量删除建模页面绑定
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFlowableModelPageByIds(String[] ids);
}
