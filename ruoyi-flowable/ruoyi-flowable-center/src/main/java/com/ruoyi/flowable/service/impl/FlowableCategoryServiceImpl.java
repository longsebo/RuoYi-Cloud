package com.ruoyi.flowable.service.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.SpringUtils;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.flowable.enums.CacheType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.flowable.mapper.FlowableCategoryMapper;
import com.ruoyi.flowable.domain.FlowableCategory;
import com.ruoyi.flowable.service.IFlowableCategoryService;

/**
 * 流程分类Service业务层处理
 *
 * @author ruoyi
 * @date 2023-11-27
 */
@Service
public class FlowableCategoryServiceImpl implements IFlowableCategoryService {
    private static final Logger log = LoggerFactory.getLogger(FlowableCategoryServiceImpl.class);
    @Autowired
    private FlowableCategoryMapper flowableCategoryMapper;

    /**
     * 查询流程分类
     *
     * @param id 流程分类主键
     * @return 流程分类
     */
    @Override
    public FlowableCategory selectFlowableCategoryById(Long id)
    {
        return flowableCategoryMapper.selectFlowableCategoryById(id);
    }

    /**
     * 查询流程分类列表
     *
     * @param flowableCategory 流程分类
     * @return 流程分类
     */
    @Override
    public List<FlowableCategory> selectFlowableCategoryList(FlowableCategory flowableCategory)
    {
        log.debug("enter selectFlowableCategoryList!");
        List<FlowableCategory> flowableCategories = flowableCategoryMapper.selectFlowableCategoryList(flowableCategory);
        updateRedis(flowableCategories);
        return flowableCategories;
    }

    /**
     * 更新缓存
     * @param flowableCategories
     */
    @Override
    public Map<String,String> updateRedis(List<FlowableCategory> flowableCategories) {
        if(flowableCategories==null ){
            flowableCategories = selectFlowableCategoryList(new FlowableCategory());
        }
        //更新缓存
        Map<String, String> categoryMap = flowableCategories.stream().collect(Collectors.toMap(FlowableCategory::getCode, FlowableCategory::getName));
        SpringUtils.getBean(RedisService.class).setCacheObject(CacheType.FLOWCATEGORY.getCode(), categoryMap);
        return categoryMap;
    }

    /**
     * 新增流程分类
     *
     * @param flowableCategory 流程分类
     * @return 结果
     */
    @Override
    public int insertFlowableCategory(FlowableCategory flowableCategory)
    {
        flowableCategory.setCreateTime(DateUtils.getNowDate());
        return flowableCategoryMapper.insertFlowableCategory(flowableCategory);
    }

    /**
     * 修改流程分类
     *
     * @param flowableCategory 流程分类
     * @return 结果
     */
    @Override
    public int updateFlowableCategory(FlowableCategory flowableCategory)
    {
        flowableCategory.setUpdateTime(DateUtils.getNowDate());
        return flowableCategoryMapper.updateFlowableCategory(flowableCategory);
    }

    /**
     * 批量删除流程分类
     *
     * @param ids 需要删除的流程分类主键
     * @return 结果
     */
    @Override
    public int deleteFlowableCategoryByIds(Long[] ids)
    {
        return flowableCategoryMapper.deleteFlowableCategoryByIds(ids);
    }

    /**
     * 删除流程分类信息
     *
     * @param id 流程分类主键
     * @return 结果
     */
    @Override
    public int deleteFlowableCategoryById(Long id)
    {
        return flowableCategoryMapper.deleteFlowableCategoryById(id);
    }
}
