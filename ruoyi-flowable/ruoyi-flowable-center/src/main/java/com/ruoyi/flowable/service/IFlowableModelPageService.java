package com.ruoyi.flowable.service;

import java.util.List;
import com.ruoyi.flowable.domain.FlowableModelPage;

/**
 * 建模页面绑定Service接口
 *
 * @author ruoyi
 * @date 2023-12-25
 */
public interface IFlowableModelPageService
{
    /**
     * 查询建模页面绑定
     *
     * @param id 建模页面绑定主键
     * @return 建模页面绑定
     */
    public FlowableModelPage selectFlowableModelPageById(String id);

    /**
     * 查询建模页面绑定列表
     *
     * @param flowableModelPage 建模页面绑定
     * @return 建模页面绑定集合
     */
    public List<FlowableModelPage> selectFlowableModelPageList(FlowableModelPage flowableModelPage);

    /**
     * 新增建模页面绑定
     *
     * @param flowableModelPage 建模页面绑定
     * @return 结果
     */
    public int insertFlowableModelPage(FlowableModelPage flowableModelPage);

    /**
     * 修改建模页面绑定
     *
     * @param flowableModelPage 建模页面绑定
     * @return 结果
     */
    public int updateFlowableModelPage(FlowableModelPage flowableModelPage);

    /**
     * 批量删除建模页面绑定
     *
     * @param ids 需要删除的建模页面绑定主键集合
     * @return 结果
     */
    public int deleteFlowableModelPageByIds(String[] ids);

    /**
     * 删除建模页面绑定信息
     *
     * @param id 建模页面绑定主键
     * @return 结果
     */
    public int deleteFlowableModelPageById(String id);

    /**
     * 查询需要绑定的建模页面
     * @param flowableModelPage
     * @return
     */
    List<FlowableModelPage> selectFlowableModelPageListByBind(FlowableModelPage flowableModelPage);

    /***
     * 建模页面单页面查询（根据模块，流程标识，页面名称查询)
     * @param flowableModelPage
     * @return
     */
    FlowableModelPage selectFlowableModelPageSingle(FlowableModelPage flowableModelPage);

    /**
     * 建模页面模块页面查询（按模块，流程标识查询)
     * @param flowableModelPage
     * @return
     */
    List<FlowableModelPage> selectFlowableModelPage(FlowableModelPage flowableModelPage);
}
