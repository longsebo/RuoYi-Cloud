package com.ruoyi.flowable.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.flowable.domain.FlowableCategory;

/**
 * 流程分类Service接口
 * 
 * @author ruoyi
 * @date 2023-11-27
 */
public interface IFlowableCategoryService 
{
    /**
     * 查询流程分类
     * 
     * @param id 流程分类主键
     * @return 流程分类
     */
    public FlowableCategory selectFlowableCategoryById(Long id);

    /**
     * 查询流程分类列表
     * 
     * @param flowableCategory 流程分类
     * @return 流程分类集合
     */
    public List<FlowableCategory> selectFlowableCategoryList(FlowableCategory flowableCategory);

    /**
     * 新增流程分类
     * 
     * @param flowableCategory 流程分类
     * @return 结果
     */
    public int insertFlowableCategory(FlowableCategory flowableCategory);

    /**
     * 修改流程分类
     * 
     * @param flowableCategory 流程分类
     * @return 结果
     */
    public int updateFlowableCategory(FlowableCategory flowableCategory);

    /**
     * 批量删除流程分类
     * 
     * @param ids 需要删除的流程分类主键集合
     * @return 结果
     */
    public int deleteFlowableCategoryByIds(Long[] ids);

    /**
     * 删除流程分类信息
     * 
     * @param id 流程分类主键
     * @return 结果
     */
    public int deleteFlowableCategoryById(Long id);

    /**
     * 更新缓存
     * @param flowableCategories
     */
    public Map<String,String> updateRedis(List<FlowableCategory> flowableCategories);
}
