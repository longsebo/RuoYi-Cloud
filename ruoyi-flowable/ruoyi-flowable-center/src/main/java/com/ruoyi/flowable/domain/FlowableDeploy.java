package com.ruoyi.flowable.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 流程部署对象 flowable_deploy
 *
 * @author ruoyi
 * @date 2023-12-18
 */
@Data
public class FlowableDeploy extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 流程定义ID */
    @Excel(name = "流程定义ID")
    private String definitionId;

    /** 流程名称 */
    @Excel(name = "流程名称")
    private String processName;

    /** 流程Key */
    @Excel(name = "流程Key")
    private String processKey;

    /** 分类编码 */
    @Excel(name = "分类编码")
    private String category;

    /** 版本 */
    @Excel(name = "版本")
    private Integer version;

    /** 部署ID */
    @Excel(name = "部署ID")
    private String deploymentId;

    /** 流程定义状态 */
    @Excel(name = "流程定义状态")
    private Boolean suspended;

    /** 部署时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "部署时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deploymentTime;


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("definitionId", getDefinitionId())
            .append("processName", getProcessName())
            .append("processKey", getProcessKey())
            .append("category", getCategory())
            .append("version", getVersion())
            .append("deploymentId", getDeploymentId())
            .append("suspended", getSuspended())
            .append("deploymentTime", getDeploymentTime())
            .toString();
    }
}
