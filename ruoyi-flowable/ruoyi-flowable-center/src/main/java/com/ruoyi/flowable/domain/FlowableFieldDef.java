package com.ruoyi.flowable.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 流程字段定义对象 flowable_field_def
 *
 * @author ruoyi
 * @date 2023-12-26
 */
public class FlowableFieldDef extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 数据库字段/表单字段 */
    @Excel(name = "数据库字段/表单字段")
    private String field;

    /** 字段名/表单字段名 */
    @Excel(name = "字段名/表单字段名")
    private String label;

    /** 字段宽度 */
    @Excel(name = "字段宽度")
    private Long width;

    /** 字段组件类型 */
    @Excel(name = "字段组件类型")
    private String type;

    /** 字段定义 */
    @Excel(name = "字段定义")
    private String scheme;

    /** 字段范围 */
    @Excel(name = "字段范围")
    private String scope;
    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;
    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setField(String field)
    {
        this.field = field;
    }

    public String getField()
    {
        return field;
    }
    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getLabel()
    {
        return label;
    }
    public void setWidth(Long width)
    {
        this.width = width;
    }

    public Long getWidth()
    {
        return width;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }
    public void setScheme(String scheme)
    {
        this.scheme = scheme;
    }

    public String getScheme()
    {
        return scheme;
    }
    public void setScope(String scope)
    {
        this.scope = scope;
    }

    public String getScope()
    {
        return scope;
    }

    public String getEnterpriseCode() {
        return enterpriseCode;
    }

    public void setEnterpriseCode(String enterpriseCode) {
        this.enterpriseCode = enterpriseCode;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("field", getField())
            .append("label", getLabel())
            .append("remark", getRemark())
            .append("width", getWidth())
            .append("type", getType())
            .append("scheme", getScheme())
            .append("scope", getScope())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
