package com.ruoyi.flowable.service;

import java.io.UnsupportedEncodingException;
import java.util.List;
import com.ruoyi.flowable.domain.FlowableModel;
import com.ruoyi.flowable.domain.bo.FlowableModelBo;

/**
 * 流程模型Service接口
 *
 * @author ruoyi
 * @date 2023-11-28
 */
public interface IFlowableModelService
{
    /**
     * 查询流程模型
     *
     * @param modelId 流程模型主键
     * @return 流程模型
     */
    public FlowableModel selectFlowableModelByModelId(String modelId);

    /**
     * 查询流程模型列表
     *
     * @param flowableModel 流程模型
     * @return 流程模型集合
     */
    public List<FlowableModel> selectFlowableModelList(FlowableModelBo flowableModel);

    /**
     * 新增流程模型
     *
     * @param flowableModel 流程模型
     * @return
     */
    public int insertFlowableModel(FlowableModel flowableModel);

    /**
     * 修改流程模型
     *
     * @param flowableModel 流程模型
     * @return
     */
    public int updateFlowableModel(FlowableModel flowableModel);

    /**
     * 批量删除流程模型
     *
     * @param modelIds 需要删除的流程模型主键集合
     * @return
     */
    public int deleteFlowableModelByModelIds(String[] modelIds);

    /**
     * 删除流程模型信息
     *
     * @param modelId 流程模型主键
     */
    public void deleteFlowableModelByModelId(String modelId);

    /**
     * 部署流程
     * @param modelId  模型id
     */
    public void deployModel(String modelId) throws UnsupportedEncodingException;

    /**
     * 获取模型xml
     * @param modelId
     * @return
     * @throws UnsupportedEncodingException
     */
    public String queryBpmnXmlById(String modelId) throws UnsupportedEncodingException;

    /**
     * 查询模型历史版本
     * @param modelBo
     * @return
     */
    List<FlowableModel> historyList(FlowableModelBo modelBo);

    /**
     * 新增或更新模型xml
     * @param modelBo
     */
    void saveModel(FlowableModelBo modelBo);

    /**
     * 设置为最新版本
     * @param modelId
     */
    void latestModel(String modelId) throws UnsupportedEncodingException;
}
