package com.ruoyi.flowable.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 建模页面绑定对象 flowable_model_page
 *
 * @author ruoyi
 * @date 2023-12-26
 */
public class FlowableModelPage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 模块 */
    @Excel(name = "模块")
    private String module;

    /** 模型标识 */
    @Excel(name = "模型标识")
    private String mkey;

    /** 页面名称 */
    @Excel(name = "页面名称")
    private String name;

    /** 页面定义 */
    @Excel(name = "页面定义")
    private String pageScheme;
    /** 企业编码 */
    @Excel(name = "企业编码")
    private String enterpriseCode;

    /** 应用编码 */
    @Excel(name = "应用编码")
    private String applicationCode;
    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setModule(String module)
    {
        this.module = module;
    }

    public String getModule()
    {
        return module;
    }
    public void setMkey(String mkey)
    {
        this.mkey = mkey;
    }

    public String getMkey()
    {
        return mkey;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setPageScheme(String pageScheme)
    {
        this.pageScheme = pageScheme;
    }

    public String getPageScheme()
    {
        return pageScheme;
    }

    public String getEnterpriseCode() {
        return enterpriseCode;
    }

    public void setEnterpriseCode(String enterpriseCode) {
        this.enterpriseCode = enterpriseCode;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("module", getModule())
            .append("mkey", getMkey())
            .append("name", getName())
            .append("pageScheme", getPageScheme())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("enterpriseCode", getEnterpriseCode())
            .append("applicationCode", getApplicationCode())
            .toString();
    }
}
