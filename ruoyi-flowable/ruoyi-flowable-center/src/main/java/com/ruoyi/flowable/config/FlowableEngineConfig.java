package com.ruoyi.flowable.config;


import lombok.Data;
import org.flowable.spring.SpringProcessEngineConfiguration;
import org.flowable.spring.boot.EngineConfigurationConfigurer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


/**
 * @author ruoyi
 * @date 2023/11/28
 */
@Data
@Configuration
public class FlowableEngineConfig implements EngineConfigurationConfigurer<SpringProcessEngineConfiguration> {
    /**
     * jdbc 驱动类
     */
    @Value("${spring.datasource.driver-class-name}")
    private String jdbcDriver;
    /**
     * jdbc url
     */
    @Value("${spring.datasource.url}")
    private String jdbcUrl;
    /**
     * jdbc 用户名
     */
    @Value("${spring.datasource.username}")
    private String jdbcUsername;
    /**
     * jdbc 密码
     */
    @Value("${spring.datasource.password}")
    private String jdbcPassword;
    /**
     * 设置在进程引擎启动和关闭时处理数据库模式的策略
     * false（默认值）：在创建流程引擎时，根据库检查DB架构的版本，如果版本不匹配，则抛出异常。
     * true：在构建流程引擎时，将执行检查，并在必要时执行模式更新。如果该架构不存在，则创建该架构。
     * create-drop：在创建流程引擎时创建架构，在关闭流程引擎时删除架构。
     */
    @Value("${spring.datasource.databaseSchemaUpdate}")
    private String databaseSchemaUpdate;
    /**
     * 指示Flowable引擎在启动时启动异步执行器。用于定时任务
     */
    @Value("${spring.datasource.asyncExecutorActivate}")
    private boolean asyncExecutorActivate;

    @Override
    public void configure(SpringProcessEngineConfiguration engineConfiguration) {
        engineConfiguration.setActivityFontName("宋体");
        engineConfiguration.setLabelFontName("宋体");
        engineConfiguration.setAnnotationFontName("宋体");
        engineConfiguration.setJdbcDriver(this.jdbcDriver);
        engineConfiguration.setJdbcUrl(this.jdbcUrl);
        engineConfiguration.setJdbcUsername(this.jdbcUsername);
        engineConfiguration.setJdbcPassword(this.jdbcPassword);
        engineConfiguration.setDatabaseSchemaUpdate(this.databaseSchemaUpdate);
        engineConfiguration.setAsyncExecutorActivate(this.asyncExecutorActivate);
    }
}

