package com.ruoyi.flowable.service.impl;

import java.util.List;

import com.ruoyi.common.core.exception.ServiceException;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.flowable.mapper.FlowableModelPageMapper;
import com.ruoyi.flowable.domain.FlowableModelPage;
import com.ruoyi.flowable.service.IFlowableModelPageService;
import org.springframework.util.CollectionUtils;

/**
 * 建模页面绑定Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-25
 */
@Service
public class FlowableModelPageServiceImpl implements IFlowableModelPageService
{
    @Autowired
    private FlowableModelPageMapper flowableModelPageMapper;

    /**
     * 查询建模页面绑定
     *
     * @param id 建模页面绑定主键
     * @return 建模页面绑定
     */
    @Override
    public FlowableModelPage selectFlowableModelPageById(String id)
    {
        return flowableModelPageMapper.selectFlowableModelPageById(id);
    }

    /**
     * 查询建模页面绑定列表
     *
     * @param flowableModelPage 建模页面绑定
     * @return 建模页面绑定
     */
    @Override
    public List<FlowableModelPage> selectFlowableModelPageList(FlowableModelPage flowableModelPage)
    {
        return flowableModelPageMapper.selectFlowableModelPageList(flowableModelPage);
    }

    /**
     * 新增建模页面绑定
     *
     * @param flowableModelPage 建模页面绑定
     * @return 结果
     */
    @Override
    public int insertFlowableModelPage(FlowableModelPage flowableModelPage)
    {
        flowableModelPage.setCreateTime(DateUtils.getNowDate());
        //设置创建用户id及用户名
        flowableModelPage.setCreateBy(SecurityUtils.getUsername());
        return flowableModelPageMapper.insertFlowableModelPage(flowableModelPage);
    }

    /**
     * 修改建模页面绑定
     *
     * @param flowableModelPage 建模页面绑定
     * @return 结果
     */
    @Override
    public int updateFlowableModelPage(FlowableModelPage flowableModelPage)
    {
        flowableModelPage.setUpdateTime(DateUtils.getNowDate());
        //设置更新用户用户名
        flowableModelPage.setUpdateBy(SecurityUtils.getUsername());
        return flowableModelPageMapper.updateFlowableModelPage(flowableModelPage);
    }

    /**
     * 批量删除建模页面绑定
     *
     * @param ids 需要删除的建模页面绑定主键
     * @return 结果
     */
    @Override
    public int deleteFlowableModelPageByIds(String[] ids)
    {
        return flowableModelPageMapper.deleteFlowableModelPageByIds(ids);
    }

    /**
     * 删除建模页面绑定信息
     *
     * @param id 建模页面绑定主键
     * @return 结果
     */
    @Override
    public int deleteFlowableModelPageById(String id)
    {
        return flowableModelPageMapper.deleteFlowableModelPageById(id);
    }

    /**
     * 查询需要绑定的建模页面
     *
     * @param flowableModelPage
     * @return
     */
    @Override
    public List<FlowableModelPage> selectFlowableModelPageListByBind(FlowableModelPage flowableModelPage) {
        FlowableModelPage searchModelPage = new FlowableModelPage();
        //参数检查
        //模块不能为空
        if(StringUtils.isEmpty(flowableModelPage.getModule()))
            throw new ServiceException("模块名称不能为空！");
        //流程标识不能为空
        if(StringUtils.isEmpty(flowableModelPage.getMkey()))
            throw new ServiceException("流程标识不能空!");
        //页面名称不能为空
        if(StringUtils.isEmpty(flowableModelPage.getName()))
            throw new ServiceException("页面名称不能为空!");
        searchModelPage.setModule(flowableModelPage.getModule());
        searchModelPage.setMkey(flowableModelPage.getMkey());
        searchModelPage.setName(flowableModelPage.getName());
        return flowableModelPageMapper.selectFlowableModelPageList(searchModelPage);
    }

    /***
     * 建模页面单页面查询（根据模块，流程标识，页面名称查询)
     * @param flowableModelPage
     * @return
     */
    @Override
    public FlowableModelPage selectFlowableModelPageSingle(FlowableModelPage flowableModelPage) {
        FlowableModelPage searchModelPage = new FlowableModelPage();
        //参数检查
        //模块不能为空
        if(StringUtils.isEmpty(flowableModelPage.getModule())) {
            return null;
        }
        //流程标识不能为空
        if(StringUtils.isEmpty(flowableModelPage.getMkey())){
            return null;
        }
        //页面名称不能为空
        if(StringUtils.isEmpty(flowableModelPage.getName())){
            return null;
        }
        searchModelPage.setModule(flowableModelPage.getModule());
        searchModelPage.setMkey(flowableModelPage.getMkey());
        searchModelPage.setName(flowableModelPage.getName());
        List<FlowableModelPage> list = flowableModelPageMapper.selectFlowableModelPageList(searchModelPage);
        if(CollectionUtils.isEmpty(list)){
            return  null;
        }else{
            return list.get(0);
        }
    }

    /**
     * 建模页面模块页面查询（按模块，流程标识查询)
     *
     * @param flowableModelPage
     * @return
     */
    @Override
    public List<FlowableModelPage> selectFlowableModelPage(FlowableModelPage flowableModelPage) {
        FlowableModelPage searchModelPage = new FlowableModelPage();
        //参数检查
        //模块不能为空
        if(StringUtils.isEmpty(flowableModelPage.getModule()))
            throw new ServiceException("模块名称不能为空！");
        //流程标识不能为空
        if(StringUtils.isEmpty(flowableModelPage.getMkey()))
            throw new ServiceException("流程标识不能空!");
        searchModelPage.setModule(flowableModelPage.getModule());
        searchModelPage.setMkey(flowableModelPage.getMkey());
        return flowableModelPageMapper.selectFlowableModelPageList(searchModelPage);
    }
}
