package com.ruoyi.flowable.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import com.ruoyi.flowable.domain.bo.FlowableModelBo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.flowable.domain.FlowableModel;
import com.ruoyi.flowable.service.IFlowableModelService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 流程模型Controller
 *
 * @author ruoyi
 * @date 2023-11-28
 */
@RestController
@RequestMapping("/model")
@Slf4j
public class FlowableModelController extends BaseController
{
    @Autowired
    private IFlowableModelService flowableModelService;

    /**
     * 查询流程模型列表
     */
    @RequiresPermissions("flow:flow_model:list")
    @GetMapping("/list")
    public TableDataInfo list(FlowableModelBo flowableModelBo)
    {
        startPage();
        List<FlowableModel> list = flowableModelService.selectFlowableModelList(flowableModelBo);
        return getDataTable(list);
    }

    /**
     * 导出流程模型列表
     */
    @RequiresPermissions("flow:flow_model:export")
    @Log(title = "流程模型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FlowableModelBo flowableModel)
    {
        List<FlowableModel> list = flowableModelService.selectFlowableModelList(flowableModel);
        ExcelUtil<FlowableModel> util = new ExcelUtil<FlowableModel>(FlowableModel.class);
        util.exportExcel(response, list, "流程模型数据");
    }

    /**
     * 获取流程模型详细信息
     */
    @RequiresPermissions("flow:flow_model:query")
    @GetMapping(value = "/{modelId}")
    public AjaxResult getInfo(@PathVariable("modelId") String modelId)
    {
        return success(flowableModelService.selectFlowableModelByModelId(modelId));
    }

    /**
     * 新增流程模型
     */
    @RequiresPermissions("flow:flow_model:add")
    @Log(title = "流程模型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FlowableModel flowableModel)
    {
        return toAjax(flowableModelService.insertFlowableModel(flowableModel));
    }

    /**
     * 修改流程模型
     */
    @RequiresPermissions("flow:flow_model:update")
    @Log(title = "流程模型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FlowableModel flowableModel)
    {
        return toAjax(flowableModelService.updateFlowableModel(flowableModel));
    }

    /**
     * 删除流程模型
     */
    @RequiresPermissions("flow:flow_model:delete")
    @Log(title = "流程模型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{modelIds}")
    public AjaxResult remove(@PathVariable String[] modelIds)
    {
        return toAjax(flowableModelService.deleteFlowableModelByModelIds(modelIds));
    }
    /**
     * 部署流程模型
     *
     * @param modelId 流程模型主键
     */
    @RequiresPermissions("flow:flow_model:deploy")
    @Log(title = "部署流程模型", businessType = BusinessType.OTHER)
    @PostMapping("/deploy")
    public AjaxResult deployModel(@RequestParam String modelId) {
        try {
            flowableModelService.deployModel(modelId);
        } catch (UnsupportedEncodingException e) {
            log.error("部署失败!",e);
        }
        return success();
    }
    /**
     * 获取流程表单详细信息
     *
     * @param modelId 模型id
     */
    @RequiresPermissions("flow:flow_model:queryXml")
    @GetMapping(value = "/getBpmnXml/{modelId}")
    public AjaxResult getBpmnXml(@NotNull(message = "主键不能为空") @PathVariable("modelId") String modelId) {
        try {
            return AjaxResult.success("查询成功",flowableModelService.queryBpmnXmlById(modelId));
        } catch (UnsupportedEncodingException e) {
            log.error("获取模型xml失败!模型id:"+modelId,e);
            return AjaxResult.error("获取模型xml失败!");
        }
    }
    /**
     * 查询流程模型版本历史列表
     *
     * @param modelBo 流程模型对象
     */
    @RequiresPermissions("flow:flow_model:historyList")
    @GetMapping("/historyList")
    public TableDataInfo historyList(FlowableModelBo modelBo) {
        startPage();
        List<FlowableModel> list = flowableModelService.historyList(modelBo);
        return getDataTable(list);
    }
    /**
     * 保存(更新或插入新版本)流程模型
     */
    @RequiresPermissions("flow:flow_model::save")
    @PostMapping("/save")
    public AjaxResult save(@RequestBody FlowableModelBo modelBo) {
        flowableModelService.saveModel(modelBo);
        return success();
    }
    /**
     * 设为最新流程模型
     * @param modelId
     * @return
     */
    @RequiresPermissions("flow:flow_model:lastest")
    @PostMapping("/latest/")
    public AjaxResult latest(@RequestParam String modelId) {
        try {
            flowableModelService.latestModel(modelId);
            return success();
        } catch (UnsupportedEncodingException e) {
            log.error("设置最新版本失败!",e);
            return error("设置最新版本失败!");
        }

    }
}
