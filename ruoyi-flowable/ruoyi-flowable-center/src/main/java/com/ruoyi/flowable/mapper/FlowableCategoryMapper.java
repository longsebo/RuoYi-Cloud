package com.ruoyi.flowable.mapper;

import java.util.List;
import com.ruoyi.flowable.domain.FlowableCategory;

/**
 * 流程分类Mapper接口
 * 
 * @author ruoyi
 * @date 2023-11-27
 */
public interface FlowableCategoryMapper 
{
    /**
     * 查询流程分类
     * 
     * @param id 流程分类主键
     * @return 流程分类
     */
    public FlowableCategory selectFlowableCategoryById(Long id);

    /**
     * 查询流程分类列表
     * 
     * @param flowableCategory 流程分类
     * @return 流程分类集合
     */
    public List<FlowableCategory> selectFlowableCategoryList(FlowableCategory flowableCategory);

    /**
     * 新增流程分类
     * 
     * @param flowableCategory 流程分类
     * @return 结果
     */
    public int insertFlowableCategory(FlowableCategory flowableCategory);

    /**
     * 修改流程分类
     * 
     * @param flowableCategory 流程分类
     * @return 结果
     */
    public int updateFlowableCategory(FlowableCategory flowableCategory);

    /**
     * 删除流程分类
     * 
     * @param id 流程分类主键
     * @return 结果
     */
    public int deleteFlowableCategoryById(Long id);

    /**
     * 批量删除流程分类
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFlowableCategoryByIds(Long[] ids);
}
