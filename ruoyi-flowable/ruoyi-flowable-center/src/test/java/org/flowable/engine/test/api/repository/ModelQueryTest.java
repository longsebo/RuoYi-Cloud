package org.flowable.engine.test.api.repository;


import java.nio.charset.StandardCharsets;
import java.util.List;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.assertj.core.api.Assertions.tuple;
import com.alibaba.fastjson.JSON;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.ProcessEngineConfiguration;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.impl.cfg.StandaloneProcessEngineConfiguration;
import org.flowable.engine.impl.persistence.entity.ModelEntity;
import org.flowable.engine.impl.test.PluggableFlowableTestCase;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.Model;
import org.flowable.engine.repository.ModelQuery;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class ModelQueryTest   {
    private String modelOneId;
    RepositoryService repositoryService;
    @BeforeEach
    protected void setUp() throws Exception {
        ProcessEngineConfiguration cfg = new StandaloneProcessEngineConfiguration()
                .setJdbcUrl("jdbc:mysql://localhost:3306/flowable?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8")
                .setJdbcUsername("root")
                .setJdbcPassword("root")
                .setJdbcDriver("com.mysql.cj.jdbc.Driver")
                .setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);

        ProcessEngine processEngine = cfg.buildProcessEngine();
        repositoryService = processEngine.getRepositoryService();
        //System.out.println(JSON.toJSONString(processEngine.getProcessEngineConfiguration()));
//        Model model = repositoryService.newModel();
//        model.setName("my model");
//        model.setKey("someKey");
//        model.setCategory("test");
//        repositoryService.saveModel(model);
//        modelOneId = model.getId();
//
//        repositoryService.addModelEditorSource(modelOneId, "bytes".getBytes(StandardCharsets.UTF_8));

    }
    @AfterEach
    protected void tearDown() throws Exception {
//        repositoryService.deleteModel(modelOneId);
    }
    @Test
    public void testByLatestVersion() {
        //ModelQuery query = repositoryService.createModelQuery().latestVersion().modelKey("someKey");
        ModelQuery query = repositoryService.createModelQuery().latestVersion().orderByCreateTime().desc();
        long pageTotal = query.count();

        //assertThat(pageTotal).isGreaterThan(0);
        Model model = query.singleResult();
        //assertThat(model).isNotNull();

        // Add a new version of the model
//        Model newVersion = repositoryService.newModel();
//        newVersion.setName("my model");
//        newVersion.setKey("someKey");
//        newVersion.setCategory("test");
//        newVersion.setVersion(model.getVersion() + 1);
//        repositoryService.saveModel(newVersion);

        // Verify query
        model = query.singleResult();
        //assertThat(model).isNotNull();
        //assertThat(model.getVersion()).isEqualTo(2);

        // Cleanup
//        repositoryService.deleteModel(model.getId());
    }
    @Test
    public void testModelQuery1(){
        ModelQuery modelQuery = repositoryService.createModelQuery()
                .modelName("business_1702865697042")
                .modelCategory("001")
                .orderByModelVersion()
                .desc();
        int offset = 1 * (1 - 1);
        List<Model> modelList = modelQuery.listPage(offset, 1);
    }
}
